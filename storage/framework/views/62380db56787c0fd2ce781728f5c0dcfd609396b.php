<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>
            Users Record
            <small>Control panel</small>
        </h1>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="<?php echo e(route('users.index')); ?>" class="btn btn-primary btn-block margin-bottom">Show All</a>
                <div class="box box-solid">
                    <?php echo Form::open(['url' => route('users.index'),'class'=>'form-horizontal','method' => 'GET']); ?>

                        <div class="classification">
                            <div class="box-header with-border">
                                <h3 class="box-title">Classifications</h3>
                            </div>
                            <div class="box-body no-padding">
                               <?php echo e(Form::select('classification', ['student'=>'Student', 'professional' => 'Professional'], null, ['class'=>'form-control select2','placeholder' => 'Select classification..'])); ?>

                            </div>
                        </div>

                        <div class="user-categories">
                            <div class="box-header with-border">
                                <h3 class="box-title">User Categories</h3>
                            </div>
                            <div class="box-body no-padding">

                                <?php echo e(Form::select('user-category',$user_categories, null, ['class'=>'form-control select2','placeholder' => 'Select category..'])); ?>


                            </div>
                        </div>

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </div>

                    <?php echo Form::close(); ?>

                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Registered Members Record</h3>
                        <div class="box-tools">
                            
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Classification</th>
                                <th>Phone</th>
                                <th>Country</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td><a href="<?php echo e(route('users.show',[$user->id])); ?>"><?php echo e($user->firstname.' '.$user->lastname); ?></a></td>
                                    <td><?php echo e($user->email); ?></td>
                                    <td><?php echo e($user->classification); ?></td>
                                    <td><?php echo e($user->phone); ?></td>
                                    <td><?php echo e($user->country); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <p>No User Available..</p>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <?php echo e($users->appends(request()->except('page'))->links()); ?>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>