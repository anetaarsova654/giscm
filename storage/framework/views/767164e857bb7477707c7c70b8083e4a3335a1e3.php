<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="<?php echo e(route('threadSpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries1')); ?>">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries2')); ?>">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>5/8-11 UNC to 7/8-32 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries5')); ?>">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries8')); ?>">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>


            </table>
        </div>
        &nbsp;<br>
        <div class="table-responsive">
            <table class="table">

                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><strong>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br>
                            <span class="red">Max Minor</span></strong></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-11 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.6234</td>
                    <td  class="smalltext">   0.6052</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5644</td>
                    <td  class="smalltext">   0.5561</td>
                    <td  class="smalltext">   0.5152</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5460</span></td>
                    <td  class="smalltext"><span class="red">   0.5270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5767</span></td>
                    <td  class="smalltext"><span class="red">   0.5660</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-11 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.6234</td>
                    <td  class="smalltext">   0.6113</td>
                    <td  class="smalltext">0.6052</td>
                    <td  class="smalltext">   0.5644</td>
                    <td  class="smalltext">   0.5589</td>
                    <td  class="smalltext">   0.5152</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5460</span></td>
                    <td  class="smalltext"><span class="red">   0.5270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5732</span></td>
                    <td  class="smalltext"><span class="red">   0.5660</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-11 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6129</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5660</td>
                    <td  class="smalltext">   0.5619</td>
                    <td  class="smalltext">   0.5168</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5391</span></td>
                    <td  class="smalltext"><span class="red">   0.5270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5714</span></td>
                    <td  class="smalltext"><span class="red">   0.5660</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.6234</td>
                    <td  class="smalltext">   0.6120</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5693</td>
                    <td  class="smalltext">   0.5639</td>
                    <td  class="smalltext">   0.5242</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5530</span></td>
                    <td  class="smalltext"><span class="red">   0.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5780</span></td>
                    <td  class="smalltext"><span class="red">   0.5709</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5709</td>
                    <td  class="smalltext">   0.5668</td>
                    <td  class="smalltext">   0.5258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5463</span></td>
                    <td  class="smalltext"><span class="red">   0.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5762</span></td>
                    <td  class="smalltext"><span class="red">   0.5709</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.6235</td>
                    <td  class="smalltext">   0.6132</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5771</td>
                    <td  class="smalltext">   0.5720</td>
                    <td  class="smalltext">   0.5385</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5640</span></td>
                    <td  class="smalltext"><span class="red">   0.5480</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5852</span></td>
                    <td  class="smalltext"><span class="red">   0.5786</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.6236</td>
                    <td  class="smalltext">   0.6142</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5830</td>
                    <td  class="smalltext">   0.5782</td>
                    <td  class="smalltext">   0.5491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5710</span></td>
                    <td  class="smalltext"><span class="red">   0.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5906</span></td>
                    <td  class="smalltext"><span class="red">   0.5844</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5844</td>
                    <td  class="smalltext">   0.5808</td>
                    <td  class="smalltext">   0.5505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5662</span></td>
                    <td  class="smalltext"><span class="red">   0.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5890</span></td>
                    <td  class="smalltext"><span class="red">   0.5844</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.6236</td>
                    <td  class="smalltext">   0.6105</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5875</td>
                    <td  class="smalltext">   0.5805</td>
                    <td  class="smalltext">   0.5575</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5780</span></td>
                    <td  class="smalltext"><span class="red">   0.5650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5980</span></td>
                    <td  class="smalltext"><span class="red">   0.5889</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.6236</td>
                    <td  class="smalltext">   0.6149</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5875</td>
                    <td  class="smalltext">   0.5828</td>
                    <td  class="smalltext">   0.5575</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5780</span></td>
                    <td  class="smalltext"><span class="red">   0.5650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5949</span></td>
                    <td  class="smalltext"><span class="red">   0.5889</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6163</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5889</td>
                    <td  class="smalltext">   0.5854</td>
                    <td  class="smalltext">   0.5589</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5730</span></td>
                    <td  class="smalltext"><span class="red">   0.5650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5934</span></td>
                    <td  class="smalltext"><span class="red">   0.5889</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.6237</td>
                    <td  class="smalltext">   0.6156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5912</td>
                    <td  class="smalltext">   0.5869</td>
                    <td  class="smalltext">   0.5642</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5820</span></td>
                    <td  class="smalltext"><span class="red">   0.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5981</span></td>
                    <td  class="smalltext"><span class="red">   0.5925</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5925</td>
                    <td  class="smalltext">   0.5893</td>
                    <td  class="smalltext">   0.5655</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5787</span></td>
                    <td  class="smalltext"><span class="red">   0.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5967</span></td>
                    <td  class="smalltext"><span class="red">   0.5925</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-24 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.6238</td>
                    <td  class="smalltext">   0.6166</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5967</td>
                    <td  class="smalltext">   0.5927</td>
                    <td  class="smalltext">   0.5742</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5900</span></td>
                    <td  class="smalltext"><span class="red">   0.5800</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6031</span></td>
                    <td  class="smalltext"><span class="red">   0.5979</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-24 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6178</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5979</td>
                    <td  class="smalltext">   0.5949</td>
                    <td  class="smalltext">   0.5754</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5869</span></td>
                    <td  class="smalltext"><span class="red">   0.5800</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6018</span></td>
                    <td  class="smalltext"><span class="red">   0.5979</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.6239</td>
                    <td  class="smalltext">   0.6172</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5998</td>
                    <td  class="smalltext">   0.5960</td>
                    <td  class="smalltext">   0.5798</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5940</span></td>
                    <td  class="smalltext"><span class="red">   0.5850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6059</span></td>
                    <td  class="smalltext"><span class="red">   0.6009</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.6239</td>
                    <td  class="smalltext">   0.6174</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6007</td>
                    <td  class="smalltext">   0.5969</td>
                    <td  class="smalltext">   0.5813</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5950</span></td>
                    <td  class="smalltext"><span class="red">   0.5860</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6067</span></td>
                    <td  class="smalltext"><span class="red">   0.6018</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6185</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6018</td>
                    <td  class="smalltext">   0.5990</td>
                    <td  class="smalltext">   0.5824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5926</span></td>
                    <td  class="smalltext"><span class="red">   0.5860</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6055</span></td>
                    <td  class="smalltext"><span class="red">   0.6018</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.6239</td>
                    <td  class="smalltext">   0.6179</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6036</td>
                    <td  class="smalltext">   0.6000</td>
                    <td  class="smalltext">   0.5867</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5990</span></td>
                    <td  class="smalltext"><span class="red">   0.5910</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6093</span></td>
                    <td  class="smalltext"><span class="red">   0.6047</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>8</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6250</td>
                    <td  class="smalltext">   0.6190</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6047</td>
                    <td  class="smalltext">   0.6020</td>
                    <td  class="smalltext">   0.5878</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5969</span></td>
                    <td  class="smalltext"><span class="red">   0.5910</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6082</span></td>
                    <td  class="smalltext"><span class="red">   0.6047</span></td>
                    <td  class="smalltext"><span class="red">   0.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-12 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.6859</td>
                    <td  class="smalltext">   0.6745</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6318</td>
                    <td  class="smalltext">   0.6264</td>
                    <td  class="smalltext">   0.5867</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6150</span></td>
                    <td  class="smalltext"><span class="red">   0.5970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6405</span></td>
                    <td  class="smalltext"><span class="red">   0.6334</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-12 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6761</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6334</td>
                    <td  class="smalltext">   0.6293</td>
                    <td  class="smalltext">   0.5883</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6085</span></td>
                    <td  class="smalltext"><span class="red">   0.5970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6387</span></td>
                    <td  class="smalltext"><span class="red">   0.6334</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-16 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.6861</td>
                    <td  class="smalltext">   0.6767</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6455</td>
                    <td  class="smalltext">   0.6407</td>
                    <td  class="smalltext">   0.6116</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6340</span></td>
                    <td  class="smalltext"><span class="red">   0.6200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6531</span></td>
                    <td  class="smalltext"><span class="red">   0.6469</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-16 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6781</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6469</td>
                    <td  class="smalltext">   0.6433</td>
                    <td  class="smalltext">   0.6130</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6284</span></td>
                    <td  class="smalltext"><span class="red">   0.6200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6515</span></td>
                    <td  class="smalltext"><span class="red">   0.6469</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-20 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.6862</td>
                    <td  class="smalltext">   0.6781</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6537</td>
                    <td  class="smalltext">   0.6494</td>
                    <td  class="smalltext">   0.6267</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6450</span></td>
                    <td  class="smalltext"><span class="red">   0.6330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6606</span></td>
                    <td  class="smalltext"><span class="red">   0.6550</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-20 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6794</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6550</td>
                    <td  class="smalltext">   0.6518</td>
                    <td  class="smalltext">   0.6280</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6412</span></td>
                    <td  class="smalltext"><span class="red">   0.6330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6592</span></td>
                    <td  class="smalltext"><span class="red">   0.6550</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-24 UNEF </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.6863</td>
                    <td  class="smalltext">   0.6791</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6592</td>
                    <td  class="smalltext">   0.6552</td>
                    <td  class="smalltext">   0.6367</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6520</span></td>
                    <td  class="smalltext"><span class="red">   0.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6656</span></td>
                    <td  class="smalltext"><span class="red">   0.6604</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-24 UNEF </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6803</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6604</td>
                    <td  class="smalltext">   0.6574</td>
                    <td  class="smalltext">   0.6379</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6494</span></td>
                    <td  class="smalltext"><span class="red">   0.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6643</span></td>
                    <td  class="smalltext"><span class="red">   0.6604</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-28 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.6864</td>
                    <td  class="smalltext">   0.6799</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6632</td>
                    <td  class="smalltext">   0.6594</td>
                    <td  class="smalltext">   0.6438</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6570</span></td>
                    <td  class="smalltext"><span class="red">   0.6490</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6692</span></td>
                    <td  class="smalltext"><span class="red">   0.6643</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-28 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6810</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6643</td>
                    <td  class="smalltext">   0.6615</td>
                    <td  class="smalltext">   0.6449</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6551</span></td>
                    <td  class="smalltext"><span class="red">   0.6490</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6680</span></td>
                    <td  class="smalltext"><span class="red">   0.6643</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-32 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.6864</td>
                    <td  class="smalltext">   0.6804</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6661</td>
                    <td  class="smalltext">   0.6625</td>
                    <td  class="smalltext">   0.6492</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6610</span></td>
                    <td  class="smalltext"><span class="red">   0.6540</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6718</span></td>
                    <td  class="smalltext"><span class="red">   0.6672</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>11</sup>/<sub>16</sub>-32 UN </td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.6875</td>
                    <td  class="smalltext">   0.6815</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6672</td>
                    <td  class="smalltext">   0.6645</td>
                    <td  class="smalltext">   0.6503</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6594</span></td>
                    <td  class="smalltext"><span class="red">   0.6540</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6707</span></td>
                    <td  class="smalltext"><span class="red">   0.6672</span></td>
                    <td  class="smalltext"><span class="red">   0.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-10 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.7482</td>
                    <td  class="smalltext">   0.7288</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6832</td>
                    <td  class="smalltext">   0.6744</td>
                    <td  class="smalltext">   0.6291</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6630</span></td>
                    <td  class="smalltext"><span class="red">   0.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6965</span></td>
                    <td  class="smalltext"><span class="red">   0.6850</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-10 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.7482</td>
                    <td  class="smalltext">   0.7353</td>
                    <td  class="smalltext">0.7288</td>
                    <td  class="smalltext">   0.6832</td>
                    <td  class="smalltext">   0.6773</td>
                    <td  class="smalltext">   0.6291</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6630</span></td>
                    <td  class="smalltext"><span class="red">   0.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6927</span></td>
                    <td  class="smalltext"><span class="red">   0.6850</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-10 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7371</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6850</td>
                    <td  class="smalltext">   0.6806</td>
                    <td  class="smalltext">   0.6309</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6545</span></td>
                    <td  class="smalltext"><span class="red">   0.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6907</span></td>
                    <td  class="smalltext"><span class="red">   0.6850</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.7483</td>
                    <td  class="smalltext">   0.7369</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6942</td>
                    <td  class="smalltext">   0.6887</td>
                    <td  class="smalltext">   0.6491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6780</span></td>
                    <td  class="smalltext"><span class="red">   0.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7031</span></td>
                    <td  class="smalltext"><span class="red">   0.6959</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.6959</td>
                    <td  class="smalltext">   0.6918</td>
                    <td  class="smalltext">   0.6508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6707</span></td>
                    <td  class="smalltext"><span class="red">   0.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7103</span></td>
                    <td  class="smalltext"><span class="red">   0.6959</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.7485</td>
                    <td  class="smalltext">   0.7382</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7021</td>
                    <td  class="smalltext">   0.6970</td>
                    <td  class="smalltext">   0.6635</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6880</span></td>
                    <td  class="smalltext"><span class="red">   0.6730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7103</span></td>
                    <td  class="smalltext"><span class="red">   0.7036</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-16 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.7485</td>
                    <td  class="smalltext">   0.7343</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7079</td>
                    <td  class="smalltext">   0.7004</td>
                    <td  class="smalltext">   0.6740</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6960</span></td>
                    <td  class="smalltext"><span class="red">   0.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7192</span></td>
                    <td  class="smalltext"><span class="red">   0.7094</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-16 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.7485</td>
                    <td  class="smalltext">   0.7391</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7079</td>
                    <td  class="smalltext">   0.7029</td>
                    <td  class="smalltext">   0.6740</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6960</span></td>
                    <td  class="smalltext"><span class="red">   0.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7159</span></td>
                    <td  class="smalltext"><span class="red">   0.7094</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-16 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7094</td>
                    <td  class="smalltext">   0.7056</td>
                    <td  class="smalltext">   0.6755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.6908</span></td>
                    <td  class="smalltext"><span class="red">   0.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7143</span></td>
                    <td  class="smalltext"><span class="red">   0.7094</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.7486</td>
                    <td  class="smalltext">   0.7399</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7125</td>
                    <td  class="smalltext">   0.7079</td>
                    <td  class="smalltext">   0.6825</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7030</span></td>
                    <td  class="smalltext"><span class="red">   0.6900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7199</span></td>
                    <td  class="smalltext"><span class="red">   0.7139</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.7487</td>
                    <td  class="smalltext">   0.7406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7162</td>
                    <td  class="smalltext">   0.7118</td>
                    <td  class="smalltext">   0.6892</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7070</span></td>
                    <td  class="smalltext"><span class="red">   0.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7232</span></td>
                    <td  class="smalltext"><span class="red">   0.7175</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7175</td>
                    <td  class="smalltext">   0.7142</td>
                    <td  class="smalltext">   0.6905</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7037</span></td>
                    <td  class="smalltext"><span class="red">   0.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7218</span></td>
                    <td  class="smalltext"><span class="red">   0.7175</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.7488</td>
                    <td  class="smalltext">   0.7416</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7217</td>
                    <td  class="smalltext">   0.7176</td>
                    <td  class="smalltext">   0.6992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7150</span></td>
                    <td  class="smalltext"><span class="red">   0.7050</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7282</span></td>
                    <td  class="smalltext"><span class="red">   0.7229</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.7488</td>
                    <td  class="smalltext">   0.7421</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7247</td>
                    <td  class="smalltext">   0.7208</td>
                    <td  class="smalltext">   0.7047</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7190</span></td>
                    <td  class="smalltext"><span class="red">   0.7100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7310</span></td>
                    <td  class="smalltext"><span class="red">   0.7259</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.7488</td>
                    <td  class="smalltext">   0.7423</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7256</td>
                    <td  class="smalltext">   0.7218</td>
                    <td  class="smalltext">   0.7062</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7200</span></td>
                    <td  class="smalltext"><span class="red">   0.7110</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7318</span></td>
                    <td  class="smalltext"><span class="red">   0.7268</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7435</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7268</td>
                    <td  class="smalltext">   0.7239</td>
                    <td  class="smalltext">   0.7074</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7176</span></td>
                    <td  class="smalltext"><span class="red">   0.7110</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7305</span></td>
                    <td  class="smalltext"><span class="red">   0.7268</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.7489</td>
                    <td  class="smalltext">   0.7429</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7286</td>
                    <td  class="smalltext">   0.7250</td>
                    <td  class="smalltext">   0.7114</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7240</span></td>
                    <td  class="smalltext"><span class="red">   0.7160</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7344</span></td>
                    <td  class="smalltext"><span class="red">   0.7297</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>4</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.7500</td>
                    <td  class="smalltext">   0.7440</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7297</td>
                    <td  class="smalltext">   0.7270</td>
                    <td  class="smalltext">   0.7128</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7219</span></td>
                    <td  class="smalltext"><span class="red">   0.7160</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7333</span></td>
                    <td  class="smalltext"><span class="red">   0.7297</span></td>
                    <td  class="smalltext"><span class="red">   0.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.8108</td>
                    <td  class="smalltext">   0.7994</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7567</td>
                    <td  class="smalltext">   0.7512</td>
                    <td  class="smalltext">   0.7116</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7400</span></td>
                    <td  class="smalltext"><span class="red">   0.7220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7656</span></td>
                    <td  class="smalltext"><span class="red">   0.7584</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8125</td>
                    <td  class="smalltext">   0.8011</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7584</td>
                    <td  class="smalltext">   0.7543</td>
                    <td  class="smalltext">   0.7133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7329</span></td>
                    <td  class="smalltext"><span class="red">   0.7220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7638</span></td>
                    <td  class="smalltext"><span class="red">   0.7584</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.8110</td>
                    <td  class="smalltext">   0.8016</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7704</td>
                    <td  class="smalltext">   0.7655</td>
                    <td  class="smalltext">   0.7365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7590</span></td>
                    <td  class="smalltext"><span class="red">   0.7450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7782</span></td>
                    <td  class="smalltext"><span class="red">   0.7719</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8125</td>
                    <td  class="smalltext">   0.8031</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7719</td>
                    <td  class="smalltext">   0.7683</td>
                    <td  class="smalltext">   0.7380</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7533</span></td>
                    <td  class="smalltext"><span class="red">   0.7450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7766</span></td>
                    <td  class="smalltext"><span class="red">   0.7719</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.8112</td>
                    <td  class="smalltext">   0.8031</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7787</td>
                    <td  class="smalltext">   0.7743</td>
                    <td  class="smalltext">   0.7517</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7700</span></td>
                    <td  class="smalltext"><span class="red">   0.7580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7857</span></td>
                    <td  class="smalltext"><span class="red">   0.7800</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8125</td>
                    <td  class="smalltext">   0.8044</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7800</td>
                    <td  class="smalltext">   0.7767</td>
                    <td  class="smalltext">   0.7530</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7662</span></td>
                    <td  class="smalltext"><span class="red">   0.7580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7843</span></td>
                    <td  class="smalltext"><span class="red">   0.7800</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.8113</td>
                    <td  class="smalltext">   0.8048</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7881</td>
                    <td  class="smalltext">   0.7843</td>
                    <td  class="smalltext">   0.7687</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7820</span></td>
                    <td  class="smalltext"><span class="red">   0.7740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7943</span></td>
                    <td  class="smalltext"><span class="red">   0.7893</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8125</td>
                    <td  class="smalltext">   0.8060</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7893</td>
                    <td  class="smalltext">   0.7864</td>
                    <td  class="smalltext">   0.7699</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7801</span></td>
                    <td  class="smalltext"><span class="red">   0.7740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7930</span></td>
                    <td  class="smalltext"><span class="red">   0.7893</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.8114</td>
                    <td  class="smalltext">   0.8054</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7911</td>
                    <td  class="smalltext">   0.7875</td>
                    <td  class="smalltext">   0.7742</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7860</span></td>
                    <td  class="smalltext"><span class="red">   0.7790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7969</span></td>
                    <td  class="smalltext"><span class="red">   0.7922</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>13</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8125</td>
                    <td  class="smalltext">   0.8065</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.7922</td>
                    <td  class="smalltext">   0.7895</td>
                    <td  class="smalltext">   0.7753</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7844</span></td>
                    <td  class="smalltext"><span class="red">   0.7790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7958</span></td>
                    <td  class="smalltext"><span class="red">   0.7922</span></td>
                    <td  class="smalltext"><span class="red">   0.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-9 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   0.8731</td>
                    <td  class="smalltext">   0.8523</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8009</td>
                    <td  class="smalltext">   0.7914</td>
                    <td  class="smalltext">   0.7408</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7780</span></td>
                    <td  class="smalltext"><span class="red">   0.7550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8151</span></td>
                    <td  class="smalltext"><span class="red">   0.8028</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-9 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   0.8731</td>
                    <td  class="smalltext">   0.8592</td>
                    <td  class="smalltext">0.8523</td>
                    <td  class="smalltext">   0.8009</td>
                    <td  class="smalltext">   0.7946</td>
                    <td  class="smalltext">   0.7408</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7780</span></td>
                    <td  class="smalltext"><span class="red">   0.7550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8110</span></td>
                    <td  class="smalltext"><span class="red">   0.8028</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-9 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8611</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8028</td>
                    <td  class="smalltext">   0.7981</td>
                    <td  class="smalltext">   0.7427</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7681</span></td>
                    <td  class="smalltext"><span class="red">   0.7550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8089</span></td>
                    <td  class="smalltext"><span class="red">   0.8028</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.8732</td>
                    <td  class="smalltext">   0.8603</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8082</td>
                    <td  class="smalltext">   0.8022</td>
                    <td  class="smalltext">   0.7542</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7880</span></td>
                    <td  class="smalltext"><span class="red">   0.7670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8178</span></td>
                    <td  class="smalltext"><span class="red">   0.8100</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.8733</td>
                    <td  class="smalltext">   0.8619</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8192</td>
                    <td  class="smalltext">   0.8137</td>
                    <td  class="smalltext">   0.7741</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8030</span></td>
                    <td  class="smalltext"><span class="red">   0.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8281</span></td>
                    <td  class="smalltext"><span class="red">   0.8209</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8209</td>
                    <td  class="smalltext">   0.8168</td>
                    <td  class="smalltext">   0.7758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.7948</span></td>
                    <td  class="smalltext"><span class="red">   0.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8263</span></td>
                    <td  class="smalltext"><span class="red">   0.8209</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-14 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.8734</td>
                    <td  class="smalltext">   0.8579</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8270</td>
                    <td  class="smalltext">   0.8189</td>
                    <td  class="smalltext">   0.7884</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8140</span></td>
                    <td  class="smalltext"><span class="red">   0.7980</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8392</span></td>
                    <td  class="smalltext"><span class="red">   0.8286</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-14 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.8734</td>
                    <td  class="smalltext">   0.8631</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8270</td>
                    <td  class="smalltext">   0.8216</td>
                    <td  class="smalltext">   0.7884</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8140</span></td>
                    <td  class="smalltext"><span class="red">   0.7980</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8356</span></td>
                    <td  class="smalltext"><span class="red">   0.8286</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-14 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8647</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8286</td>
                    <td  class="smalltext">   0.8245</td>
                    <td  class="smalltext">   0.7900</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8068</span></td>
                    <td  class="smalltext"><span class="red">   0.7980</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8339</span></td>
                    <td  class="smalltext"><span class="red">   0.8286</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.8735</td>
                    <td  class="smalltext">   0.8641</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8329</td>
                    <td  class="smalltext">   0.8280</td>
                    <td  class="smalltext">   0.7900</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8210</span></td>
                    <td  class="smalltext"><span class="red">   0.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8407</span></td>
                    <td  class="smalltext"><span class="red">   0.8344</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8344</td>
                    <td  class="smalltext">   0.8308</td>
                    <td  class="smalltext">   0.8005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8158</span></td>
                    <td  class="smalltext"><span class="red">   0.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8391</span></td>
                    <td  class="smalltext"><span class="red">   0.8344</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.8736</td>
                    <td  class="smalltext">   0.8649</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8375</td>
                    <td  class="smalltext">   0.8329</td>
                    <td  class="smalltext">   0.8075</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8280</span></td>
                    <td  class="smalltext"><span class="red">   0.8150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8449</span></td>
                    <td  class="smalltext"><span class="red">   0.8389</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.8737</td>
                    <td  class="smalltext">   0.8656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8412</td>
                    <td  class="smalltext">   0.8368</td>
                    <td  class="smalltext">   0.8142</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8320</span></td>
                    <td  class="smalltext"><span class="red">   0.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8482</span></td>
                    <td  class="smalltext"><span class="red">   0.8425</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8425</td>
                    <td  class="smalltext">   0.8392</td>
                    <td  class="smalltext">   0.8155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8287</span></td>
                    <td  class="smalltext"><span class="red">   0.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8468</span></td>
                    <td  class="smalltext"><span class="red">   0.8425</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.8738</td>
                    <td  class="smalltext">   0.8666</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8467</td>
                    <td  class="smalltext">   0.8426</td>
                    <td  class="smalltext">   0.8242</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8400</span></td>
                    <td  class="smalltext"><span class="red">   0.8300</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8532</span></td>
                    <td  class="smalltext"><span class="red">   0.8479</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.8738</td>
                    <td  class="smalltext">   0.8671</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8497</td>
                    <td  class="smalltext">   0.8458</td>
                    <td  class="smalltext">   0.8297</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8440</span></td>
                    <td  class="smalltext"><span class="red">   0.8350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8560</span></td>
                    <td  class="smalltext"><span class="red">   0.8509</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.8738</td>
                    <td  class="smalltext">   0.8673</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8506</td>
                    <td  class="smalltext">   0.8468</td>
                    <td  class="smalltext">   0.8312</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8450</span></td>
                    <td  class="smalltext"><span class="red">   0.8360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8568</span></td>
                    <td  class="smalltext"><span class="red">   0.8518</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8685</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8518</td>
                    <td  class="smalltext">   0.8489</td>
                    <td  class="smalltext">   0.8324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8426</span></td>
                    <td  class="smalltext"><span class="red">   0.8360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8555</span></td>
                    <td  class="smalltext"><span class="red">   0.8518</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.8739</td>
                    <td  class="smalltext">   0.8679</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8536</td>
                    <td  class="smalltext">   0.8500</td>
                    <td  class="smalltext">   0.8367</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8490</span></td>
                    <td  class="smalltext"><span class="red">   0.8410</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8594</span></td>
                    <td  class="smalltext"><span class="red">   0.8547</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>8</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.8750</td>
                    <td  class="smalltext">   0.8690</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8547</td>
                    <td  class="smalltext">   0.8520</td>
                    <td  class="smalltext">   0.8378</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8469</span></td>
                    <td  class="smalltext"><span class="red">   0.8410</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8583</span></td>
                    <td  class="smalltext"><span class="red">   0.8547</span></td>
                    <td  class="smalltext"><span class="red">   0.8750</span></td>

                </tr>


            </table>
        </div>

    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>