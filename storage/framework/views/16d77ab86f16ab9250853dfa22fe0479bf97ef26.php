<?php $__env->startSection('content'); ?>

<div class="container edit-profile-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="header">
                <h2>Edit Profile</h2>
            </div>
        </div>
    </div>
    <?php echo Form::model($user,['url' => route('editprofile.update'),'files'=>true,'class'=>'form-horizontal','method'=>'POST']); ?>
    <?php echo e(csrf_field()); ?>
    <?php echo e(method_field('PATCH')); ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-inputs">
                    <div class="form-group">
                        <?php echo e(Form::label('firstname', 'First Name')); ?>
                        <?php echo e(Form::text('firstname' ,null, ['class' => 'form-control','id'=>'firstname','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('lasttname', 'Last Name')); ?>
                        <?php echo e(Form::text('lastname' ,null, ['class' => 'form-control','id'=>'lastname','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('company', 'Company')); ?>
                        <?php echo e(Form::text('company' ,null, ['class' => 'form-control','id'=>'company'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('address', 'Address')); ?>
                        <?php echo e(Form::text('address' ,null, ['class' => 'form-control','id'=>'address','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('phone', 'Phone')); ?>
                        <?php echo e(Form::text('phone' ,null, ['class' => 'form-control','id'=>'phone','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select name="country" class="form-control" id="country" required>
                            <?php echo $__env->make('dashboard.partials._countries', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('zipcode', 'Zip Code')); ?>
                        <?php echo e(Form::number('zipcode' ,null, ['class' => 'form-control','id'=>'zipcode','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('city', 'City')); ?>
                        <?php echo e(Form::text('city' ,null, ['class' => 'form-control','id'=>'city','required' => 'required'])); ?>
                    </div>
                    <div class="form-group">
                        <label for="state">State/Province</label>
                        <select name="state" class="form-control" id="state" required>
                            <?php echo $__env->make('dashboard.partials._states', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-inputs">
                    <div class="form-group">
                        <?php echo e(Form::label('skills', 'Skills')); ?>
                        <?php echo e(Form::textarea('skills' ,null, ['class' => 'form-control' ,'id'=>'skills','rows'=>'4'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('interests', 'Interests')); ?>
                        <?php echo e(Form::textarea('interests' ,null, ['class' => 'form-control' ,'id'=>'interests','rows'=>'4'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('about', 'About')); ?>
                        <?php echo e(Form::textarea('about' ,null, ['class' => 'form-control' ,'id'=>'about','rows'=>'4'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('website', 'Website')); ?>
                        <?php echo e(Form::text('website' ,null, ['class' => 'form-control','id'=>'website'])); ?>
                    </div>
                    <div class="form-group">
                        <?php echo e(Form::label('image_url', 'Profile Image')); ?>
                        <?php echo e(Form::file('image' ,null, ['class' => 'form-control','id'=>'image_url'])); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-submit">
                    <input type="submit" class="btn btn-primary btnsubmit" value="Update">
                </div>
            </div>
        </div>
    <?php echo Form::close(); ?>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>