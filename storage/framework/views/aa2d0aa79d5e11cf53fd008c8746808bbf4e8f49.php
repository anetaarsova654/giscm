<!--header-->
<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-top-in">
                <div class="logo">
                    <a href="https://giscm.com"><img src="../images/giscm2.png" alt=" "></a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="h_menu4">
                <a class="toggleMenu" href="#" style="display: none;">Menu</a>
                <ul class="nav">
                    <li class=""><a href="<?php echo e(route('home')); ?>"><i> </i>Home</a></li>
                    <li class=""><a href="<?php echo e(route('library')); ?>">Library</a></li>
                    <li class=""><a href="<?php echo e(route('members.index')); ?>">Members</a></li>
                   
                    <li class=""><a href="<?php echo e(route('cadmodels.create')); ?>">Upload</a></li>
                    <li class=""><a href="<?php echo e(route('cadmodels.index')); ?>">Community</a></li>
                    <?php if(Auth::guard('web')->check()): ?>
                    <li class=""><a href="<?php echo e(route('dashboard')); ?>">My Dashboard</a></li>
                    <?php endif; ?>
                    <li class=""><a href="<?php echo e(route('chats.index')); ?>">Chat</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>