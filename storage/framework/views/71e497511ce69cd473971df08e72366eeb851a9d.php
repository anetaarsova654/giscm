<?php $__env->startSection('content'); ?>
    <div class="container user-dashboard-content">
        <div class="row">
            <div class="user-dashboard-header">
                <h3 align="left">My Dashboard</h3>
                <div class="user-dashboard-detail">
                    <div class="user-detail">
                        <h5 class="detail"><?php echo e($user->firstname." ".$user->lastname); ?></h5>
                        <h5 class="detail">Title:<?php echo e($user->classification); ?></h5>
                        <h5 class="detail edit-profile-link"><a href="<?php echo e(route('editprofile')); ?>">Edit Profile</a></h5>
                    </div>
                    <a href="" title="Edit Profile"><img  alt class="userProfileImg" src="<?php echo e(Storage::url($user->getProfileImage())); ?>"></a>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-10">
                        <div class="user-dashboard-contributions">
                            <div class="user-dashboard-contribution-header">
                                <h4 class="contribution-heading" align="left">My Files</h4>
                                <div class="form-group contribution-categories">
                                    
                                    <?php echo Form::open(['url' => route('dashboard',$user->id),'class'=>'form-horizontal','method' => 'GET','id'=>'category-form']); ?>
                                        <?php echo e(Form::select('category',$categories, null, ['class'=>'form-control','id'=>'category','placeholder' => 'All'])); ?>
                                    <?php echo Form::close(); ?>
                                </div>
                            </div>
                            <div >
                                <?php $__empty_1 = true; $__currentLoopData = array_chunk($cadmodels->items(),4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadmodel_chunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <div class="row">
                                        <?php $__currentLoopData = $cadmodel_chunk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadmodel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-sm-3">
                                                <div class="user-dashboard-contribution-content">
                                                    <div class="contributor-model">
                                                        <div class="model-image">
                                                            <a href="<?php echo e(route('cadmodels.show',$cadmodel['id'])); ?>"><img alt="Model Image" src="<?php echo e(Storage::url($cadmodel['image_url'])); ?>" class="" style="width: 150px"></a>
                                                            <button onclick="window.location.href='<?php echo e(route('cadmodels.edit',$cadmodel['id'])); ?>'" class="btn btn-primary edit"><i class="fa fa-pencil-square"></i></button>
                                                            <form  action="<?php echo e(route('cadmodels.destroy',$cadmodel['id'])); ?>" method="POST">
                                                                <?php echo e(csrf_field()); ?>
                                                                <?php echo e(method_field('DELETE')); ?>
                                                                 <button class="btn btn-primary delete"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        </div>
                                                        <br>
                                                        <div class="model-name" >
                                                            <a href="<?php echo e(route('cadmodels.show',$cadmodel['id'])); ?>" class=""><b><?php echo e($cadmodel['name']); ?></b></a><br>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <p>No Contributions Available...</p>
                                <?php endif; ?>
                                    <?php echo e($cadmodels->appends(request()->except('page'))->links()); ?>
                            </div>
                        </div>
                        <div class="user-dashboard-comments">
                            <div class="user-dashboard-comment-header">
                                <h4 class="comment-heading" align="left">Comments</h4>
                                <div class="form-group comment-categories">
                                    
                                </div>
                            </div>
                            <div class="user-dashboard-comment-content">
                                <?php $__empty_1 = true; $__currentLoopData = $user->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <div class="comment">
                                    <?php echo e($comment->comment); ?>
                                    <br>
                                    Posted on&nbsp;
                                    <a href="<?php echo e(route('cadmodels.show',$comment->cadmodel->id)); ?>" ><?php echo e($comment->cadmodel->name); ?></a> (<?php echo e($comment->created_at->format('d-M-Y')); ?>)
                                    <br>
                                    <hr>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <p>No Comments Available..</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>

        $('#category').change(function(){

            if(this.value == ''){
                console.log(this.value);

                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);

                    window.location.href = clean_uri;
                }
            }
            else{
                document.getElementById("category-form").submit();
            }


        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>