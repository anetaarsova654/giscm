<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="<?php echo e(route('threadSpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries1')); ?>">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>5/16-18 UNC to 9/16 -32 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries3')); ?>">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries5')); ?>">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries8')); ?>">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table">

                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br><span class="red">Max Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>
                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-18 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.3113</td>
                    <td  class="smalltext">   0.2982</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2752</td>
                    <td  class="smalltext">   0.2691</td>
                    <td  class="smalltext">   0.2452</td>
                </tr>
                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2650</span></td>
                    <td  class="smalltext"><span class="red">   0.2520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2843</span></td>
                    <td  class="smalltext"><span class="red">   0.2764</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>
                </tr>
                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-18 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.3113</td>
                    <td  class="smalltext">   0.3026</td>
                    <td  class="smalltext">0.2982</td>
                    <td  class="smalltext">   0.2752</td>
                    <td  class="smalltext">   0.2712</td>
                    <td  class="smalltext">   0.2452</td>
                </tr>
                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2650</span></td>
                    <td  class="smalltext"><span class="red">   0.2520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2817</span></td>
                    <td  class="smalltext"><span class="red">   0.2764</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>
                </tr>
                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-18 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3125</td>
                    <td  class="smalltext">   0.3038</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2764</td>
                    <td  class="smalltext">   0.2734</td>
                    <td  class="smalltext">   0.2464</td>
                </tr>
                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2630</span></td>
                    <td  class="smalltext"><span class="red">   0.2520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2803</span></td>
                    <td  class="smalltext"><span class="red">   0.2764</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>
                </tr>
                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.3113</td>
                    <td  class="smalltext">   0.3032</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2788</td>
                    <td  class="smalltext">   0.2748</td>
                    <td  class="smalltext">   0.2518</td>
                </tr>
                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2700</span></td>
                    <td  class="smalltext"><span class="red">   0.2580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2852</span></td>
                    <td  class="smalltext"><span class="red">   0.2800</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>
                </tr>
                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3125</td>
                    <td  class="smalltext">   0.3044</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2800</td>
                    <td  class="smalltext">   0.2770</td>
                    <td  class="smalltext">   0.2530</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2680</span></td>
                    <td  class="smalltext"><span class="red">   0.2580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2839</span></td>
                    <td  class="smalltext"><span class="red">   0.2800</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3114</td>
                    <td  class="smalltext">   0.3006</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2843</td>
                    <td  class="smalltext">   0.2788</td>
                    <td  class="smalltext">   0.2618</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2770</span></td>
                    <td  class="smalltext"><span class="red">   0.2670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2925</span></td>
                    <td  class="smalltext"><span class="red">   0.2854</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3114</td>
                    <td  class="smalltext">   0.3042</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2843</td>
                    <td  class="smalltext">   0.2806</td>
                    <td  class="smalltext">   0.2618</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2770</span></td>
                    <td  class="smalltext"><span class="red">   0.2670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2902</span></td>
                    <td  class="smalltext"><span class="red">   0.2854</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3125</td>
                    <td  class="smalltext">   0.3053</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2854</td>
                    <td  class="smalltext">   0.2827</td>
                    <td  class="smalltext">   0.2629</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2754</span></td>
                    <td  class="smalltext"><span class="red">   0.2670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2890</span></td>
                    <td  class="smalltext"><span class="red">   0.2854</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.3115</td>
                    <td  class="smalltext">   0.3048</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2874</td>
                    <td  class="smalltext">   0.2839</td>
                    <td  class="smalltext">   0.2674</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2810</span></td>
                    <td  class="smalltext"><span class="red">   0.2720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2929</span></td>
                    <td  class="smalltext"><span class="red">   0.2884</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.3115</td>
                    <td  class="smalltext">   0.3050</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2883</td>
                    <td  class="smalltext">   0.2849</td>
                    <td  class="smalltext">   0.2689</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2820</span></td>
                    <td  class="smalltext"><span class="red">   0.2740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2937</span></td>
                    <td  class="smalltext"><span class="red">   0.2893</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3125</td>
                    <td  class="smalltext">   0.3060</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2893</td>
                    <td  class="smalltext">   0.2867</td>
                    <td  class="smalltext">   0.2699</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2807</span></td>
                    <td  class="smalltext"><span class="red">   0.2740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2926</span></td>
                    <td  class="smalltext"><span class="red">   0.2893</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-32 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.3115</td>
                    <td  class="smalltext">   0.3055</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2912</td>
                    <td  class="smalltext">   0.2880</td>
                    <td  class="smalltext">   0.2743</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2860</span></td>
                    <td  class="smalltext"><span class="red">   0.2790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2964</span></td>
                    <td  class="smalltext"><span class="red">   0.2922</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-32 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3125</td>
                    <td  class="smalltext">   0.3065</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2922</td>
                    <td  class="smalltext">   0.2898</td>
                    <td  class="smalltext">   0.2753</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2847</span></td>
                    <td  class="smalltext"><span class="red">   0.2790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2953</span></td>
                    <td  class="smalltext"><span class="red">   0.2922</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-36 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0009</td>
                    <td  class="smalltext">   0.3116</td>
                    <td  class="smalltext">   0.3061</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2936</td>
                    <td  class="smalltext">   0.2905</td>
                    <td  class="smalltext">   0.2785</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2890</span></td>
                    <td  class="smalltext"><span class="red">   0.2820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2985</span></td>
                    <td  class="smalltext"><span class="red">   0.2945</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-40 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0009</td>
                    <td  class="smalltext">   0.3116</td>
                    <td  class="smalltext">   0.3065</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2954</td>
                    <td  class="smalltext">   0.2925</td>
                    <td  class="smalltext">   0.2818</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2910</span></td>
                    <td  class="smalltext"><span class="red">   0.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3001</span></td>
                    <td  class="smalltext"><span class="red">   0.2963</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>5</sup>/<sub>16</sub>-48 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0008</td>
                    <td  class="smalltext">   0.3117</td>
                    <td  class="smalltext">   0.3072</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.2982</td>
                    <td  class="smalltext">   0.2955</td>
                    <td  class="smalltext">   0.2869</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.2950</span></td>
                    <td  class="smalltext"><span class="red">   0.2900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3026</span></td>
                    <td  class="smalltext"><span class="red">   0.2990</span></td>
                    <td  class="smalltext"><span class="red">   0.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-16 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.3737</td>
                    <td  class="smalltext">   0.3595</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3331</td>
                    <td  class="smalltext">   0.3266</td>
                    <td  class="smalltext">   0.2992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3210</span></td>
                    <td  class="smalltext"><span class="red">   0.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3429</span></td>
                    <td  class="smalltext"><span class="red">   0.3344</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-16 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.3737</td>
                    <td  class="smalltext">   0.3643</td>
                    <td  class="smalltext">0.3595</td>
                    <td  class="smalltext">   0.3331</td>
                    <td  class="smalltext">   0.3287</td>
                    <td  class="smalltext">   0.2992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3210</span></td>
                    <td  class="smalltext"><span class="red">   0.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3401</span></td>
                    <td  class="smalltext"><span class="red">   0.3344</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-16 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3750</td>
                    <td  class="smalltext">   0.3656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3344</td>
                    <td  class="smalltext">   0.3311</td>
                    <td  class="smalltext">   0.3005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3182</span></td>
                    <td  class="smalltext"><span class="red">   0.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3387</span></td>
                    <td  class="smalltext"><span class="red">   0.3344</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.3737</td>
                    <td  class="smalltext">   0.3650</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3376</td>
                    <td  class="smalltext">   0.3333</td>
                    <td  class="smalltext">   0.3076</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3280</span></td>
                    <td  class="smalltext"><span class="red">   0.3150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3445</span></td>
                    <td  class="smalltext"><span class="red">   0.3389</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.3738</td>
                    <td  class="smalltext">   0.3657</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3413</td>
                    <td  class="smalltext">   0.3372</td>
                    <td  class="smalltext">   0.3143</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3320</span></td>
                    <td  class="smalltext"><span class="red">   0.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3479</span></td>
                    <td  class="smalltext"><span class="red">   0.3425</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3750</td>
                    <td  class="smalltext">   0.3669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3425</td>
                    <td  class="smalltext">   0.3394</td>
                    <td  class="smalltext">   0.3155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3297</span></td>
                    <td  class="smalltext"><span class="red">   0.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3465</span></td>
                    <td  class="smalltext"><span class="red">   0.3425</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3739</td>
                    <td  class="smalltext">   0.3631</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3468</td>
                    <td  class="smalltext">   0.3411</td>
                    <td  class="smalltext">   0.3243</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3400</span></td>
                    <td  class="smalltext"><span class="red">   0.3300</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3553</span></td>
                    <td  class="smalltext"><span class="red">   0.3479</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3739</td>
                    <td  class="smalltext">   0.3667</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3468</td>
                    <td  class="smalltext">   0.3430</td>
                    <td  class="smalltext">   0.3243</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3400</span></td>
                    <td  class="smalltext"><span class="red">   0.3300</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3528</span></td>
                    <td  class="smalltext"><span class="red">   0.3479</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-24 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3750</td>
                    <td  class="smalltext">   0.3678</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3479</td>
                    <td  class="smalltext">   0.3450</td>
                    <td  class="smalltext">   0.3254</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3372</span></td>
                    <td  class="smalltext"><span class="red">   0.3300</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3516</span></td>
                    <td  class="smalltext"><span class="red">   0.3479</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3739</td>
                    <td  class="smalltext">   0.3672</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3498</td>
                    <td  class="smalltext">   0.3462</td>
                    <td  class="smalltext">   0.3298</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3440</span></td>
                    <td  class="smalltext"><span class="red">   0.3350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3556</span></td>
                    <td  class="smalltext"><span class="red">   0.3509</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3739</td>
                    <td  class="smalltext">   0.3674</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3507</td>
                    <td  class="smalltext">   0.3471</td>
                    <td  class="smalltext">   0.3313</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3450</span></td>
                    <td  class="smalltext"><span class="red">   0.3360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3564</span></td>
                    <td  class="smalltext"><span class="red">   0.3518</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3750</td>
                    <td  class="smalltext">   0.3685</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3518</td>
                    <td  class="smalltext">   0.3491</td>
                    <td  class="smalltext">   0.3324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3426</span></td>
                    <td  class="smalltext"><span class="red">   0.3360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3553</span></td>
                    <td  class="smalltext"><span class="red">   0.3518</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-32 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.3740</td>
                    <td  class="smalltext">   0.3680</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3537</td>
                    <td  class="smalltext">   0.3503</td>
                    <td  class="smalltext">   0.3368</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3490</span></td>
                    <td  class="smalltext"><span class="red">   0.3410</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3591</span></td>
                    <td  class="smalltext"><span class="red">   0.3547</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-32 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.3750</td>
                    <td  class="smalltext">   0.3690</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3547</td>
                    <td  class="smalltext">   0.3522</td>
                    <td  class="smalltext">   0.3378</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3469</span></td>
                    <td  class="smalltext"><span class="red">   0.3410</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3580</span></td>
                    <td  class="smalltext"><span class="red">   0.3547</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-36 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.3740</td>
                    <td  class="smalltext">   0.3685</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3560</td>
                    <td  class="smalltext">   0.3528</td>
                    <td  class="smalltext">   0.3409</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3520</span></td>
                    <td  class="smalltext"><span class="red">   0.3450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3612</span></td>
                    <td  class="smalltext"><span class="red">   0.3570</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>3</sup>/<sub>8</sub>-40 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0009</td>
                    <td  class="smalltext">   0.3741</td>
                    <td  class="smalltext">   0.3690</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3579</td>
                    <td  class="smalltext">   0.3548</td>
                    <td  class="smalltext">   0.3443</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3540</span></td>
                    <td  class="smalltext"><span class="red">   0.3480</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3628</span></td>
                    <td  class="smalltext"><span class="red">   0.3588</span></td>
                    <td  class="smalltext"><span class="red">   0.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>0.390—27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.3889</td>
                    <td  class="smalltext">   0.3822</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3648</td>
                    <td  class="smalltext">   0.3612</td>
                    <td  class="smalltext">   0.3448</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3590</span></td>
                    <td  class="smalltext"><span class="red">   0.3500</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3706</span></td>
                    <td  class="smalltext"><span class="red">   0.3659</span></td>
                    <td  class="smalltext"><span class="red">   0.3900</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-14 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.4361</td>
                    <td  class="smalltext">   0.4206</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3897</td>
                    <td  class="smalltext">   0.3826</td>
                    <td  class="smalltext">   0.3511</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3760</span></td>
                    <td  class="smalltext"><span class="red">   0.3600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4003</span></td>
                    <td  class="smalltext"><span class="red">   0.3911</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-14 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.4361</td>
                    <td  class="smalltext">   0.4258</td>
                    <td  class="smalltext">0.4206</td>
                    <td  class="smalltext">   0.3897</td>
                    <td  class="smalltext">   0.3850</td>
                    <td  class="smalltext">   0.3511</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3760</span></td>
                    <td  class="smalltext"><span class="red">   0.3600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3972</span></td>
                    <td  class="smalltext"><span class="red">   0.3911</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-14 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4375</td>
                    <td  class="smalltext">   0.4272</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3911</td>
                    <td  class="smalltext">   0.3876</td>
                    <td  class="smalltext">   0.3525</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3717</span></td>
                    <td  class="smalltext"><span class="red">   0.3600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3957</span></td>
                    <td  class="smalltext"><span class="red">   0.3911</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.4361</td>
                    <td  class="smalltext">   0.4267</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3955</td>
                    <td  class="smalltext">   0.3909</td>
                    <td  class="smalltext">   0.3616</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3840</span></td>
                    <td  class="smalltext"><span class="red">   0.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4028</span></td>
                    <td  class="smalltext"><span class="red">   0.3969</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4375</td>
                    <td  class="smalltext">   0.4281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.3969</td>
                    <td  class="smalltext">   0.3935</td>
                    <td  class="smalltext">   0.3630</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3800</span></td>
                    <td  class="smalltext"><span class="red">   0.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4014</span></td>
                    <td  class="smalltext"><span class="red">   0.3969</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4362</td>
                    <td  class="smalltext">   0.4275</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4001</td>
                    <td  class="smalltext">   0.3958</td>
                    <td  class="smalltext">   0.3701</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3900</span></td>
                    <td  class="smalltext"><span class="red">   0.3770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4070</span></td>
                    <td  class="smalltext"><span class="red">   0.4014</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4362</td>
                    <td  class="smalltext">   0.4240</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4037</td>
                    <td  class="smalltext">   0.3975</td>
                    <td  class="smalltext">   0.3767</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3950</span></td>
                    <td  class="smalltext"><span class="red">   0.3830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4131</span></td>
                    <td  class="smalltext"><span class="red">   0.4050</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4362</td>
                    <td  class="smalltext">   0.4281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4037</td>
                    <td  class="smalltext">   0.3995</td>
                    <td  class="smalltext">   0.3767</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3950</span></td>
                    <td  class="smalltext"><span class="red">   0.3830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4104</span></td>
                    <td  class="smalltext"><span class="red">   0.4050</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4375</td>
                    <td  class="smalltext">   0.4294</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4050</td>
                    <td  class="smalltext">   0.4019</td>
                    <td  class="smalltext">   0.3780</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.3916</span></td>
                    <td  class="smalltext"><span class="red">   0.3830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4091</span></td>
                    <td  class="smalltext"><span class="red">   0.4050</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.4364</td>
                    <td  class="smalltext">   0.4292</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4093</td>
                    <td  class="smalltext">   0.4055</td>
                    <td  class="smalltext">   0.3868</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4020</span></td>
                    <td  class="smalltext"><span class="red">   0.3920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4153</span></td>
                    <td  class="smalltext"><span class="red">   0.4104</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.4364</td>
                    <td  class="smalltext">   0.4297</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4123</td>
                    <td  class="smalltext">   0.4087</td>
                    <td  class="smalltext">   0.3923</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4060</span></td>
                    <td  class="smalltext"><span class="red">   0.3970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4181</span></td>
                    <td  class="smalltext"><span class="red">   0.4134</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-28 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.4364</td>
                    <td  class="smalltext">   0.4299</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4132</td>
                    <td  class="smalltext">   0.4096</td>
                    <td  class="smalltext">   0.3838</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4070</span></td>
                    <td  class="smalltext"><span class="red">   0.3990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4189</span></td>
                    <td  class="smalltext"><span class="red">   0.4143</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-28 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4375</td>
                    <td  class="smalltext">   0.4310</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4143</td>
                    <td  class="smalltext">   0.4116</td>
                    <td  class="smalltext">   0.3949</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4051</span></td>
                    <td  class="smalltext"><span class="red">   0.3990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4178</span></td>
                    <td  class="smalltext"><span class="red">   0.4143</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.4365</td>
                    <td  class="smalltext">   0.4305</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4162</td>
                    <td  class="smalltext">   0.4128</td>
                    <td  class="smalltext">   0.3993</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4110</span></td>
                    <td  class="smalltext"><span class="red">   0.4040</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4216</span></td>
                    <td  class="smalltext"><span class="red">   0.4172</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>7</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4375</td>
                    <td  class="smalltext">   0.4315</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4172</td>
                    <td  class="smalltext">   0.4147</td>
                    <td  class="smalltext">   0.4003</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4094</span></td>
                    <td  class="smalltext"><span class="red">   0.4040</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4205</span></td>
                    <td  class="smalltext"><span class="red">   0.4172</span></td>
                    <td  class="smalltext"><span class="red">   0.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-12 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.4984</td>
                    <td  class="smalltext">   0.4870</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4443</td>
                    <td  class="smalltext">   0.4389</td>
                    <td  class="smalltext">   0.3992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4280</span></td>
                    <td  class="smalltext"><span class="red">   0.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4529</span></td>
                    <td  class="smalltext"><span class="red">   0.4459</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-12 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4459</td>
                    <td  class="smalltext">   0.4419</td>
                    <td  class="smalltext">   0.4008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4223</span></td>
                    <td  class="smalltext"><span class="red">   0.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4511</span></td>
                    <td  class="smalltext"><span class="red">   0.4459</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-13 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.4985</td>
                    <td  class="smalltext">   0.4822</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4485</td>
                    <td  class="smalltext">   0.4411</td>
                    <td  class="smalltext">   0.4069</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4340</span></td>
                    <td  class="smalltext"><span class="red">   0.4170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4597</span></td>
                    <td  class="smalltext"><span class="red">   0.4500</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-13 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.4985</td>
                    <td  class="smalltext">   0.4876</td>
                    <td  class="smalltext">0.4822</td>
                    <td  class="smalltext">   0.4485</td>
                    <td  class="smalltext">   0.4435</td>
                    <td  class="smalltext">   0.4069</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4340</span></td>
                    <td  class="smalltext"><span class="red">   0.4170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4565</span></td>
                    <td  class="smalltext"><span class="red">   0.4500</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-13 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4891</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4500</td>
                    <td  class="smalltext">   0.4463</td>
                    <td  class="smalltext">   0.4084</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4284</span></td>
                    <td  class="smalltext"><span class="red">   0.4170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4548</span></td>
                    <td  class="smalltext"><span class="red">   0.4500</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.4985</td>
                    <td  class="smalltext">   0.4882</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4521</td>
                    <td  class="smalltext">   0.4471</td>
                    <td  class="smalltext">   0.4135</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4380</span></td>
                    <td  class="smalltext"><span class="red">   0.4230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4601</span></td>
                    <td  class="smalltext"><span class="red">   0.4536</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.4986</td>
                    <td  class="smalltext">   0.4892</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4580</td>
                    <td  class="smalltext">   0.4533</td>
                    <td  class="smalltext">   0.4241</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4460</span></td>
                    <td  class="smalltext"><span class="red">   0.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4655</span></td>
                    <td  class="smalltext"><span class="red">   0.4594</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4594</td>
                    <td  class="smalltext">   0.4559</td>
                    <td  class="smalltext">   0.4255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4419</span></td>
                    <td  class="smalltext"><span class="red">   0.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4640</span></td>
                    <td  class="smalltext"><span class="red">   0.4594</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4987</td>
                    <td  class="smalltext">   0.4900</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4626</td>
                    <td  class="smalltext">   0.4582</td>
                    <td  class="smalltext">   0.4326</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4530</span></td>
                    <td  class="smalltext"><span class="red">   0.4400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4697</span></td>
                    <td  class="smalltext"><span class="red">   0.4639</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4987</td>
                    <td  class="smalltext">   0.4865</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4662</td>
                    <td  class="smalltext">   0.4598</td>
                    <td  class="smalltext">   0.4392</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4570</span></td>
                    <td  class="smalltext"><span class="red">   0.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4759</span></td>
                    <td  class="smalltext"><span class="red">   0.4675</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.4987</td>
                    <td  class="smalltext">   0.4906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4662</td>
                    <td  class="smalltext">   0.4619</td>
                    <td  class="smalltext">   0.4392</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4570</span></td>
                    <td  class="smalltext"><span class="red">   0.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4731</span></td>
                    <td  class="smalltext"><span class="red">   0.4675</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-20 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4675</td>
                    <td  class="smalltext">   0.4643</td>
                    <td  class="smalltext">   0.4405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4537</span></td>
                    <td  class="smalltext"><span class="red">   0.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4717</span></td>
                    <td  class="smalltext"><span class="red">   0.4675</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.4988</td>
                    <td  class="smalltext">   0.4916</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4717</td>
                    <td  class="smalltext">   0.4678</td>
                    <td  class="smalltext">   0.4492</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4650</span></td>
                    <td  class="smalltext"><span class="red">   0.4550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4780</span></td>
                    <td  class="smalltext"><span class="red">   0.4729</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.4989</td>
                    <td  class="smalltext">   0.4922</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4748</td>
                    <td  class="smalltext">   0.4711</td>
                    <td  class="smalltext">   0.4548</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4690</span></td>
                    <td  class="smalltext"><span class="red">   0.4600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4807</span></td>
                    <td  class="smalltext"><span class="red">   0.4759</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-28 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.4989</td>
                    <td  class="smalltext">   0.4924</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4757</td>
                    <td  class="smalltext">   0.4720</td>
                    <td  class="smalltext">   0.4563</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4700</span></td>
                    <td  class="smalltext"><span class="red">   0.4610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4816</span></td>
                    <td  class="smalltext"><span class="red">   0.4768</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-28 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4935</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4768</td>
                    <td  class="smalltext">   0.4740</td>
                    <td  class="smalltext">   0.4574</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4676</span></td>
                    <td  class="smalltext"><span class="red">   0.4610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4804</span></td>
                    <td  class="smalltext"><span class="red">   0.4768</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.4990</td>
                    <td  class="smalltext">   0.4930</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4787</td>
                    <td  class="smalltext">   0.4752</td>
                    <td  class="smalltext">   0.4618</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4740</span></td>
                    <td  class="smalltext"><span class="red">   0.4660</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4842</span></td>
                    <td  class="smalltext"><span class="red">   0.4797</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>1</sup>/<sub>2</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5000</td>
                    <td  class="smalltext">   0.4940</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.4797</td>
                    <td  class="smalltext">   0.4771</td>
                    <td  class="smalltext">   0.4628</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4719</span></td>
                    <td  class="smalltext"><span class="red">   0.4660</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4831</span></td>
                    <td  class="smalltext"><span class="red">   0.4797</span></td>
                    <td  class="smalltext"><span class="red">   0.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-12 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.5609</td>
                    <td  class="smalltext">   0.5437</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5068</td>
                    <td  class="smalltext">   0.4990</td>
                    <td  class="smalltext">   0.4617</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4900</span></td>
                    <td  class="smalltext"><span class="red">   0.4720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5186</span></td>
                    <td  class="smalltext"><span class="red">   0.5084</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-12 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   0.5609</td>
                    <td  class="smalltext">   0.5495</td>
                    <td  class="smalltext">0.5437</td>
                    <td  class="smalltext">   0.5068</td>
                    <td  class="smalltext">   0.5016</td>
                    <td  class="smalltext">   0.4617</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4900</span></td>
                    <td  class="smalltext"><span class="red">   0.4720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5152</span></td>
                    <td  class="smalltext"><span class="red">   0.5084</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-12 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5511</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5084</td>
                    <td  class="smalltext">   0.5045</td>
                    <td  class="smalltext">   0.4633</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.4843</span></td>
                    <td  class="smalltext"><span class="red">   0.4720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5135</span></td>
                    <td  class="smalltext"><span class="red">   0.5084</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.5610</td>
                    <td  class="smalltext">   0.5507</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5146</td>
                    <td  class="smalltext">   0.5096</td>
                    <td  class="smalltext">   0.4760</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5010</span></td>
                    <td  class="smalltext"><span class="red">   0.4850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5226</span></td>
                    <td  class="smalltext"><span class="red">   0.5161</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.5611</td>
                    <td  class="smalltext">   0.5517</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5205</td>
                    <td  class="smalltext">   0.5158</td>
                    <td  class="smalltext">   0.4866</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5090</span></td>
                    <td  class="smalltext"><span class="red">   0.4950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5280</span></td>
                    <td  class="smalltext"><span class="red">   0.5219</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5531</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5219</td>
                    <td  class="smalltext">   0.5184</td>
                    <td  class="smalltext">   0.4880</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5040</span></td>
                    <td  class="smalltext"><span class="red">   0.4950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5265</span></td>
                    <td  class="smalltext"><span class="red">   0.5219</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.5611</td>
                    <td  class="smalltext">   0.5480</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5250</td>
                    <td  class="smalltext">   0.5182</td>
                    <td  class="smalltext">   0.4950</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5150</span></td>
                    <td  class="smalltext"><span class="red">   0.5020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5353</span></td>
                    <td  class="smalltext"><span class="red">   0.5264</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.5611</td>
                    <td  class="smalltext">   0.5524</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5250</td>
                    <td  class="smalltext">   0.5205</td>
                    <td  class="smalltext">   0.4950</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5150</span></td>
                    <td  class="smalltext"><span class="red">   0.5020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5323</span></td>
                    <td  class="smalltext"><span class="red">   0.5264</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-18 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5538</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5264</td>
                    <td  class="smalltext">   0.5230</td>
                    <td  class="smalltext">   0.4964</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5106</span></td>
                    <td  class="smalltext"><span class="red">   0.5020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5308</span></td>
                    <td  class="smalltext"><span class="red">   0.5264</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.5612</td>
                    <td  class="smalltext">   0.5531</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5287</td>
                    <td  class="smalltext">   0.5245</td>
                    <td  class="smalltext">   0.5017</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5200</span></td>
                    <td  class="smalltext"><span class="red">   0.5080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5355</span></td>
                    <td  class="smalltext"><span class="red">   0.5300</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5544</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5300</td>
                    <td  class="smalltext">   0.5268</td>
                    <td  class="smalltext">   0.5030</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5162</span></td>
                    <td  class="smalltext"><span class="red">   0.5080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5341</span></td>
                    <td  class="smalltext"><span class="red">   0.5300</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-24 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.5613</td>
                    <td  class="smalltext">   0.5541</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5342</td>
                    <td  class="smalltext">   0.5303</td>
                    <td  class="smalltext">   0.5117</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5270</span></td>
                    <td  class="smalltext"><span class="red">   0.5170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5405</span></td>
                    <td  class="smalltext"><span class="red">   0.5354</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-24 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5553</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5354</td>
                    <td  class="smalltext">   0.5325</td>
                    <td  class="smalltext">   0.5129</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5244</span></td>
                    <td  class="smalltext"><span class="red">   0.5170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5392</span></td>
                    <td  class="smalltext"><span class="red">   0.5354</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.5614</td>
                    <td  class="smalltext">   0.5547</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5373</td>
                    <td  class="smalltext">   0.5336</td>
                    <td  class="smalltext">   0.5173</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5310</span></td>
                    <td  class="smalltext"><span class="red">   0.5220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5432</span></td>
                    <td  class="smalltext"><span class="red">   0.5384</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.5614</td>
                    <td  class="smalltext">   0.5549</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5382</td>
                    <td  class="smalltext">   0.5345</td>
                    <td  class="smalltext">   0.5188</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5320</span></td>
                    <td  class="smalltext"><span class="red">   0.5240</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5441</span></td>
                    <td  class="smalltext"><span class="red">   0.5393</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5560</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5393</td>
                    <td  class="smalltext">   0.5365</td>
                    <td  class="smalltext">   0.5199</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5301</span></td>
                    <td  class="smalltext"><span class="red">   0.5240</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5429</span></td>
                    <td  class="smalltext"><span class="red">   0.5393</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0010</td>
                    <td  class="smalltext">   0.5615</td>
                    <td  class="smalltext">   0.5555</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5412</td>
                    <td  class="smalltext">   0.5377</td>
                    <td  class="smalltext">   0.5243</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5360</span></td>
                    <td  class="smalltext"><span class="red">   0.5290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5467</span></td>
                    <td  class="smalltext"><span class="red">   0.5422</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>9</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.5625</td>
                    <td  class="smalltext">   0.5565</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.5422</td>
                    <td  class="smalltext">   0.5396</td>
                    <td  class="smalltext">   0.5253</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5344</span></td>
                    <td  class="smalltext"><span class="red">   0.5290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.5456</span></td>
                    <td  class="smalltext"><span class="red">   0.5422</span></td>
                    <td  class="smalltext"><span class="red">   0.5625</span></td>

                </tr>
            </table>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>