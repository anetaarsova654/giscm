<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <?php if(!Auth::check()): ?>

                    <p class="wel"><a href="<?php echo e(route('login')); ?>">Welcome visitor you can login or create an account.</a></p><br>

                <?php endif; ?>
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">

                        <?php echo Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']); ?>


                        <?php echo e(Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name'])); ?>


                        <input type="submit" value="">

                        <?php echo Form::close(); ?>


                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12"><a><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
            <div class="col-md-12 col-xs-12">
                <h2 class="subheading"><b>UN/UNF THREADS</b></h2>
                <ul class="list-group links">
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries1')); ?>">Inch Thread sizes from 0-80 UNF to &frac14;-56 UNS</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries2')); ?>">Inch Thread sizes from 5/16-18 UNC to 9/16 -32 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries3')); ?>">Inch Thread sizes from 5/8-11 UNC to 7/8-32 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">Inch Thread sizes from 15/16-12 UN to 1 3/16-28 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries5')); ?>">Inch Thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">Inch Thread sizes from 1 5/8-6 UN to 1 15/16-20 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">Inch Thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('unScrewThreadsSeries8')); ?>">Inch Thread sizes from 2-7/8-6 UN to 4-16 UN</a></li>
                </ul>
                <h2 class="subheading"><b>Metric threads</b></h2>
                <ul class="list-group links">
                    <li class="list-group-item"><a href="<?php echo e(route('metricThreads')); ?>">Metric threads</a></li>
                </ul>
                <h2 class="subheading"><b>NPT Threads</b></h2>
                <ul class="list-group links">
                    <li class="list-group-item"><a href="<?php echo e(route('nptHoleSizes')); ?>">NPT Hole Sizes</a></li>
                    <li class="list-group-item"><a href="<?php echo e(route('nptfThreads')); ?>">External threads</a></li>
                </ul>
            </div>

        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>