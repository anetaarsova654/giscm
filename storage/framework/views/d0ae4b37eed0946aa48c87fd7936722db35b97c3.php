<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel">

                    <a href="../../../wamp64/www/GISCM_asp/scripts/scripts/10_Logon.asp?action=logon">Welcome visitor you can login or create an account.</a>

                </p>
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">
                        <form action="../../../wamp64/www/GISCM_asp/scripts/scripts/prodList.asp" method="post" id="searchBox" name="searchBox">

                            <input type="text" name="strSearch"  value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}" align="middle" />
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="https://giscm.net/scripts/prodSearch.asp" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php echo e(route('adapterAssemblySpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Adapter Assembly Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>Tube Size vs Thread Size Chart</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row">
            <!--Table begining-->
            <div class="table-responsive">
                <table class="table">

                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b><span class="red">Dash No.</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Tube<br>Size</b></td>
                        <td  valign="bottom" class="smalltext"><b>Flareless</b></td>
                        <td  valign="bottom" class="smalltext"><b><span class="red">7000-8000</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>37 Flare<br>5000</b></td>
                        <td  valign="bottom" class="smalltext"><b>Pipe<br>3000</b></td>
                        <td  valign="bottom" class="smalltext"><b>Face-seal<br>4000</b></td>
                        <td  valign="bottom" class="smalltext"><b>Swivel<br>Adapters<br>9000</b></td>
                        <td  valign="bottom" class="smalltext"><b>Straight<br>Thread<br>"O" Ring<br>Boss</b></td>
                        <td  valign="bottom" class="smalltext"><b>7/8 Taper<br>Pressure<br>Plugs<br>100 P</b></td>
                        <td  valign="bottom" class="smalltext"><b>Metric</b></td>
                        <td  valign="bottom" class="smalltext"><b>BSPP</b></td>

                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1</td>
                        <td  class="smalltext" nowrap>1/16"</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1/16-27</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1/16-27</td>
                        <td  class="smalltext">M8 X 1.0</td>
                        <td  class="smalltext">1/16-28</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">2</td>
                        <td  class="smalltext" nowrap>1/8"</td>
                        <td  class="smalltext">5/16-24</td>
                        <td  class="smalltext">5/16-24</td>
                        <td  class="smalltext">5/16-24</td>
                        <td  class="smalltext">1/8-27</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1/8-27</td>
                        <td  class="smalltext">5/16-24</td>
                        <td  class="smalltext">1/8-27</td>
                        <td  class="smalltext">M10 X 1.0</td>
                        <td  class="smalltext">1/8-28</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">3</td>
                        <td  class="smalltext" nowrap>3/16"</td>
                        <td  class="smalltext">3/8-24</td>
                        <td  class="smalltext">3/8-24</td>
                        <td  class="smalltext">3/8-24</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">3/8-24</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">M12 X 1.5</td>
                        <td  class="smalltext"></td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">4</td>
                        <td  class="smalltext" nowrap>1/4"</td>
                        <td  class="smalltext">7/16-20</td>
                        <td  class="smalltext">1/2-20</td>
                        <td  class="smalltext">7/16-20</td>
                        <td  class="smalltext">1/4-18</td>
                        <td  class="smalltext">9/16-18</td>
                        <td  class="smalltext">1/4-18</td>
                        <td  class="smalltext">7/16-20</td>
                        <td  class="smalltext">1/4-18</td>
                        <td  class="smalltext">M14 X 1.5</td>
                        <td  class="smalltext">1/4-19</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">5</td>
                        <td  class="smalltext" nowrap>5/16"</td>
                        <td  class="smalltext">1/2-20</td>
                        <td  class="smalltext">9/16-18</td>
                        <td  class="smalltext">1/2-20</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1/2-20</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">M16 X 1.5</td>
                        <td  class="smalltext"></td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">6</td>
                        <td  class="smalltext" nowrap>3/8"</td>
                        <td  class="smalltext">9/16-18</td>
                        <td  class="smalltext">5/8-18</td>
                        <td  class="smalltext">9/16-18</td>
                        <td  class="smalltext">3/8-18</td>
                        <td  class="smalltext">11/16-16</td>
                        <td  class="smalltext">3/8-18</td>
                        <td  class="smalltext">9/16-18</td>
                        <td  class="smalltext">3/8-18</td>
                        <td  class="smalltext">M18 X 1.5</td>
                        <td  class="smalltext">3/8-19</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">8</td>
                        <td  class="smalltext" nowrap>1/2"</td>
                        <td  class="smalltext">3/4-16</td>
                        <td  class="smalltext">3/4-16</td>
                        <td  class="smalltext">3/4-16</td>
                        <td  class="smalltext">1/2-14</td>
                        <td  class="smalltext">13/16-16</td>
                        <td  class="smalltext">1/2-14</td>
                        <td  class="smalltext">3/4-16</td>
                        <td  class="smalltext">1/2-14</td>
                        <td  class="smalltext">M20 X 1.5</td>
                        <td  class="smalltext">1/2-14</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">10</td>
                        <td  class="smalltext" nowrap>5/8"</td>
                        <td  class="smalltext">7/8-14</td>
                        <td  class="smalltext">15/16-16</td>
                        <td  class="smalltext">7/8-14</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1-14</td>
                        <td  class="smalltext">7/8-14</td>
                        <td  class="smalltext">7/8-14</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">M22 X 1.5</td>
                        <td  class="smalltext">5/8-14</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">12</td>
                        <td  class="smalltext" nowrap>3/4"</td>
                        <td  class="smalltext">1-1/16-12</td>
                        <td  class="smalltext">1-1/16-16</td>
                        <td  class="smalltext">1-1/16-12</td>
                        <td  class="smalltext">3/4-14</td>
                        <td  class="smalltext">1-3/16-12</td>
                        <td  class="smalltext">3/4-14</td>
                        <td  class="smalltext">1-1/16-12</td>
                        <td  class="smalltext">3/4-14</td>
                        <td  class="smalltext">M27 X 2.0</td>
                        <td  class="smalltext">3/4-14</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">14</td>
                        <td  class="smalltext" nowrap>7/8"</td>
                        <td  class="smalltext">1-3/16-12</td>
                        <td  class="smalltext">1-3/16-16</td>
                        <td  class="smalltext">1-3/16-12</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1-3/16-12</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">M30 X 2.0</td>
                        <td  class="smalltext"></td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">16</td>
                        <td  class="smalltext" nowrap>1"</td>
                        <td  class="smalltext">1-5/16-12</td>
                        <td  class="smalltext">1-5/16-16</td>
                        <td  class="smalltext">1-5/16-12</td>
                        <td  class="smalltext">1-11-1/2</td>
                        <td  class="smalltext">1-7/16-12</td>
                        <td  class="smalltext">1-11-12</td>
                        <td  class="smalltext">1-5/16-12</td>
                        <td  class="smalltext">1-11-1/2</td>
                        <td  class="smalltext">M33 X 2.0</td>
                        <td  class="smalltext">1-16</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">20</td>
                        <td  class="smalltext" nowrap>1-1/4"</td>
                        <td  class="smalltext">1-5/8-12</td>
                        <td  class="smalltext">1-5/8-16</td>
                        <td  class="smalltext">1-5/8-12</td>
                        <td  class="smalltext">1-1/4-11-1/2</td>
                        <td  class="smalltext">1-11/16-12</td>
                        <td  class="smalltext">1-1/4-11-1/2</td>
                        <td  class="smalltext">1-5/8-12</td>
                        <td  class="smalltext">1-1/4-11-1/2</td>
                        <td  class="smalltext">M42 X 2.0</td>
                        <td  class="smalltext">1-1/4-11</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">24</td>
                        <td  class="smalltext" nowrap>1-1/2"</td>
                        <td  class="smalltext">1-7/8-12</td>
                        <td  class="smalltext">1-7/8-16</td>
                        <td  class="smalltext">1-7/8-12</td>
                        <td  class="smalltext">1-1/2-11-1/2</td>
                        <td  class="smalltext">2-12</td>
                        <td  class="smalltext">1-1/2-11-1/2</td>
                        <td  class="smalltext">1-7/8-12</td>
                        <td  class="smalltext">1-1/2-11-1/2</td>
                        <td  class="smalltext">M48 X 2.0</td>
                        <td  class="smalltext">1-1/2-11</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">32</td>
                        <td  class="smalltext" nowrap>2"</td>
                        <td  class="smalltext">2-1/2-12</td>
                        <td  class="smalltext">2-1/2-12</td>
                        <td  class="smalltext">2-1/2-12</td>
                        <td  class="smalltext">2-11-1/2</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">2-11-1/2</td>
                        <td  class="smalltext">2-1/2-12</td>
                        <td  class="smalltext">2-11-1/2</td>
                        <td  class="smalltext">M60 X 2.0</td>
                        <td  class="smalltext">2-11</td>
                    </tr>

                </table>
            </div>
            <!--Table ending-->
        </div>

    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>