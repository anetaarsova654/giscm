<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Global Industry Standard CAD Models (GISCM)</title>

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

        <!-- //for-mobile-apps -->
        <link href="<?php echo asset('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo asset('css/bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo asset('css/style.css'); ?>" rel="stylesheet">
        <!-- animation-effect -->
        <link href="<?php echo asset('css/animate.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    </head>
    <body>
        <!-- banner -->
        <div class="banner">
            <div class="container">
                <div class="head-top wow fadeInLeft animated" data-wow-delay=".5s">
                    <div class="navigation">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <nav class="cl-effect-11">
                                    <ul class="nav navbar-nav navbar-style">
                                        <li><a href="<?php echo e(route('home')); ?>" data-hover="Home">Home</a></li>
                                        
                                        <li><a href="<?php echo e(route('members.index')); ?>" data-hover="Members">Members </a></li>
                                        
                                        <li><a href="<?php echo e(route('cadmodels.index')); ?>" data-hover="Community">Community</a></li>
                                        <li><a href="<?php echo e(route('cadmodels.create')); ?>" data-hover="Upload">Upload</a></li>
                                        <?php if(Auth::check()): ?>
                                            <li><a href="<?php echo e(route('dashboard')); ?>" data-hover="My Dashboard">My Dashboard</a></li>
                                        <?php else: ?>
                                            <li><a href="<?php echo e(route('login')); ?>" data-hover="Login">Login</a></li>
                                            <li><a href="<?php echo e(route('register')); ?>" data-hover="Sign Up">Sign Up</a></li>
                                        <?php endif; ?>
                                        <li><a href="<?php echo e(route('contactus')); ?>" data-hover="Contact">Contact</a></li>
                                        <?php if(Auth::check()): ?>
                                            <li>
                                                <a data-hover="Logout" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                                    <?php echo e(csrf_field()); ?>

                                                </form>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="logo wow fadeInRight animated" data-wow-delay=".5s">
                    <img src="/images/giscm.png" width="301" height="78" longdesc="/index.html"> </div>
                <h1 class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0"><p style="text-align:center">Students...Designers...Engineers...simplify your life™</p></h1>
                <p class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">Engineering Community</p>
            </div>
        </div>

        <!-- aboutus -->
        

        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-main">
                    <div class="col-md-3 footer-left wow fadeInLeft animated" data-wow-delay=".5s">
                        <h4>Information</h4>
                        <ul>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=2','policy2','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Disclaimer</a></li>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=5','policy5','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Privacy</a></li>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=7','policy7','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Terms &amp; Conditions</a></li>
                            <!-- <li><a href="#" onClick="MM_openBrWindow('Scripts/ContactUs.asp''scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Contact</a></li> -->
                            <li><a href="<?php echo e(route('contactus')); ?>">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-left wow fadeInLeft animated" data-wow-delay=".5s"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy">
            <div class="container">
                <p class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">&copy 2016-2017 GISCM Inc. GISCM is a registered trademark. All rights reserved.
                <div class="copy-rights animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">
                    <ul>
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><span class="fa"> </span></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><span class="tw"> </span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- //footer -->
        <!-- js -->
        <script src="<?php echo asset('js/jquery-1.12.4.min.js'); ?>"></script>
        <script src="<?php echo asset('js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo asset('js/wow.min.js'); ?>"></script>
        <script src="<?php echo asset('/js/slideshow.js'); ?>"></script>

    </body>
</html>
