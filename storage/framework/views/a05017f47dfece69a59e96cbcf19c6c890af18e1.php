<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>Global Industry Standard CAD Models (GISCM)</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Styles -->
    <link href="<?php echo asset('css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('css/style-custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- animation-effect -->
    <link href="<?php echo asset('css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <?php echo $__env->yieldContent('head'); ?>

</head>
<body>
    <div class="header-top22">
        <div class="wrap">
            <div class="header-top-left">
                <div class="box"></div>
                <div class="clearfix"></div>
            </div>
            <div class="cssmenu">
                <ul>
                 <?php if(auth()->guard()->guest()): ?>
                    <li class="active"><a href="<?php echo e(route('register')); ?>">Sign Up</a></li> |
                    <li><a href="<?php echo e(route('login')); ?>">Log In</a></li>
                 <?php else: ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 55px;">
                            <img src="<?php echo e(Storage::url(Auth::guard('web')->user()->getProfileImage())); ?>" style="width: 34px; height:34px;left: 10px; border-radius: 50%; ">
                            <?php echo e(Auth::user()->firstname.' '.Auth::user()->lastname); ?> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="text" href="<?php echo e(route('editprofile')); ?>">Edit Profile</a>
                            </li>
                            <li>
                                <a class="text" href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                 <?php endif; ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php echo $__env->make('partials.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container">
        <?php if(session()->has('message.level')): ?>
            <div class="alert alert-<?php echo e(session('message.level')); ?>">
                <?php echo session('message.content'); ?>

            </div>
        <?php endif; ?>
    </div>
    <div id="app">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <!-- js -->
    <script src="<?php echo asset('js/jquery-2.2.3.min.js'); ?>"></script>
    <script src="<?php echo asset('js/nav.js'); ?>"></script>
    <script src="<?php echo asset('js/bootstrap.js'); ?>"></script>
    <script src="<?php echo asset('js/wow.min.js'); ?>"></script>
    <script>
        new WOW().init();
    </script>
    <script src="<?php echo mix('js/app.js'); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>
</body>
</html>
