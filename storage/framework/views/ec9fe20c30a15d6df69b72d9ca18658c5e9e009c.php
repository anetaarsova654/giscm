<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="form-container">
                    <?php echo Form::model($cadmodel,['url' => route('cadmodels.update',$cadmodel->id),'files'=>true,'class'=>'form-horizontal','method'=>'patch']); ?>

                        <table border="0" cellpadding="0" cellspacing="0"  class="uploadform">
                        <tbody>
                        <tr>
                            <td colspan="2"><h3>Edit File</h3><br></td>
                        </tr>
                        <tr>
                            <td nowrap="">Category</td>
                            <td>
                                <select class="categories-dropdown" id="category-dropdown" name="category" size="1" required>
                                    <option value="" default>Select Category</option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="step2" >
                            <td nowrap="">File Name </td>
                            <td>
                                <?php echo e(Form::text('name' ,null, ['class' => 'step2-inputs','id'=>'name','required' => 'required','maxlength' => '70'])); ?>

                            </td>
                        </tr>
                        <tr class="step2" >
                            <td nowrap="">Price(US DOLLARS) </td>
                            <td>
                                <?php echo e(Form::text('price' ,null, ['class' => 'step2-inputs','id'=>'price','required' => 'required','maxlength' => '70'])); ?>

                            </td>
                        </tr>
                        <tr class="step2">
                            <td nowrap="">Description </td>
                            <td>
                                <?php echo e(Form::textarea('description' ,null, ['class' => 'form-textarea step2-inputs' ,'id'=>'description','rows'=>'4','required' => 'required'])); ?>

                            </td>
                        </tr>
                        <tr class="step3" >
                            <td nowrap="">File </td>
                            <td>
                                <?php echo e(Form::file('product_file' , ['class' => 'product_file','required' => 'required'])); ?>

                                <div class="hidden hint"><b>Hint:</b> Upload .STL or .STEP file only......!!!</div>
                            </td>
                        </tr>
                        <tr class="model-dropdown" >
                            <td nowrap="">Model Type</td>
                            <td>
                                <select class="categories-dropdown" name="model_type" size="1" required>
                                    <option value=""></option>
                                    <option value="One Comonent">One Comonent</option>
                                    <option value="Assembly">Assembly(more than one component)</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="step3">
                            <td nowrap="">Product Image </td>
                            <td>
                                <?php echo e(Form::file('image_url' ,null, ['required' => 'required'])); ?>

                            </td>
                        </tr>
                        <tr class="step3">
                            <td colspan="2"><input type="submit"  class="btn btn-primary" name="Submit" value="Update"></td>
                        </tr>
                        </tbody>
                    </table>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function(){

            $('.product_file').change(function(){
                var file = $(this).val();
                var ext = file.split('.').pop();

                if ( ext == 'STEP' || ext == 'STL')
                {
                    $('.model-dropdown').removeClass('hidden');
                    $(".hint").addClass('hidden');
                }
                else
                {

                    $(".model-dropdown").addClass('hidden');
                    $(".hint").removeClass('hidden');
                }
            });

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>