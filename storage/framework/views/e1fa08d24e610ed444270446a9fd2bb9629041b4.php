

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel"><a href="">Welcome visitor you can login or create an account.</a></p><br>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-9">
                <!-- Outer Table Cell -->
                <form class="form-horizontal login-form" method="POST" action="<?php echo e(route('login')); ?>">
                <table border="0" cellpadding="0" cellspacing="0" width="350">
                    <tbody>
                    <tr>
                        <td>
                            <!-- Inner Table -->
                            <table border="0" cellspacing="0" cellpadding="2" width="100%">
                                <tbody>
                                <tr>
                                    <td valign="middle" class="CPpageHead" width="49%">
                                        <b>Login</b>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                    <td valign="middle" class="CPpageHead" width="49%">
                                        <b>New Customer</b>
                                    </td>
                                </tr>
                                <tr>
                                    <?php echo e(csrf_field()); ?>

                                    <td valign="top">
                                    <br>If you bought from us before, please logon below.<br>
                                    <br>
                                    Email
                                    <br>
                                    <input type="text" name="email" size="20" maxlength="50" value="<?php echo e(old('email')); ?>" required><br>
                                        <?php if($errors->has('email')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                             </span>
                                        <?php endif; ?>
                                    Password<br>
                                    <input type="password" name="password" size="20" maxlength="25" required><br><br>
                                    <input class="loginbtn" type="submit" name="Submit" value="Log On"><br><br>
                                    <a href="<?php echo e(route('password.request')); ?>"><i>Forgot your Password?</i></a>
                                    </td>
                                    <td valign="top">&nbsp;</td>
                                    <td valign="top">
                                    <br>If this is your first visit to our store, please click the button below.<br><br>
                                    <a class="registerbtn" href="<?php echo e(route('register')); ?>">New Customer</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" class="CPpageHead" width="49%">
                                        <b>Log In with Facebook:</b>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                    <td valign="middle" width="49%"></td>
                                </tr>
                                <tr>
                                    <td valign="middle" width="49%">
                                        <fb:login-button autologoutlink="false" login_text="" class=" fb_iframe_widget" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=1222956894447184&amp;auto_logout_link=false&amp;container_width=0&amp;locale=en_US&amp;sdk=joey"><span style="vertical-align: bottom; width: 64px; height: 22px;"><iframe name="f1eab9adb08d4e" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:login_button Facebook Social Plugin" src="https://www.facebook.com/plugins/login_button.php?app_id=1222956894447184&amp;auto_logout_link=false&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F0sTQzbapM8j.js%3Fversion%3D42%23cb%3Df32c1feded1f7dc%26domain%3Dgiscm.net%26origin%3Dhttps%253A%252F%252Fgiscm.net%252Ff194257c08d237%26relation%3Dparent.parent&amp;container_width=0&amp;locale=en_US&amp;sdk=joey" style="border: none; visibility: visible; width: 64px; height: 22px;" class=""></iframe></span></fb:login-button>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Outer Table Cell -->
                        </td>
                    </tr>
                    </tbody>
                </table>
                </form>
                <br>
            </div>
        </div>
    </div>









<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>