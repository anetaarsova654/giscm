<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php echo e(route('threadSpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>0-80 UNF to &frac14;-56 UNS</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries2')); ?>">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries3')); ?>">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries5')); ?>">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries8')); ?>">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>


            </table>
        </div>

        &nbsp;<br />
        <div class="table-responsive">

        </div>
        <table class="table">

            <tr align="center" bgcolor="ccccbb">
                <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
            </tr>
            <tr align="center" bgcolor="cccc99">
                <td  valign="bottom" class="smalltext">&nbsp;</td>
                <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                <td  valign="bottom" class="smalltext"><b>Class</b></td>
                <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br><span class="red">Max Minor</span></b></td>
                <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                <td  valign="bottom" class="smalltext"><strong>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</strong></td>
                <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
            </tr>

            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>0-80 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0005</td>
                <td  class="smalltext">   0.0595</td>
                <td  class="smalltext">   0.0563</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0514</td>
                <td  class="smalltext">   0.0496</td>
                <td  class="smalltext">   0.0446</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0514</span></td>
                <td  class="smalltext"><span class="red">   0.0465</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0542</span></td>
                <td  class="smalltext"><span class="red">   0.0519</span></td>
                <td  class="smalltext"><span class="red">   0.0600</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>0-80 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0600</td>
                <td  class="smalltext">   0.0568</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0519</td>
                <td  class="smalltext">   0.0506</td>
                <td  class="smalltext">   0.0451</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0514</span></td>
                <td  class="smalltext"><span class="red">   0.0465</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0536</span></td>
                <td  class="smalltext"><span class="red">   0.0519</span></td>
                <td  class="smalltext"><span class="red">   0.0600</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>1-64 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0006</td>
                <td  class="smalltext">   0.0724</td>
                <td  class="smalltext">   0.0686</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0623</td>
                <td  class="smalltext">   0.0603</td>
                <td  class="smalltext">   0.0538</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0623</span></td>
                <td  class="smalltext"><span class="red">   0.0561</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0655</span></td>
                <td  class="smalltext"><span class="red">   0.0629</span></td>
                <td  class="smalltext"><span class="red">   0.0730</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>1-64 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0730</td>
                <td  class="smalltext">   0.0692</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0629</td>
                <td  class="smalltext">   0.0614</td>
                <td  class="smalltext">   0.0544</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0623</span></td>
                <td  class="smalltext"><span class="red">   0.0561</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0648</span></td>
                <td  class="smalltext"><span class="red">   0.0629</span></td>
                <td  class="smalltext"><span class="red">   0.0730</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>1-72 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0006</td>
                <td  class="smalltext">   0.0724</td>
                <td  class="smalltext">   0.0689</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0634</td>
                <td  class="smalltext">   0.0615</td>
                <td  class="smalltext">   0.0559</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0635</span></td>
                <td  class="smalltext"><span class="red">   0.0580</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0665</span></td>
                <td  class="smalltext"><span class="red">   0.0640</span></td>
                <td  class="smalltext"><span class="red">   0.0730</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>1-72 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0730</td>
                <td  class="smalltext">   0.0695</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0640</td>
                <td  class="smalltext">   0.0626</td>
                <td  class="smalltext">   0.0565</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0635</span></td>
                <td  class="smalltext"><span class="red">   0.0580</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0659</span></td>
                <td  class="smalltext"><span class="red">   0.0640</span></td>
                <td  class="smalltext"><span class="red">   0.0730</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>2-56 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0006</td>
                <td  class="smalltext">   0.0854</td>
                <td  class="smalltext">   0.0813</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0738</td>
                <td  class="smalltext">   0.0717</td>
                <td  class="smalltext">   0.0642</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0737</span></td>
                <td  class="smalltext"><span class="red">   0.0667</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0772</span></td>
                <td  class="smalltext"><span class="red">   0.0744</span></td>
                <td  class="smalltext"><span class="red">   0.0860</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>2-56 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0860</td>
                <td  class="smalltext">   0.0819</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0744</td>
                <td  class="smalltext">   0.0728</td>
                <td  class="smalltext">   0.0648</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0737</span></td>
                <td  class="smalltext"><span class="red">   0.0667</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0765</span></td>
                <td  class="smalltext"><span class="red">   0.0744</span></td>
                <td  class="smalltext"><span class="red">   0.0860</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>2-64 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0006</td>
                <td  class="smalltext">   0.0854</td>
                <td  class="smalltext">   0.0816</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0753</td>
                <td  class="smalltext">   0.0733</td>
                <td  class="smalltext">   0.0668</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0753</span></td>
                <td  class="smalltext"><span class="red">   0.0691</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0786</span></td>
                <td  class="smalltext"><span class="red">   0.0759</span></td>
                <td  class="smalltext"><span class="red">   0.0860</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>2-64 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0860</td>
                <td  class="smalltext">   0.0822</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0759</td>
                <td  class="smalltext">   0.0744</td>
                <td  class="smalltext">   0.0674</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0753</span></td>
                <td  class="smalltext"><span class="red">   0.0691</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0779</span></td>
                <td  class="smalltext"><span class="red">   0.0759</span></td>
                <td  class="smalltext"><span class="red">   0.0860</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>3-48 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.0983</td>
                <td  class="smalltext">   0.0938</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0848</td>
                <td  class="smalltext">   0.0825</td>
                <td  class="smalltext">   0.0734</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0845</span></td>
                <td  class="smalltext"><span class="red">   0.0764</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0885</span></td>
                <td  class="smalltext"><span class="red">   0.0855</span></td>
                <td  class="smalltext"><span class="red">   0.0990</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>3-48 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0990</td>
                <td  class="smalltext">   0.0945</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0855</td>
                <td  class="smalltext">   0.0838</td>
                <td  class="smalltext">   0.0741</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0845</span></td>
                <td  class="smalltext"><span class="red">   0.0764</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0877</span></td>
                <td  class="smalltext"><span class="red">   0.0855</span></td>
                <td  class="smalltext"><span class="red">   0.0990</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>3-56 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.0983</td>
                <td  class="smalltext">   0.0942</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0867</td>
                <td  class="smalltext">   0.0845</td>
                <td  class="smalltext">   0.0771</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0865</span></td>
                <td  class="smalltext"><span class="red">   0.0797</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0902</span></td>
                <td  class="smalltext"><span class="red">   0.0874</span></td>
                <td  class="smalltext"><span class="red">   0.0990</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>3-56 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.0990</td>
                <td  class="smalltext">   0.0949</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0874</td>
                <td  class="smalltext">   0.0858</td>
                <td  class="smalltext">   0.0778</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0865</span></td>
                <td  class="smalltext"><span class="red">   0.0797</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0895</span></td>
                <td  class="smalltext"><span class="red">   0.0874</span></td>
                <td  class="smalltext"><span class="red">   0.0990</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>4-40 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1112</td>
                <td  class="smalltext">   0.1061</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0950</td>
                <td  class="smalltext">   0.0925</td>
                <td  class="smalltext">   0.0814</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0939</span></td>
                <td  class="smalltext"><span class="red">   0.0849</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0991</span></td>
                <td  class="smalltext"><span class="red">   0.0958</span></td>
                <td  class="smalltext"><span class="red">   0.1120</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>4-40 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1120</td>
                <td  class="smalltext">   0.1069</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0958</td>
                <td  class="smalltext">   0.0939</td>
                <td  class="smalltext">   0.0822</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0939</span></td>
                <td  class="smalltext"><span class="red">   0.0849</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0982</span></td>
                <td  class="smalltext"><span class="red">   0.0958</span></td>
                <td  class="smalltext"><span class="red">   0.1120</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>4-48 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.1113</td>
                <td  class="smalltext">   0.1068</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0978</td>
                <td  class="smalltext">   0.0954</td>
                <td  class="smalltext">   0.0864</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0968</span></td>
                <td  class="smalltext"><span class="red">   0.0894</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1016</span></td>
                <td  class="smalltext"><span class="red">   0.0985</span></td>
                <td  class="smalltext"><span class="red">   0.1120</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>4-48 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1120</td>
                <td  class="smalltext">   0.1075</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.0985</td>
                <td  class="smalltext">   0.0967</td>
                <td  class="smalltext">   0.0871</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.0968</span></td>
                <td  class="smalltext"><span class="red">   0.0894</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1008</span></td>
                <td  class="smalltext"><span class="red">   0.0985</span></td>
                <td  class="smalltext"><span class="red">   0.1120</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>5-40 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1242</td>
                <td  class="smalltext">   0.1191</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1080</td>
                <td  class="smalltext">   0.1054</td>
                <td  class="smalltext">   0.0944</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1062</span></td>
                <td  class="smalltext"><span class="red">   0.0979</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1121</span></td>
                <td  class="smalltext"><span class="red">   0.1088</span></td>
                <td  class="smalltext"><span class="red">   0.1250</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>5-40 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1250</td>
                <td  class="smalltext">   0.1199</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1088</td>
                <td  class="smalltext">   0.1069</td>
                <td  class="smalltext">   0.0952</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1062</span></td>
                <td  class="smalltext"><span class="red">   0.0979</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1113</span></td>
                <td  class="smalltext"><span class="red">   0.1088</span></td>
                <td  class="smalltext"><span class="red">   0.1250</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>5-44 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.1243</td>
                <td  class="smalltext">   0.1195</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1095</td>
                <td  class="smalltext">   0.1070</td>
                <td  class="smalltext">   0.0972</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1079</span></td>
                <td  class="smalltext"><span class="red">   0.1004</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1134</span></td>
                <td  class="smalltext"><span class="red">   0.1102</span></td>
                <td  class="smalltext"><span class="red">   0.1250</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>5-44 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1250</td>
                <td  class="smalltext">   0.1202</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1102</td>
                <td  class="smalltext">   0.1083</td>
                <td  class="smalltext">   0.0979</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1079</span></td>
                <td  class="smalltext"><span class="red">   0.1004</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1126</span></td>
                <td  class="smalltext"><span class="red">   0.1102</span></td>
                <td  class="smalltext"><span class="red">   0.1250</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>6-32 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1372</td>
                <td  class="smalltext">   0.1312</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1169</td>
                <td  class="smalltext">   0.1141</td>
                <td  class="smalltext">   0.1000</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1140</span></td>
                <td  class="smalltext"><span class="red">   0.1040</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1214</span></td>
                <td  class="smalltext"><span class="red">   0.1177</span></td>
                <td  class="smalltext"><span class="red">   0.1380</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>6-32 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1380</td>
                <td  class="smalltext">   0.1320</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1177</td>
                <td  class="smalltext">   0.1156</td>
                <td  class="smalltext">   0.1008</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1140</span></td>
                <td  class="smalltext"><span class="red">   0.1040</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1204</span></td>
                <td  class="smalltext"><span class="red">   0.1177</span></td>
                <td  class="smalltext"><span class="red">   0.1380</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>6-40 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1372</td>
                <td  class="smalltext">   0.1321</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1210</td>
                <td  class="smalltext">   0.1184</td>
                <td  class="smalltext">   0.1074</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1190</span></td>
                <td  class="smalltext"><span class="red">   0.1110</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1252</span></td>
                <td  class="smalltext"><span class="red">   0.1218</span></td>
                <td  class="smalltext"><span class="red">   0.1380</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>6-40 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1380</td>
                <td  class="smalltext">   0.1329</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1218</td>
                <td  class="smalltext">   0.1198</td>
                <td  class="smalltext">   0.1082</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1186</span></td>
                <td  class="smalltext"><span class="red">   0.1110</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1243</span></td>
                <td  class="smalltext"><span class="red">   0.1218</span></td>
                <td  class="smalltext"><span class="red">   0.1380</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>8-32 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.1631</td>
                <td  class="smalltext">   0.1571</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1428</td>
                <td  class="smalltext">   0.1399</td>
                <td  class="smalltext">   0.1259</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1390</span></td>
                <td  class="smalltext"><span class="red">   0.1300</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1475</span></td>
                <td  class="smalltext"><span class="red">   0.1437</span></td>
                <td  class="smalltext"><span class="red">   0.1640</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>8-32 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1640</td>
                <td  class="smalltext">   0.1580</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1437</td>
                <td  class="smalltext">   0.1415</td>
                <td  class="smalltext">   0.1268</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1389</span></td>
                <td  class="smalltext"><span class="red">   0.1300</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1465</span></td>
                <td  class="smalltext"><span class="red">   0.1437</span></td>
                <td  class="smalltext"><span class="red">   0.1640</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>8-36 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1632</td>
                <td  class="smalltext">   0.1577</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1452</td>
                <td  class="smalltext">   0.1424</td>
                <td  class="smalltext">   0.1301</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1420</span></td>
                <td  class="smalltext"><span class="red">   0.1340</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1496</span></td>
                <td  class="smalltext"><span class="red">   0.1460</span></td>
                <td  class="smalltext"><span class="red">   0.1640</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>8-36 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1640</td>
                <td  class="smalltext">   0.1585</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1460</td>
                <td  class="smalltext">   0.1439</td>
                <td  class="smalltext">   0.1309</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1416</span></td>
                <td  class="smalltext"><span class="red">   0.1340</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1487</span></td>
                <td  class="smalltext"><span class="red">   0.1460</span></td>
                <td  class="smalltext"><span class="red">   0.1640</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-24 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.1890</td>
                <td  class="smalltext">   0.1818</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1619</td>
                <td  class="smalltext">   0.1586</td>
                <td  class="smalltext">   0.1394</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1560</span></td>
                <td  class="smalltext"><span class="red">   0.1450</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1672</span></td>
                <td  class="smalltext"><span class="red">   0.1629</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-24 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1900</td>
                <td  class="smalltext">   0.1828</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1629</td>
                <td  class="smalltext">   0.1604</td>
                <td  class="smalltext">   0.1404</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1555</span></td>
                <td  class="smalltext"><span class="red">   0.1450</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1661</span></td>
                <td  class="smalltext"><span class="red">   0.1629</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-28 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.1890</td>
                <td  class="smalltext">   0.1825</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1658</td>
                <td  class="smalltext">   0.1625</td>
                <td  class="smalltext">   0.1464</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1600</span></td>
                <td  class="smalltext"><span class="red">   0.1510</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1711</span></td>
                <td  class="smalltext"><span class="red">   0.1668</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-32UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.1891</td>
                <td  class="smalltext">   0.1831</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1688</td>
                <td  class="smalltext">   0.1658</td>
                <td  class="smalltext">   0.1519</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1640</span></td>
                <td  class="smalltext"><span class="red">   0.1560</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1736</span></td>
                <td  class="smalltext"><span class="red">   0.1697</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-32UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.1900</td>
                <td  class="smalltext">   0.1840</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1697</td>
                <td  class="smalltext">   0.1674</td>
                <td  class="smalltext">   0.1528</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1641</span></td>
                <td  class="smalltext"><span class="red">   0.1560</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1726</span></td>
                <td  class="smalltext"><span class="red">   0.1697</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-36 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.1891</td>
                <td  class="smalltext">   0.1836</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1711</td>
                <td  class="smalltext">   0.1681</td>
                <td  class="smalltext">   0.1560</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1660</span></td>
                <td  class="smalltext"><span class="red">   0.1600</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1759</span></td>
                <td  class="smalltext"><span class="red">   0.1720</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-40 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.1891</td>
                <td  class="smalltext">   0.1840</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1729</td>
                <td  class="smalltext">   0.1700</td>
                <td  class="smalltext">   0.1592</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1690</span></td>
                <td  class="smalltext"><span class="red">   0.1630</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1775</span></td>
                <td  class="smalltext"><span class="red">   0.1738</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-48 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.1892</td>
                <td  class="smalltext">   0.1847</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1757</td>
                <td  class="smalltext">   0.1731</td>
                <td  class="smalltext">   0.1644</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1720</span></td>
                <td  class="smalltext"><span class="red">   0.1670</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1799</span></td>
                <td  class="smalltext"><span class="red">   0.1765</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>10-56 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.1893</td>
                <td  class="smalltext">   0.1852</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1777</td>
                <td  class="smalltext">   0.1752</td>
                <td  class="smalltext">   0.1681</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1750</span></td>
                <td  class="smalltext"><span class="red">   0.1710</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1816</span></td>
                <td  class="smalltext"><span class="red">   0.1784</span></td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-24 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2150</td>
                <td  class="smalltext">   0.2078</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1879</td>
                <td  class="smalltext">   0.1845</td>
                <td  class="smalltext">   0.1654</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1810</span></td>
                <td  class="smalltext"><span class="red">   0.1710</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1933</span></td>
                <td  class="smalltext"><span class="red">   0.1889</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-24 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2160</td>
                <td  class="smalltext">   0.2088</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1889</td>
                <td  class="smalltext">   0.1863</td>
                <td  class="smalltext">   0.1664</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1807</span></td>
                <td  class="smalltext"><span class="red">   0.1710</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1922</span></td>
                <td  class="smalltext"><span class="red">   0.1889</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-28 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2150</td>
                <td  class="smalltext">   0.2085</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1918</td>
                <td  class="smalltext">   0.1886</td>
                <td  class="smalltext">   0.1724</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1860</span></td>
                <td  class="smalltext"><span class="red">   0.1770</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1970</span></td>
                <td  class="smalltext"><span class="red">   0.1928</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-28 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2160</td>
                <td  class="smalltext">   0.2095</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1928</td>
                <td  class="smalltext">   0.1904</td>
                <td  class="smalltext">   0.1734</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1857</span></td>
                <td  class="smalltext"><span class="red">   0.1770</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1959</span></td>
                <td  class="smalltext"><span class="red">   0.1928</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-32 UNEF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.2151</td>
                <td  class="smalltext">   0.2091</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1948</td>
                <td  class="smalltext">   0.1917</td>
                <td  class="smalltext">   0.1779</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1900</span></td>
                <td  class="smalltext"><span class="red">   0.1820</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1998</span></td>
                <td  class="smalltext"><span class="red">   0.1957</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-32 UNEF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2160</td>
                <td  class="smalltext">   0.2100</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1957</td>
                <td  class="smalltext">   0.1933</td>
                <td  class="smalltext">   0.1788</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1895</span></td>
                <td  class="smalltext"><span class="red">   0.1820</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1988</span></td>
                <td  class="smalltext"><span class="red">   0.1957</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-36 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.2151</td>
                <td  class="smalltext">   0.2096</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1971</td>
                <td  class="smalltext">   0.1941</td>
                <td  class="smalltext">   0.1821</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1920</span></td>
                <td  class="smalltext"><span class="red">   0.1860</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2019</span></td>
                <td  class="smalltext"><span class="red">   0.1980</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-40 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.2151</td>
                <td  class="smalltext">   0.2100</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.1989</td>
                <td  class="smalltext">   0.1960</td>
                <td  class="smalltext">   0.1835</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1950</span></td>
                <td  class="smalltext"><span class="red">   0.1890</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2035</span></td>
                <td  class="smalltext"><span class="red">   0.1998</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-48 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.2152</td>
                <td  class="smalltext">   0.2107</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2017</td>
                <td  class="smalltext">   0.1991</td>
                <td  class="smalltext">   0.1904</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.1980</span></td>
                <td  class="smalltext"><span class="red">   0.1930</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2059</span></td>
                <td  class="smalltext"><span class="red">   0.2025</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap>12-56 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0007</td>
                <td  class="smalltext">   0.2153</td>
                <td  class="smalltext">   0.2112</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2037</td>
                <td  class="smalltext">   0.2012</td>
                <td  class="smalltext">   0.1941</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2010</span></td>
                <td  class="smalltext"><span class="red">   0.1970</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2076</span></td>
                <td  class="smalltext"><span class="red">   0.2044</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-20 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                <td  class="smalltext">   0.0011</td>
                <td  class="smalltext">   0.2489</td>
                <td  class="smalltext">   0.2367</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2164</td>
                <td  class="smalltext">   0.2108</td>
                <td  class="smalltext">   0.1894</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2070</span></td>
                <td  class="smalltext"><span class="red">   0.1960</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2248</span></td>
                <td  class="smalltext"><span class="red">   0.2175</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-20 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0011</td>
                <td  class="smalltext">   0.2489</td>
                <td  class="smalltext">   0.2408</td>
                <td  class="smalltext">0.2367</td>
                <td  class="smalltext">   0.2164</td>
                <td  class="smalltext">   0.2127</td>
                <td  class="smalltext">   0.1894</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2070</span></td>
                <td  class="smalltext"><span class="red">   0.1960</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2224</span></td>
                <td  class="smalltext"><span class="red">   0.2175</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-20 UNC</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2500</td>
                <td  class="smalltext">   0.2419</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2175</td>
                <td  class="smalltext">   0.2147</td>
                <td  class="smalltext">   0.1905</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2067</span></td>
                <td  class="smalltext"><span class="red">   0.1960</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2211</span></td>
                <td  class="smalltext"><span class="red">   0.2175</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-24 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0011</td>
                <td  class="smalltext">   0.2489</td>
                <td  class="smalltext">   0.2417</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2218</td>
                <td  class="smalltext">   0.2181</td>
                <td  class="smalltext">   0.1993</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2150</span></td>
                <td  class="smalltext"><span class="red">   0.2050</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2277</span></td>
                <td  class="smalltext"><span class="red">   0.2229</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-27 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2490</td>
                <td  class="smalltext">   0.2423</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2249</td>
                <td  class="smalltext">   0.2214</td>
                <td  class="smalltext">   0.2049</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2190</span></td>
                <td  class="smalltext"><span class="red">   0.2100</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2304</span></td>
                <td  class="smalltext"><span class="red">   0.2259</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-28 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2490</td>
                <td  class="smalltext">   0.2392</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2258</td>
                <td  class="smalltext">   0.2208</td>
                <td  class="smalltext">   0.2064</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2200</span></td>
                <td  class="smalltext"><span class="red">   0.2110</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2333</span></td>
                <td  class="smalltext"><span class="red">   0.2268</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-28 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2490</td>
                <td  class="smalltext">   0.2425</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2258</td>
                <td  class="smalltext">   0.2225</td>
                <td  class="smalltext">   0.2064</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2200</span></td>
                <td  class="smalltext"><span class="red">   0.2110</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2311</span></td>
                <td  class="smalltext"><span class="red">   0.2268</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-28 UNF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2500</td>
                <td  class="smalltext">   0.2435</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2268</td>
                <td  class="smalltext">   0.2243</td>
                <td  class="smalltext">   0.2074</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2190</span></td>
                <td  class="smalltext"><span class="red">   0.2110</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2300</span></td>
                <td  class="smalltext"><span class="red">   0.2268</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-32 UNEF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0010</td>
                <td  class="smalltext">   0.2490</td>
                <td  class="smalltext">   0.2430</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2287</td>
                <td  class="smalltext">   0.2255</td>
                <td  class="smalltext">   0.2118</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2240</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2339</span></td>
                <td  class="smalltext"><span class="red">   0.2297</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-32 UNEF</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                <td  class="smalltext">   0.0000</td>
                <td  class="smalltext">   0.2500</td>
                <td  class="smalltext">   0.2440</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2297</td>
                <td  class="smalltext">   0.2273</td>
                <td  class="smalltext">   0.2128</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2229</span></td>
                <td  class="smalltext"><span class="red">   0.2160</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2328</span></td>
                <td  class="smalltext"><span class="red">   0.2297</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-36 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.2491</td>
                <td  class="smalltext">   0.2436</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2311</td>
                <td  class="smalltext">   0.2280</td>
                <td  class="smalltext">   0.2161</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2260</span></td>
                <td  class="smalltext"><span class="red">   0.2200</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2360</span></td>
                <td  class="smalltext"><span class="red">   0.2320</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-40 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0009</td>
                <td  class="smalltext">   0.2491</td>
                <td  class="smalltext">   0.2440</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2329</td>
                <td  class="smalltext">   0.2300</td>
                <td  class="smalltext">   0.2193</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2290</span></td>
                <td  class="smalltext"><span class="red">   0.2230</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2376</span></td>
                <td  class="smalltext"><span class="red">   0.2338</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-48 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.2492</td>
                <td  class="smalltext">   0.2447</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2357</td>
                <td  class="smalltext">   0.2330</td>
                <td  class="smalltext">   0.2243</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2320</span></td>
                <td  class="smalltext"><span class="red">   0.2270</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2401</span></td>
                <td  class="smalltext"><span class="red">   0.2365</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>


            <tr  align="center" bgcolor="eeeeee">
                <td  class="smalltext">Ext.</td>
                <td  class="smalltext" nowrap><sup>1</sup>/<sub>4</sub>-56 UNS</td>
                <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                <td  class="smalltext">   0.0008</td>
                <td  class="smalltext">   0.2492</td>
                <td  class="smalltext">   0.2451</td>
                <td  class="smalltext">-</td>
                <td  class="smalltext">   0.2376</td>
                <td  class="smalltext">   0.2350</td>
                <td  class="smalltext">   0.2280</td>
            </tr>

            <tr  align="center" bgcolor="eeeee0">
                <td  class="smalltext"><span class="red">Int.</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2350</span></td>
                <td  class="smalltext"><span class="red">   0.2310</span></td>
                <td  class="smalltext">&nbsp;</td>
                <td  class="smalltext"><span class="red">   0.2417</span></td>
                <td  class="smalltext"><span class="red">   0.2384</span></td>
                <td  class="smalltext"><span class="red">   0.2500</span></td>

            </tr>
        </table>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>