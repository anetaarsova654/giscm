<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>
            Edit Profile
            <small>Control panel</small>
        </h1>
    </section>
    <div class="content admin-edit-profile">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success" role="alert" style="font-size: medium; text-align: center;">
                        <?php echo e(Session::get('flash_message')); ?>

                        <?php echo e(Session::forget('flash_message')); ?>

                    </div>
                <?php endif; ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Profile</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <?php echo Form::model($admin,['url' => route('adminprofile.update',$admin->id),'class'=>'form-horizontal','method'=>'patch']); ?>

                        <div class="form-group form-inputs">
                            <?php echo e(Form::label('name', 'Full Name')); ?>

                            <?php echo e(Form::text('name' ,null, ['class' => 'form-control','id'=>'name','required' => 'required'])); ?>

                        </div>
                        <div class="form-group form-inputs">
                            <?php echo e(Form::label('email', 'Email')); ?>

                            <?php echo e(Form::email('email' ,null, ['class' => 'form-control','id'=>'email','required' => 'required'])); ?>

                        </div>
                        <div class="form-group form-inputs">
                            <?php echo e(Form::label('password', 'Password')); ?>

                            <?php echo e(Form::text('password' ,null, ['class' => 'form-control','id'=>'password','required' => 'required'])); ?>

                        </div>
                        <input type="submit" class="btn btn-primary" value="Update"></input>
                        <?php echo Form::close(); ?>

                    </div>
                        <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>