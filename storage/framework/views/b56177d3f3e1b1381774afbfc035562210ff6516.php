<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container comment-form-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <h3>Add Comment for <span><?php echo e($cadmodel->name); ?></span></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <form class="form-horizontal" method="POST" action="<?php echo e(route('cadmodels.comments.store',$cadmodel->id)); ?>" >
                    <?php echo e(csrf_field()); ?>

                <div class="form-inputs">
                    <div class="form-group">
                        <label for="stars"><span>*</span>Select a Rating:</label>
                        <div class="stars-container" id="stars">
                            <input class="star star-1" id="star-1" type="radio" value="5" name="star" required>
                            <label class="star star-1" for="star-1"></label>
                            <input class="star star-2" id="star-2" type="radio" value="4" name="star">
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-3" id="star-3" type="radio" value="3" name="star">
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-4" id="star-4" type="radio" value="2" name="star">
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-5" id="star-5" type="radio" value="1" name="star">
                            <label class="star star-5" for="star-5"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="summary"><span>*</span>One Line Summary:</label>
                        <input type="text" name="summary" class="form-control" id="summary" required>
                    </div>
                    <div class="form-group">
                        <label for="comment"><span>*</span>Comment:</label>
                        <textarea type="text" name="comment" class="form-control" id="comment" row="6" required></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="Submit" class="btn btn-primary"  value="Submit" id="btnSubmit">
                        <a href="<?php echo e(url()->previous()); ?>" name="Cancel"  class="btn btn-primary" value="Cancel" id="btnCancel">Cancel</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>