<?php $__env->startSection('content'); ?>
    <div class="container cadmodel-content">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel">
                    <a href="">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <form action="" method="post" id="searchBox" name="searchBox">
                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                <?php echo $__env->make('cadmodels.partials._categories', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-md-9">
                <p align="center"><font size="3" color="#ff0000"></font></p>
                <div class="top-in-single">
                    <h3 class="future"></h3>
                    <div class="col-md-5 single-top">
                        <?php if(Session::has('flash_message')): ?>
                            <div class="alert alert-success" role="alert" style="font-size: medium; text-align: center;">
                                <?php echo e(Session::get('flash_message')); ?>

                                <?php echo e(Session::forget('flash_message')); ?>

                            </div>
                        <?php endif; ?>
                        <?php echo Form::open(['url' => route('cadmodels.share'),'class'=>'form-horizontal','method'=>'POST']); ?>

                        <?php echo e(csrf_field()); ?>

                        <div class="form-inputs">
                            <div class="form-group">
                                <?php echo e(Form::label('yourname', 'Your Name')); ?>

                                <?php echo e(Form::text('yourname' ,null, ['class' => 'form-control','id'=>'yourname','required' => 'required'])); ?>

                            </div>
                            <div class="form-group">
                                <?php echo e(Form::label('friendemail', 'Friend Email')); ?>

                                <?php echo e(Form::email('friendemail' ,null, ['class' => 'form-control','id'=>'friendemail','required' => 'required'])); ?>

                            </div>
                            <div class="form-group">
                                <?php echo e(Form::label('subject', 'Subject')); ?>

                                <?php echo e(Form::text('subject' ,null, ['class' => 'form-control','id'=>'subject','required' => 'required'])); ?>

                            </div>
                            <div class="form-group">
                                <?php echo e(Form::label('messagetext', 'Message')); ?>

                                <?php echo e(Form::textarea('messagetext' ,null, ['class' => 'form-control' ,'id'=>'messagetext','rows'=>'6'])); ?>

                            </div>
                        </div>
                        <div class="form-submit">
                            <input type="submit" class="btn btn-primary btnsubmit" value="Send">
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>
    <script>

            $(document).ready(function(){
                var uri = window.location.toString();
                var clean_uri;
                if (uri.indexOf("/emailfriend") > 0) {
                    clean_uri = uri.substring(0, uri.indexOf("/emailfriend"));
                }

                var content = "Hi, GISCM.Inc has item that I thought you would really like to know about.\n\n<?php echo e($cadmodel->name); ?>\n\n" + "Check this link to see more info:\n\n" +  clean_uri ;
                $("#messagetext").val(content);
            });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>