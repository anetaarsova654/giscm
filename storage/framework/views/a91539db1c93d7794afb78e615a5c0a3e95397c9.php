<?php $__env->startSection('content'); ?>
    <div class="container contributors-content">
        <div class="row">
            <div class="contributor-page-header">
                <h2>Members' Directory on GISCM</h2>
               
            </div>
            <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-sm-3">
                    <div class="profile-thumbnail">
                        <div class="media">
                            <div class="media-left">
                                <img alt="User Image" src="<?php echo e(Storage::url($user->getProfileImage())); ?>" class="media-object" >
                            </div>
                            <div class="media-body">
                                <div class="thumbnail-detail"><a href="<?php echo e(route('members.show',[$user->id])); ?>"><h5 class="media-heading"><?php echo e($user->firstname." ".$user->lastname); ?></h5></a></div>
                                <div class="thumbnail-detail">
                                    <h5 class="media-heading">Contributions:</h5>
                                    <a href="<?php echo e(route('members.show',[$user->id])); ?>"><?php echo e(count($user->cadmodels)); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <p class="no-contributors-text">No Members Available</p>
            <?php endif; ?>
        </div>
        <?php echo e($users->appends(request()->except('page'))->links()); ?>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>