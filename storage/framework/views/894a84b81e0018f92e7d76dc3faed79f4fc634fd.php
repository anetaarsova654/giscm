<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="<?php echo e(route('threadSpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries1')); ?>">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries2')); ?>">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries3')); ?>">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>

                        15/16-12 UN to 1 3/16-28 UN
                    </td>
                    <td class="footnotetext">
                        <a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>1 1/4-7 UNC to 1 9/16-20 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries8')); ?>">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table">
                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br><span class="red">Max Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.2478</td>
                    <td  class="smalltext">   1.2232</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1550</td>
                    <td  class="smalltext">   1.1439</td>
                    <td  class="smalltext">   1.0777</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1230</span></td>
                    <td  class="smalltext"><span class="red">   1.0950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1716</span></td>
                    <td  class="smalltext"><span class="red">   1.1572</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.2478</td>
                    <td  class="smalltext">   1.2314</td>
                    <td  class="smalltext">1.2232</td>
                    <td  class="smalltext">   1.1550</td>
                    <td  class="smalltext">   1.1476</td>
                    <td  class="smalltext">   1.0777</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1230</span></td>
                    <td  class="smalltext"><span class="red">   1.0950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1668</span></td>
                    <td  class="smalltext"><span class="red">   1.1572</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2336</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1572</td>
                    <td  class="smalltext">   1.1517</td>
                    <td  class="smalltext">   1.0799</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1125</span></td>
                    <td  class="smalltext"><span class="red">   1.0950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1644</span></td>
                    <td  class="smalltext"><span class="red">   1.1572</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   1.2479</td>
                    <td  class="smalltext">   1.2329</td>
                    <td  class="smalltext">1.2254</td>
                    <td  class="smalltext">   1.1667</td>
                    <td  class="smalltext">   1.1597</td>
                    <td  class="smalltext">   1.0991</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1400</span></td>
                    <td  class="smalltext"><span class="red">   1.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1780</span></td>
                    <td  class="smalltext"><span class="red">   1.1688</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1688</td>
                    <td  class="smalltext">   1.1635</td>
                    <td  class="smalltext">   1.1012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1297</span></td>
                    <td  class="smalltext"><span class="red">   1.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1757</span></td>
                    <td  class="smalltext"><span class="red">   1.1688</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.2481</td>
                    <td  class="smalltext">   1.2352</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1831</td>
                    <td  class="smalltext">   1.1768</td>
                    <td  class="smalltext">   1.1291</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1630</span></td>
                    <td  class="smalltext"><span class="red">   1.1420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1932</span></td>
                    <td  class="smalltext"><span class="red">   1.1850</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.2482</td>
                    <td  class="smalltext">   1.2310</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1941</td>
                    <td  class="smalltext">   1.1849</td>
                    <td  class="smalltext">   1.1490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1780</span></td>
                    <td  class="smalltext"><span class="red">   1.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2079</span></td>
                    <td  class="smalltext"><span class="red">   1.1959</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.2482</td>
                    <td  class="smalltext">   1.2368</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1941</td>
                    <td  class="smalltext">   1.1879</td>
                    <td  class="smalltext">   1.1490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1780</span></td>
                    <td  class="smalltext"><span class="red">   1.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2039</span></td>
                    <td  class="smalltext"><span class="red">   1.1959</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1959</td>
                    <td  class="smalltext">   1.1913</td>
                    <td  class="smalltext">   1.1508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1698</span></td>
                    <td  class="smalltext"><span class="red">   1.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2019</span></td>
                    <td  class="smalltext"><span class="red">   1.1959</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.2484</td>
                    <td  class="smalltext">   1.2381</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2020</td>
                    <td  class="smalltext">   1.1966</td>
                    <td  class="smalltext">   1.1634</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1880</span></td>
                    <td  class="smalltext"><span class="red">   1.1730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2106</span></td>
                    <td  class="smalltext"><span class="red">   1.2036</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.2485</td>
                    <td  class="smalltext">   1.2391</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2079</td>
                    <td  class="smalltext">   1.2028</td>
                    <td  class="smalltext">   1.1740</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1960</span></td>
                    <td  class="smalltext"><span class="red">   1.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2160</span></td>
                    <td  class="smalltext"><span class="red">   1.2094</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2094</td>
                    <td  class="smalltext">   1.2056</td>
                    <td  class="smalltext">   1.1755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1908</span></td>
                    <td  class="smalltext"><span class="red">   1.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2144</span></td>
                    <td  class="smalltext"><span class="red">   1.2094</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.2485</td>
                    <td  class="smalltext">   1.2398</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2124</td>
                    <td  class="smalltext">   1.2075</td>
                    <td  class="smalltext">   1.1824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2030</span></td>
                    <td  class="smalltext"><span class="red">   1.1900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2202</span></td>
                    <td  class="smalltext"><span class="red">   1.2139</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2413</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2139</td>
                    <td  class="smalltext">   1.2103</td>
                    <td  class="smalltext">   1.1839</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1980</span></td>
                    <td  class="smalltext"><span class="red">   1.1900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2186</span></td>
                    <td  class="smalltext"><span class="red">   1.2139</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.2486</td>
                    <td  class="smalltext">   1.2405</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2161</td>
                    <td  class="smalltext">   1.2114</td>
                    <td  class="smalltext">   1.1891</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2070</span></td>
                    <td  class="smalltext"><span class="red">   1.1960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2236</span></td>
                    <td  class="smalltext"><span class="red">   1.2175</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2175</td>
                    <td  class="smalltext">   1.2140</td>
                    <td  class="smalltext">   1.1905</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2037</span></td>
                    <td  class="smalltext"><span class="red">   1.1960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2220</span></td>
                    <td  class="smalltext"><span class="red">   1.2175</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.2487</td>
                    <td  class="smalltext">   1.2415</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2216</td>
                    <td  class="smalltext">   1.2173</td>
                    <td  class="smalltext">   1.1991</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2150</span></td>
                    <td  class="smalltext"><span class="red">   1.2050</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2285</span></td>
                    <td  class="smalltext"><span class="red">   1.2229</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.2488</td>
                    <td  class="smalltext">   1.2423</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2256</td>
                    <td  class="smalltext">   1.2215</td>
                    <td  class="smalltext">   1.2062</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2200</span></td>
                    <td  class="smalltext"><span class="red">   1.2110</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2321</span></td>
                    <td  class="smalltext"><span class="red">   1.2268</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>4</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.2500</td>
                    <td  class="smalltext">   1.2435</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2268</td>
                    <td  class="smalltext">   1.2237</td>
                    <td  class="smalltext">   1.2074</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2176</span></td>
                    <td  class="smalltext"><span class="red">   1.2110</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2308</span></td>
                    <td  class="smalltext"><span class="red">   1.2268</span></td>
                    <td  class="smalltext"><span class="red">   1.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   1.3104</td>
                    <td  class="smalltext">   1.2954</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2292</td>
                    <td  class="smalltext">   1.2221</td>
                    <td  class="smalltext">   1.1616</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2020</span></td>
                    <td  class="smalltext"><span class="red">   1.1770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2405</span></td>
                    <td  class="smalltext"><span class="red">   1.2313</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.2975</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2313</td>
                    <td  class="smalltext">   1.2260</td>
                    <td  class="smalltext">   1.1637</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1922</span></td>
                    <td  class="smalltext"><span class="red">   1.1770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2382</span></td>
                    <td  class="smalltext"><span class="red">   1.2313</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.3108</td>
                    <td  class="smalltext">   1.2994</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2567</td>
                    <td  class="smalltext">   1.2509</td>
                    <td  class="smalltext">   1.2116</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2400</span></td>
                    <td  class="smalltext"><span class="red">   1.2220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2659</span></td>
                    <td  class="smalltext"><span class="red">   1.2584</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.3011</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2584</td>
                    <td  class="smalltext">   1.2541</td>
                    <td  class="smalltext">   1.2133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2323</span></td>
                    <td  class="smalltext"><span class="red">   1.2220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2640</span></td>
                    <td  class="smalltext"><span class="red">   1.2584</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.3110</td>
                    <td  class="smalltext">   1.3016</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2704</td>
                    <td  class="smalltext">   1.2653</td>
                    <td  class="smalltext">   1.2365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2590</span></td>
                    <td  class="smalltext"><span class="red">   1.2450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2785</span></td>
                    <td  class="smalltext"><span class="red">   1.2719</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.3031</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2719</td>
                    <td  class="smalltext">   1.2681</td>
                    <td  class="smalltext">   1.2380</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2533</span></td>
                    <td  class="smalltext"><span class="red">   1.2450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2769</span></td>
                    <td  class="smalltext"><span class="red">   1.2719</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.3110</td>
                    <td  class="smalltext">   1.3023</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2749</td>
                    <td  class="smalltext">   1.2700</td>
                    <td  class="smalltext">   1.2449</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2650</span></td>
                    <td  class="smalltext"><span class="red">   1.2520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2827</span></td>
                    <td  class="smalltext"><span class="red">   1.2764</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.3038</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2764</td>
                    <td  class="smalltext">   1.2728</td>
                    <td  class="smalltext">   1.2464</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2605</span></td>
                    <td  class="smalltext"><span class="red">   1.2520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2811</span></td>
                    <td  class="smalltext"><span class="red">   1.2764</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.3111</td>
                    <td  class="smalltext">   1.3030</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2786</td>
                    <td  class="smalltext">   1.2739</td>
                    <td  class="smalltext">   1.2516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2700</span></td>
                    <td  class="smalltext"><span class="red">   1.2580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2861</span></td>
                    <td  class="smalltext"><span class="red">   1.2800</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.3044</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2800</td>
                    <td  class="smalltext">   1.2765</td>
                    <td  class="smalltext">   1.2530</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2662</span></td>
                    <td  class="smalltext"><span class="red">   1.2580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2845</span></td>
                    <td  class="smalltext"><span class="red">   1.2800</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.3113</td>
                    <td  class="smalltext">   1.3048</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2881</td>
                    <td  class="smalltext">   1.2840</td>
                    <td  class="smalltext">   1.2687</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2820</span></td>
                    <td  class="smalltext"><span class="red">   1.2740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2946</span></td>
                    <td  class="smalltext"><span class="red">   1.2893</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3125</td>
                    <td  class="smalltext">   1.3060</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2893</td>
                    <td  class="smalltext">   1.2862</td>
                    <td  class="smalltext">   1.2699</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2801</span></td>
                    <td  class="smalltext"><span class="red">   1.2740</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2933</span></td>
                    <td  class="smalltext"><span class="red">   1.2893</span></td>
                    <td  class="smalltext"><span class="red">   1.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.3726</td>
                    <td  class="smalltext">   1.3453</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2643</td>
                    <td  class="smalltext">   1.2523</td>
                    <td  class="smalltext">   1.1742</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2250</span></td>
                    <td  class="smalltext"><span class="red">   1.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2822</span></td>
                    <td  class="smalltext"><span class="red">   1.2667</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.3726</td>
                    <td  class="smalltext">   1.3544</td>
                    <td  class="smalltext">1.3453</td>
                    <td  class="smalltext">   1.2643</td>
                    <td  class="smalltext">   1.2563</td>
                    <td  class="smalltext">   1.1742</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2250</span></td>
                    <td  class="smalltext"><span class="red">   1.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2771</span></td>
                    <td  class="smalltext"><span class="red">   1.2667</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2667</td>
                    <td  class="smalltext">   1.2607</td>
                    <td  class="smalltext">   1.1766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2146</span></td>
                    <td  class="smalltext"><span class="red">   1.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2745</span></td>
                    <td  class="smalltext"><span class="red">   1.2667</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.3728</td>
                    <td  class="smalltext">   1.3578</td>
                    <td  class="smalltext">1.3503</td>
                    <td  class="smalltext">   1.2916</td>
                    <td  class="smalltext">   1.2844</td>
                    <td  class="smalltext">   1.2240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2650</span></td>
                    <td  class="smalltext"><span class="red">   1.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3031</span></td>
                    <td  class="smalltext"><span class="red">   1.2938</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.2938</td>
                    <td  class="smalltext">   1.2884</td>
                    <td  class="smalltext">   1.2262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2547</span></td>
                    <td  class="smalltext"><span class="red">   1.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3008</span></td>
                    <td  class="smalltext"><span class="red">   1.2938</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.3731</td>
                    <td  class="smalltext">   1.3602</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3081</td>
                    <td  class="smalltext">   1.3018</td>
                    <td  class="smalltext">   1.2541</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2880</span></td>
                    <td  class="smalltext"><span class="red">   1.2670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3182</span></td>
                    <td  class="smalltext"><span class="red">   1.3100</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.3731</td>
                    <td  class="smalltext">   1.3559</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3190</td>
                    <td  class="smalltext">   1.3096</td>
                    <td  class="smalltext">   1.2739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3030</span></td>
                    <td  class="smalltext"><span class="red">   1.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3332</span></td>
                    <td  class="smalltext"><span class="red">   1.3209</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.3731</td>
                    <td  class="smalltext">   1.3617</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3190</td>
                    <td  class="smalltext">   1.3127</td>
                    <td  class="smalltext">   1.2739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3030</span></td>
                    <td  class="smalltext"><span class="red">   1.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3291</span></td>
                    <td  class="smalltext"><span class="red">   1.3209</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3209</td>
                    <td  class="smalltext">   1.3162</td>
                    <td  class="smalltext">   1.2758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2948</span></td>
                    <td  class="smalltext"><span class="red">   1.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3270</span></td>
                    <td  class="smalltext"><span class="red">   1.3209</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.3734</td>
                    <td  class="smalltext">   1.3631</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3270</td>
                    <td  class="smalltext">   1.3216</td>
                    <td  class="smalltext">   1.2884</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3140</span></td>
                    <td  class="smalltext"><span class="red">   1.2980</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3356</span></td>
                    <td  class="smalltext"><span class="red">   1.3286</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.3735</td>
                    <td  class="smalltext">   1.3641</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3329</td>
                    <td  class="smalltext">   1.3278</td>
                    <td  class="smalltext">   1.2990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3210</span></td>
                    <td  class="smalltext"><span class="red">   1.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3410</span></td>
                    <td  class="smalltext"><span class="red">   1.3344</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3344</td>
                    <td  class="smalltext">   1.3306</td>
                    <td  class="smalltext">   1.3005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3158</span></td>
                    <td  class="smalltext"><span class="red">   1.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3394</span></td>
                    <td  class="smalltext"><span class="red">   1.3344</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.3735</td>
                    <td  class="smalltext">   1.3648</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3374</td>
                    <td  class="smalltext">   1.3325</td>
                    <td  class="smalltext">   1.3074</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3280</span></td>
                    <td  class="smalltext"><span class="red">   1.3150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3452</span></td>
                    <td  class="smalltext"><span class="red">   1.3389</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3663</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3389</td>
                    <td  class="smalltext">   1.3353</td>
                    <td  class="smalltext">   1.3089</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3230</span></td>
                    <td  class="smalltext"><span class="red">   1.3150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3436</span></td>
                    <td  class="smalltext"><span class="red">   1.3389</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.3736</td>
                    <td  class="smalltext">   1.3655</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3411</td>
                    <td  class="smalltext">   1.3364</td>
                    <td  class="smalltext">   1.3141</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3320</span></td>
                    <td  class="smalltext"><span class="red">   1.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3486</span></td>
                    <td  class="smalltext"><span class="red">   1.3425</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3425</td>
                    <td  class="smalltext">   1.3390</td>
                    <td  class="smalltext">   1.3155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3287</span></td>
                    <td  class="smalltext"><span class="red">   1.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3470</span></td>
                    <td  class="smalltext"><span class="red">   1.3425</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.3737</td>
                    <td  class="smalltext">   1.3665</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3466</td>
                    <td  class="smalltext">   1.3423</td>
                    <td  class="smalltext">   1.3241</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3400</span></td>
                    <td  class="smalltext"><span class="red">   1.3300</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3535</span></td>
                    <td  class="smalltext"><span class="red">   1.3479</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.3738</td>
                    <td  class="smalltext">   1.3673</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3506</td>
                    <td  class="smalltext">   1.3465</td>
                    <td  class="smalltext">   1.3312</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3450</span></td>
                    <td  class="smalltext"><span class="red">   1.3360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3571</span></td>
                    <td  class="smalltext"><span class="red">   1.3518</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.3750</td>
                    <td  class="smalltext">   1.3685</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3518</td>
                    <td  class="smalltext">   1.3487</td>
                    <td  class="smalltext">   1.3324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3426</span></td>
                    <td  class="smalltext"><span class="red">   1.3360</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3558</span></td>
                    <td  class="smalltext"><span class="red">   1.3518</span></td>
                    <td  class="smalltext"><span class="red">   1.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.4351</td>
                    <td  class="smalltext">   1.4169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3268</td>
                    <td  class="smalltext">   1.3188</td>
                    <td  class="smalltext">   1.2367</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2880</span></td>
                    <td  class="smalltext"><span class="red">   1.2570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3396</span></td>
                    <td  class="smalltext"><span class="red">   1.3292</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4193</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3292</td>
                    <td  class="smalltext">   1.3232</td>
                    <td  class="smalltext">   1.2391</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.2771</span></td>
                    <td  class="smalltext"><span class="red">   1.2570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3370</span></td>
                    <td  class="smalltext"><span class="red">   1.3292</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.4353</td>
                    <td  class="smalltext">   1.4203</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3541</td>
                    <td  class="smalltext">   1.3469</td>
                    <td  class="smalltext">   1.2865</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3270</span></td>
                    <td  class="smalltext"><span class="red">   1.3020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3657</span></td>
                    <td  class="smalltext"><span class="red">   1.3563</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4225</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3563</td>
                    <td  class="smalltext">   1.3509</td>
                    <td  class="smalltext">   1.2887</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3172</span></td>
                    <td  class="smalltext"><span class="red">   1.3020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3634</span></td>
                    <td  class="smalltext"><span class="red">   1.3563</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.4357</td>
                    <td  class="smalltext">   1.4243</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3816</td>
                    <td  class="smalltext">   1.3757</td>
                    <td  class="smalltext">   1.3365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3650</span></td>
                    <td  class="smalltext"><span class="red">   1.3470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3910</span></td>
                    <td  class="smalltext"><span class="red">   1.3834</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4261</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3834</td>
                    <td  class="smalltext">   1.3790</td>
                    <td  class="smalltext">   1.3383</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3573</span></td>
                    <td  class="smalltext"><span class="red">   1.3470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3891</span></td>
                    <td  class="smalltext"><span class="red">   1.3834</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.4359</td>
                    <td  class="smalltext">   1.4265</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3953</td>
                    <td  class="smalltext">   1.3901</td>
                    <td  class="smalltext">   1.3614</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3840</span></td>
                    <td  class="smalltext"><span class="red">   1.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4037</span></td>
                    <td  class="smalltext"><span class="red">   1.3969</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3969</td>
                    <td  class="smalltext">   1.3930</td>
                    <td  class="smalltext">   1.3630</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3783</span></td>
                    <td  class="smalltext"><span class="red">   1.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4020</span></td>
                    <td  class="smalltext"><span class="red">   1.3969</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.4360</td>
                    <td  class="smalltext">   1.4273</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3999</td>
                    <td  class="smalltext">   1.3949</td>
                    <td  class="smalltext">   1.3699</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3900</span></td>
                    <td  class="smalltext"><span class="red">   1.3770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4079</span></td>
                    <td  class="smalltext"><span class="red">   1.4014</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4288</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4014</td>
                    <td  class="smalltext">   1.3977</td>
                    <td  class="smalltext">   1.3714</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3855</span></td>
                    <td  class="smalltext"><span class="red">   1.3770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4062</span></td>
                    <td  class="smalltext"><span class="red">   1.4014</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.4361</td>
                    <td  class="smalltext">   1.4280</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4036</td>
                    <td  class="smalltext">   1.3988</td>
                    <td  class="smalltext">   1.3766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3950</span></td>
                    <td  class="smalltext"><span class="red">   1.3830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4112</span></td>
                    <td  class="smalltext"><span class="red">   1.4050</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4294</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4050</td>
                    <td  class="smalltext">   1.4014</td>
                    <td  class="smalltext">   1.3780</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3912</span></td>
                    <td  class="smalltext"><span class="red">   1.3830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4096</span></td>
                    <td  class="smalltext"><span class="red">   1.4050</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.4362</td>
                    <td  class="smalltext">   1.4297</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4130</td>
                    <td  class="smalltext">   1.4088</td>
                    <td  class="smalltext">   1.3936</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4070</span></td>
                    <td  class="smalltext"><span class="red">   1.3990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4198</span></td>
                    <td  class="smalltext"><span class="red">   1.4143</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.4375</td>
                    <td  class="smalltext">   1.4310</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4143</td>
                    <td  class="smalltext">   1.4112</td>
                    <td  class="smalltext">   1.3949</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4051</span></td>
                    <td  class="smalltext"><span class="red">   1.3990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4184</span></td>
                    <td  class="smalltext"><span class="red">   1.4143</span></td>
                    <td  class="smalltext"><span class="red">   1.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.4976</td>
                    <td  class="smalltext">   1.4703</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3893</td>
                    <td  class="smalltext">   1.3772</td>
                    <td  class="smalltext">   1.2992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3500</span></td>
                    <td  class="smalltext"><span class="red">   1.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4075</span></td>
                    <td  class="smalltext"><span class="red">   1.3917</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.4976</td>
                    <td  class="smalltext">   1.4794</td>
                    <td  class="smalltext">1.4703</td>
                    <td  class="smalltext">   1.3893</td>
                    <td  class="smalltext">   1.3812</td>
                    <td  class="smalltext">   1.2992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3500</span></td>
                    <td  class="smalltext"><span class="red">   1.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4022</span></td>
                    <td  class="smalltext"><span class="red">   1.3917</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-6 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.3917</td>
                    <td  class="smalltext">   1.3856</td>
                    <td  class="smalltext">   1.3016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3396</span></td>
                    <td  class="smalltext"><span class="red">   1.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3996</span></td>
                    <td  class="smalltext"><span class="red">   1.3917</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.4978</td>
                    <td  class="smalltext">   1.4828</td>
                    <td  class="smalltext">1.4753</td>
                    <td  class="smalltext">   1.4166</td>
                    <td  class="smalltext">   1.4093</td>
                    <td  class="smalltext">   1.3490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3900</span></td>
                    <td  class="smalltext"><span class="red">   1.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4283</span></td>
                    <td  class="smalltext"><span class="red">   1.4188</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4188</td>
                    <td  class="smalltext">   1.4133</td>
                    <td  class="smalltext">   1.3512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.3797</span></td>
                    <td  class="smalltext"><span class="red">   1.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4259</span></td>
                    <td  class="smalltext"><span class="red">   1.4188</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.4981</td>
                    <td  class="smalltext">   1.4852</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4331</td>
                    <td  class="smalltext">   1.4267</td>
                    <td  class="smalltext">   1.3791</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4130</span></td>
                    <td  class="smalltext"><span class="red">   1.3920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4433</span></td>
                    <td  class="smalltext"><span class="red">   1.4350</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.4981</td>
                    <td  class="smalltext">   1.4809</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4440</td>
                    <td  class="smalltext">   1.4344</td>
                    <td  class="smalltext">   1.3989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4280</span></td>
                    <td  class="smalltext"><span class="red">   1.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4584</span></td>
                    <td  class="smalltext"><span class="red">   1.4459</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.4981</td>
                    <td  class="smalltext">   1.4867</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4440</td>
                    <td  class="smalltext">   1.4376</td>
                    <td  class="smalltext">   1.3989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4280</span></td>
                    <td  class="smalltext"><span class="red">   1.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4542</span></td>
                    <td  class="smalltext"><span class="red">   1.4459</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4459</td>
                    <td  class="smalltext">   1.4411</td>
                    <td  class="smalltext">   1.4008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4198</span></td>
                    <td  class="smalltext"><span class="red">   1.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4522</span></td>
                    <td  class="smalltext"><span class="red">   1.4459</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.4983</td>
                    <td  class="smalltext">   1.4880</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4519</td>
                    <td  class="smalltext">   1.4464</td>
                    <td  class="smalltext">   1.4133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4380</span></td>
                    <td  class="smalltext"><span class="red">   1.4230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4608</span></td>
                    <td  class="smalltext"><span class="red">   1.4536</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.4984</td>
                    <td  class="smalltext">   1.4890</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4578</td>
                    <td  class="smalltext">   1.4526</td>
                    <td  class="smalltext">   1.4239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4460</span></td>
                    <td  class="smalltext"><span class="red">   1.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4662</span></td>
                    <td  class="smalltext"><span class="red">   1.4594</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4594</td>
                    <td  class="smalltext">   1.4555</td>
                    <td  class="smalltext">   1.4255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4408</span></td>
                    <td  class="smalltext"><span class="red">   1.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4645</span></td>
                    <td  class="smalltext"><span class="red">   1.4594</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.4985</td>
                    <td  class="smalltext">   1.4898</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4624</td>
                    <td  class="smalltext">   1.4574</td>
                    <td  class="smalltext">   1.4324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4520</span></td>
                    <td  class="smalltext"><span class="red">   1.4400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4704</span></td>
                    <td  class="smalltext"><span class="red">   1.4639</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4913</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4639</td>
                    <td  class="smalltext">   1.4602</td>
                    <td  class="smalltext">   1.4339</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4480</span></td>
                    <td  class="smalltext"><span class="red">   1.4400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4687</span></td>
                    <td  class="smalltext"><span class="red">   1.4639</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.4986</td>
                    <td  class="smalltext">   1.4905</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4661</td>
                    <td  class="smalltext">   1.4613</td>
                    <td  class="smalltext">   1.4391</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4570</span></td>
                    <td  class="smalltext"><span class="red">   1.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4737</span></td>
                    <td  class="smalltext"><span class="red">   1.4675</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4675</td>
                    <td  class="smalltext">   1.4639</td>
                    <td  class="smalltext">   1.4405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4537</span></td>
                    <td  class="smalltext"><span class="red">   1.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4721</span></td>
                    <td  class="smalltext"><span class="red">   1.4675</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.4987</td>
                    <td  class="smalltext">   1.4915</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4716</td>
                    <td  class="smalltext">   1.4672</td>
                    <td  class="smalltext">   1.4991</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4650</span></td>
                    <td  class="smalltext"><span class="red">   1.4550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4787</span></td>
                    <td  class="smalltext"><span class="red">   1.4729</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.4987</td>
                    <td  class="smalltext">   1.4922</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4755</td>
                    <td  class="smalltext">   1.4713</td>
                    <td  class="smalltext">   1.4561</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4700</span></td>
                    <td  class="smalltext"><span class="red">   1.4610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4823</span></td>
                    <td  class="smalltext"><span class="red">   1.4768</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>2</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5000</td>
                    <td  class="smalltext">   1.4935</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4768</td>
                    <td  class="smalltext">   1.4737</td>
                    <td  class="smalltext">   1.4574</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   4.4676</span></td>
                    <td  class="smalltext"><span class="red">   1.4610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4809</span></td>
                    <td  class="smalltext"><span class="red">   1.4768</span></td>
                    <td  class="smalltext"><span class="red">   1.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   1.5601</td>
                    <td  class="smalltext">   1.5419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4518</td>
                    <td  class="smalltext">   1.4436</td>
                    <td  class="smalltext">   1.3617</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4130</span></td>
                    <td  class="smalltext"><span class="red">   1.3820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4648</span></td>
                    <td  class="smalltext"><span class="red">   1.4542</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5443</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4542</td>
                    <td  class="smalltext">   1.4481</td>
                    <td  class="smalltext">   1.3641</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4021</span></td>
                    <td  class="smalltext"><span class="red">   1.3820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4622</span></td>
                    <td  class="smalltext"><span class="red">   1.4542</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.5603</td>
                    <td  class="smalltext">   1.5453</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4791</td>
                    <td  class="smalltext">   1.4717</td>
                    <td  class="smalltext">   1.4115</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4520</span></td>
                    <td  class="smalltext"><span class="red">   1.4270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4909</span></td>
                    <td  class="smalltext"><span class="red">   1.4813</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5475</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.4813</td>
                    <td  class="smalltext">   1.4758</td>
                    <td  class="smalltext">   1.4137</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4422</span></td>
                    <td  class="smalltext"><span class="red">   1.4270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4885</span></td>
                    <td  class="smalltext"><span class="red">   1.4813</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.5607</td>
                    <td  class="smalltext">   1.5493</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5066</td>
                    <td  class="smalltext">   1.5007</td>
                    <td  class="smalltext">   1.4615</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4900</span></td>
                    <td  class="smalltext"><span class="red">   1.4720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5160</span></td>
                    <td  class="smalltext"><span class="red">   1.5084</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5511</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5084</td>
                    <td  class="smalltext">   1.5040</td>
                    <td  class="smalltext">   1.4633</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4823</span></td>
                    <td  class="smalltext"><span class="red">   1.4720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5141</span></td>
                    <td  class="smalltext"><span class="red">   1.5084</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.5609</td>
                    <td  class="smalltext">   1.5515</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5203</td>
                    <td  class="smalltext">   1.5151</td>
                    <td  class="smalltext">   1.4864</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5090</span></td>
                    <td  class="smalltext"><span class="red">   1.4950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5287</span></td>
                    <td  class="smalltext"><span class="red">   1.5219</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5531</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5219</td>
                    <td  class="smalltext">   1.5180</td>
                    <td  class="smalltext">   1.4880</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5033</span></td>
                    <td  class="smalltext"><span class="red">   1.4950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5270</span></td>
                    <td  class="smalltext"><span class="red">   1.5219</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.5610</td>
                    <td  class="smalltext">   1.5523</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5249</td>
                    <td  class="smalltext">   1.5199</td>
                    <td  class="smalltext">   1.4949</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5150</span></td>
                    <td  class="smalltext"><span class="red">   1.5020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5329</span></td>
                    <td  class="smalltext"><span class="red">   1.5264</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5538</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5264</td>
                    <td  class="smalltext">   1.5227</td>
                    <td  class="smalltext">   1.4964</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5105</span></td>
                    <td  class="smalltext"><span class="red">   1.5020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5312</span></td>
                    <td  class="smalltext"><span class="red">   1.5264</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.5611</td>
                    <td  class="smalltext">   1.5530</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5286</td>
                    <td  class="smalltext">   1.5238</td>
                    <td  class="smalltext">   1.5016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5200</span></td>
                    <td  class="smalltext"><span class="red">   1.5080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5362</span></td>
                    <td  class="smalltext"><span class="red">   1.5300</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>9</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.5625</td>
                    <td  class="smalltext">   1.5544</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5300</td>
                    <td  class="smalltext">   1.5264</td>
                    <td  class="smalltext">   1.5030</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5162</span></td>
                    <td  class="smalltext"><span class="red">   1.5080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5346</span></td>
                    <td  class="smalltext"><span class="red">   1.5300</span></td>
                    <td  class="smalltext"><span class="red">   1.5625</span></td>

                </tr>


                </td>
                </tr>
            </table>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>