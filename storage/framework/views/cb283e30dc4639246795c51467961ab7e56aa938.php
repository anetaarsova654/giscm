

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <?php if(!Auth::check()): ?>
                <p class="wel">
                    <a href="<?php echo e(route('login')); ?>">Welcome visitor you can login or create an account.</a>
                </p><br>
                <?php endif; ?>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12">
                <div class="container">
                    <div class="contact">
                        <div class="col-md-6 contact-top">
                            <h3>Info</h3>
                            <div class="map">
                                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2923.17959316683!2d-85.63592788409873!3d42.89015747915484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8819b304d744dd79%3A0xf8342b3771d8ef8f!2s4821+Curwood%2C+Grand+Rapids%2C+MI+49508!5e0!3m2!1spt-BR!2sus!4v1489503531206" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                            </div>
                            <p>We are students, engineers, designers, and overall... people that have a passion for designing and building things. Our community is growing. Join us!</p>
                            
                        </div>
                        <div class="col-md-6 contact-top">
                            <form method="POST" name="contactus" action="<?php echo e(route('contactus')); ?>">
                                <h3>HAVE A QUESTION? FILL OUT SHORT FORM BELOW.</h3>
                                <?php if(Session::has('flash_message')): ?>
                                    <div class="alert alert-success" role="alert" style="font-size: medium; text-align: center;">
                                        <?php echo e(Session::get('flash_message')); ?>

                                        <?php echo e(Session::forget('flash_message')); ?>

                                    </div>
                                <?php endif; ?>
                                <?php echo e(csrf_field()); ?>

                                <div>
                                    <span style="color:#000;"> Full Name  </span>
                                    <input type="text" name="fullname" maxlength="50" required>
                                </div>
                                <div>
                                    <span style="color:#000;">Email  </span>
                                    <input type="email" name="email" maxlength="50" required>
                                </div>
                                <div>
                                    <span style="color:#000;">Subject </span>
                                    <input type="text" name="subject" maxlength="50" required>
                                </div>
                                <div>
                                    <span style="color:#000;">Message </span>
                                    <textarea name="messagetext" rows="6" cols="40" wrap="soft" required></textarea>
                                </div>
                                
                                <input type="submit" name="submit" value="Send">
                            </form>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>