

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <?php if(!Auth::check()): ?>
                <p class="wel">
                    <a href="<?php echo e(route('login')); ?>">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                <?php endif; ?>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <?php echo Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']); ?>

                            <?php echo e(Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name'])); ?>

                            <input type="submit" value="">
                        <?php echo Form::close(); ?>

                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                <?php echo $__env->make('cadmodels.partials._categories', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-md-9">
                <div class="content-top-in">
                    <h3 class="future">FEATURED</h3>

                    <?php $__empty_1 = true; $__currentLoopData = $cadmodels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadmodel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="col-md-3 md-col" style="padding-bottom:20px;">
                            <div class="col-md">
                                <center><img src="<?php echo e(Storage::url($cadmodel->image_url)); ?>" border="0" alt="a"></center><br>
                                <div class="top-content">
                                    <h5><?php echo e($cadmodel->name); ?></h5>
                                    <p>
                                        <?php for($i = 0 , $filled = $cadmodel->getRating() ; $i<5 ; $i++): ?>
                                            <?php if($i<$filled): ?>
                                            <i class="fa fa-star"></i>
                                            <?php else: ?>
                                                <i class="fa fa-star-o"></i>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </p>
                                   
                                    <div class="white">
                                        <a href="<?php echo e(route('cadmodels.show',[$cadmodel->id])); ?>" align="center" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">
                                            View
                                            <div class="clearfix"></div>
                                        </a>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>No Data Available</p>
                    <?php endif; ?>
                </div>
            </div>
            <?php echo e($cadmodels->appends(request()->except('page'))->links()); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>