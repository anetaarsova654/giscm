<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="<?php echo e(route('threadSpecifications')); ?>"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries1')); ?>">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries2')); ?>">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries3')); ?>">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>

                        <a href="<?php echo e(route('unScrewThreadsSeries4')); ?>">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries5')); ?>">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries6')); ?>">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="<?php echo e(route('unScrewThreadsSeries7')); ?>">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>2 7/8-6 UN to 4-16 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>

        &nbsp;<br>
        <div class="table-responsive">
            <table class="table">

                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br><span class="red">Max Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><strong>Min <a href="unified.cfm#footer"><sup></sup></a></strong></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0028</td>
                    <td  class="smalltext">   2.8722</td>
                    <td  class="smalltext">   2.8540</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7639</td>
                    <td  class="smalltext">   2.7547</td>
                    <td  class="smalltext">   2.6738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7250</span></td>
                    <td  class="smalltext"><span class="red">   2.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7787</span></td>
                    <td  class="smalltext"><span class="red">   2.7667</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.8750</td>
                    <td  class="smalltext">   2.8568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7667</td>
                    <td  class="smalltext">   2.7598</td>
                    <td  class="smalltext">   2.6766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7146</span></td>
                    <td  class="smalltext"><span class="red">   2.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7757</span></td>
                    <td  class="smalltext"><span class="red">   2.7667</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   2.8725</td>
                    <td  class="smalltext">   2.8575</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7913</td>
                    <td  class="smalltext">   2.7829</td>
                    <td  class="smalltext">   2.7237</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7650</span></td>
                    <td  class="smalltext"><span class="red">   2.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8048</span></td>
                    <td  class="smalltext"><span class="red">   2.7938</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.8750</td>
                    <td  class="smalltext">   2.8600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7938</td>
                    <td  class="smalltext">   2.7875</td>
                    <td  class="smalltext">   2.7262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7547</span></td>
                    <td  class="smalltext"><span class="red">   2.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8020</span></td>
                    <td  class="smalltext"><span class="red">   2.7938</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.8731</td>
                    <td  class="smalltext">   2.8617</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8190</td>
                    <td  class="smalltext">   2.8127</td>
                    <td  class="smalltext">   2.7739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8030</span></td>
                    <td  class="smalltext"><span class="red">   2.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8291</span></td>
                    <td  class="smalltext"><span class="red">   2.8209</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.8750</td>
                    <td  class="smalltext">   2.8636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8209</td>
                    <td  class="smalltext">   2.8162</td>
                    <td  class="smalltext">   2.7758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7948</span></td>
                    <td  class="smalltext"><span class="red">   2.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8271</span></td>
                    <td  class="smalltext"><span class="red">   2.8209</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.8733</td>
                    <td  class="smalltext">   2.8639</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8327</td>
                    <td  class="smalltext">   2.8271</td>
                    <td  class="smalltext">   2.7988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8210</span></td>
                    <td  class="smalltext"><span class="red">   2.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8417</span></td>
                    <td  class="smalltext"><span class="red">   2.8344</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.8750</td>
                    <td  class="smalltext">   2.8656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8344</td>
                    <td  class="smalltext">   2.8302</td>
                    <td  class="smalltext">   2.8005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8158</span></td>
                    <td  class="smalltext"><span class="red">   2.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8399</span></td>
                    <td  class="smalltext"><span class="red">   2.8344</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.8734</td>
                    <td  class="smalltext">   2.8653</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8409</td>
                    <td  class="smalltext">   2.8357</td>
                    <td  class="smalltext">   2.8139</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8320</span></td>
                    <td  class="smalltext"><span class="red">   2.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8493</span></td>
                    <td  class="smalltext"><span class="red">   2.8425</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.8750</td>
                    <td  class="smalltext">   2.8669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8425</td>
                    <td  class="smalltext">   2.8386</td>
                    <td  class="smalltext">   2.8155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8287</span></td>
                    <td  class="smalltext"><span class="red">   2.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8476</span></td>
                    <td  class="smalltext"><span class="red">   2.8425</span></td>
                    <td  class="smalltext"><span class="red">   2.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0032</td>
                    <td  class="smalltext">   2.9968</td>
                    <td  class="smalltext">   2.9611</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8344</td>
                    <td  class="smalltext">   2.8183</td>
                    <td  class="smalltext">   2.6991</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7670</span></td>
                    <td  class="smalltext"><span class="red">   2.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8585</span></td>
                    <td  class="smalltext"><span class="red">   2.8376</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0032</td>
                    <td  class="smalltext">   2.9968</td>
                    <td  class="smalltext">   2.9730</td>
                    <td  class="smalltext">2.9611</td>
                    <td  class="smalltext">   2.8344</td>
                    <td  class="smalltext">   2.8237</td>
                    <td  class="smalltext">   2.6991</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7670</span></td>
                    <td  class="smalltext"><span class="red">   2.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8515</span></td>
                    <td  class="smalltext"><span class="red">   2.8376</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9762</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8376</td>
                    <td  class="smalltext">   2.8296</td>
                    <td  class="smalltext">   2.7023</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7594</span></td>
                    <td  class="smalltext"><span class="red">   2.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8480</span></td>
                    <td  class="smalltext"><span class="red">   2.8376</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0028</td>
                    <td  class="smalltext">   2.9972</td>
                    <td  class="smalltext">   2.9790</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8889</td>
                    <td  class="smalltext">   2.8796</td>
                    <td  class="smalltext">   2.7988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8500</span></td>
                    <td  class="smalltext"><span class="red">   2.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9038</span></td>
                    <td  class="smalltext"><span class="red">   2.8917</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.8917</td>
                    <td  class="smalltext">   2.8847</td>
                    <td  class="smalltext">   2.8016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8396</span></td>
                    <td  class="smalltext"><span class="red">   2.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9008</span></td>
                    <td  class="smalltext"><span class="red">   2.8917</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   2.9974</td>
                    <td  class="smalltext">   2.9824</td>
                    <td  class="smalltext">2.9749</td>
                    <td  class="smalltext">   2.9162</td>
                    <td  class="smalltext">   2.9077</td>
                    <td  class="smalltext">   2.8486</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8900</span></td>
                    <td  class="smalltext"><span class="red">   2.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9299</span></td>
                    <td  class="smalltext"><span class="red">   2.9188</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9188</td>
                    <td  class="smalltext">   2.9124</td>
                    <td  class="smalltext">   2.8512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.8797</span></td>
                    <td  class="smalltext"><span class="red">   2.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9271</span></td>
                    <td  class="smalltext"><span class="red">   2.9188</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   2.9980</td>
                    <td  class="smalltext">   2.9851</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9330</td>
                    <td  class="smalltext">   2.9262</td>
                    <td  class="smalltext">   2.8790</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9130</span></td>
                    <td  class="smalltext"><span class="red">   2.8920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9439</span></td>
                    <td  class="smalltext"><span class="red">   2.9350</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.9981</td>
                    <td  class="smalltext">   2.9867</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9440</td>
                    <td  class="smalltext">   2.9377</td>
                    <td  class="smalltext">   2.8989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9280</span></td>
                    <td  class="smalltext"><span class="red">   2.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9541</span></td>
                    <td  class="smalltext"><span class="red">   2.9459</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9459</td>
                    <td  class="smalltext">   2.9412</td>
                    <td  class="smalltext">   2.9008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9198</span></td>
                    <td  class="smalltext"><span class="red">   2.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9521</span></td>
                    <td  class="smalltext"><span class="red">   2.9459</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   2.9982</td>
                    <td  class="smalltext">   2.9879</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9518</td>
                    <td  class="smalltext">   2.9459</td>
                    <td  class="smalltext">   2.9132</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9380</span></td>
                    <td  class="smalltext"><span class="red">   2.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9613</span></td>
                    <td  class="smalltext"><span class="red">   2.9536</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.9983</td>
                    <td  class="smalltext">   2.9889</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9577</td>
                    <td  class="smalltext">   2.9521</td>
                    <td  class="smalltext">   2.9238</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9460</span></td>
                    <td  class="smalltext"><span class="red">   2.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9667</span></td>
                    <td  class="smalltext"><span class="red">   2.9594</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9594</td>
                    <td  class="smalltext">   2.9552</td>
                    <td  class="smalltext">   2.9255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9408</span></td>
                    <td  class="smalltext"><span class="red">   2.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9649</span></td>
                    <td  class="smalltext"><span class="red">   2.9594</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.9984</td>
                    <td  class="smalltext">   2.9897</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9623</td>
                    <td  class="smalltext">   2.9569</td>
                    <td  class="smalltext">   2.9323</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9530</span></td>
                    <td  class="smalltext"><span class="red">   2.9400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9709</span></td>
                    <td  class="smalltext"><span class="red">   2.9639</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.9984</td>
                    <td  class="smalltext">   2.9903</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9659</td>
                    <td  class="smalltext">   2.9607</td>
                    <td  class="smalltext">   2.9389</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9570</span></td>
                    <td  class="smalltext"><span class="red">   2.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9743</span></td>
                    <td  class="smalltext"><span class="red">   2.9675</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.0000</td>
                    <td  class="smalltext">   2.9919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.9675</td>
                    <td  class="smalltext">   2.9636</td>
                    <td  class="smalltext">   2.9405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9537</span></td>
                    <td  class="smalltext"><span class="red">   2.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9726</span></td>
                    <td  class="smalltext"><span class="red">   2.9675</span></td>
                    <td  class="smalltext"><span class="red">   3.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0028</td>
                    <td  class="smalltext">   3.1222</td>
                    <td  class="smalltext">   3.1040</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0139</td>
                    <td  class="smalltext">   3.0045</td>
                    <td  class="smalltext">   2.9238</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9750</span></td>
                    <td  class="smalltext"><span class="red">   2.9450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0289</span></td>
                    <td  class="smalltext"><span class="red">   3.0167</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.1250</td>
                    <td  class="smalltext">   3.1068</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0167</td>
                    <td  class="smalltext">   3.0097</td>
                    <td  class="smalltext">   2.9266</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.9646</span></td>
                    <td  class="smalltext"><span class="red">   2.9450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0259</span></td>
                    <td  class="smalltext"><span class="red">   3.0167</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   3.1224</td>
                    <td  class="smalltext">   3.1074</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0412</td>
                    <td  class="smalltext">   3.0326</td>
                    <td  class="smalltext">   2.9736</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0150</span></td>
                    <td  class="smalltext"><span class="red">   2.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0550</span></td>
                    <td  class="smalltext"><span class="red">   3.0438</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.1250</td>
                    <td  class="smalltext">   3.1100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0438</td>
                    <td  class="smalltext">   3.0374</td>
                    <td  class="smalltext">   2.9762</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0047</span></td>
                    <td  class="smalltext"><span class="red">   2.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0522</span></td>
                    <td  class="smalltext"><span class="red">   3.0438</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.1231</td>
                    <td  class="smalltext">   3.1117</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0690</td>
                    <td  class="smalltext">   3.0627</td>
                    <td  class="smalltext">   3.0239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0530</span></td>
                    <td  class="smalltext"><span class="red">   3.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0791</span></td>
                    <td  class="smalltext"><span class="red">   3.0709</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.1250</td>
                    <td  class="smalltext">   3.1136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0709</td>
                    <td  class="smalltext">   3.0662</td>
                    <td  class="smalltext">   3.0258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0448</span></td>
                    <td  class="smalltext"><span class="red">   3.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0771</span></td>
                    <td  class="smalltext"><span class="red">   3.0709</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.1233</td>
                    <td  class="smalltext">   3.1139</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0827</td>
                    <td  class="smalltext">   3.0771</td>
                    <td  class="smalltext">   3.0488</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0710</span></td>
                    <td  class="smalltext"><span class="red">   3.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0917</span></td>
                    <td  class="smalltext"><span class="red">   3.0844</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.1250</td>
                    <td  class="smalltext">   3.1156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0844</td>
                    <td  class="smalltext">   3.0802</td>
                    <td  class="smalltext">   3.0505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0658</span></td>
                    <td  class="smalltext"><span class="red">   3.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0899</span></td>
                    <td  class="smalltext"><span class="red">   3.0844</span></td>
                    <td  class="smalltext"><span class="red">   3.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0033</td>
                    <td  class="smalltext">   3.2467</td>
                    <td  class="smalltext">   3.2110</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0843</td>
                    <td  class="smalltext">   3.0680</td>
                    <td  class="smalltext">   2.9490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0170</span></td>
                    <td  class="smalltext"><span class="red">   2.9790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1088</span></td>
                    <td  class="smalltext"><span class="red">   3.0876</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0033</td>
                    <td  class="smalltext">   3.2467</td>
                    <td  class="smalltext">   3.2229</td>
                    <td  class="smalltext">3.2110</td>
                    <td  class="smalltext">   3.0843</td>
                    <td  class="smalltext">   3.0734</td>
                    <td  class="smalltext">   2.9490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0170</span></td>
                    <td  class="smalltext"><span class="red">   2.9790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1017</span></td>
                    <td  class="smalltext"><span class="red">   3.0876</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.2500</td>
                    <td  class="smalltext">   3.2262</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.0876</td>
                    <td  class="smalltext">   3.0794</td>
                    <td  class="smalltext">   2.9523</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0094</span></td>
                    <td  class="smalltext"><span class="red">   2.9790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0982</span></td>
                    <td  class="smalltext"><span class="red">   3.0876</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0028</td>
                    <td  class="smalltext">   3.2472</td>
                    <td  class="smalltext">   3.2290</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1389</td>
                    <td  class="smalltext">   3.1294</td>
                    <td  class="smalltext">   3.0488</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1000</span></td>
                    <td  class="smalltext"><span class="red">   3.0700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1540</span></td>
                    <td  class="smalltext"><span class="red">   3.1417</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.2500</td>
                    <td  class="smalltext">   3.2318</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1417</td>
                    <td  class="smalltext">   3.1346</td>
                    <td  class="smalltext">   3.0516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.0896</span></td>
                    <td  class="smalltext"><span class="red">   3.0700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1509</span></td>
                    <td  class="smalltext"><span class="red">   3.1417</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   3.2474</td>
                    <td  class="smalltext">   3.2324</td>
                    <td  class="smalltext">3.2249</td>
                    <td  class="smalltext">   3.1662</td>
                    <td  class="smalltext">   3.1575</td>
                    <td  class="smalltext">   3.0986</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1400</span></td>
                    <td  class="smalltext"><span class="red">   3.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1801</span></td>
                    <td  class="smalltext"><span class="red">   3.1688</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.2500</td>
                    <td  class="smalltext">   3.2350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1688</td>
                    <td  class="smalltext">   3.1623</td>
                    <td  class="smalltext">   3.1012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1297</span></td>
                    <td  class="smalltext"><span class="red">   3.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1773</span></td>
                    <td  class="smalltext"><span class="red">   3.1688</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   3.2480</td>
                    <td  class="smalltext">   3.2351</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1830</td>
                    <td  class="smalltext">   3.1762</td>
                    <td  class="smalltext">   3.1290</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1630</span></td>
                    <td  class="smalltext"><span class="red">   3.1420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1939</span></td>
                    <td  class="smalltext"><span class="red">   3.1850</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.2481</td>
                    <td  class="smalltext">   3.2367</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1940</td>
                    <td  class="smalltext">   3.1877</td>
                    <td  class="smalltext">   3.1489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1780</span></td>
                    <td  class="smalltext"><span class="red">   3.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2041</span></td>
                    <td  class="smalltext"><span class="red">   3.1959</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.2500</td>
                    <td  class="smalltext">   3.2386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.1959</td>
                    <td  class="smalltext">   3.1912</td>
                    <td  class="smalltext">   3.1508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1698</span></td>
                    <td  class="smalltext"><span class="red">   3.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2021</span></td>
                    <td  class="smalltext"><span class="red">   3.1959</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.2482</td>
                    <td  class="smalltext">   3.2379</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2018</td>
                    <td  class="smalltext">   3.1959</td>
                    <td  class="smalltext">   3.1632</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1880</span></td>
                    <td  class="smalltext"><span class="red">   3.1730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2113</span></td>
                    <td  class="smalltext"><span class="red">   3.2036</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.2483</td>
                    <td  class="smalltext">   3.2389</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2077</td>
                    <td  class="smalltext">   3.2021</td>
                    <td  class="smalltext">   3.1738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1960</span></td>
                    <td  class="smalltext"><span class="red">   3.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2167</span></td>
                    <td  class="smalltext"><span class="red">   3.2094</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.2500</td>
                    <td  class="smalltext">   3.2406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2094</td>
                    <td  class="smalltext">   3.2052</td>
                    <td  class="smalltext">   3.1755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.1908</span></td>
                    <td  class="smalltext"><span class="red">   3.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2149</span></td>
                    <td  class="smalltext"><span class="red">   3.2094</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   3.2484</td>
                    <td  class="smalltext">   3.2397</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2123</td>
                    <td  class="smalltext">   3.2069</td>
                    <td  class="smalltext">   3.1823</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2030</span></td>
                    <td  class="smalltext"><span class="red">   3.1900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2209</span></td>
                    <td  class="smalltext"><span class="red">   3.2139</span></td>
                    <td  class="smalltext"><span class="red">   3.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   3.3721</td>
                    <td  class="smalltext">   3.3539</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2638</td>
                    <td  class="smalltext">   3.2543</td>
                    <td  class="smalltext">   3.1737</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2250</span></td>
                    <td  class="smalltext"><span class="red">   3.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2791</span></td>
                    <td  class="smalltext"><span class="red">   3.2667</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.3750</td>
                    <td  class="smalltext">   3.3568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2667</td>
                    <td  class="smalltext">   3.2595</td>
                    <td  class="smalltext">   3.1766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2146</span></td>
                    <td  class="smalltext"><span class="red">   3.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2760</span></td>
                    <td  class="smalltext"><span class="red">   3.2667</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   3.3724</td>
                    <td  class="smalltext">   3.3574</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2912</td>
                    <td  class="smalltext">   3.2824</td>
                    <td  class="smalltext">   3.2236</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2650</span></td>
                    <td  class="smalltext"><span class="red">   3.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3052</span></td>
                    <td  class="smalltext"><span class="red">   3.2938</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.3750</td>
                    <td  class="smalltext">   3.3600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.2938</td>
                    <td  class="smalltext">   3.2872</td>
                    <td  class="smalltext">   3.2262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2547</span></td>
                    <td  class="smalltext"><span class="red">   3.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3023</span></td>
                    <td  class="smalltext"><span class="red">   3.2938</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.3731</td>
                    <td  class="smalltext">   3.3617</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3190</td>
                    <td  class="smalltext">   3.3126</td>
                    <td  class="smalltext">   3.2739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3030</span></td>
                    <td  class="smalltext"><span class="red">   3.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3293</span></td>
                    <td  class="smalltext"><span class="red">   3.3209</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.3750</td>
                    <td  class="smalltext">   3.3636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3209</td>
                    <td  class="smalltext">   3.3161</td>
                    <td  class="smalltext">   3.2758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2948</span></td>
                    <td  class="smalltext"><span class="red">   3.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3272</span></td>
                    <td  class="smalltext"><span class="red">   3.3209</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.3733</td>
                    <td  class="smalltext">   3.3639</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3327</td>
                    <td  class="smalltext">   3.3269</td>
                    <td  class="smalltext">   3.2988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3210</span></td>
                    <td  class="smalltext"><span class="red">   3.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3419</span></td>
                    <td  class="smalltext"><span class="red">   3.3344</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.3750</td>
                    <td  class="smalltext">   3.3656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3344</td>
                    <td  class="smalltext">   3.3301</td>
                    <td  class="smalltext">   3.3005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3158</span></td>
                    <td  class="smalltext"><span class="red">   3.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3400</span></td>
                    <td  class="smalltext"><span class="red">   3.3344</span></td>
                    <td  class="smalltext"><span class="red">   3.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0033</td>
                    <td  class="smalltext">   3.4967</td>
                    <td  class="smalltext">   3.4610</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3343</td>
                    <td  class="smalltext">   3.3177</td>
                    <td  class="smalltext">   3.1990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2670</span></td>
                    <td  class="smalltext"><span class="red">   3.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3591</span></td>
                    <td  class="smalltext"><span class="red">   3.3376</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0033</td>
                    <td  class="smalltext">   3.4967</td>
                    <td  class="smalltext">   3.4729</td>
                    <td  class="smalltext">3.4610</td>
                    <td  class="smalltext">   3.3343</td>
                    <td  class="smalltext">   3.3233</td>
                    <td  class="smalltext">   3.1990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2670</span></td>
                    <td  class="smalltext"><span class="red">   3.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3519</span></td>
                    <td  class="smalltext"><span class="red">   3.3376</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.5000</td>
                    <td  class="smalltext">   3.4762</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3376</td>
                    <td  class="smalltext">   3.3293</td>
                    <td  class="smalltext">   3.2023</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.2594</span></td>
                    <td  class="smalltext"><span class="red">   3.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3484</span></td>
                    <td  class="smalltext"><span class="red">   3.3376</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   3.4971</td>
                    <td  class="smalltext">   3.4789</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3888</td>
                    <td  class="smalltext">   3.3792</td>
                    <td  class="smalltext">   3.2987</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3500</span></td>
                    <td  class="smalltext"><span class="red">   3.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4042</span></td>
                    <td  class="smalltext"><span class="red">   3.3917</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.5000</td>
                    <td  class="smalltext">   3.4818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.3917</td>
                    <td  class="smalltext">   3.3845</td>
                    <td  class="smalltext">   3.3016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3396</span></td>
                    <td  class="smalltext"><span class="red">   3.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4011</span></td>
                    <td  class="smalltext"><span class="red">   3.3917</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   3.4974</td>
                    <td  class="smalltext">   3.4824</td>
                    <td  class="smalltext">3.4749</td>
                    <td  class="smalltext">   3.4162</td>
                    <td  class="smalltext">   3.4074</td>
                    <td  class="smalltext">   3.3486</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3900</span></td>
                    <td  class="smalltext"><span class="red">   3.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4303</span></td>
                    <td  class="smalltext"><span class="red">   3.4188</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.5000</td>
                    <td  class="smalltext">   3.4850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4188</td>
                    <td  class="smalltext">   3.4122</td>
                    <td  class="smalltext">   3.3512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.3797</span></td>
                    <td  class="smalltext"><span class="red">   3.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4274</span></td>
                    <td  class="smalltext"><span class="red">   3.4188</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   3.4979</td>
                    <td  class="smalltext">   3.4850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4329</td>
                    <td  class="smalltext">   3.4260</td>
                    <td  class="smalltext">   3.3789</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4130</span></td>
                    <td  class="smalltext"><span class="red">   3.3920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4440</span></td>
                    <td  class="smalltext"><span class="red">   3.4350</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.4981</td>
                    <td  class="smalltext">   3.4867</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4440</td>
                    <td  class="smalltext">   3.4376</td>
                    <td  class="smalltext">   3.3989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4280</span></td>
                    <td  class="smalltext"><span class="red">   3.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4543</span></td>
                    <td  class="smalltext"><span class="red">   3.4459</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.5000</td>
                    <td  class="smalltext">   3.4886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4459</td>
                    <td  class="smalltext">   3.4411</td>
                    <td  class="smalltext">   3.4008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4198</span></td>
                    <td  class="smalltext"><span class="red">   3.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4522</span></td>
                    <td  class="smalltext"><span class="red">   3.4459</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.4982</td>
                    <td  class="smalltext">   3.4879</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4518</td>
                    <td  class="smalltext">   3.4132</td>
                    <td  class="smalltext">   3.4132</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4380</span></td>
                    <td  class="smalltext"><span class="red">   3.4230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4615</span></td>
                    <td  class="smalltext"><span class="red">   3.4536</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.4983</td>
                    <td  class="smalltext">   3.4889</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4577</td>
                    <td  class="smalltext">   3.4519</td>
                    <td  class="smalltext">   3.4238</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4460</span></td>
                    <td  class="smalltext"><span class="red">   3.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4669</span></td>
                    <td  class="smalltext"><span class="red">   3.4594</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.5000</td>
                    <td  class="smalltext">   3.4906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4594</td>
                    <td  class="smalltext">   3.4551</td>
                    <td  class="smalltext">   3.4255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4408</span></td>
                    <td  class="smalltext"><span class="red">   3.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4650</span></td>
                    <td  class="smalltext"><span class="red">   3.4594</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>1</sup>/<sub>2</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.4983</td>
                    <td  class="smalltext">   3.4896</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.4622</td>
                    <td  class="smalltext">   3.4322</td>
                    <td  class="smalltext">   3.4322</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4530</span></td>
                    <td  class="smalltext"><span class="red">   3.4400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4711</span></td>
                    <td  class="smalltext"><span class="red">   3.4639</span></td>
                    <td  class="smalltext"><span class="red">   3.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   3.6221</td>
                    <td  class="smalltext">   3.6039</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5138</td>
                    <td  class="smalltext">   3.5041</td>
                    <td  class="smalltext">   3.4237</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4750</span></td>
                    <td  class="smalltext"><span class="red">   3.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5293</span></td>
                    <td  class="smalltext"><span class="red">   3.5167</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.6250</td>
                    <td  class="smalltext">   3.6068</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5167</td>
                    <td  class="smalltext">   3.5094</td>
                    <td  class="smalltext">   3.4266</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.4646</span></td>
                    <td  class="smalltext"><span class="red">   3.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5262</span></td>
                    <td  class="smalltext"><span class="red">   3.5167</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   3.6223</td>
                    <td  class="smalltext">   3.6073</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5411</td>
                    <td  class="smalltext">   3.5322</td>
                    <td  class="smalltext">   3.4735</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5150</span></td>
                    <td  class="smalltext"><span class="red">   3.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5554</span></td>
                    <td  class="smalltext"><span class="red">   3.5438</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.6250</td>
                    <td  class="smalltext">   3.6100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5438</td>
                    <td  class="smalltext">   3.5371</td>
                    <td  class="smalltext">   3.4762</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5047</span></td>
                    <td  class="smalltext"><span class="red">   3.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5525</span></td>
                    <td  class="smalltext"><span class="red">   3.5438</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.6231</td>
                    <td  class="smalltext">   3.6117</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5690</td>
                    <td  class="smalltext">   3.5626</td>
                    <td  class="smalltext">   3.5239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5530</span></td>
                    <td  class="smalltext"><span class="red">   3.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5793</span></td>
                    <td  class="smalltext"><span class="red">   3.5709</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.6250</td>
                    <td  class="smalltext">   3.6136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5709</td>
                    <td  class="smalltext">   3.5661</td>
                    <td  class="smalltext">   3.5258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5448</span></td>
                    <td  class="smalltext"><span class="red">   3.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5772</span></td>
                    <td  class="smalltext"><span class="red">   3.5709</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.6233</td>
                    <td  class="smalltext">   3.6139</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5827</td>
                    <td  class="smalltext">   3.5769</td>
                    <td  class="smalltext">   3.5488</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5710</span></td>
                    <td  class="smalltext"><span class="red">   3.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5919</span></td>
                    <td  class="smalltext"><span class="red">   3.5844</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.6250</td>
                    <td  class="smalltext">   3.6156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5844</td>
                    <td  class="smalltext">   3.5801</td>
                    <td  class="smalltext">   3.5505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5658</span></td>
                    <td  class="smalltext"><span class="red">   3.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5900</span></td>
                    <td  class="smalltext"><span class="red">   3.5844</span></td>
                    <td  class="smalltext"><span class="red">   3.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0034</td>
                    <td  class="smalltext">   3.7466</td>
                    <td  class="smalltext">   3.7109</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5842</td>
                    <td  class="smalltext">   3.5674</td>
                    <td  class="smalltext">   3.4489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5170</span></td>
                    <td  class="smalltext"><span class="red">   3.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6094</span></td>
                    <td  class="smalltext"><span class="red">   3.5876</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0034</td>
                    <td  class="smalltext">   3.7466</td>
                    <td  class="smalltext">   3.7228</td>
                    <td  class="smalltext">3.7109</td>
                    <td  class="smalltext">   3.5842</td>
                    <td  class="smalltext">   3.5730</td>
                    <td  class="smalltext">   3.4489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5170</span></td>
                    <td  class="smalltext"><span class="red">   3.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6021</span></td>
                    <td  class="smalltext"><span class="red">   3.5876</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.7500</td>
                    <td  class="smalltext">   3.7262</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.5876</td>
                    <td  class="smalltext">   3.5792</td>
                    <td  class="smalltext">   3.4523</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5094</span></td>
                    <td  class="smalltext"><span class="red">   3.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5985</span></td>
                    <td  class="smalltext"><span class="red">   3.5876</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   3.7471</td>
                    <td  class="smalltext">   3.7289</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6388</td>
                    <td  class="smalltext">   3.6290</td>
                    <td  class="smalltext">   3.5487</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6000</span></td>
                    <td  class="smalltext"><span class="red">   3.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6544</span></td>
                    <td  class="smalltext"><span class="red">   3.6417</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.7500</td>
                    <td  class="smalltext">   3.7318</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6417</td>
                    <td  class="smalltext">   3.6344</td>
                    <td  class="smalltext">   3.5516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.5896</span></td>
                    <td  class="smalltext"><span class="red">   3.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6512</span></td>
                    <td  class="smalltext"><span class="red">   3.6417</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   3.7473</td>
                    <td  class="smalltext">   3.7323</td>
                    <td  class="smalltext">3.7248</td>
                    <td  class="smalltext">   3.6661</td>
                    <td  class="smalltext">   3.6571</td>
                    <td  class="smalltext">   3.5985</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6400</span></td>
                    <td  class="smalltext"><span class="red">   3.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6805</span></td>
                    <td  class="smalltext"><span class="red">   3.6688</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.7500</td>
                    <td  class="smalltext">   3.7350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6688</td>
                    <td  class="smalltext">   3.6621</td>
                    <td  class="smalltext">   3.6012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6297</span></td>
                    <td  class="smalltext"><span class="red">   3.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6776</span></td>
                    <td  class="smalltext"><span class="red">   3.6688</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   3.7479</td>
                    <td  class="smalltext">   3.7350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6829</td>
                    <td  class="smalltext">   3.6289</td>
                    <td  class="smalltext">   3.6289</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6630</span></td>
                    <td  class="smalltext"><span class="red">   3.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6940</span></td>
                    <td  class="smalltext"><span class="red">   3.6850</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   3.7481</td>
                    <td  class="smalltext">   3.7367</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6940</td>
                    <td  class="smalltext">   3.6876</td>
                    <td  class="smalltext">   3.6489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6780</span></td>
                    <td  class="smalltext"><span class="red">   3.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7043</span></td>
                    <td  class="smalltext"><span class="red">   3.6959</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.7500</td>
                    <td  class="smalltext">   3.7386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.6959</td>
                    <td  class="smalltext">   3.6911</td>
                    <td  class="smalltext">   3.6508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6698</span></td>
                    <td  class="smalltext"><span class="red">   3.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7022</span></td>
                    <td  class="smalltext"><span class="red">   3.6959</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.7482</td>
                    <td  class="smalltext">   3.7379</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7018</td>
                    <td  class="smalltext">   3.6632</td>
                    <td  class="smalltext">   3.6632</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6880</span></td>
                    <td  class="smalltext"><span class="red">   3.6730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7115</span></td>
                    <td  class="smalltext"><span class="red">   3.7036</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.7483</td>
                    <td  class="smalltext">   3.7389</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7077</td>
                    <td  class="smalltext">   3.7019</td>
                    <td  class="smalltext">   3.6738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6960</span></td>
                    <td  class="smalltext"><span class="red">   3.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7169</span></td>
                    <td  class="smalltext"><span class="red">   3.7094</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.7500</td>
                    <td  class="smalltext">   3.7406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7094</td>
                    <td  class="smalltext">   3.7051</td>
                    <td  class="smalltext">   3.6755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.6908</span></td>
                    <td  class="smalltext"><span class="red">   3.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7150</span></td>
                    <td  class="smalltext"><span class="red">   3.7094</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>3</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   3.7483</td>
                    <td  class="smalltext">   3.7396</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7122</td>
                    <td  class="smalltext">   3.6822</td>
                    <td  class="smalltext">   3.6822</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7030</span></td>
                    <td  class="smalltext"><span class="red">   3.6900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7211</span></td>
                    <td  class="smalltext"><span class="red">   3.7139</span></td>
                    <td  class="smalltext"><span class="red">   3.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0030</td>
                    <td  class="smalltext">   3.8720</td>
                    <td  class="smalltext">   3.8538</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7637</td>
                    <td  class="smalltext">   3.7538</td>
                    <td  class="smalltext">   3.6736</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7250</span></td>
                    <td  class="smalltext"><span class="red">   3.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7795</span></td>
                    <td  class="smalltext"><span class="red">   3.7667</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.8750</td>
                    <td  class="smalltext">   3.8568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7667</td>
                    <td  class="smalltext">   3.7593</td>
                    <td  class="smalltext">   3.6766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7146</span></td>
                    <td  class="smalltext"><span class="red">   3.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7763</span></td>
                    <td  class="smalltext"><span class="red">   3.7667</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   3.8723</td>
                    <td  class="smalltext">   3.8573</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7911</td>
                    <td  class="smalltext">   3.7820</td>
                    <td  class="smalltext">   3.7235</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7650</span></td>
                    <td  class="smalltext"><span class="red">   3.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8056</span></td>
                    <td  class="smalltext"><span class="red">   3.7938</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.8750</td>
                    <td  class="smalltext">   3.8600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.7938</td>
                    <td  class="smalltext">   3.7870</td>
                    <td  class="smalltext">   3.7262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7547</span></td>
                    <td  class="smalltext"><span class="red">   3.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8026</span></td>
                    <td  class="smalltext"><span class="red">   3.7938</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   3.8730</td>
                    <td  class="smalltext">   3.8616</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8189</td>
                    <td  class="smalltext">   3.8124</td>
                    <td  class="smalltext">   3.7738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8030</span></td>
                    <td  class="smalltext"><span class="red">   3.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8294</span></td>
                    <td  class="smalltext"><span class="red">   3.8209</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.8750</td>
                    <td  class="smalltext">   3.8636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8209</td>
                    <td  class="smalltext">   3.8160</td>
                    <td  class="smalltext">   3.7758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7948</span></td>
                    <td  class="smalltext"><span class="red">   3.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8273</span></td>
                    <td  class="smalltext"><span class="red">   3.8209</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-16UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.8732</td>
                    <td  class="smalltext">   3.8638</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8326</td>
                    <td  class="smalltext">   3.8267</td>
                    <td  class="smalltext">   3.7987</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8210</span></td>
                    <td  class="smalltext"><span class="red">   3.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8420</span></td>
                    <td  class="smalltext"><span class="red">   3.8344</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>3<sup>7</sup>/<sub>8</sub>-16UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   3.8750</td>
                    <td  class="smalltext">   3.8656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8344</td>
                    <td  class="smalltext">   3.8300</td>
                    <td  class="smalltext">   3.8005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8158</span></td>
                    <td  class="smalltext"><span class="red">   3.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8401</span></td>
                    <td  class="smalltext"><span class="red">   3.8344</span></td>
                    <td  class="smalltext"><span class="red">   3.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0034</td>
                    <td  class="smalltext">   3.9966</td>
                    <td  class="smalltext">   3.9609</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8342</td>
                    <td  class="smalltext">   3.8172</td>
                    <td  class="smalltext">   3.6989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7670</span></td>
                    <td  class="smalltext"><span class="red">   3.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8597</span></td>
                    <td  class="smalltext"><span class="red">   3.8376</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0034</td>
                    <td  class="smalltext">   3.9966</td>
                    <td  class="smalltext">   3.9728</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8342</td>
                    <td  class="smalltext">   3.8229</td>
                    <td  class="smalltext">   3.6989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7670</span></td>
                    <td  class="smalltext"><span class="red">   3.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8523</span></td>
                    <td  class="smalltext"><span class="red">   3.8376</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   4.0000</td>
                    <td  class="smalltext">   3.9762</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8376</td>
                    <td  class="smalltext">   3.8291</td>
                    <td  class="smalltext">   3.7023</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.7594</span></td>
                    <td  class="smalltext"><span class="red">   3.7290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8487</span></td>
                    <td  class="smalltext"><span class="red">   3.8376</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0030</td>
                    <td  class="smalltext">   3.9970</td>
                    <td  class="smalltext">   3.9788</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8887</td>
                    <td  class="smalltext">   3.8788</td>
                    <td  class="smalltext">   3.7986</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8500</span></td>
                    <td  class="smalltext"><span class="red">   3.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9046</span></td>
                    <td  class="smalltext"><span class="red">   3.8917</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   4.0000</td>
                    <td  class="smalltext">   3.9818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.8917</td>
                    <td  class="smalltext">   3.8843</td>
                    <td  class="smalltext">   3.8016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8396</span></td>
                    <td  class="smalltext"><span class="red">   3.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9014</span></td>
                    <td  class="smalltext"><span class="red">   3.8917</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   3.9973</td>
                    <td  class="smalltext">   3.9823</td>
                    <td  class="smalltext">3.9748</td>
                    <td  class="smalltext">   3.9161</td>
                    <td  class="smalltext">   3.9070</td>
                    <td  class="smalltext">   3.8485</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8900</span></td>
                    <td  class="smalltext"><span class="red">   3.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9307</span></td>
                    <td  class="smalltext"><span class="red">   3.9188</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   4.0000</td>
                    <td  class="smalltext">   3.9850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9188</td>
                    <td  class="smalltext">   3.9120</td>
                    <td  class="smalltext">   3.8512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.8797</span></td>
                    <td  class="smalltext"><span class="red">   3.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9277</span></td>
                    <td  class="smalltext"><span class="red">   3.9188</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   3.9979</td>
                    <td  class="smalltext">   3.9850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9329</td>
                    <td  class="smalltext">   3.9259</td>
                    <td  class="smalltext">   3.8768</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9130</span></td>
                    <td  class="smalltext"><span class="red">   3.8920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9441</span></td>
                    <td  class="smalltext"><span class="red">   3.9350</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   3.9980</td>
                    <td  class="smalltext">   3.9866</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9439</td>
                    <td  class="smalltext">   3.9374</td>
                    <td  class="smalltext">   3.8988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9280</span></td>
                    <td  class="smalltext"><span class="red">   3.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9544</span></td>
                    <td  class="smalltext"><span class="red">   3.9459</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   4.0000</td>
                    <td  class="smalltext">   3.9886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9459</td>
                    <td  class="smalltext">   3.9410</td>
                    <td  class="smalltext">   3.9008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9198</span></td>
                    <td  class="smalltext"><span class="red">   3.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9523</span></td>
                    <td  class="smalltext"><span class="red">   3.9459</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.9982</td>
                    <td  class="smalltext">   3.9879</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9518</td>
                    <td  class="smalltext">   3.9456</td>
                    <td  class="smalltext">   3.9132</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9380</span></td>
                    <td  class="smalltext"><span class="red">   3.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9616</span></td>
                    <td  class="smalltext"><span class="red">   3.9536</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   3.9982</td>
                    <td  class="smalltext">   3.9888</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9576</td>
                    <td  class="smalltext">   3.9517</td>
                    <td  class="smalltext">   3.9237</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9460</span></td>
                    <td  class="smalltext"><span class="red">   3.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9670</span></td>
                    <td  class="smalltext"><span class="red">   3.9594</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>4-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   4.0000</td>
                    <td  class="smalltext">   3.9906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   3.9594</td>
                    <td  class="smalltext">   3.9550</td>
                    <td  class="smalltext">   3.9255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9408</span></td>
                    <td  class="smalltext"><span class="red">   3.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   3.9651</span></td>
                    <td  class="smalltext"><span class="red">   3.9594</span></td>
                    <td  class="smalltext"><span class="red">   4.0000</span></td>

                </tr>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>