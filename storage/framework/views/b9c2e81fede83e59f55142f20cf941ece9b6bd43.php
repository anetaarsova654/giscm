<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2478828590166997",
    enable_page_level_ads: true
  });</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><meta name="keywords" content="Automotive , Aerospace , Construction , Mining , Agriculture , FREE 3D CAD models , Caterpillar , John Deere , CNH , New Holland , Volvo , Komatsu , Hitachi , Liebherr , Doosan Infracore , Sany , XCMG , Terex , 3D CAD , AutoCad , Solidworks , PTC , SurfCad , Catia , VR , Virtual Reality , CAD , CAM , 3D Printing , Hewlett Packard , HP , pdm , plm , cloud based , version control , cad revisions , cad , file management , solidworks epdm , product data management , share cad , collaborate , library , free , cad models , cad drawings , models , open source , solid works , solidedge , autodesk , alibre , autocad , catia , sketchup , keycreator , pro/engineer , spaceclaim , creo , stratasys , 3d printing , 3d printers , additive manufacturing , 3d print , digital manufacturing , product development , 3d printing app , 3d printing software , uprint , fortus , dimension , fdm " /><meta name="description" content="Global Industry Standard CAD Models (GISCM), a resource and social networking platform for engineering students and professionals "/><meta name="keywords" content="GISCM , CAD , CAD drawings , drawings , online CAD drawings , 3D models , SAE standards , ISO standards , JIS standards , 2D drawings , STEP format , o-ring drawings , external hex plug drawings, barbed hose adapters , Hose Adapters , Hose Fittings , Tube Fittings , Tube Adapters , Tube Nuts, 37 Degree fittings , 37 Degree Adapters , JIC Adapters ,  Pipe Adapters , Pipe fittings , Swivel  Adapters , Swivel fittings , 90 degree elbow fittings , Metric plugs , Inch plugs , BSP Plugs , Parker fittings , Parker adapters , Flange adapters , O-ring face seal , oring face seal ORFS , ORFS adapters , O-ring Face Seal adapters , O-ring Face Seal Fittings , O-rings "/><meta name="author" content="GISCM Inc"/><meta name="expires" content="Never"/><meta name="copyright" content="© 2017"/><meta name="designer" content="Chicad Group"/><meta name="publisher" content="GISCM, 2017"/><meta name="classification" content="Engineering Resource and Social Community"/><meta name="revisit-after" content="30 Days"/><meta name="distribution" content="Global"/><meta name="robots" content="INDEX, FOLLOW"/><meta name="GOOGLEBOT" content="INDEX, FOLLOW" /><meta name="rating" content="15-64"/><meta name="location" content="Grand Rapids, Michigan"><meta name="city" content="Chicago, Mumbai, Toronto, Shanghai, London, Hong Kong, Houston"><meta name="country" content="USA, India, China, United Kingdom, Japan"><meta name="rating" content="GENERAL">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>Global Industry Standard CAD Models (GISCM)</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Styles -->
    <link href="<?php echo asset('css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('css/style-custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- animation-effect -->
    <link href="<?php echo asset('css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <?php echo $__env->yieldContent('head'); ?>

</head>
<body>
    <div class="header-top22">
        <div class="wrap">
            <div class="header-top-left">
                <div class="box"></div>
                <div class="clearfix"></div>
            </div>
            <div class="cssmenu">
                <ul>
                 <?php if(auth()->guard()->guest()): ?>
                    <li class="active"><a href="<?php echo e(route('register')); ?>">Sign Up</a></li> |
                    <li><a href="<?php echo e(route('login')); ?>">Log In</a></li>
                 <?php else: ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 55px;">
                            <img src="<?php echo e(Storage::url(Auth::guard('web')->user()->getProfileImage())); ?>" style="width: 34px; height:34px;left: 10px; border-radius: 50%; ">
                            <?php echo e(Auth::user()->firstname.' '.Auth::user()->lastname); ?> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="text" href="<?php echo e(route('editprofile')); ?>">Edit Profile</a>
                            </li>
                            <li>
                                <a class="text" href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                 <?php endif; ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php echo $__env->make('partials.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container">
        <?php if(session()->has('message.level')): ?>
            <div class="alert alert-<?php echo e(session('message.level')); ?>">
                <?php echo session('message.content'); ?>

            </div>
        <?php endif; ?>
    </div>
    <div id="app">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- js -->
    <script src="<?php echo asset('js/jquery-2.2.3.min.js'); ?>"></script>
    <script src="<?php echo asset('js/nav.js'); ?>"></script>
    <script src="<?php echo asset('js/bootstrap.js'); ?>"></script>
    <script src="<?php echo asset('js/wow.min.js'); ?>"></script>
    <script>
        new WOW().init();
    </script>
    <script src="<?php echo mix('js/app.js'); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>
</body>
</html>
