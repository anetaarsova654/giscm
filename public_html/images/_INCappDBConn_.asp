<%
'*************************************************************************
' DO NOT MODIFY THIS SCRIPT IF YOU WANT UPDATES TO WORK!
' Function : Database functions.
' Version  : 5.2
' Modified : May 2007
'*************************************************************************
'	Date					Description
' 10/29/05	Changed how dbLocked is handled in openDB
' 06/28/08  Added test for SQL injection attack to openRSexecute
' 07/09/08  Changed test for SQL Inject attack to openRSexecut and openRSopen 
'           to use base code written by Dale of CPMods.com with permission
'*************************************************************************
'*************************************************************************
'Declare some standard ADO variables
'*************************************************************************
Const adOpenKeyset		= 1
Const adOpenDynamic		= 2
Const adOpenStatic		= 3
Const adLockReadOnly	= 1
Const adLockOptimistic	= 3
Const adStateClosed		= &H00000000
Const adUseServer		= 2
Const adUseClient		= 3
Const adCmdText			= &H0001
Const adCmdTable		= &H0002

'*************************************************************************
'Open Database Connection
'*************************************************************************
function openDB()
	on error resume next
	set connTemp = server.createobject("adodb.connection")
    'Dim connString = "provider=sqloledb;Data Source=MSSQLSERVER2012;User Id=mallmon;Password=D@vids45;Initial Catalog=GISCMNET"
	connTemp.Open connString
	 
	
	

	if err.number <> 0 then
		dim errMsg
		errMsg = "" _
			& "<b>Module :</b> scripts/_INCappDBConn_.asp : openDB()<br><br>" _
			& "<b>Number :</b> " & err.number & "<br><br>" _
			& "<b>Page :</b> "   & Request.ServerVariables("PATH_INFO") & "<br><br>" _
			& "<b>Desc :</b> "   & err.Description  
		call errorDB("",errMsg)
	end if
	on error goto 0
	if UCase(dbLocked) = "Y" then
		call errorDB("<b>" & LangText("ErrStoreClosed","") & "</b>","")
		call closeDB()

	end if
end function

'*************************************************************************
'Close Database Connection
'*************************************************************************
function closeDB()
	on error resume next
	connTemp.close
	set connTemp = nothing
	on error goto 0
end function

' ************************************************************************
' Support routines to stop SQL Injection Attacks
' ************************************************************************
function findExploit(str)
    Dim testStr
    dim exploit
    dim exploitArray
    dim i
   
    exploit = ";drop| drop |cast|exec|create "
    exploitArray = split(exploit,"|")
    for i = 0 to UBound(exploitArray)
        if instr(LCase(str),exploitArray(i)) > 0 then
            findExploit = true
            exit function
        end if
    next
    findExploit = false 
end function

Function stripText(strText)
'Strips the text expressions from stripText

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "([""'])(?:\\\1|[\S\s])*?\1"

  'Replace all text expression matches with the empty string
  strOutput = objRegExp.Replace(strText, "")
 
  stripText = strOutput    'Return the value of strOutput
  Set objRegExp = Nothing
End Function

'*************************************************************************
'Open RecordSet using "Execute" method
'*************************************************************************
function openRSexecute(mySQL)

  if findExploit(stripText(mySQL)) then
    dim exploitMsg
    exploitMsg = "Hacking attempt has been detected from <strong>" & Request.ServerVariables("REMOTE_ADDR") & "</strong> using the <strong>" & Request.ServerVariables("HTTP_REFERER") & "</strong> page <br /> <br /> " & mySQL
    response.Redirect "notify.asp?errMsg=" & server.URLEncode(exploitMsg)
  end if  
	on error resume next
	set openRSexecute = conntemp.execute(mySQL)
	if err.number <> 0 then
		dim errMsg
		errMsg = "" _
			& "<b>Module :</b> scripts/_INCappDBConn_.asp : openRSexecute(mySQL)<br><br>" _
			& "<b>Number :</b> " & err.number & "<br><br>" _
			& "<b>Page :</b> "   & Request.ServerVariables("PATH_INFO") & "<br><br>" _
			& "<b>Desc :</b> "   & err.Description & "<br><br>" _
			& "<b>SQL :</b> "    & mySQL
		call errorDB("",errMsg)
	end if
	on error goto 0
end function

'*************************************************************************
'Open RecordSet using "Open" method
'*************************************************************************
function openRSopen(dbSource,dbCursorLoc,dbCursorType,dbLockType,dbOptions,dbCache)

  if findExploit(stripText(mySQL)) then
    dim exploitMsg
    exploitMsg = "Hacking attempt has been detected from <strong>" & Request.ServerVariables("REMOTE_ADDR") & "</strong> using the <strong>" & Request.ServerVariables("HTTP_REFERER") & "</strong> page <br /> <br /> " & mySQL
    response.Redirect "notify.asp?errMsg=" & server.URLEncode(exploitMsg)
  end if  

	on error resume next
	set openRSopen = Server.CreateObject("ADODB.Recordset")
	if dbCache > 0 then
		openRSopen.CacheSize = dbCache
	end if
	if dbCursorLoc > 0 then
		openRSopen.CursorLocation = dbCursorLoc
	end if
	openRSopen.Open dbSource,connTemp,dbCursorType,dbLockType,dbOptions
	if err.number <> 0 then
		dim errMsg
		errMsg = "" _
			& "<b>Module :</b> scripts/_INCappDBConn_.asp : openRSopen(dbSource,dbCursorLoc,dbCursorType,dbLockType,dbOptions,dbCache)<br><br>" _
			& "<b>Number :</b> " & err.number & "<br><br>" _
			& "<b>Page :</b> "   & Request.ServerVariables("PATH_INFO") & "<br><br>" _
			& "<b>Desc :</b> "   & err.Description & "<br><br>" _
			& "<b>SQL :</b> "    & dbSource
		call errorDB("",errMsg)
	end if
	on error goto 0
end function

'*************************************************************************
'Close Recordset
'*************************************************************************
function closeRS(rs)
	on error resume next
	rs.Close
	set rs = nothing
	on error goto 0
end function

'*************************************************************************
'Handle database errors
'*************************************************************************
sub errorDB(errMsgShow,errMsgHide)
	
	'Clear output buffer and declare work variables
	Response.Clear
	dim errMsg
	dim hideError
		
	'Decide which error to display, and if we must hide the error
	if len(trim(errMsgShow)) > 0 then
		errMsg		= trim(errMsgShow)
		hideError	= false
	else
		errMsg		= trim(errMsgHide)
		hideError	= true
	end if
	
	'Force detailed error to be displayed if debug mode is on
	on error resume next
	if UCase(debugMode) = "Y" then
		if err.number = 0 then
			hideError = false
		end if
	end if
	on error goto 0
%>
	<HTML>
	<HEAD></HEAD>
	<BODY>
		<P align=center>
			<br><br><br>
			<font face="verdana,arial" size="2" color=red>
				<b>System Error</b>
			</font><br><br>
			<table border="1" bgcolor="#EEEEEE" cellpadding="15" width="50%"><tr><td align=left>
				<font face="verdana,arial" size="2">
<%
					if hideError then
%>
						Note : The detail of this error can be 
						viewed by activating debug mode for 
						this store.
<%
					else
						Response.Write errMsg
					end if
%>
				</font>
			</td></tr></table>
		</P>
	</BODY>
	</HTML>
<%
	'Close open database connections and end
	call closeDB()

	Response.End
end sub
%>
