<h3 class="future">CATEGORIES</h3>
<div class="list-group">
    
    <a href="<?php echo e(route('cadmodels.index')); ?>" class="list-group-item">All Categories</a>
    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a class="list-group-item" href="<?php echo e(route('cadmodels.index')); ?>?category=<?php echo e($category->id); ?>"><?php echo e($category->name); ?></a>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>