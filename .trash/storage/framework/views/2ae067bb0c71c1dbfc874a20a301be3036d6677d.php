<div class="comments-section">
    <h5>Comments:</h5>
    <?php if($cadmodel->comments): ?>
        <?php $__currentLoopData = $cadmodel->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="media">
                <div class="media-left">
                    <img src="<?php echo e(Storage::url($comment->user->getProfileImage())); ?>" class="media-object" style="width:60px">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo e($comment->user->firstname." ".$comment->user->lastname); ?></h4>
                    <p class="rating-text text-right"><b>Rating: <?php echo e($comment->rating." "); ?>stars</b></p>
                    <p><?php echo e($comment->created_at->format('d-M-Y')); ?></p>
                    <p class="smmary"><b>Summary: </b><?php echo e($comment->summary); ?></p>
                    <p class="comment"><b>Comment: </b><?php echo e($comment->comment); ?></p>
                </div>
            </div>
            <hr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <p>No Comments Found for this Model</p>
    <?php endif; ?>
</div>

