<?php $__env->startSection('content'); ?>
    <div class="container contributor-profile-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                       <div class="profile-sidebar">
                           <div class="profile-image">
                               <a id="" href="">
                                   <img alt="Profile Image" class="userProfile" src="<?php echo e(Storage::url($user->getProfileImage())); ?>" style="width: 150px;border-width:1px;border-style:solid;">
                               </a>
                           </div>
                           <div>
                               <hr></hr>
                           </div>
                           <div class="profile-informaion">
                               <p><b>Name:</b> <?php echo e($user->firstname." ".$user->lastname); ?></p>
                               <p><b>Title:</b> <?php echo e($user->classification); ?></p>
                               <p><b>About</b>: <?php echo e($user->about); ?></p>
                               <p><b>Skills</b>: <?php echo e($user->skills); ?></p>
                               <p><b>Interests</b>: <?php echo e($user->interests); ?></p>
                               <p><b>Website</b>: <?php echo e($user->website); ?></p>
                           </div>
                           <div>
                               <hr></hr>
                           </div>
                           <div class="profile-contribution">
                               <h4 class="blueHeading"><?php echo e($user->firstname." ".$user->lastname); ?> on GISCM</h4>
                               <p><b>User since:</b> <?php echo e($user->created_at); ?></p>
                               <p><b>Contributions</b>: <?php echo e(count($cadmodels)); ?></p>
                           </div>
                       </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="contributions">
                            <div class="contribution-header">
                                <h4 class="contribution-heading" align="left">Contributions</h4>
                                
                                <div class="form-group contribution-categories">
                                    <?php echo Form::open(['url' => route('members.show',$user->id),'class'=>'form-horizontal','method' => 'GET','id'=>'category-form']); ?>

                                            <?php echo e(Form::select('category',$categories, null, ['class'=>'form-control','id'=>'category','placeholder' => 'All'])); ?>

                                    <?php echo Form::close(); ?>

                                </div>
                            </div>
                            <div>
                                <?php $__empty_1 = true; $__currentLoopData = array_chunk($cadmodels->items(),4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadmodel_chunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <div class="row">
                                        <?php $__currentLoopData = $cadmodel_chunk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadmodel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-sm-3">
                                                <div class="contribution-content">
                                                    <div class="contributor-model">
                                                        <div class="model-image">
                                                            <a href="<?php echo e(route('cadmodels.show',$cadmodel['id'])); ?>"><img alt="Model Image" src="<?php echo e(Storage::url($cadmodel['image_url'])); ?>" class="" style="width: 100px"></a>
                                                        </div>
                                                        <br>
                                                        <div class="model-name" >
                                                            <a href="<?php echo e(route('cadmodels.show',$cadmodel['id'])); ?>" class=""><b><?php echo e($cadmodel['name']); ?></b></a><br>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <p>No Contributions Available...</p>
                                <?php endif; ?>
                                <?php echo e($cadmodels->appends(request()->except('page'))->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>


        $('#category').change(function(){

            if(this.value == ''){
                console.log(this.value);

                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);

                    window.location.href = clean_uri;
                }
            }
            else{
                document.getElementById("category-form").submit();
            }


        });


    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>