<?php $__env->startSection('content'); ?>
    <section class="content">
        <!-- SECTION 1 -->
        <div class="box box-default">
            <div class="box-header with-border text-center">
                <h1 class="box-title"><b>User Information</b></h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <p><?php echo e($user->firstname." ".$user->lastname); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <p><?php echo e($user->email); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Classification</label>
                            <div class="col-sm-9">
                                <p><?php echo e($user->classification); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">IP Address</label>
                            <div class="col-sm-9">
                                <p><?php echo e($user->ip_address); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">IP Location</label>
                            <div class="col-sm-9">
                                <p><?php echo e($ip_city); ?></p>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>