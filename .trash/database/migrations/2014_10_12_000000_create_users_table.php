<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('company')->nullable()->default(null);
            $table->string('classification');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image_url')->nullable()->default(null);
            $table->string('skills')->nullable()->default(null);
            $table->string('interests')->nullable()->default(null);
            $table->string('about')->nullable()->default(null);
            $table->string('website')->nullable()->default(null);
            $table->string('address');
            $table->string('phone');
            $table->string('country');
            $table->string('zipcode');
            $table->string('city');
            $table->string('state');
            $table->string('ip_address')->nullable()->default(null);
            $table->boolean('disclaimer_status')->default(0);
            $table->boolean('activation_status')->default(0);
            $table->string('token',254)->nullable();
            $table->string('stripe_id')->nullable()->default(null);
            $table->string('card_brand')->nullable()->default(null);
            $table->string('card_last_four')->nullable()->default(null);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
