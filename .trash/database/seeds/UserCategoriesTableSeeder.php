<?php

use Illuminate\Database\Seeder;

class UserCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\UserCategory::create([
            'name'=> 'Automation',
        ]);

        \App\UserCategory::create([
            'name'=> 'Product',
        ]);

       \App\UserCategory::create([
            'name'=> 'Electrical',
        ]);

        \App\UserCategory::create([
            'name'=> 'Civil',
        ]);

        \App\UserCategory::create([
            'name'=> 'Hydraulic',
        ]);

        \App\UserCategory::create([
            'name'=> 'Test',
        ]);

        \App\UserCategory::create([
            'name'=> 'Aerospace',
        ]);

        \App\UserCategory::create([
            'name'=> 'Agriculture',
        ]);

        \App\UserCategory::create([
            'name'=> 'Automotive',
        ]);

        \App\UserCategory::create([
            'name'=> 'Chemical',
        ]);

        \App\UserCategory::create([
            'name'=> 'Computer',
        ]);

        \App\UserCategory::create([
            'name'=> 'Biomedical',
        ]);

        \App\UserCategory::create([
            'name'=> 'Environmental',
        ]);

        \App\UserCategory::create([
            'name'=> 'Geological and Mining',
        ]);

        \App\UserCategory::create([
            'name'=> 'Marine',
        ]);

        \App\UserCategory::create([
            'name'=> 'Petroleum',
        ]);

        \App\UserCategory::create([
            'name'=> 'Industrial',
        ]);

        \App\UserCategory::create([
            'name'=> 'Nuclear',
        ]);

        \App\UserCategory::create([
            'name'=> 'Structural',
        ]);

        \App\UserCategory::create([
            'name'=> 'Telecommunications',
        ]);

        \App\UserCategory::create([
            'name'=> 'Transportation',
        ]);
    }
}
