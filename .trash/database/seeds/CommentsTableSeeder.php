<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0;$i<15;$i++)
        {
            DB::table ('comments')->insert([
                'user_id'=> $faker->numberBetween(1,2),
                'cad_model_id'=> $faker->numberBetween(1,13),
                'rating'=> $faker->numberBetween(1,5),
                'summary'=> $faker->sentence(),
                'comment'=> $faker->sentence(),
                'created_at'=> \Carbon\Carbon::now()->format('YmdHis'),
                'updated_at'=> \Carbon\Carbon::now()->format('YmdHis'),

            ]);
        }
    }
}
