<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table ('categories')->insert([
            'name'=> 'Medical',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Healthcare',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Machine',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Automation',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Hydraulic',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Automotive',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Aerospace',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Industrial',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Computer',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Custom',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Construction',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Agriculture',
        ]);

        DB::table ('categories')->insert([
            'name'=> 'Locomotive',
        ]);


    }
}
