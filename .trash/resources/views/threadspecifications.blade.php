@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel">
                    <a href="">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <form action="/scripts/prodList.asp" method="post" id="searchBox" name="searchBox">
                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></div>
                <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
                <div class="col-md-12 col-xs-12">
                    <h2 class="subheading"><b>UN/UNF THREADS</b></h2>
                    <ul class="list-group links">
                        <li class="list-group-item"><a href="">Inch Thread sizes from 0-80 UNF to ¼-56 UNS</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 5/16-18 UNC to 9/16 -32 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 5/8-11 UNC to 7/8-32 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 15/16-12 UN to 1 3/16-28 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 1 5/8-6 UN to 1 15/16-20 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 2-4½ UNC to 2¾-20 UN</a></li>
                        <li class="list-group-item"><a href="">Inch Thread sizes from 2-7/8-6 UN to 4-16 UN</a></li>
                    </ul>
                    <h2 class="subheading"><b>Metric Threads</b></h2>
                    <ul class="list-group links">
                        <li class="list-group-item"><a href="">Metric Threads</a></li>
                    </ul>
                    <h2 class="subheading"><b>NPTF Threads</b></h2>
                    <ul class="list-group links">
                        <li class="list-group-item"><a href="">NPT Hole Sizes</a></li>
                        <li class="list-group-item"><a href="">External Threads</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
