<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Global Industry Standard CAD Models (GISCM)</title>

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

        <!-- //for-mobile-apps -->
        <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
        <link href="{!! asset('css/bootstrap.css') !!}" rel="stylesheet">
        <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
        <!-- animation-effect -->
        <link href="{!! asset('css/animate.min.css') !!}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <!-- banner -->
        <div class="banner">
            <div class="container">
                <div class="head-top wow fadeInLeft animated" data-wow-delay=".5s">
                    <div class="navigation">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <nav class="cl-effect-11">
                                    <ul class="nav navbar-nav navbar-style">
                                        <li><a href="{{route('home')}}" data-hover="Home">Home</a></li>
                                        {{--<li><a href="#aboutus" data-hover="About">About</a></li>--}}
                                        <li><a href="{{route('members.index')}}" data-hover="Members">Members </a></li>
                                        {{--<li><a href="{{route('library')}}" data-hover="CAD Library">CAD Library </a></li>--}}
                                        <li><a href="{{route('cadmodels.index')}}" data-hover="Community">Community</a></li>
                                        <li><a href="{{ route('cadmodels.create') }}" data-hover="Upload">Upload</a></li>
                                        @if(Auth::check())
                                            <li><a href="{{ route('dashboard') }}" data-hover="My Dashboard">My Dashboard</a></li>
                                        @else
                                            <li><a href="{{ route('login') }}" data-hover="Login">Login</a></li>
                                            <li><a href="{{ route('register') }}" data-hover="Sign Up">Sign Up</a></li>
                                        @endif
                                        <li><a href="{{ route('contactus') }}" data-hover="Contact">Contact</a></li>
                                        @if(Auth::check())
                                            <li>
                                                <a data-hover="Logout" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="logo wow fadeInRight animated" data-wow-delay=".5s">
                    <img src="/images/giscm.png" width="301" height="78" longdesc="/index.html"> </div>
                <h1 class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0"><p style="text-align:center">Students...Designers...Engineers...simplify your life™</p></h1>
                <p class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">Engineering Community</p>
            </div>
        </div>

        <!-- aboutus -->
        {{--<div class="about-section" id="aboutus">
            <div class="about agile-44">
                <div class="container">
                    <div class="about-top heading">
                        <h2>About Us</h2>
                    </div>
                    <div class="about-bottom text-center">
                        <!-- w3l -->
                        <div class="w3l">
                            <div class="container">
                                <h3>Industry specifications in a 3D CAD model</h3>
                                <p class="wt">A college engineering student project internet search for industry standard components produced thousands of manufacturers; however, ninety eight percent of those manufacturers would not share 3D CAD models with the engineering students UNLESS products were purchased. So, the team purchased the required standards to design the components themselves. Component manufacturers rejection to share CAD models led to establishing Global Industry Standard CAD Models (GISCM) a FREE CAD Model resource for engineers, designers, enthusists, hobbyists, ie.. for industry standard CAD models. There are no gimmicks nor advertisements to click through. Just register on our website and search for components you need. Then download the CAD models you need...just that simple. If you donot see the component you are searching for in our catalog, please contact us to request our assistance to locate or design the component. Simplify your life! </p>
                                <div class="wthree_w3l_grids">
                                    <div class="col-md-6 wthree_w3l_grid wow fadeInLeft animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="col-xs-4 wthree_w3l_grid-left">
                                            <div class="wthree_w3l_grid-left1">
                                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-8 wthree_w3l_grid-right">
                                            <h4>Automotive</h4><img src="images/Fittings.jpg" alt="Image" width="150" height="80" class="img-responsive" style="width:100%"> <p>CAD Models designed from SAE, ISO, DIN, and other industry standards for automotive applications. </p>

                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="col-md-6 wthree_w3l_grid wow fadeInRight animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInRight;">
                                        <div class="col-xs-4 wthree_w3l_grid-left">
                                            <div class="wthree_w3l_grid-left1">
                                                <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-8 wthree_w3l_grid-right">
                                            <h4>Construction</h4>
                                            <img src="images/construction.png" alt="Image" width="150" height="80" class="img-responsive" style="width:100%"> <p>CAD Models designed from SAE, ISO, DIN, and other industry standards for construction and heavy equipment applications.
                                            </p></div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="wthree_w3l_grids">
                                    <div class="col-md-6 wthree_w3l_grid wow fadeInLeft animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="col-xs-4 wthree_w3l_grid-left">
                                            <div class="wthree_w3l_grid-left1">
                                                <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-8 wthree_w3l_grid-right">
                                            <h4>Agriculture</h4>
                                            <img src="images/ag.png" alt="Image" width="150" height="80" class="img-responsive" style="width:100%"><p>CAD Models designed from SAE, ISO, DIN, and other industry standards for agricultural equipment applications.</p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="col-md-6 wthree_w3l_grid wow fadeInRight animated animated" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeInRight;">
                                        <div class="col-xs-4 wthree_w3l_grid-left">
                                            <div class="wthree_w3l_grid-left1">
                                                <span class="glyphicon glyphicon-oil" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-8 wthree_w3l_grid-right">
                                            <h4>Mining, Oil &amp; Gas</h4>
                                            <img src="images/mining.png" alt="Image" width="150" height="80" class="img-responsive" style="width:100%"><p>CAD Models designed from SAE, ISO, DIN, and other industry standards for mining and Oil &amp; Gas applications.</p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </div>
                        <!-- w3l -->
                        <!-- agile -->
                        <div class="agile">
                            <div class="container">
                                <div class="agile-botom animated wow fadeInUp animated animated animated" data-wow-duration="0" data-wow-delay="0" style="visibility: visible; animation-name: fadeInUp;">
                                    <h3 class="zoomOutUp">Simplifying Industry Standards</h3>
                                    <p class="tag-line">Models are designed and saved to formats used in all popular CAD programs</p>
                                </div>
                            </div>
                        </div>
                        <!-- agile -->
                        <!-- w3ls -->
                        <div class="w3ls">
                            <div class="container">
                                <div class="w3ls-top heading">
                                    <h3>Industry standards in 3D CAD model format</h3>
                                    <p class="wt">Designers, Engineers, Students...Hobbyists....simplify your life.</p>
                                </div>
                                <div class="w3ls-bottom">
                                    <div class="w3ls-one">
                                        <div class="col-md-6 w3ls-left wow fadeInLeft animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInLeft;">
                                            <div class="ad-left">
                                                <img src="images/5.jpg" alt="">
                                            </div>
                                            <div class="ad-right">
                                                <h4>Easily import into CAD program</h4>
                                                <p>We utilize .STEP and .STL file formats for easy import into solid modeling and 3D printer programs</p>

                                                <span></span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 w3ls-left wow fadeInRight animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInRight;">
                                            <div class="ad-left">
                                                <img src="images/6.jpg" alt="">
                                            </div>
                                            <div class="ad-right">
                                                <h4>3D model to 2D drawing ready</h4>
                                                <p>Quickly and easily make 2D drawings for machining and/or assembly operations </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="w3ls-two">
                                        <div class="col-md-6 w3ls-left wow fadeInLeft animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInLeft;">
                                            <div class="ad-left">
                                                <img src="images/7.jpg" alt="">
                                            </div>
                                            <div class="ad-right">
                                                <h4>Drag and Drop 3D model into assembly files</h4>
                                                <p>Importing 3D models into equipment operations within your favorite CAD program </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 w3ls-left wow fadeInRight animated animated" data-wow-delay="0" style="visibility: visible; animation-name: fadeInRight;">
                                            <div class="ad-left">
                                                <img src="images/8.jpg" alt="">
                                            </div>
                                            <div class="ad-right">
                                                <h4>Complete your system FEA or Testing</h4>
                                                <p>Including industry standard 3D models into systems can cut design and testing time down considerably</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}

        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-main">
                    <div class="col-md-3 footer-left wow fadeInLeft animated" data-wow-delay=".5s">
                        <h4>Information</h4>
                        <ul>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=2','policy2','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Disclaimer</a></li>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=5','policy5','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Privacy</a></li>
                            <li><a href="#" onClick="MM_openBrWindow('Scripts/openPolicy.asp?policy=7','policy7','scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Terms &amp; Conditions</a></li>
                            <!-- <li><a href="#" onClick="MM_openBrWindow('Scripts/ContactUs.asp''scrollbars=yes,resizable=yes,width=600,height=400,top=100,left=100,')">Contact</a></li> -->
                            <li><a href="{{route('contactus')}}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-left wow fadeInLeft animated" data-wow-delay=".5s"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy">
            <div class="container">
                <p class="animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">&copy 2016-2017 GISCM Inc. GISCM is a registered trademark. All rights reserved.
                <div class="copy-rights animated wow fadeInUp animated animated" data-wow-duration="0" data-wow-delay="0">
                    <ul>
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><span class="fa"> </span></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><span class="tw"> </span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- //footer -->
        <!-- js -->
        <script src="{!! asset('js/jquery-1.12.4.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('js/wow.min.js') !!}"></script>
        <script src="{!! asset('/js/slideshow.js') !!}"></script>

    </body>
</html>
