<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="col-md-4 footer-in">
                <h4><!--<i> </i>--></h4>
                <p></p>
            </div>
            <div class="col-md-4 footer-in">
                <!--<h4><i class="cross"> </i>Suspendisse sed</h4>-->
            </div>
            <div class="col-md-4 footer-in">
                <!--<h4><i class="down"> </i>Suspendisse sed</h4>-->
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!---->
    <div class="footer-middle">
        <div class="container">
            <div class="footer-middle-in">
                <h6>About us</h6>
                <p>CAD Models designed from SAE, ISO, DIN, and other industry standards for automotive, agricultural, construction and heavy equipment, mining and Oil &amp; Gas applications.</p>
            </div>
            <div class="footer-middle-in">
                <h6>Information</h6>
                <ul>
                    {{--<li><a href="{{route('home','#aboutus')}}">About Us</a></li>--}}
                    <li><a href="#" >Disclaimer</a></li>
                    <li><a href="#" >Privacy Policy</a></li>
                    <li><a href="#" >Terms &amp; Conditions</a></li>
                </ul>
            </div>
            <div class="footer-middle-in">
                <h6>Customer Service</h6>
                <ul>
                    <li><a href="{{route('contactus')}}">Contact Us</a></li>
                    <li><a href="{{route('contactus')}}">Request 3D Model or Drawing</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <p class="footer-class">Copyright2017 &nbsp;GISCM, INC&nbsp;&nbsp;-&nbsp;Powered by <a href="">&nbsp;GISCM INC</a> </p>
    <a href="#" id="toTop" style="display: none;"><span id="toTopHover" style="opacity: 0;"></span> <span id="toTopHover" style="opacity: 1;"> </span></a>
</div>