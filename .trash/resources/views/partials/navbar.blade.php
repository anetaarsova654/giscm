<!--header-->
<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-top-in">
                <div class="logo">
                    <a href="https://giscm.net"><img src="../images/giscm2.png" alt=" "></a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="h_menu4">
                <a class="toggleMenu" href="#" style="display: none;">Menu</a>
                <ul class="nav">
                    <li class=""><a href="{{route('home')}}"><i> </i>Home</a></li>
                    <li class=""><a href="{{ route('members.index') }}">Members</a></li>
                   {{-- <li><a class="primary-link"  href="#">Find</a>
                        <ul class="secondary-list">
                            <li><a class="secondary-link" href="{{ route('contributors.index') }}">Contributor's Community</a></li>
                            <li><a class="secondary-link" href="#">Parts and Assemblies</a></li>
                        </ul>
                    </li>--}}
                    <li class=""><a href="{{route('cadmodels.create')}}">Upload</a></li>
                    <li class=""><a href="{{route('cadmodels.index')}}">Community</a></li>
                    @if(Auth::guard('web')->check())
                    <li class=""><a href="{{route('dashboard')}}">My Dashboard</a></li>
                    @endif
                    <li class=""><a href="{{ route('chats.index') }}">Chat</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>