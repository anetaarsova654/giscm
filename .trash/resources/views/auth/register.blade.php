@extends('layouts.app')

@section('head')
    <link href="{!! asset('css/AdminLTE.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="header-bottom-on">
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-9">
                <!-- Outer Table Cell -->
                <table  border="0" cellpadding="0" cellspacing="0" width="350">
                    <tbody>
                    <tr>
                        <td>
                            <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            <!-- Inner Table -->
                            <table class="register-form" border="0" cellspacing="0" cellpadding="2" width="100%">

                                <!-- General Info -->
                                <tbody>
                                <tr>
                                    <td colspan="2" valign="middle" class="CPpageHead">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td nowrap="" align="left">
                                                    <b>Profile Information</b>
                                                </td>
                                                <td nowrap="" align="right">
                                                    <b><font color="#800000">New Account</font></b>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><br></td>
                                </tr>
                                {{ csrf_field() }}
                                <tr>
                                    <td nowrap="">First Name </td>
                                    <td><input type="text" name="firstname" size="30" maxlength="70" value="{{ old('firstname') }}" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Last Name </td>
                                    <td><input type="text" name="lastname" size="30" maxlength="50" value="{{ old('lastname') }}" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Company </td>
                                    <td><input type="text" name="company" size="30" maxlength="50"></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Classification</td>
                                    <td>
                                        <select name="classification" size="1" required>
                                            <option value="Student" >Student</option>
                                            <option value="Professional" >Professional</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="">Email </td>
                                    <td><input type="email" name="email" size="30" maxlength="50" value="{{ old('email') }}" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Categories</td>
                                    <td>
                                        <select name="usercategories[]" class="select2" multiple="multiple" data-placeholder="Select Categories " style="width: 100%;" required>
                                            @foreach($user_categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="">Password </td>
                                    <td><input type="password" name="password" size="10" maxlength="10" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Profile Image</td>
                                    <td><input type="file" name="image" size="10" maxlength="10" ></td>
                                </tr>

                                <!-- General Information -->

                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="middle" class="CPpageHead">
                                        <b>General Information</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><br></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Skills </td>
                                    <td><textarea type="text" name="skills" ></textarea></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Interests </td>
                                    <td><textarea type="text" name="interests"></textarea> </td>
                                </tr>
                                <tr>
                                    <td nowrap="">About </td>
                                    <td><textarea type="text" name="about"></textarea></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Website </td>
                                    <td><input type="text" name="website" size="30" maxlength="50"></td>
                                </tr>

                                <!-- Contact Information -->

                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="middle" class="CPpageHead">
                                        <b>Contact Information</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><br></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Address </td>
                                    <td><input type="text" name="address" size="30" maxlength="70"  required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Phone </td>
                                    <td><input type="text" name="phone" size="30" maxlength="30" value="" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">Country </td>
                                    <td>
                                        <select name="country" size="1" required>
                                            @include('auth.partials._countries')
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="">Zip/PostCode </td>
                                    <td><input type="number"  name="zipcode" size="10" maxlength="10" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">City </td>
                                    <td><input type="text" name="city" size="30" maxlength="50" required></td>
                                </tr>
                                <tr>
                                    <td nowrap="">State/Province </td>
                                    <td>
                                        <select class="states" name="state" size="1" required>
                                            @include('auth.partials._states')
                                        </select>
                                    </td>
                                </tr>
                                <tr class="hidden state-input">
                                    <td>&nbsp;</td>
                                    <td>State/Prov. not listed? Enter it below.</td>
                                </tr>
                                <tr class="hidden state-input">
                                    <td align="right">&nbsp;&nbsp;</td>
                                    <td><input class="second-state" type="text" name="custom_state" size="30" maxlength="100" value=""></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input class ="registerbtn" type="submit" name="Submit" value="Register"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                </tbody>

                            </table>
                            </form>
                            <!-- End Outer Table Cell -->
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{!! asset('js/select2.full.min.js') !!}"></script>
    <script src="{!! asset('js/app.min.js') !!}"></script>
    <script src="{!! asset('js/demo.js') !!}"></script>
    <script>
        $(function () {
            $(".select2").select2();
            $(".select2").select2({
                maximumSelectionLength: 5
            });
        });



        $('.states').change(function(){
            var selected_option = $('.states').val();
            if (selected_option === 'other')
            {
                $('.state-input').removeClass('hidden');
            }
            else
            {
                $('.state-input').addClass('hidden');
            }
        });

    </script>
@endsection