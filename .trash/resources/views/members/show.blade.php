@extends('layouts.app')

@section('content')
    <div class="container contributor-profile-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                       <div class="profile-sidebar">
                           <div class="profile-image">
                               <a id="" href="">
                                   <img alt="Profile Image" class="userProfile" src="{{ Storage::url($user->getProfileImage())}}" style="width: 150px;border-width:1px;border-style:solid;">
                               </a>
                           </div>
                           <div>
                               <hr></hr>
                           </div>
                           <div class="profile-informaion">
                               <p><b>Name:</b> {{$user->firstname." ".$user->lastname}}</p>
                               <p><b>Title:</b> {{$user->classification}}</p>
                               <p><b>About</b>: {{$user->about}}</p>
                               <p><b>Skills</b>: {{$user->skills}}</p>
                               <p><b>Interests</b>: {{$user->interests}}</p>
                               <p><b>Website</b>: {{$user->website}}</p>
                           </div>
                           <div>
                               <hr></hr>
                           </div>
                           <div class="profile-contribution">
                               <h4 class="blueHeading">{{$user->firstname." ".$user->lastname}} on GISCM</h4>
                               <p><b>User since:</b> {{$user->created_at}}</p>
                               <p><b>Contributions</b>: {{count($cadmodels)}}</p>
                           </div>
                       </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="contributions">
                            <div class="contribution-header">
                                <h4 class="contribution-heading" align="left">Contributions</h4>
                                {{--<h4 class="contribution-categories" align="right">Categories</h4>--}}
                                <div class="form-group contribution-categories">
                                    {!!  Form::open(['url' => route('members.show',$user->id),'class'=>'form-horizontal','method' => 'GET','id'=>'category-form']) !!}
                                            {{Form::select('category',$categories, null, ['class'=>'form-control','id'=>'category','placeholder' => 'All'])}}
                                    {!!  Form::close() !!}
                                </div>
                            </div>
                            <div>
                                @forelse(array_chunk($cadmodels->items(),4) as $cadmodel_chunk )
                                    <div class="row">
                                        @foreach($cadmodel_chunk as $cadmodel)
                                            <div class="col-sm-3">
                                                <div class="contribution-content">
                                                    <div class="contributor-model">
                                                        <div class="model-image">
                                                            <a href="{{route('cadmodels.show',$cadmodel['id'])}}"><img alt="Model Image" src="{{ Storage::url($cadmodel['image_url']) }}" class="" style="width: 100px"></a>
                                                        </div>
                                                        <br>
                                                        <div class="model-name" >
                                                            <a href="{{route('cadmodels.show',$cadmodel['id'])}}" class=""><b>{{ $cadmodel['name'] }}</b></a><br>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @empty
                                    <p>No Contributions Available...</p>
                                @endforelse
                                {{ $cadmodels->appends(request()->except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>


        $('#category').change(function(){

            if(this.value == ''){
                console.log(this.value);

                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);

                    window.location.href = clean_uri;
                }
            }
            else{
                document.getElementById("category-form").submit();
            }


        });


    </script>

@endsection
