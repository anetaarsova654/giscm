@extends('layouts.app')

@section('content')
    <div class="container contributors-content">
        <div class="row">
            <div class="contributor-page-header">
                <h2>Members' Directory on GISCM</h2>
               {{-- <div class="thumbnail-pagination">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span class="page-link">Previous</span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                              <span class="page-link">
                                2
                                <span class="sr-only">(current)</span>
                              </span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>--}}
            </div>
            @forelse($users as $user)
                <div class="col-sm-3">
                    <div class="profile-thumbnail">
                        <div class="media">
                            <div class="media-left">
                                <img alt="User Image" src="{{ Storage::url($user->getProfileImage())}}" class="media-object" >
                            </div>
                            <div class="media-body">
                                <div class="thumbnail-detail"><a href="{{route('members.show',[$user->id])}}"><h5 class="media-heading">{{$user->firstname." ".$user->lastname}}</h5></a></div>
                                <div class="thumbnail-detail">
                                    <h5 class="media-heading">Contributions:</h5>
                                    <a href="{{route('members.show',[$user->id])}}">{{count($user->cadmodels)}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p class="no-contributors-text">No Members Available</p>
            @endforelse
        </div>
        {{ $users->appends(request()->except('page'))->links() }}
    </div>

@endsection