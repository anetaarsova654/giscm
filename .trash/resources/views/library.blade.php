@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if (!Auth::check())
                <p class="wel"><a href="">Welcome visitor you can login or create an account.</a></p><br>
                @endif
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <form action="" method="post" id="searchBox" name="searchBox">
                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2 class="subheading"><b>Library</b></h2>
                </div>
                <div class="col-md-12 cad-menu">
                    <div class="col-md-2 col-md col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{url('/cadmodels')}}"></a>
                        <center>
                            <a href="{{ route('cadmodels.index') }}"></a>
                            <a href="{{url('/cadmodels')}}"><img src="{{url('/images/CAD_Models.jpg')}}" border="0" alt="CAD Library"></a>
                        </center><br>
                        <div class="top-content">
                            <center><h4><a href="{{url('/cadmodels')}}">CAD Models</a></h4></center>
                        </div>
                    </div>
                    <div class="col-md-2 col-md col-md-offset-1 col-xs-5 col-sm col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{route('threadspecifications')}}"></a>
                        <center>
                            <a href="{{route('threadspecifications')}}"> </a>
                            <a href="{{route('threadspecifications')}}"><img src="{{url('/images/screw_thread.png')}}" border="0" alt="Thread Specifications"></a>
                        </center><br>
                        <div class="top-content">
                            <center><h4><a href="{{route('threadspecifications')}}">Thread Specifications</a></h4></center>
                        </div>
                    </div>
                    <div class="col-md-2 col-md col-xs-5 col-xs-offset-1 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{route('adapterassemblyspecifications')}}"></a>
                        <center>
                            <a href="{{route('adapterassemblyspecifications')}}"></a>
                            <a href="{{route('adapterassemblyspecifications')}}"><img src="{{url('/images/adapter_specs.jpg')}}" border="0" alt="Adapter Assembly Specifications"></a>
                        </center><br>
                        <div class="top-content">
                            <center><h4><a href="{{route('adapterassemblyspecifications')}}">Adapter Assembly Specifications</a></h4></center>
                        </div>
                    </div>
                   {{-- @if (Auth::check())
                    <div class="col-md-2 col-md col-xs-5 col-xs-offset-1 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{ route('cadmodels.create') }}"></a>
                        <center>
                            <a href="{{ route('cadmodels.create')}}"></a>
                            <a href="{{route('cadmodels.create') }}"><img src="{{url('/images/upload.png')}}" border="0" alt="Adapter Assembly Specifications"></a>
                        </center><br>
                        <div class="top-content">
                            <center><h4><a href="{{ route('cadmodels.create') }}">Upload New File</a></h4></center>
                        </div>
                    </div>
                    @endif--}}
                </div>
            </div>
        </div>
    </div>
@endsection
