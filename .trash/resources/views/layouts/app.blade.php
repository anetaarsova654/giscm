<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Global Industry Standard CAD Models (GISCM)</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Styles -->
    <link href="{!! asset('css/bootstrap.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/style-custom.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet">
    <!-- animation-effect -->
    <link href="{!! asset('css/animate.min.css') !!}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head')

</head>
<body>
    <div class="header-top22">
        <div class="wrap">
            <div class="header-top-left">
                <div class="box"></div>
                <div class="clearfix"></div>
            </div>
            <div class="cssmenu">
                <ul>
                 @guest
                    <li class="active"><a href="{{route('register')}}">Sign Up</a></li> |
                    <li><a href="{{ route('login') }}">Log In</a></li>
                 @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 55px;">
                            <img src="{{ Storage::url(Auth::guard('web')->user()->getProfileImage())}}" style="width: 34px; height:34px;left: 10px; border-radius: 50%; ">
                            {{ Auth::user()->firstname.' '.Auth::user()->lastname }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="text" href="{{route('editprofile')}}">Edit Profile</a>
                            </li>
                            <li>
                                <a class="text" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                 @endguest
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    @include('partials.navbar')
    <div class="container">
        @if(session()->has('message.level'))
            <div class="alert alert-{{ session('message.level') }}">
                {!! session('message.content') !!}
            </div>
        @endif
    </div>
    <div id="app">
        @yield('content')
    </div>

    @include('partials.footer')


    <!-- js -->
    <script src="{!! asset('js/jquery-2.2.3.min.js') !!}"></script>
    <script src="{!! asset('js/nav.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.js') !!}"></script>
    <script src="{!! asset('js/wow.min.js') !!}"></script>
    <script>
        new WOW().init();
    </script>
    <script src="{!! mix('js/app.js') !!}"></script>
    @yield('js')
</body>
</html>
