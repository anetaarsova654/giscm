@extends('admin.layouts.app')

@section('content')
    <section class="content">
        <!-- SECTION 1 -->
        <div class="box box-default">
            <div class="box-header with-border text-center">
                <h1 class="box-title"><b>User Information</b></h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <p>{{$user->firstname." ".$user->lastname}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <p>{{$user->email}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Classification</label>
                            <div class="col-sm-9">
                                <p>{{$user->classification}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">IP Address</label>
                            <div class="col-sm-9">
                                <p>{{$user->ip_address}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">IP Location</label>
                            <div class="col-sm-9">
                                <p>{{$ip_city}}</p>
                            </div>
                        </div>
                    </div>

                    {{--<div class="col-sm-12">
                        <h3 class="text-center">General Information</h3>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Skills</label>
                            <div class="col-sm-9">
                                <p>{{$user->skills}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Interests</label>
                            <div class="col-sm-9">
                                <p>{{$user->interests}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">About</label>
                            <div class="col-sm-9">
                                <p>{{$user->about}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Website</label>
                            <div class="col-sm-9">
                                <p>{{$user->website}}</p>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            </div>
        </div>
    </section>

@endsection