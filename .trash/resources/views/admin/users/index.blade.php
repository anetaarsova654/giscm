@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Users Record
            <small>Control panel</small>
        </h1>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="{{route('users.index')}}" class="btn btn-primary btn-block margin-bottom">Show All</a>
                <div class="box box-solid">
                    {!!   Form::open(['url' => route('users.index'),'class'=>'form-horizontal','method' => 'GET']) !!}
                        <div class="classification">
                            <div class="box-header with-border">
                                <h3 class="box-title">Classifications</h3>
                            </div>
                            <div class="box-body no-padding">
                               {{Form::select('classification', ['student'=>'Student', 'professional' => 'Professional'], null, ['class'=>'form-control select2','placeholder' => 'Select classification..'])}}
                            </div>
                        </div>

                        <div class="user-categories">
                            <div class="box-header with-border">
                                <h3 class="box-title">User Categories</h3>
                            </div>
                            <div class="box-body no-padding">

                                {{Form::select('user-category',$user_categories, null, ['class'=>'form-control select2','placeholder' => 'Select category..'])}}

                            </div>
                        </div>

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </div>

                    {!!  Form::close() !!}
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Registered Members Record</h3>
                        <div class="box-tools">
                            {{--<div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Classification</th>
                                <th>Phone</th>
                                <th>Country</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                <tr>
                                    <td><a href="{{route('users.show',[$user->id])}}">{{$user->firstname.' '.$user->lastname }}</a></td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->classification}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->country}}</td>
                                </tr>
                            @empty
                                <p>No User Available..</p>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $users->appends(request()->except('page'))->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
@endsection

