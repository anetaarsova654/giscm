@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="form-container">
                    {!!   Form::open(['url' => route('cadmodels.store'),'files'=>true,'class'=>'form-horizontal']) !!}
                        @include('cadmodels._form',['heading'=> 'UPLOAD FILE','btnname'=>'Upload File'])
                    {!!  Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var step2inputs = {
            'name':false,
            'price':false,
            'description':false,

            allTrue:function () {

                return(this.name && this.price && this.description);
            }
        };

        $(document).ready(function(){

            $('#category-dropdown').change(function(){
               if($(this).val())
               {
                   $('.step2').removeClass('hidden');
               }
               else {
                   $('.step2').addClass('hidden');
               }
            });

            $('.step2-inputs').on('change paste keyup', function () {
                if($(this)[0].value)
                {
                    step2inputs[$(this)[0].id] = true;
                }
                else
                {
                    step2inputs[$(this)[0].id] = false;
                }
                if(step2inputs.allTrue() == true)
                {
                    console.log('all true');
                    $('.step3').removeClass('hidden');
                }
                else
                {
                    $('.step3').addClass('hidden');
                }
            });

            $('.step3').change(function(){
                if($(this).val())
                {
                    $('.step4').removeClass('hidden');
                }
                else {
                    $('.step4').addClass('hidden');
                }
            });

            $('.product_file').change(function(){
                var file = $(this).val();
                var ext = file.split('.').pop();

                if ( ext == 'STEP' || ext == 'STL')
                {
                    $('.model-dropdown').removeClass('hidden');
                    $(".hint").addClass('hidden');
                }
                else
                {

                    $(".model-dropdown").addClass('hidden');
                    $(".hint").removeClass('hidden');
                }
            });

        });
    </script>
@endsection

