<div class="comments-section">
    <h5>Comments:</h5>
    @if($cadmodel->comments)
        @foreach($cadmodel->comments as $comment)
            <div class="media">
                <div class="media-left">
                    <img src="{{ Storage::url($comment->user->getProfileImage())}}" class="media-object" style="width:60px">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$comment->user->firstname." ".$comment->user->lastname}}</h4>
                    <p class="rating-text text-right"><b>Rating: {{$comment->rating." "}}stars</b></p>
                    <p>{{$comment->created_at->format('d-M-Y')}}</p>
                    <p class="smmary"><b>Summary: </b>{{$comment->summary}}</p>
                    <p class="comment"><b>Comment: </b>{{$comment->comment}}</p>
                </div>
            </div>
            <hr>
        @endforeach
    @else
        <p>No Comments Found for this Model</p>
    @endif
</div>

