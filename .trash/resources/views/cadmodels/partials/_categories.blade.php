<h3 class="future">CATEGORIES</h3>
<div class="list-group">
    {{--@if(Auth::check())
        <a href="{{route('cadmodels.myfiles',Auth::user()->id)}}" class="list-group-item">My Files</a>
    @endif--}}
    <a href="{{route('cadmodels.index')}}" class="list-group-item">All Categories</a>
    @foreach($categories as $category)
        <a class="list-group-item" href="{{route('cadmodels.index')}}?category={{ $category->id }}">{{$category->name}}</a>
    @endforeach
</div>