@extends('layouts.app')

@section('content')
    <div class="container cadmodel-content">
        <div class="row">
            <div class="header-bottom-on">
                @if(!Auth::check())
                <p class="wel">
                    <a href="">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                @endif
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    {{--<div class="search">
                        <form action="" method="post" id="searchBox" name="searchBox">
                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>--}}
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                @include('cadmodels.partials._categories')
            </div>
            <div class="col-md-9">
                <p align="center"><font size="3" color="#ff0000"></font></p>
                <div class="top-in-single">
                    <h3 class="future"></h3>
                    <div class="col-md-5 single-top">
                        <table>
                            <tbody>
                            <tr>
                                <td align="left"><img src="{{ Storage::url($cadmodel->image_url) }}" border="0" width="320" height="320" alt="File Image"><br></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div>
                                        <div class="product-details">
                                            <div class="product-content-container-header">Product Details</div>
                                            <div class="product-content-container-body">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td class="col1">Name:</td>
                                                        <td class="col2">{{ $cadmodel->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col1">Description:</td>
                                                        <td class="col2">{{ $cadmodel->description }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col1">By:</td>
                                                        <td class="col2">{{ $cadmodel->owner->firstname.' '.$cadmodel->owner->lastname }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col1">Price:</td>
                                                        <td class="col2">{{ $cadmodel->price }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col1">Category:</td>
                                                        <td class="col2">{{ $cadmodel->category->name }}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#" id="myBtn"><img src="" style="height: 103px; width: 217px; display:none;"></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 single-top-in">
                        <div class="single-para">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#download">Download</a></li>
                                <li><a data-toggle="tab" href="#rating">Rating & Comments</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="download" class="tab-pane fade in active">
                                    <table class="lightBlueBorder" cellspacing="0" cellpadding="0" style="width: 100%; margin-top: 10px;">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 15px; text-align: center;">
                                                Instant download file that includes 3D CAD .STEP and .STL file format models.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 15px; text-align: center;">
                                                <a href="{{route('cadmodels.download',$cadmodel->id)}}"><div><img src="../images/Download-button.png" alt="" height="80"></div></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div id="rating" class="tab-pane fade">
                                    @if(Auth::check())
                                        @if(Auth::guard('web')->user()->id != $cadmodel->user_id)
                                            <a href="{{route('cadmodels.comments.create',[$cadmodel->id])}}">--> Click Here To Rate this Product</a>
                                        @endif
                                    @else
                                        <a href="{{route('cadmodels.comments.create',[$cadmodel->id])}}">--> Click Here To Rate this Product</a>
                                    @endif
                                    @include('cadmodels.comments.index')
                                </div>
                            </div>
                        </div>
                        <div class="tabsBottomBorder">
                            <hr style="border: 1px solid #ccc">
                        </div>
                        <div id="" class="member-container" style="width:100%;">
                            <div class="member-content-container">
                                <div class="member-content-container-header">
                                    <span id="">Member</span>
                                </div>
                                <div class="member-content-container-body">
                                    <div class="member-detail">
                                        <img alt="Member Image" class="userProfile" src="{{ Storage::url($cadmodel->owner->getProfileImage())}}" style="border-width:1px;border-style:solid;">
                                        <div>
                                            <span><b>{{ $cadmodel->owner->firstname.' '.$cadmodel->owner->lastname }}</b></span><br>
                                            <div class="">
                                                <b>Title:</b>
                                                {{ $cadmodel->owner->classification}}<br>
                                            </div>
                                         </div>
                                        <div  class="bottomRight_viewprofile">
                                            <a class="profile_link" href="{{route('members.show',[$cadmodel->owner->id])}}">View Profile</a>
                                        </div>
                                    </div>
                                    <div class="clearLeft">&nbsp;</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="miscellaneous">
                        <br><b>Miscellaneous :</b><br>
                        <a href="{{route('cadmodels.emailfriend',$cadmodel->id)}}">Email a Friend</a><br>
                        <a href="{{route('contactus')}}">Product Inquiry</a><br>
                        <br>
                        <table border="0">
                            <tbody>
                            <tr>
                                <td valign="top">
                                    <div id="shareBtn" class="fb-share-button fb_iframe_widget"  data-layout="button" data-size="small" data-mobile-iframe="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=1222956894447184&amp;container_width=0&amp;href=https%3A%2F%2Fgiscm.net%2Fscripts%2FprodView.asp%3FidProduct%3D124&amp;layout=button&amp;locale=en_US&amp;mobile_iframe=false&amp;sdk=joey&amp;size=small">
                            		<span style="vertical-align: bottom; width: 59px; height: 20px;">
                                        <iframe name="fd2db64a586c88" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:share_button Facebook Social Plugin" src="https://www.facebook.com/plugins/share_button.php?app_id=1222956894447184&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F5oivrH7Newv.js%3Fversion%3D42%23cb%3Df378115765b7164%26domain%3Dgiscm.net%26origin%3Dhttps%253A%252F%252Fgiscm.net%252Ff1612c92822df2%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fgiscm.net%2Fscripts%2FprodView.asp%3FidProduct%3D124&amp;layout=button&amp;locale=en_US&amp;mobile_iframe=false&amp;sdk=joey&amp;size=small" style="border: none; visibility: visible; width: 59px; height: 20px;" class=""></iframe>
                            		</span>
                                    </div>&nbsp;
                                </td>
                                <td valign="top">

                                    <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.0e55a364e448deca530e9a13f68e1486.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Fgiscm.net%2Fscripts%2FprodView.asp%3Fidproduct%3D124&amp;size=m&amp;text=Global%20Industry%20Standard%20CAD%20Models%20(GISCM)-&amp;time=1505390619488&amp;type=share&amp;" style="position: static; visibility: visible; width: 60px; height: 20px;"></iframe>&nbsp;
                                </td>
                                <td valign="top">
                                    <div id="___plus_0" style="text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 59px; height: 20px;">
                                        <iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 59px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1505390616759" name="I0_1505390616759" src="https://apis.google.com/se/0/_/+1/sharebutton?plusShare=true&amp;usegapi=1&amp;action=share&amp;annotation=none&amp;hl=en-US&amp;origin=https%3A%2F%2Fgiscm.net&amp;url=https%3A%2F%2Fgiscm.net%2Fscripts%2FprodView.asp%3Fidproduct%3D124&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en_GB.huiz860g2xw.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCOivKSGXQOXJgnoW4KTuJ_KNf2Z5w#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1505390616759&amp;parent=https%3A%2F%2Fgiscm.net&amp;pfname=&amp;rpctoken=24296155" data-gapiattached="true" title="G+">
                                        </iframe>
                                    </div>&nbsp;
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="float:right; width: 51%;">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td align="left"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>
//        var href = window.location;
//        jQuery(document).on('ready', function($){
//            var url = window.location;
//            //console.log(url.href);
//            $('.fb-share-button').attr('data-href', url.href);
//        });


        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: 'https://developers.facebook.com/docs/',
            }, function(response){});
        }


    </script>
@endsection