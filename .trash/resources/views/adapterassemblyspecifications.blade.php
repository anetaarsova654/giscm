@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel">
                    <a href="">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMINC" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <form action="" method="post" id="searchBox" name="searchBox">

                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>Adapter Assembly Specifications</b></h2></div>
                <div class="col-md-12 cad-menu">
                    <div class="col-md-2 col-md col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{route('library')}}">
                        </a><center><a href="{{route('library')}}"></a><a href="{{route('library')}}"><img src="images/home-new.jpg" border="0" alt="Library"></a></center><br>

                        <div class="top-content">
                            <center><h4><a href="{{route('library')}}">Library</a></h4></center>
                        </div>
                    </div>
                    <div class="col-md-2 col-md col-md-offset-1 col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{route('threadspecifications')}}">
                        </a><center><a href="{{route('threadspecifications')}}"> </a><a href="{{route('threadspecifications')}}"><img src="images/screw_thread.png" border="0" alt="GISCM Thread Specifications"></a></center>
                        <br>
                        <div class="top-content">
                            <center><h4><a href="{{route('threadspecifications')}}">GISCM Thread Specifications</a></h4></center>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-1 col-md col-xs-5 col-xs-offset-1 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                        <a href="{{url('/cadmodels')}}">
                        </a><center><a href="{{url('/cadmodels')}}"></a><a href="{{url('/cadmodels')}}"><img src="images/CAD_Models.jpg" border="0" alt="CAD Models"></a></center><br>

                        <div class="top-content">
                            <center><h4><a href="{{url('/cadmodels')}}">CAD Models</a></h4></center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <ul class="list-group links">
                        <li class="list-group-item"><a href="">37 Degree (JIC) Adapter Assembly Instructions</a></li>
                        <li class="list-group-item"><a href="">Adapter Assembly Instructions and Torques</a></li>
                        <li class="list-group-item"><a href="">37 Degree Flareless Adapter Specifications</a></li>
                        <li class="list-group-item"><a href="">Adapter Tube Size vs. Thread Size Chart</a></li>
                        <li class="list-group-item"><a href="">Adapter Warning</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
