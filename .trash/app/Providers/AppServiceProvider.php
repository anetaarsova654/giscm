<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Categories;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('cadmodels.partials._categories', function ($view) {
            $categories = Categories::all();
            return $view->with('categories',$categories);
        });

        \Stripe\Stripe::setApiKey('sk_test_2f66Z95hgkgAgJetIu81TsQI');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
