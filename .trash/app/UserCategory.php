<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $table = 'user_categories';

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
