<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CadModel extends Model
{
    protected $fillable = ['name','category_name','description','image_url'];

   /* public function categories()
    {
        return $this->belongsTo('App\Categories');
    }*/

//    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
//    public function sluggable()
//    {
//        return [
//            'slug' => [
//                'source' => 'name'
//            ]
//        ];
//    }


    public function scopeFilterByCategory($query)
    {
        if(request()->has('category'))
        {
            $query->where('category_id',request()->get('category'));
        }
    }

    public function scopeFilterByName($query)
    {
        if(request()->has('q'))
        {
            $query->where('name','LIKE','%'.request()->get('q').'%');
        }
    }




    public function category()
    {
        return $this->belongsTo(Categories::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'cad_model_id');
    }


    public function getRating()
    {
        if(count($this->comments->toArray()))
        {
            return (int)ceil($this->comments->sum('rating')/count($this->comments->toArray()));
        }

        return 0;
    }
    
}
