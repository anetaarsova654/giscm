<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CadModel;
use File;
use Illuminate\Support\Facades\Input;
use Storage;
use Auth;
use App\User;
use App\Categories;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use View;
use Session;

class CadmodelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth:web')->only('download','buy');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $cadmodels = CadModel::filterByCategory()->filterByName()->paginate(8);
        $cadmodels->load('comments');

        return view('cadmodels.index', ['cadmodels' => $cadmodels]);
    }

    public function storedisclaimer(Request $request)
    {
        $categories = Categories::all();
        $disclaimer = $request->get('disclaimer_status');
        if($disclaimer == 1)
        {
            $user = Auth::guard('web')->user();
            $user->disclaimer_status = $request->get('disclaimer_status');
            $user->save();
            return redirect()->route('cadmodels.create');
        }
        else
        {
            return redirect()->route('home');
        }
        
    }

    public function create(Request $request)
    {
        $categories = Categories::all();

        if(Auth::check())
        {
            $disclaimer_status = Auth::user()->disclaimer_status;
            if($disclaimer_status == 0)
            {
                return view('cadmodels.disclaimer');
            }
            else
            {
                return view('cadmodels.create',['categories' => $categories]);
            }
        }
        else
        {
            return redirect('login');
        }

    }

    public function show(Cadmodel $cadmodel)
    {
        $cadmodel->load('category','owner','comments.user');

        $categories = Categories::all();
        return view('cadmodels.show',['cadmodel' => $cadmodel, 'categories' => $categories]);
    }
    
    public function myFiles()
    {
        $user_id = Auth::user()->id;
        $categories = Categories::all();
        $cadmodels = CadModel::where('user_id',$user_id)->get();
        return view('cadmodels.index',['cadmodels' => $cadmodels, 'categories' => $categories, 'myFiles'=>true]);
    }

    public function store(Request $request){
        $user = Auth::user();
        $categories = Categories::all();

        $image = request()->file('image_url');
        $image_extension = $image->guessClientExtension();
        $image_path = $image->storePubliclyAs('public/uploads/products/images',$user->id.\Carbon\Carbon::now()->format('YmdHis').".{$image_extension}");

        $file = request()->file('product_file');
        $file_extension = $file->getClientOriginalExtension();
        $file_path = $file->storePubliclyAs('public/uploads/products/files',$user->id.\Carbon\Carbon::now()->format('YmdHis').".{$file_extension}");

        $file = new CadModel();
        $file->name = $request->get('name');
        $file->category_id= $request->get('category');
        $file->price= $request->get('price');
        $file->description = $request->get('description');
        $file->image_url = $image_path;
        $file->product_file = $file_path;
        $file->model_type= $request->get('model_type');
        $file->user_id = $user->id;
        $file->save();

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'File successfully added!');
        return redirect()->route('cadmodels.index');
    }

    public function update(Request $request,CadModel $cadmodel){
        $user = Auth::user();
        $categories = Categories::all();

        $file = request()->file('product_file');
        $file_extension = $file->guessClientExtension();
        $file_path = $file->storePubliclyAs('public/uploads/products/files',$user->id.\Carbon\Carbon::now()->format('YmdHis').".{$file_extension}");


        if($request->hasFile('image_url'))
        {
            $image = request()->file('image_url');
            $image_extension = $image->guessClientExtension();
            $image_path = $image->storePubliclyAs('public/uploads/products/images',$user->id.\Carbon\Carbon::now()->format('YmdHis').".{$image_extension}");
            $cadmodel->image_url = $image_path;
        }

        $cadmodel->name = $request->get('name');
        $cadmodel->category_id= $request->get('category');
        $cadmodel->price= $request->get('price');
        $cadmodel->description = $request->get('description');
        $cadmodel->product_file = $file_path;
        $cadmodel->user_id = $user->id;
        $cadmodel->save();

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'File successfully updated!');
        return redirect()->route('dashboard');
    }

    public function destroy(Request $request,CadModel $cadmodel)
    {
        $cadmodel->load('comments');
        $cadmodel->comments()->delete();
        $cadmodel->delete();
        
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'File Deleted Successfully!');
        return redirect()->route('dashboard');
    }
    
    public function edit(CadModel $cadmodel)
    {
        $categories = Categories::all();
        return view('cadmodels.edit',['cadmodel'=>$cadmodel,'categories'=>$categories]);
    }
    
    public function download( CadModel $cadmodel )
    {
        $user = Auth::guard('web')->user();
        $user->hasPurchased($cadmodel);
        
        
        if($cadmodel->price && !$user->hasPurchased($cadmodel) && !($user->id == $cadmodel->user_id))
        {
            return redirect()->route('cadmodels.buy',$cadmodel->id);

        }
        else
        {
            $filename = $cadmodel->product_file;
            if (Storage::exists($filename))
            {
                $path = storage_path('app/' . $filename);
                return response()->download($path);
            } else
            {
                exit('Requested file does not exist on our server!');
            }
        }
    }

    public function buy(Cadmodel $cadmodel)
    {
        $user = Auth::guard('web')->user();
        if(($cadmodel->price && !$user->hasPurchased($cadmodel))  )
        {
            return view('cadmodels.paymentform',['cadmodel'=>$cadmodel]);

        }
        else
        {
            return redirect()->route('cadmodels.show',$cadmodel->id);
        }


    }

    public function readFile()
    {
        $lines=array();
        $fp=fopen('categories.txt', 'r');
        while (!feof($fp))
        {
            $line=fgets($fp);
            $line=trim($line);
            //add to array
            $lines[]=$line;
        }
        fclose($fp);

        foreach($lines as $line){
            $categoryname = new Categories;
            $categoryname->name = $line;
            $categoryname->save();
        }
        return ($lines);
    }

    public function emailFriend(CadModel $cadmodel){
        return view('cadmodels.emailfriend',['cadmodel'=>$cadmodel]);
    }

    public function sendcadmodelMail(Request $request) {
        $this->validate($request, array(
            'yourname' => 'Required',
            'friendemail' => 'Required|Email',
            'subject' => 'Required',
            'messagetext' => 'Required'
        ));

        $data = array(
            'yourname' => $request->yourname,
            'friendemail' => $request->friendemail,
            'subject' => $request->subject,
            'messagetext' => $request->messagetext,
        );

        Mail::send('cadmodels.mailtemplate', $data, function($message) use ($data) {
            $message->from('moteeb83@gmail.com', $data['yourname']);
            $message->to($data['friendemail'])->subject($data['subject']);
            //$message->to($data['friendemail']);
            //$message->subject($data['subject']);
        });
        Session::put('flash_message', 'Email Sent....!!');
        return redirect()->back();

    }

}
