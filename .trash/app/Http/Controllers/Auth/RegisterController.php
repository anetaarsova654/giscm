<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Http\Request ;
use App\UserCategory;
use Storage;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $image_path = 'public/uploads/profile_images/images/default.png';

        if(Input::hasFile('image'))
        {
            $email = Input::get('email');
            $image = Input::file('image');
            $image_extension = $image->guessClientExtension();
            $image_path = $image->storePubliclyAs('public/uploads/profile_images/images', $email . ".{$image_extension}") ;
        }


            $data['token'] = str_random(25);
            $user = new User([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'company' => $data['company'],
                'classification' => $data['classification'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'image_url' => $image_path,
                'skills' => $data['skills'],
                'interests' => $data['interests'],
                'about' => $data['about'],
                'website' => $data['website'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'country' => $data['country'],
                'zipcode' => $data['zipcode'],
                'city' => $data['city'],
                'state' => $data['state'] == "other" ? $data['custom_state'] : $data['state'],
                'token' => $data['token'],
                'ip_address' =>  \Request::ip(),
            ]);

            $user->save();


            $usercategories = $data['usercategories'];
            foreach($usercategories as $category)
            {
                if(!$user->categories->contains($category))
                {
                    $user->categories()->attach($category);
                }
            }

            return($user);


    }

    

    public function showRegistrationForm()
    {
        $user_categories = UserCategory::all();
        return view('auth.register', ['user_categories' => $user_categories]);
    }

    public function accountConfirmation($token,Request $request)
    {
        $user = User::where('token',$token)->first();

        if(!is_null($user))
        {
            $user->activation_status = 1;
            $user->token = "";
            $user->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Your activation is completed');
            return redirect(route('login'));
        }
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Something Went wrong.Please Signup Again');
        return redirect(route('login'));
    }



    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());
//        event(new Registered($user = $this->create($request->all())));

        $data = array(
            'username' => $user->firstname,
            'email' => $user->email,
        );

        if($user->classification !== 'Professional')
        {
            $this->guard()->login($user);
        }
        else
        {
            Mail::send('auth.mails.confirmation', ['user' => $user], function($message) use ($user) {
                $message->to($user->email)->subject('Registration Confirmation');
            });
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Confirmation email has been send.Please check your email');
            return redirect(route('login'));
        }


        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


}
