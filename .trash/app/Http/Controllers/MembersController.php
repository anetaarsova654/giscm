<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Categories;
use App\CadModel;
class MembersController extends Controller
{
    public function index()
    {

        $users = User::paginate();
        $users->load('cadmodels');

        return view('members.index',['users'=>$users]);
    }

    public function show(User $member,Request $request)
    {
        $user = $member;
        $cadmodels = CadModel::where('user_id',$user->id)->filterByCategory()->paginate(8);
        $categories = Categories::pluck('name','id');


        return view('members.show',
            [
                'user'=>$user,
                'categories'=>$categories,
                'cadmodels'=>$cadmodels,
            ]);
    }
}
