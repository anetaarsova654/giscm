<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use View;
use Session;

class ContactusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contactus');
    }

    /*public function sendMail()
    {
        $data = Input::all();
        $rules = array (
            'fullname' => 'Required',
            'email' => 'Required|Email',
            'subject' => 'Required',
            'message' => 'Required'
        );

        $validator = Validator::make ($data, $rules);
        if ($validator -> passes()){

            Mail::send('contactmail', $data, function($message) use ($data)
            {
                $message->from($data['email'] , $data['fullname']);
                $message->to('moteeb83@gmail.com', 'Moteeb Asad')->subject('Contact Request');

            });
            Session::flash('flash_message', 'Thank you for contacting us – we will get back to you soon!');
            return redirect()->back();
        }else{
            return Redirect::to('/contactus')->withErrors($validator);
        }
    }*/

    public function sendMail(Request $request) {
        $this->validate($request, array(
            'fullname' => 'Required',
            'email' => 'Required|Email',
            'subject' => 'Required',
            'messagetext' => 'Required'
        ));

        $data = array(
            'fullname' => $request->fullname,
            'email' => $request->email,
            'subject' => $request->subject,
            'messagetext' => $request->messagetext,
        );

        Mail::send('contactmail', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('moteeb83@gmail.com');
            $message->subject($data['subject']);
        });
        Session::put('flash_message', 'Thank you for contacting us – we will get back to you soon!');
        return redirect()->back();

    }

}
