<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CadModel;
use Auth;
use App\Comment;
use App\Categories;

class CommentsController extends Controller
{

    public function __construct(){
        $this->middleware('auth:web')->only(['create']);
    }

    
    public function create(Cadmodel $cadmodel){
        return view ('cadmodels.comments.create',compact('cadmodel'));
    }

    public function store(Request $request,$cadmodel){
        $user_id = Auth::user()->id;

        $comment = new Comment();
        $comment->user_id = $user_id;
        $comment->cad_model_id = $cadmodel;
        $comment->rating = $request->get('star');
        $comment->summary = $request->get('summary');
        $comment->comment = $request->get('comment');
        $comment->save();

        return redirect()->route('cadmodels.index');

    }
    
    
}
