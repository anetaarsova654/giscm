<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CadModel;
use App\Order;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Paypal;

class PaypalController extends Controller
{
    private $apiContext;
    public function __construct()
    {
        $this->middleware('auth:web');

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENT_ID'),
                env('PAYPAL_SECRET')
            )
        );
        $this->apiContext->setConfig([
            'mode' => 'sandbox',
            'http.ConnectionTimeOut' => 100,
            'log.logEnabled' => true,
            'log.FileName' => 'paypal_log.txt',
            'log.logLevel' => 'FINE',
            'validation.level' => 'log',
        ]);
    }


    public function create(CadModel $cadmodel)
    {

        $formatted_price = number_format((float)$cadmodel->price, 2, ".", "");
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");


        $item = new Item();
        $item->setName($cadmodel->name)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku($cadmodel->id)
            ->setPrice($formatted_price);
//            ->setPrice($cadmodel->price);


        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setShipping("0.00")
            ->setTax("0.00")
            ->setSubtotal($formatted_price);

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($formatted_price)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment For ".$cadmodel->name)
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(route('cadmodels.paypal.execute',$cadmodel->id))
            ->setCancelUrl(route('cadmodels.paypal.cancelled',$cadmodel->id));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions([$transaction])
            ->setRedirectUrls($redirectUrls);


        try {
            $payment->create($this->apiContext);
            return redirect($payment->getApprovalLink());
        }
        catch(PayPal\Exception\PayPalConnectionException $ex)
        {
            dd($ex);
        }

    }

    public function execute(CadModel $cadmodel)
    {
        $user = Auth::guard('web')->user();

        $paymentId = request()->get('paymentId');
        $payerId = request()->get('PayerID');
        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            $payment->execute($execution,$this->apiContext); // TODO uncomment this to execute payment
            $transaction = $payment->getTransactions()[0];
            $itemList = $transaction->getItemList();
            $item = $itemList->getItems()[0];
            $sku = $item->getSku();

            $order = new Order();
            $order->user_id = $user->id;
            $order->cad_model_id= $sku;
            $order->paypal_id= $paymentId;
            $order->save();
            session()->flash('message.level', 'success');
            session()->flash('message.content', 'File Purchased SuccessFully');
            return redirect()->route('cadmodels.show',$cadmodel->id);
        }
        catch (PayPal\Exception\PayPalConnectionException $ex) {
            session()->flash('message.level', 'Danger');
            session()->flash('message.content', 'Payment Failed!');
            return redirect()->route('cadmodels.show',$cadmodel->id);
        }

        //dd($payment);
    }

    public function cancelled(CadModel $cadmodel)
    {
        session()->flash('message.level', 'danger');
        session()->flash('message.content', 'Payment Cancelled!');
        return redirect()->route('cadmodels.show',$cadmodel->id);
    }

}
