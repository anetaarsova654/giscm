<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
use Session;
class EditProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $admin = Auth::guard('admin')->user();

        return view ('admin.editprofile',['admin'=>$admin]);
    }

    public function update(Request $request,Admin $admin)
    {
        $admin->name = $request->get('name');
        $admin->email = $request->get('email');
        $admin->password= $request->get('password');
        $admin->save();

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Profile Updated Successfully !');
        return redirect()->route('admin.dashboard');

    }

}
