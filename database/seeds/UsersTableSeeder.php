<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table ('users')->insert([
            'firstname'=> 'User', 
            'lastname'=> 'One', 
            'company'=> null,
            'classification'=> 'Student',
            'email'=> 'userone@gmail.com', 
            'password'=> bcrypt('123456'),
            'image_url'=> null,
            'skills'=> null,
            'interests'=> null,
            'about'=> null,
            'website'=> 'www.giscm.net',
            'address'=> 'Rawalpindi',
            'phone'=> '0302',
            'country'=> 'Pakistan',
            'zipcode'=> '46000',
            'city'=> 'Rawalpindi',
            'state'=> 'Rawalpindi',
            'ip_address'=> '207.244.79.136',
            'disclaimer_status'=> '1',
            'created_at'=>'2017-09-27 19:13:27',
            'updated_at'=>'2017-09-27 20:13:27',

        ]);


        DB::table ('users')->insert([
            'firstname'=> 'User',
            'lastname'=> 'Two',
            'company'=> null,
            'classification'=> 'Professional',
            'email'=> 'usertwo@gmail.com',
            'password'=> bcrypt('123456'),
            'image_url'=> null,
            'skills'=> null,
            'interests'=> null,
            'about'=> null,
            'website'=> 'www.giscm.net',
            'address'=> 'Rawalpindi',
            'phone'=> '0300',
            'country'=> 'Pakistan',
            'zipcode'=> '46000',
            'city'=> 'Rawalpindi',
            'state'=> 'Rawalpindi',
            'ip_address'=> '207.244.79.136',
            'disclaimer_status'=> '0',
            'activation_status'=> '1',
            'created_at'=>'2017-09-26 19:13:27',
            'updated_at'=>'2017-09-27 20:13:27',
        ]);
        

    }
}
