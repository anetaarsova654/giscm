<?php

use Illuminate\Database\Seeder;

class CadModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0;$i<15;$i++)
        {
            DB::table ('cad_models')->insert([
                'user_id'=> $faker->numberBetween(1,2),
                'category_id'=> $faker->numberBetween(1,13),
                'name'=> $faker->name(),
                'price'=> $faker->randomFloat(2,100,500),
                'description'=> $faker->sentence(),
                'image_url'=> 'public/uploads/products/images/'.$faker->numberBetween(1,10).".jpg",
                'product_file'=> 'public/uploads/products/files/120170918095812.pdf',
                'model_type'=> 'Assembly',
            ]);
        }
    }
}
