@extends('layouts.app')

@section('content')
    <div class="container payment-form-content">
        <h3 class="text-center">Payment for {{ $cadmodel->name }} ( ${{ $cadmodel->price }} )</h3>
        <div class="row">
            <div class="col-sm-6 bottom-padding">
                <div class="payment-form">
                    <h3>Payment Using Credit Card</h3>
                    <form action="{{route('cadmodels.stripe.charge',$cadmodel->id)}}" method="post" id="payment-form">
                        <div class="form-group">
                            <label for="card-element">
                                Credit or debit card
                            </label>
                            <div id="card-element">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display Element errors -->
                            <div id="card-errors" role="alert"></div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Pay ${{ $cadmodel->price }}">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6 bottom-padding">
                <div class="payment-form">
                    <h3 class="paypal-heading">Payment Using PayPal</h3>
                    <div class="form-group">
                        <form action="{{route('cadmodels.paypal.create',$cadmodel->id)}}" method="post">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-primary" value="PayPal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{route('cadmodels.stripe.charge',$cadmodel->id)}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="stripe_token" value="" id="stripeTokenInput">
    </form>


@endsection

@section('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        // Create a Stripe client
        var stripe = Stripe('pk_test_RqCv2jQ0RXzD8zfussw2H5Ch');

        // Create an instance of Elements
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element
        var card = elements.create('card',
                {
                    style: style,
                    hidePostalCode:true
                });

        // Add an instance of the card Element into the `card-element` <div>
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    stripeTokenHandler(result);
                }
            });
        });

        function  stripeTokenHandler(result)
        {
            res = result;
            input = document.querySelector('#stripeTokenInput');
            input.value = result.token.id;
            input.parentElement.submit();
        }
    </script>

@endsection