@extends('layouts.app')

@section('content')
    <div class="container disclaimer-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="disclaimer-form-content">
                    <form class="form-horizontal disclaimer-form" method="POST" action="{{ route('disclaimer.store') }}">
                        {{ csrf_field() }}
                        <div class="disclaimer-note">
                            <h2 class="bottom"><strong>Disclaimer</strong></h2>
                            <p class="bottom">Only original work designed by member. No patent or licensed models or drawings allowed.</p>
                        </div>
                        <div class="disclaimer-option bottom">
                            <input type="radio" name="disclaimer_status" value="1">Accept<br>
                            <input type="radio" name="disclaimer_status" value="0">Reject<br>
                        </div>
                        <input class ="btn btn-primary bottom btnsubmit" type="submit" name="Submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection