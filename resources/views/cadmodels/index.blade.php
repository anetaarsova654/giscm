@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if(!Auth::check())
                <p class="wel">
                    <a href="{{ route('login') }}">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                @endif
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}
                            {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}
                            <input type="submit" value="">
                        {!!  Form::close() !!}
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                @include('cadmodels.partials._categories')
            </div>
            <div class="col-md-9">
                <div class="content-top-in">
                    <h3 class="future">FEATURED</h3>

                    @forelse($cadmodels as $cadmodel)
                        <div class="col-md-3 md-col" style="padding-bottom:20px;">
                            <div class="col-md">
                                <center><img src="{{ Storage::url($cadmodel->image_url) }}" border="0" alt="a"></center><br>
                                <div class="top-content">
                                    <h5>{{ $cadmodel->name }}</h5>
                                    <p>
                                        @for($i = 0 , $filled = $cadmodel->getRating() ; $i<5 ; $i++)
                                            @if($i<$filled)
                                            <i class="fa fa-star"></i>
                                            @else
                                                <i class="fa fa-star-o"></i>
                                            @endif
                                        @endfor
                                    </p>
                                   {{-- <p>By:{{ $cadmodel->name }}</p>--}}
                                    <div class="white">
                                        <a href="{{route('cadmodels.show',[$cadmodel->id])}}" align="center" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">
                                            View
                                            <div class="clearfix"></div>
                                        </a>
                                    {{--    @if(Auth::check())
                                            @if(Auth::user()->id === $cadmodel->user_id)
                                               <a href="{{route('cadmodels.edit',[$cadmodel->id])}}" align="center" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">
                                                    Edit
                                                    <div class="clearfix"></div>
                                                </a>
                                                <form  class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 " action="{{route('cadmodels.destroy',[$cadmodel->id])}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="delete-file">Delete</button>
                                                </form>
                                            @endif
                                        @endif--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No Data Available</p>
                    @endforelse
                </div>
            </div>
            {{ $cadmodels->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
