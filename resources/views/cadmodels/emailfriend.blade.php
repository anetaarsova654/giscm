@extends('layouts.app')

@section('content')
    <div class="container cadmodel-content">
        <div class="row">
            <div class="header-bottom-on">
                <p class="wel">
                    <a href="">Welcome visitor you can login or create an account.</a>
                </p>
                <br>
                <div class="header-can">
                    <ul class="social-in">
                        <li><a href="https://www.facebook.com/GISCMINC" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>
                    </ul>
                    <div class="search">
                        <form action="" method="post" id="searchBox" name="searchBox">
                            <input type="text" name="strSearch" value="Search" align="middle">
                            <input type="submit" value="" name="SubSearch2">
                            <center><a href="" style="font-size:x-small;">Advanced Search</a></center>
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                @include('cadmodels.partials._categories')
            </div>
            <div class="col-md-9">
                <p align="center"><font size="3" color="#ff0000"></font></p>
                <div class="top-in-single">
                    <h3 class="future"></h3>
                    <div class="col-md-5 single-top">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success" role="alert" style="font-size: medium; text-align: center;">
                                {{ Session::get('flash_message') }}
                                {{ Session::forget('flash_message') }}
                            </div>
                        @endif
                        {!!  Form::open(['url' => route('cadmodels.share'),'class'=>'form-horizontal','method'=>'POST']) !!}
                        {{ csrf_field() }}
                        <div class="form-inputs">
                            <div class="form-group">
                                {{ Form::label('yourname', 'Your Name') }}
                                {{ Form::text('yourname' ,null, ['class' => 'form-control','id'=>'yourname','required' => 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('friendemail', 'Friend Email') }}
                                {{ Form::email('friendemail' ,null, ['class' => 'form-control','id'=>'friendemail','required' => 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('subject', 'Subject') }}
                                {{ Form::text('subject' ,null, ['class' => 'form-control','id'=>'subject','required' => 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('messagetext', 'Message') }}
                                {{ Form::textarea('messagetext' ,null, ['class' => 'form-control' ,'id'=>'messagetext','rows'=>'6']) }}
                            </div>
                        </div>
                        <div class="form-submit">
                            <input type="submit" class="btn btn-primary btnsubmit" value="Send">
                        </div>
                        {!!  Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>

            $(document).ready(function(){
                var uri = window.location.toString();
                var clean_uri;
                if (uri.indexOf("/emailfriend") > 0) {
                    clean_uri = uri.substring(0, uri.indexOf("/emailfriend"));
                }

                var content = "Hi, GISCM.Inc has item that I thought you would really like to know about.\n\n{{$cadmodel->name}}\n\n" + "Check this link to see more info:\n\n" +  clean_uri ;
                $("#messagetext").val(content);
            });

    </script>
@endsection