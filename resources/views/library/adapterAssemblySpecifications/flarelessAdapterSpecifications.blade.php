@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if (!Auth::check())

                    <p class="wel"><a href="{{route('login')}}">Welcome visitor you can login or create an account.</a></p><br>

                @endif
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">

                        {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}

                        {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}

                        <input type="submit" value="">

                        {!!  Form::close() !!}

                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('adapterAssemblySpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Assembly Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>37 Degree Flareless Adapter Specifications</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row fontType">
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <img src="{{asset('images/flareless-1.JPG')}}" alt=" " class="img img-responsive">
            </div>
            <div class="col-md-6 col-xs-12">
                <br/>
                <h5 style="text-decoration: underline; color: #38ba5f;"><b>CONSTRUCTION</b></h5>
                <br/>
                <p>
                    7000 Series Fittings consist of three main components:
                    Bodies, Nuts, and Sleeves.</p>
                <p><b>Bodies</b> – Straight bodies are machined from bar stock. Shape fittings (elbows, tees, & crosses)
                    are machined from forgings.</p>
                <p><b>7000 Series fittings are machined with NPTF</b> - Dryseal Pipe Threads and SAE straight threads for
                    easy installation.</p>
                <p><b>Tube Nuts</b> – Tube nuts in sizes 1/4", 5/16 ', 3/8", 1/2", 5/8", 3/4", 1” are Cold Formed in 1010
                    steel. Tube nuts in sizes 1 1/4", 1 1/2", & 2" are hot forged from 1045.</p>
                <p> <b>Tube Sleeves</b> – Flareless tube sleeves are machined from cold drawn12L14 bar stock and are available in both SAE "A" and "B" styles (see tube sleeve drawings to view the "A" and "B" style sleeves). After machining and inspection the tube sleeves are heat treated to provide them with the necessary hardness. CAUTION: Stainless steel sleeves must be preset into tubing during installation.
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>MATERIAL</b></h5>
                <br/>
                <ul style="list-style: none">
                    <li>Steel – Forged Bodies 1045</li>
                    <li>Straight Bodies 12L14, 1045</li>
                    <li>Cold Formed Nuts 1010</li>
                    <li>Bar Stock Nut 12L14</li>
                    <li>Flareless Sleeves 12L14</li>
                    <li>Stainless Steel – Forged Bodies 316</li>
                    <li>Stainless Steel – Straight Bodies 316</li>
                    <li>Stainless Steel –Bar Stock Nuts 316</li>
                    <li>Stainless Steel –Flareless Sleeves 17-4PH</li>
                </ul>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>PLATING</b></h5>
                <br/>
                <ul style="list-style: none;">
                    <li>Clear Zinc Trivalent</li>
                    <li>Alternate plating finishes are available upon request.</li>
                    <li>Contact World Wide Fittings for details.</li>
                </ul>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>APPLICATIONS</b></h5>
                <br/>
                <p>
                    For use with applications such as: Construction machinery, Textile machinery, Machine tools, Chemical,
                    Marine, Military, and most other high pressure hydraulic applications. Applications using thick wall
                    tubing where  flaring is not practical.
                </p>
            </div>
            <div class="col-md-6 col-xs-12">
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>CAUTION:</b></h5>
                <br/>
                <p>
                    7000 Flareless Fittings are NOT RECOMMENDED for use in instrumentation applications where toxic and
                    noxious gases are present.
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>TEMPERATURE RANGE</b></h5>
                <br/>
                <p>
                    -30 Degrees Fahrenheit to +450 Degrees Fahrenheit Straight thread fittings which use "O" Rings to seal
                    are restricted to the temperature range of the material used to construct the "O" Ring.
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>PRESSURE RATINGS</b></h5>
                <br/>
                <p>
                    Working Pressure up to 8000 PSI.
                    <br/>
                    <b>CAUTION:</b>
                    Working pressures will vary depending on tube and fitting size. (For applications using extreme pressures
                    at 5000 PSI or above (or) for application with extreme pressure fluctuations, contact World Wide Fittings.)

                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>HOW TO ORDER</b></h5>
                <br/>
                <p>
                    Tubing sizes are measured in sixteenths of an inch. Available sizes are 1/8" through 2". When ordering,
                    the tube end of the fitting must be specified first followed by the pipe thread or straight thread end.
                    All fittings in this section are ordered as assemblies only unless otherwise specified by customers when
                    ordering. (Contact World Wide Fittings when ordering fittings as bodies only or configurations which are
                    non-standard.)
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>CONFORMANCE</b></h5>
                <br/>
                <p>
                    7000 Series fittings meet SAE J514 and MS (Military Specifications).
                </p>
                <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <img src="{{asset('images/flareless-2.JPG')}}" alt=" " >
                </div>
            </div>

        </div>
        <!--second page-->
        <div class="row fontType">
            <div class="col-md-6 col-xs-12">
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>ASSEMBLY INSTRUCTIONS</b></h5>
                <br/>
                <p>
                    Using a presetting tool, the flareless sleeve should be preset and inspected before final assembly of
                    the fittings is made. All Stainless Sleeves must be preset before final installation.
                </p>
                <ul style="list-style: decimal;">
                    <li>Cut the tubing to the desired length making sure the ends are square. (Refer to the tube penetration
                        chart when measuring tube length.) Remove all burrs and foreign matter from the end of the tubing and
                        clean thoroughly. </li>
                    <li>
                        Lubricate the threads of the preset body, tube nut, and outer edge of both ends of the flareless sleeve.
                    </li>
                    <li>
                        Slide the tube nut and the flareless sleeve onto the tubing making sure the open end of the nut and
                        the leading "bite" edge of the sleeve are facing the end of the tubing.
                    </li>
                    <li>
                        Insert the tubing into the preset body until it hits the bottom or shoulder of the tool. Bring the
                        flareless sleeve and tube nut down to the preset tool and tighten the nut to the finger-tight position.
                    </li>
                    <li>
                        Using a marker make a reference line on the tubing and tube nut. Holding the tubing firmly in the
                        preset tool and using a wrench, tighten the tube nut an additional 1-3/4 turns past the finger-tight position.
                        Disassemble the nut from the preset tool and inspect the .areless sleeve for the proper bite into the tube.
                        (Refer to example drawings for correct and incorrect preset sleeves.)
                    </li>
                    <li>
                        Should the preset sleeve look acceptable, insert the tube with the preset sleeve into the fitting
                        body to be assembled. Tighten the tube nut to the finger-tight position. Then using a wrench tighten
                        the nut until a sudden rise in torque is felt. From the point where the sudden rise in torque started
                        turn the nut an additional 1/3 to 1/4 turn. Your assembly is now ready for use.
                    </li>
                </ul>
                <p>
                    (Refer to the assembly torque charts during installation of 7000 series fittings from the port and
                    thread assemblies section.)
                </p>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-md-6  col-xs-6">
                    <img src="{{asset('images/flareless-3.JPG')}}" alt=" " >
                </div>
                <div class="col-md-6 col-xs-6">
                    <img src="{{asset('images/flareless-4.JPG')}}" alt=" " >
                </div>
                <div class="col-md-6 col-xs-6">
                    <img src="{{asset('images/flareless-5.JPG')}}" alt=" " >
                </div>
                <div class="col-md-6 col-xs-6">
                    <img src="{{asset('images/flareless-6.JPG')}}" alt=" " >
                </div>
                <div class="col-md-6 col-xs-6">
                    <img src="{{asset('images/flareless-7.JPG')}}" alt=" " >
                </div>
            </div>
        </div>
        <!--end of second page-->
        <div class="row">
            <br/>
            <br/>
            <div class="col-md-6 col-xs-12">
                <img src="{{asset('images/flareless-8.JPG')}}" alt=" " >
            </div>
            <div class="col-md-6 col-xs-12">
                <img src="{{asset('images/flareless-9.JPG')}}" alt=" " >
            </div>
            <br/>
            <div class="col-md-6 col-md-offset-2 col-xs-12">
                <br/>
                <!--Table begining-->
                <table class="table">

                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b>Nominal Tube<br>O.D</b></td>
                        <td  valign="bottom" class="smalltext"><b>A</b></td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/8</td>
                        <td  class="smalltext">0.19</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/16</td>
                        <td  class="smalltext">0.23</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/4</td>
                        <td  class="smalltext" nowrap>0.23</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">5/16</td>
                        <td  class="smalltext">0.25</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">3/8</td>
                        <td  class="smalltext">0.25</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1/2</td>
                        <td  class="smalltext">0.31</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">5/8</td>
                        <td  class="smalltext">0.35</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/4</td>
                        <td  class="smalltext">0.35</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">7/8</td>
                        <td  class="smalltext">0.35</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1</td>
                        <td  class="smalltext">0.42</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-1/4</td>
                        <td  class="smalltext">0.42</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1-1/2</td>
                        <td  class="smalltext">0.49</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">2</td>
                        <td  class="smalltext">0.49</td>
                    </tr>


                </table>
                <!--Table ending-->
            </div>
            <div class="col-md-6 col-xs-12 fontType">
                <br/>
                <p>
                    The above example shows an assembly using the SAE "A" Style flareless sleeve. When
                    assembled properly the SAE "A"style  flareless sleeve assembly will exhibit the following characteristics.
                </p>
                <br/>
                <ul style="list-style: decimal; padding-left: 20px;">
                    <li>
                        A slight indentation around the end of the tube which indicates that the tube was fully inserted
                        during installation.
                    </li>
                    <li>
                        A visible metal lip will have been raised 75% to 100% of the thickness of the sleeves lead biting
                        edge all the way around the tube. (See illustration above.)
                    </li>
                    <li>
                        The midsection of the sleeve behind the leading edge has a slight bow to provide contact with the
                        internal taper of the fitting body and form a sealing point.
                    </li>
                    <li>
                        The back (tail) end of the sleeve will be in firm contact with the O.D. of the tube.
                    </li>
                </ul>
                <br/>
            </div>
            <div class="col-md-6 col-xs-12 fontType">
                <br/>
                <p>
                    The above example shows an assembly using the SAE "B" Style flareless sleeve. When assembled properly
                    the SAE "B" style  flareless sleeve assembly will exhibit the following characteristics.
                </p>
                <br/>
                <ul style="list-style: decimal; padding-left: 20px;">
                    <li>
                        A slight indentation around the end of the tube which indicates that the tube was fully inserted
                        during installation.
                    </li>
                    <li>
                        A metal lip will have been raised 75% to 100% of the thickness of the sleeves biting edge which is
                        hidden under the leading end of the sleeve.(See illustration above.)
                    </li>
                    <li>
                        The midsection of the sleeve behind the leading edge has a slight bow to provide contact with the
                        internal taper of the fitting body and form a sealing point.
                    </li>

                </ul>
                <br/>
            </div>
        </div>
    </div>
    @endsection