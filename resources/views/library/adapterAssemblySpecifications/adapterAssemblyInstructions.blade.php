@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if (!Auth::check())

                    <p class="wel"><a href="{{route('login')}}">Welcome visitor you can login or create an account.</a></p><br>

                @endif
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">

                        {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}

                        {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}

                        <input type="submit" value="">

                        {!!  Form::close() !!}

                    </div>

                    <div class="clearfix"> </div>
                </div>

            </div>
        </div>
        <!--Main content-->
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('adapterAssemblySpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Assembly Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>37 Flare (JIC) Fitting Adapter Assembly Instuctions</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />

        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <img src="{{asset('images/5000series-1.JPG')}}" alt=" " class="img img-responsive">
            </div>
            <div class="col-md-6 col-xs-12 fontType">
                <br/>
                <h5 style="text-decoration: underline; color: #38ba5f;"><b>CONSTRUCTION</b></h5>
                <br/>
                <p>
                    5000 Series Fittings consist of three main components: Bodies, Nuts, and Sleeves.
                    Bodies – Straight bodies are machined from bar stock. Shape fittings (elbows, tees, & crosses) are made from forgings.
                    Tube Nuts – Tube Nuts in sizes 1/4", 3/8", 1/2", 5/8",3/4", 1”, are Cold Formed in 1010 steel. Tube Nuts
                    in sizes 1/8", 3/16", 5/16", 7/8", 1-1/4", 1-1/2", & 2" are hot forged from 1045.
                    Tube Sleeves – Tube Sleeves for 5000 series fittings are machined from 12L14 bar stock.

                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>APPLICATIONS</b></h5>
                <br/>
                <p>
                    For use with machine tools, agricultural and earth moving machinery, instrumentation, chemical processing
                    systems, and most other high pressure hydraulic applications.
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>PRESSURE RATINGS</b></h5>
                <br/>
                <p>
                    Working pressures up to 5000 PSI. Working pressures will vary depending on tube and fitting size. (For
                    applications using extreme pressures at 5000 PSI or above, contact World Wide Fittings.)
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>MATERIAL</b></h5>
                <br/>
                <ul style="list-style: none;padding-left: 20px; ">
                    <li>Steel – Bar Stock 12L14, 1045 </li>
                    <li>Cold Formed 1010 </li>
                    <li>Stainless Steel – Type 316</li>
                </ul>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>PLATING</b></h5>
                <br/>
                <ul style="list-style: none; padding-left: 20px;">
                    <li>Clear Zinc Trivalent</li>
                    <li>Passivated</li>
                </ul>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>HOW TO ORDER</b></h5>
                <br/>
                <p>
                    Tubing sizes are measured in sixteenths of an inch. Available sizes are 1/8" through 2". When ordering,
                    the tube end of the fitting must be specified first followed by the pipe thread or straight thread end.
                    All fittings in this section are ordered as bodies only unless specified by customers when ordering.
                    (Contact World Wide for details on ordering fittings preassembled with nuts and sleeves.)
                </p>
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>CONFORMANCE</b></h5>
                <br/>
                <p>
                    5000 Series Fittings Meet SAE J514 and MS
                    (Military Specifications).
                </p>

            </div>
            <div class="col-md-6 col-xs-12 fontType">
                <br/>
                <h5 style="text-decoration: underline;  color: #38ba5f;"><b>ASSEMBLY INSTRUCTIONS</b></h5>
                <br/>
                <ul style="list-style: decimal; padding-left: 20px;">
                    <li>Cut the tubing to the desired length making sure the
                        ends are square. Remove all burrs from the tubing ends and clean thoroughly.
                    </li>
                    <br/>
                    <li>Slide the tube nut on the tubing making sure that the
                        open end of the nut is facing the tube end. Then slide the tube sleeve on the tubing making sure the
                        seat of the sleeve is facing the tube end.
                    </li>
                    <br/>
                    <li>Using a 37° flaring tool, flare the tubing end. Inspect the flared end for dimensions that are out of
                        tolerance and may cause underflare, overflare, and excessive wall thinning which can interfere with
                        the operation and quality of the assembled fitting. (For examples of underflaring and overflaring refer to the tube flaring drawings.)
                    </li>
                    <br/>
                    <li>Pull the sleeve up to the flared end and then pull the tube nut up and over the sleeve and flared end.
                    </li>
                    <br/>
                    <li>Lubricate the threads on both the nut and the fitting body, align the flared end against the nose of
                        the fitting and assemble to the finger-tight position.
                    </li>
                    <br/>
                    <li>
                        Using a wrench tighten the assembly until the nut is snug, then tighten the nut an additional one to two flats further.
                        (Refer to the assembly torque charts during installation of
                        5000 series fittings from the port and thread assemblies section.)
                    </li>

                </ul>
                <div class="col-md-10 col-md-offset-2 col-xs-12" style="padding: 40px;">
                    <img src="{{asset('images/5000series-2.JPG')}}" alt=" " >
                </div>
                <br/>
            </div>

        </div>

        <div class="row" style="padding-top: 5%;">
            <div class="col-md-6 col-xs-12">
                <div class="col-md-10 col-md-offset-2 col-xs-12" style="padding-bottom: 30px;">
                    <img src="{{asset('images/5000series-3.JPG')}}"alt=" " class="img img-responsive" >
                </div>
                <!--Table begining-->
                <table class="table">

                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext" colspan="2"><b><span class="red">Nominal Tube O.D</span></b></td>
                        <td  valign="bottom" class="smalltext" colspan="1"><b> </b></td>

                    </tr>

                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b><span class="red">Inch</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Metric</b></td>
                        <td  valign="bottom" class="smalltext"><b>A</b></td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/8</td>
                        <td  class="smalltext" nowrap>-</td>
                        <td  class="smalltext">0.07</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/16</td>
                        <td  class="smalltext" nowrap>-</td>
                        <td  class="smalltext">0.08</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/4</td>
                        <td  class="smalltext" nowrap>-</td>
                        <td  class="smalltext">0.09</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">5/16</td>
                        <td  class="smalltext" nowrap>8</td>
                        <td  class="smalltext">0.08</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">3/8</td>
                        <td  class="smalltext" nowrap>10</td>
                        <td  class="smalltext">0.08</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1/2</td>
                        <td  class="smalltext" nowrap>12</td>
                        <td  class="smalltext">0.12</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">5/8</td>
                        <td  class="smalltext" nowrap>14,15,16</td>
                        <td  class="smalltext">0.13</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/4</td>
                        <td  class="smalltext" nowrap>18,20</td>
                        <td  class="smalltext">0.15</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">7/8</td>
                        <td  class="smalltext" nowrap>22</td>
                        <td  class="smalltext">0.15</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1</td>
                        <td  class="smalltext" nowrap>25</td>
                        <td  class="smalltext">0.15</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-1/4</td>
                        <td  class="smalltext" nowrap>30, 32</td>
                        <td  class="smalltext">0.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1-1/2</td>
                        <td  class="smalltext" nowrap>38</td>
                        <td  class="smalltext">0.18</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">2</td>
                        <td  class="smalltext" nowrap>42</td>
                        <td  class="smalltext">0.28</td>
                    </tr>


                </table>
                <!--Table ending-->
            </div>

            <div class="col-md-6 col-xs-12 fontType">
                <div class="col-md-10 col-md-offset-2 col-xs-12" style="padding-bottom: 30px;">
                    <img src="{{asset('images/5000series-4.JPG')}}" alt=" " >
                    <p>
                        <b>Underflaring</b> reduces contact area causing excessive nose collapse
                        and leakage; or in extreme cases, tube pull out under pressure.
                    </p>
                </div>

                <div class="col-md-10 col-md-offset-2 col-xs-12" style="padding-bottom: 30px;">
                    <img src="{{asset('images/5000series-5.JPG')}}" alt=" " class="img img-responsive">
                    <p>
                        <b>Overflaring</b> causes tube nut thread interference, either preventing
                        assembly altogether, or giving a false sense of joint tightness resulting in
                        leakage.
                    </p>
                </div>

            </div>
            <div class="col-md-6 col-xs-12 fontType">
                <div class="col-md-10 col-md-offset-2 col-xs-12" style="padding-bottom: 30px;">
                    <img src="{{asset('images/5000series-6.JPG')}}" alt=" " class="img img-responsive">
                    <p>
                        <b>Comparing flare O.D with sleeve seat and O.D.</b>
                    </p>
                </div>
            </div>
        </div>
        <!--End of main content-->
    </div>
    @endsection
