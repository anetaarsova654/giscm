@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if (!Auth::check())

                    <p class="wel"><a href="{{route('login')}}">Welcome visitor you can login or create an account.</a></p><br>

                @endif
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">

                        {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}

                        {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}

                        <input type="submit" value="">

                        {!!  Form::close() !!}

                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-md-12 col-sm-12 col-xs-12"><a href="{{route('adapterAssemblySpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Adapter Assembly Specifications</h3></a></div>
            <div class="col-md-9 col-sm-offset-3 col-xs-12"><h2 class="subheading"><b>Safety information warning</b></h2></div>
            <div class="col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p style="font-size: medium;">
                Proper selection of tubing, adapters, and installation, is essential to the proper
                operation of fluid power equipment and overall safety.
                </p>
                <br/>
                <p style="font-size: medium; color: #38ba5f;">
                <b>
                    Note: IMPROPER SELECTION OR IMPROPER USE OF THE ADAPTERS DESCRIBED WITHIN
                    GISCM LIBRARY CAN CAUSE DEATH, PERSONAL INJURY, AND PROPERTY DAMAGE.

                </b>
                </p>
                <br/>
                <p style="font-size: medium;">
                Due to operating conditions and applications for adapters, users through their own
                Analysis, testing, and discretion, is solely responsible for making adapter selection; in addition, to
                assuring that all performance and safety requirements for the application are met.
                </p>
                <br/>
                <p style="font-size: medium; ">
                The following conditions should be considered when specifying components
                and equipment for your application:
                </p>
                <br/>
                <ul style="list-style: decimal; padding-left: 20px; font-size: medium;">
                <li>Oil type</li>
                <li>Tube size wall thickness</li>
                <li>Tube size Inside diameter</li>
                <li>Temperature</li>
                <li>Pressure/Flowrate</li>
                <li>Component and System Safety Factor</li>
                </ul>
            </div>
            <div class="col-md-2"></div>
            <div class="clearfix"></div>
            <br/>
            <br/>
        </div>

    </div>
    @endsection