@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        <div class="container">
            <div class="row">
                <div class="header-bottom-on">
                    @if (!Auth::check())

                        <p class="wel"><a href="{{route('login')}}">Welcome visitor you can login or create an account.</a></p><br>

                    @endif
                    <br />
                    <div class="header-can">
                        <ul class="social-in">

                            <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                            <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                        </ul>
                        <div class="search">

                            {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}

                            {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}

                            <input type="submit" value="">

                            {!!  Form::close() !!}

                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('adapterAssemblySpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Adapter Assembly Specifications</h3></a></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>Adapter Assembly into Ports</b></h2></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12 col-xs-12 fontType">
                    <p>
                        Hydraulic tube and pipe fittings are most commonly used with three different types of port threads:
                        1) SAE Straight Thread “O” Ring 2) ISO Metric Thread “O” Ring 3) National Pipe Thread
                    </p>
                    <br/>
                </div>
                <div class="col-md-6 col-xs-12 fontType">
                    <p><b>STRAIGHT THREAD PORTS</b></p>
                    <br/>
                    <p>
                        SAE straight threads can be machined for either adjustable or nonadjustable hydraulic fittings. Carefully
                        machined internal and external UN/UNF straight threads provide the holding power for hydraulic fittings
                        when installed in high pressure hydraulic systems.
                    </p>
                    <br/>
                    <p>
                        SAE straight threads sealing power comes from the use of a high quality 90-durometer Buna-N “O” Ring as
                        specified by SAE-J514. During assembly, the straight threads provide adjustability on shape fittings
                        where maneuverability and alignment are necessary for tube line routing.
                    </p>
                    <br/>
                    <p>
                        After the hydraulic fittings with the SAE straight thread have been properly installed, the hydraulic
                        “O” Ring provides a high pressure seal by becoming seated between the angular groove in the female port
                        and the male end of the hydraulic fittings.
                    </p>
                    <br/>
                    <h5 style="text-decoration: underline;  color: #38ba5f;"><b>ASSEMBLY INSTRUCTIONS</b></h5>
                    <br/>
                    <ul style="list-style:decimal; padding-left: 20px;">
                        <li>
                            Inspect the hydraulic fitting and mating port to ensure that there is no foreign matter present or
                            nicks in the “O” Ring to cause a leak.
                        </li>
                        <li>Lubricate the “O” Ring with a light coating of oil or hydraulic fluid. </li>
                        <li>
                            On the fittings adjustable thread, back the locknut off as far as possible and screw the fitting into
                            the port until the flat back-up washer makes contact with the face of the port.
                        </li>
                        <li>
                            Align the adjustable fitting with the end of the tube assembly by unscrewing the fitting to the
                            proper direction. (Do not unscrew the fitting more than one full turn.)
                        </li>
                        <li>
                            Using a wrench to hold the fitting in the desired position, tighten the locknut to the proper torque
                            value as shown in the torque chart for adjustable SAE threads.
                        </li>
                        <li>
                            Visually inspect the fitting assembly to confirm that the “O” Ring has seated properly in the female
                            ports angular seating groove and the back-up washer is seated .at against the face of the port.
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12 fontType">
                    <p>
                        Inspect threads before assembly to be assured that the male thread and female port are free of burrs
                        and other foreign matter.
                    </p>
                    <br/>
                    <p>
                        Lubricate the hydraulic “O” Ring before installing the fitting to prevent the “O” Ring from possible
                        damage during installation.
                    </p>
                    <br/>
                    <p>
                        Hydraulic fittings machined from forgings may have higher assembly torque values than are shown in the
                        chart below.
                    </p>
                    <br/>
                    <p>
                        (See drawing below for example of proper installation.)
                    </p>
                    <div class="col-md-12 col-xs-12">
                        <img src="{{asset('images/assembly-1.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <img src="{{asset('images/assembly-2.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>Port Thread Assembly Torques</b></h2></div>
                <div class="col-md-8 col-md-offset-1 col-xs-12 fontType">
                    <br/>
                    <p>
                        The charts below shows the approximate assembly torque values for hydraulic fittings and “O” Ring plugs
                        with adjustable and non-adjustable SAE straight threads and ISO metric threads.
                    </p>
                    <br/>
                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <!--Table begining-->
                    <div class="table-responsive">
                        <table class="table" style="margin-bottom: 0px;">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="4" valign="bottom" class="smalltext"><b>J1926-2<br><span class="red">ORFS Adjustable & Non-adjustable</span></b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="4" valign="bottom" class="smalltext"><b>Stud End QualificationsTorque</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b><span class="red">Dash Size.</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>Thread</b></td>
                                <td  valign="bottom" class="smalltext"><b>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">3</td>
                                <td  class="smalltext" nowrap>3/8-24</td>
                                <td  class="smalltext">10<sup>(2)</sup></td>

                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">4</td>
                                <td  class="smalltext" nowrap>7/16-20</td>
                                <td  class="smalltext">20<sup>(2)</sup></td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">5</td>
                                <td  class="smalltext" nowrap>1/3-20</td>
                                <td  class="smalltext">40</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">6</td>
                                <td  class="smalltext" nowrap>9/16-18</td>
                                <td  class="smalltext">45</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">8</td>
                                <td  class="smalltext" nowrap>3/4-16</td>
                                <td  class="smalltext">85</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">10</td>
                                <td  class="smalltext" nowrap>7/8-14</td>
                                <td  class="smalltext">115</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">12</td>
                                <td  class="smalltext" nowrap>1-1/16-12</td>
                                <td  class="smalltext">170</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">14</td>
                                <td  class="smalltext" nowrap>1-3/16-12</td>
                                <td  class="smalltext">215</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">16</td>
                                <td  class="smalltext" nowrap>1-5/16-12</td>
                                <td  class="smalltext">270</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">20</td>
                                <td  class="smalltext" nowrap>1-5/8-12</td>
                                <td  class="smalltext">285</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">24</td>
                                <td  class="smalltext" nowrap>1-7/8-12</td>
                                <td  class="smalltext">370</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">32</td>
                                <td  class="smalltext" nowrap>2-1/2-12</td>
                                <td  class="smalltext">540</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table ending-->

                    <div style="background-color: #CCCC99">
                        <ul style="list-style-type: none;">
                            <li class="smalltext"><b>1. To convert from N*m to lb.ft, multiply by 0.737. </b></li>
                            <li class="smalltext"><b>2. These values are for adjustable stud end, Nonadjustable values are 15 and 35 for sizes 3/8-24 and 7/16-20, respectively.</b></li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <!--Table 2 begining-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="4" valign="bottom" class="smalltext"><b>J1926-4</b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="4" valign="bottom" class="smalltext"><b>Test torque for internal hex port plugs<br>and test torques for heavy-duty(s-series)<br>external hex plug screws</b></td>
                            </tr>

                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b> </b></td>
                                <td colspan="2" valign="bottom" class="smalltext"><b>Plug</b></td>
                            </tr>

                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b><span class="red">Dash Size</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>Port or Plug<br>Thread sizes</b></td>
                                <td  valign="bottom" class="smalltext"><b>Internal hex<br>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                                <td  valign="bottom" class="smalltext"><b><span class="red">External hex<br>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</span></b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2</td>
                                <td  class="smalltext" nowrap>5/16-24</td>
                                <td  class="smalltext">7</td>
                                <td  class="smalltext">12</td>

                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">3</td>
                                <td  class="smalltext" nowrap>3/8-24</td>
                                <td  class="smalltext">14</td>
                                <td  class="smalltext">20</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">4</td>
                                <td  class="smalltext" nowrap>7/16-20</td>
                                <td  class="smalltext">20</td>
                                <td  class="smalltext">35</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">5</td>
                                <td  class="smalltext" nowrap>1/2-20</td>
                                <td  class="smalltext">27</td>
                                <td  class="smalltext">40</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">6</td>
                                <td  class="smalltext" nowrap>9/16-18</td>
                                <td  class="smalltext">45</td>
                                <td  class="smalltext">45</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">8</td>
                                <td  class="smalltext" nowrap>3/4-16</td>
                                <td  class="smalltext">85</td>
                                <td  class="smalltext">85</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">10</td>
                                <td  class="smalltext" nowrap>7/8-14</td>
                                <td  class="smalltext">110</td>
                                <td  class="smalltext">110</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">12</td>
                                <td  class="smalltext" nowrap>1-1/16-12</td>
                                <td  class="smalltext">170</td>
                                <td  class="smalltext">170</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">14</td>
                                <td  class="smalltext" nowrap>1-3/16-12</td>
                                <td  class="smalltext">235</td>
                                <td  class="smalltext">235</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">16</td>
                                <td  class="smalltext" nowrap>1-5/16-12</td>
                                <td  class="smalltext">310</td>
                                <td  class="smalltext">310</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">20</td>
                                <td  class="smalltext" nowrap>1-5/8-12</td>
                                <td  class="smalltext">340</td>
                                <td  class="smalltext">340</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">24</td>
                                <td  class="smalltext" nowrap>1-7/8-12</td>
                                <td  class="smalltext">420</td>
                                <td  class="smalltext">420</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">32</td>
                                <td  class="smalltext" nowrap>2-1/2-12</td>
                                <td  class="smalltext">510</td>
                                <td  class="smalltext">510</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 2 ending-->
                </div>
                <div class="col-md-offset-1 col-md-4 col-xs-12">
                    <!--Table 3 begining-->
                    <div class="table-responsive">
                        <table class="table" style="margin-bottom: 0px;">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="3" valign="bottom" class="smalltext"><b>J1926-3<br><span class="red">JIC, Flareless adjustable & non-adjustable</span></b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="3" valign="bottom" class="smalltext"><b>Stud End Qualifications Torque</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b><span class="red">Dash Size.</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>Thread</b></td>
                                <td  valign="bottom" class="smalltext"><b>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2</td>
                                <td  class="smalltext" nowrap>5/16-24</td>
                                <td  class="smalltext">8</td>

                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">3</td>
                                <td  class="smalltext" nowrap>3/8-24</td>
                                <td  class="smalltext">10</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">4</td>
                                <td  class="smalltext" nowrap>7/16-20</td>
                                <td  class="smalltext">18</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">5</td>
                                <td  class="smalltext" nowrap>1/2-20</td>
                                <td  class="smalltext">25</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">6</td>
                                <td  class="smalltext" nowrap>9/16-18</td>
                                <td  class="smalltext">30</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">8</td>
                                <td  class="smalltext" nowrap>3/4-16</td>
                                <td  class="smalltext">50</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">10</td>
                                <td  class="smalltext" nowrap>7/8-14</td>
                                <td  class="smalltext">60</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">12</td>
                                <td  class="smalltext" nowrap>1-1/16-12</td>
                                <td  class="smalltext">95</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">14</td>
                                <td  class="smalltext" nowrap>1-3/16-12</td>
                                <td  class="smalltext">125</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">16</td>
                                <td  class="smalltext" nowrap>1-5/16-12</td>
                                <td  class="smalltext">150</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">20</td>
                                <td  class="smalltext" nowrap>1-5/8-12</td>
                                <td  class="smalltext">200</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">24</td>
                                <td  class="smalltext" nowrap>1-7/8-12</td>
                                <td  class="smalltext">210</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">32</td>
                                <td  class="smalltext" nowrap>2-1/2-12</td>
                                <td  class="smalltext">300</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 3 ending-->
                    <div style="background-color: #CCCC99">
                        <ul style="list-style-type: none;">
                            <li class="smalltext"><b>1. To convert from N*m to lb.ft, multiply by 0.737. </b></li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <img src="{{asset('images/assembly-3.JPG')}}" alt=" "  class="img img-responsive" style="padding-top: 50px;">
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <br/>
                <div class="col-md-offset-1 col-md-4 col-xs-12">
                    <!--Table 4 begining ISO-->
                    <div class="table-responsive">
                        <table class="table" style="margin-bottom: 0px;">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="10" valign="bottom" class="smalltext"><b>ISO 6149-2<br><span class="red">ORFS adjustable & non-adjustable</span></b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="10" valign="bottom" class="smalltext"><b>Torques for Stud End Qualification test</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Thread</b></td>
                                <td  valign="bottom" class="smalltext"><b>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M8 X 1</td>
                                <td  class="smalltext" nowrap>10</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M10 X 1</td>
                                <td  class="smalltext" nowrap>20</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M12 X 1.5</td>
                                <td  class="smalltext" nowrap>35</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M14 X 1.5</td>
                                <td  class="smalltext" nowrap>45</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M16 X 1.5</td>
                                <td  class="smalltext" nowrap>55</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M18 X 1.5</td>
                                <td  class="smalltext" nowrap>70</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M20 X 1.5<sup>a</sup></td>
                                <td  class="smalltext" nowrap>80</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M22 X 1.5</td>
                                <td  class="smalltext" nowrap>100</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M27 X 2.0</td>
                                <td  class="smalltext" nowrap>170</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M30 X 2.0</td>
                                <td  class="smalltext" nowrap>215</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M33 X 2.0</td>
                                <td  class="smalltext" nowrap>310</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M42 X 2.0</td>
                                <td  class="smalltext" nowrap>330</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M48 X 2.0</td>
                                <td  class="smalltext" nowrap>420</td>
                            </tr>
                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M60 X 2.0</td>
                                <td  class="smalltext" nowrap>500</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 4 ending ISO-->
                    <div style="background-color: #CCCC99">
                        <ul style="list-style-type: none;">
                            <li class="smalltext"><b>a. For plugs for cartridge valve cavities only (see ISO 6149-4 and ISO 7789). </b></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-4 col-xs-12">
                    <!--Table 5 begining ISO-->
                    <div class="table-responsive">
                        <table class="table" style="margin-bottom: 0px;">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="3" valign="bottom" class="smalltext"><b>ISO 6149-4<br><span class="red">ORFS adjustable & non-adjustable</span></b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="3" valign="bottom" class="smalltext"><b>Torques for Plug Qualification test</b></td>
                            </tr>

                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b> </b></td>
                                <td colspan="2" valign="bottom" class="smalltext"><b>Plug</b></td>
                            </tr>

                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Thread</b></td>
                                <td  valign="bottom" class="smalltext"><b>Internal hex<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                                <td  valign="bottom" class="smalltext"><b>External hexTorque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M8 X 1</td>
                                <td  class="smalltext" nowrap>8</td>
                                <td  class="smalltext" nowrap>10</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M10 X 1</td>
                                <td  class="smalltext" nowrap>15</td>
                                <td  class="smalltext" nowrap>20</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M12 X 1.5</td>
                                <td  class="smalltext" nowrap>22</td>
                                <td  class="smalltext" nowrap>35</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M14 X 1.5</td>
                                <td  class="smalltext" nowrap>45</td>
                                <td  class="smalltext" nowrap>45</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M16 X 1.5</td>
                                <td  class="smalltext" nowrap>55</td>
                                <td  class="smalltext" nowrap>55</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M18 X 1.5</td>
                                <td  class="smalltext" nowrap>70</td>
                                <td  class="smalltext" nowrap>70</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M20 X 1.5<sup>a</sup></td>
                                <td  class="smalltext" nowrap>80</td>
                                <td  class="smalltext" nowrap>80</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M22 X 1.5</td>
                                <td  class="smalltext" nowrap>100</td>
                                <td  class="smalltext" nowrap>100</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M27 X 2.0</td>
                                <td  class="smalltext" nowrap>170</td>
                                <td  class="smalltext" nowrap>170</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M30 X 2.0</td>
                                <td  class="smalltext" nowrap>215</td>
                                <td  class="smalltext" nowrap>215</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M33 X 2.0</td>
                                <td  class="smalltext" nowrap>310</td>
                                <td  class="smalltext" nowrap>310</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M42 X 2.0</td>
                                <td  class="smalltext" nowrap>330</td>
                                <td  class="smalltext" nowrap>330</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M48 X 2.0</td>
                                <td  class="smalltext" nowrap>420</td>
                                <td  class="smalltext" nowrap>420</td>
                            </tr>
                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M60 X 2.0</td>
                                <td  class="smalltext" nowrap>500</td>
                                <td  class="smalltext" nowrap>500</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 5 ending ISO-->
                    <div style="background-color: #CCCC99">
                        <ul style="list-style-type: none;">
                            <li class="smalltext"><b>a. For cartridge valve cavity applications only (see ISO 7789). </b></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-4 col-xs-12">

                    <!--Table 6 begining ISO-->
                    <div class="table-responsive">
                        <table class="table ">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="2" valign="bottom" class="smalltext"><b>ISO 6149-2<br><span class="red">ORFS adjustable & non-adjustable</span></b></td>
                            </tr>
                            <tr  align="center" bgcolor="ccccbb">
                                <td colspan="2" valign="bottom" class="smalltext"><b>Torques for Stud End Qualification test</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Thread</b></td>
                                <td  valign="bottom" class="smalltext"><b>Torque<br>[N-m]<sup>1</sup><br>+10%-0%</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M8 X 1</td>
                                <td  class="smalltext" nowrap>8</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M10 X 1</td>
                                <td  class="smalltext" nowrap>15</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M12 X 1.5</td>
                                <td  class="smalltext" nowrap>25</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M14 X 1.5</td>
                                <td  class="smalltext" nowrap>35</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M16 X 1.5</td>
                                <td  class="smalltext" nowrap>40</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M18 X 1.5</td>
                                <td  class="smalltext" nowrap>45</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M20 X 1.5></td>
                                <td  class="smalltext" nowrap>60</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M22 X 1.5</td>
                                <td  class="smalltext" nowrap>100</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M27 X 2.0</td>
                                <td  class="smalltext" nowrap>130</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M30 X 2.0</td>
                                <td  class="smalltext" nowrap>130</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M33 X 2.0</td>
                                <td  class="smalltext" nowrap>160</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M42 X 2.0</td>
                                <td  class="smalltext" nowrap>210</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">M48 X 2.0</td>
                                <td  class="smalltext" nowrap>260</td>
                            </tr>
                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">M60 X 2.0</td>
                                <td  class="smalltext" nowrap>315</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 6 ending ISO-->

                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <img src="{{asset('images/assembly-4.JPG')}}" alt=" "  class="img img-responsive" style="padding-top: 50px;">
                </div>
            </div>
            <div class="row">
                <br/>
                <div class="col-md-6 col-xs-12 fontType">
                    <h5 style="text-decoration: underline;  color: #38ba5f;"><b>DRYSEAL PIPE THREADS</b></h5>
                    <br/>
                    <p>
                        NPTF threads are formed to have a 3/4” taper with tightly machined tolerances to provide contact at the
                        crest and root of the thread as well as the flank. The metal to metal seal of the thread crest, root,
                        and flank, together provide a complete seal to help prevent the possibility of spiral leakage.
                    </p>
                    <br/>
                    <p>
                        NPTF threads also provide the holding power for the connection while in high pressure service, which
                        limits the reliability of the seal which the threads are making with the female port.
                    </p>
                    <br/>
                    <p>
                        For high pressure applications where constant vibration is running through the hydraulic systems,
                        hydraulic fittings with SAE straight threads which connect to female or male ports are recommended.
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h5 style="text-decoration: underline;  color: #38ba5f;"><b>ASSEMBLY INSTRUCTIONS</b></h5>
                    <br/>
                    <ul style="list-style: decimal; padding-left: 20px;">
                        <li>
                            Visually inspect the fitting and the mating port and remove any burrs or foreign matter which may
                            be present.
                        </li>
                        <br/>
                        <li>
                            Apply thread sealant if needed to the male threads. Do not apply thread sealant to the be- ginning
                            1/3rd of the male threads so as to keep the hydraulic system free from being contaminated by sealant
                            which enters the system from the fitting connection.
                        </li>
                        <br/>
                        <li>
                            Install the fitting to the finger-tight position.Using a wrench turn the fitting approximately
                            2 - 2 1/2 turns past the finger-tight position.
                        </li>
                        <br/>
                    </ul>
                    <p>
                        (See drawing below for example of proper installation.)
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <div class="col-md-12 col-xs-12">
                        <img src="{{asset('images/assembly-5.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 fontType">
                    <!--Table 7  Tapered pipe thread size-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="2" valign="bottom" class="smalltext"><b>Tapered Pipe Thread size</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>NPTF</b></td>
                                <td  valign="bottom" class="smalltext"><b>T.F.F.T</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1/8-27</td>
                                <td  class="smalltext" nowrap>2 - 3</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1/4-18</td>
                                <td  class="smalltext" nowrap>2 - 3</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">3/8-18</td>
                                <td  class="smalltext" nowrap>2 - 3</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1/2-14</td>
                                <td  class="smalltext" nowrap>2 - 3</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">3/4-14</td>
                                <td  class="smalltext" nowrap>2 - 3</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1-11-1/2</td>
                                <td  class="smalltext" nowrap>1.5-2.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1-1/4-11-1/2</td>
                                <td  class="smalltext" nowrap>1.5-2.5</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1-1/2-11-1/2</td>
                                <td  class="smalltext" nowrap>1.5-2.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2-11-1/2</td>
                                <td  class="smalltext" nowrap>1.5-2.5</td>
                            </tr>
                        </table>
                    </div>
                    <!--Table 7 ending-->
                    <!--Table 8 Steel assembly-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="4" valign="bottom" class="smalltext"><b>Steel Assembly</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>NPSM<br>Size<br>In.</b></td>
                                <td  valign="bottom" class="smalltext"><b>In.-Lb.<br>Torque (+10%-0%)</b></td>
                                <td  valign="bottom" class="smalltext"><b>Ft.-Lb.<br>Torque (+10%-0%)</b></td>
                                <td  valign="bottom" class="smalltext"><b>F.F.F.T</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1/8-27</td>
                                <td  class="smalltext" nowrap>106</td>
                                <td  class="smalltext">9</td>
                                <td  class="smalltext" >1.0-1.5</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1/4-18</td>
                                <td  class="smalltext" nowrap>156</td>
                                <td  class="smalltext">13</td>
                                <td  class="smalltext" >1.0-1.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">3/8-18</td>
                                <td  class="smalltext" nowrap>192</td>
                                <td  class="smalltext">16</td>
                                <td  class="smalltext" >1.0-1.5</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1/2-14</td>
                                <td  class="smalltext" nowrap>396</td>
                                <td  class="smalltext">33</td>
                                <td  class="smalltext" >1.0-1.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">3/4-14</td>
                                <td  class="smalltext" nowrap>580</td>
                                <td  class="smalltext">48</td>
                                <td  class="smalltext" >2.0-2.5</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1-11-1/2</td>
                                <td  class="smalltext" nowrap>1140</td>
                                <td  class="smalltext">95</td>
                                <td  class="smalltext" >2.0-2.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1-1/4-11-1/2</td>
                                <td  class="smalltext" nowrap>1420</td>
                                <td  class="smalltext">118</td>
                                <td  class="smalltext" >2.0-2.5</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1-1/2-11-1/2</td>
                                <td  class="smalltext" nowrap>2480</td>
                                <td  class="smalltext">237</td>
                                <td  class="smalltext" >2.0-2.5</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2-11-1/2</td>
                                <td  class="smalltext" nowrap>3720</td>
                                <td  class="smalltext">310</td>
                                <td  class="smalltext" >2.0-2.5</td>
                            </tr>
                        </table>
                    </div>
                    <!--Table 8 ending-->
                    <div class="col-md-12 col-xs-12">
                        <p>Steps:</p>
                        <ul style="list-style:decimal;">
                            <li>Finger tight</li>
                            <li>French tighten to torque specs in table above.</li>
                        </ul>
                        <img src="{{asset('images/assembly-6.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                </div>
            </div><!--end of the row-->
            <div class="row">
                <br/>
                <div class="col-md-9 col-md-offset-1 col-xs-12">

                    <!--Table 9 begining-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="5" valign="bottom" class="smalltext"><b>J1453-2-3 & J1453-3</b></td>
                            </tr>
                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="5" valign="bottom" class="smalltext"><b>Face seal end qualification torque</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b><span class="red">Nominical<br>Metric<br>Tube OD<br>mm</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>Inch<br>Tube OD<br>or Hose ID<br>Dash size</b></td>
                                <td  valign="bottom" class="smalltext"><b>Nominal<br>Inch<br>Tube OD<br>mm</b></td>
                                <td  valign="bottom" class="smalltext"><b><span class="red">"O" Ring<br>Face seal end<br>Thread<br>Inch</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>Torque<br>[N-m]</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">6</td>
                                <td  class="smalltext" nowrap>4</td>
                                <td  class="smalltext">6.35</td>
                                <td  class="smalltext" nowrap>9/16-28</td>
                                <td  class="smalltext">25</td>

                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">8</td>
                                <td  class="smalltext" nowrap>5</td>
                                <td  class="smalltext">7.94</td>
                                <td  class="smalltext" nowrap>5/8-18</td>
                                <td  class="smalltext">30</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">10</td>
                                <td  class="smalltext" nowrap>6</td>
                                <td  class="smalltext">9.52</td>
                                <td  class="smalltext" nowrap>11/16-16</td>
                                <td  class="smalltext">40</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">12</td>
                                <td  class="smalltext" nowrap>8</td>
                                <td  class="smalltext">12.70</td>
                                <td  class="smalltext" nowrap>13/16-16</td>
                                <td  class="smalltext">55</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">16</td>
                                <td  class="smalltext" nowrap>10</td>
                                <td  class="smalltext">15.88</td>
                                <td  class="smalltext" nowrap>1-14</td>
                                <td  class="smalltext">60</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">20</td>
                                <td  class="smalltext" nowrap>12</td>
                                <td  class="smalltext">19.05</td>
                                <td  class="smalltext" nowrap>1-3/16-12</td>
                                <td  class="smalltext">90</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">22</td>
                                <td  class="smalltext" nowrap>14</td>
                                <td  class="smalltext">22.23</td>
                                <td  class="smalltext" nowrap>1-5/16-12</td>
                                <td  class="smalltext">115</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">25</td>
                                <td  class="smalltext" nowrap>16</td>
                                <td  class="smalltext">25.40</td>
                                <td  class="smalltext" nowrap>1-7/16-12</td>
                                <td  class="smalltext">125</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">30</td>
                                <td  class="smalltext" nowrap>20</td>
                                <td  class="smalltext">31.75</td>
                                <td  class="smalltext" nowrap>1-11/16-12</td>
                                <td  class="smalltext">170</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">38</td>
                                <td  class="smalltext" nowrap>24</td>
                                <td  class="smalltext">38.10</td>
                                <td  class="smalltext" nowrap>2-12</td>
                                <td  class="smalltext">200</td>
                            </tr>
                        </table>
                    </div>
                    <!--Table 9 ending-->

                </div>
                <br/>
                <div class="col-md-12 col-xs-12">
                    <div class="col-md-5 col-md-offset-1 col-xs-12">
                        <img src="{{asset('images/assembly-7.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <img src="{{asset('images/assembly-8.JPG')}}" alt=" " class="img img-responsive">
                    </div>
                </div>
            </div>
            <div class="row">
                <br/>
                <br/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading" style="padding-left: 12%;"><b>Tubing selection</b></h2></div>
                <div class="col-lg-8 col-md-3 col-sm-12 col-xs-12"><h4 style="padding-left: 15%; color:#38ba5f;"><b>Tubing desing pressures</b><br/>For steel and stainless steel tubing based on 4-1 safety factor</h4>
                    <br/>
                    <br/>
                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <!--Table 10 begining Tubing design pressures-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="10" valign="bottom" class="smalltext"><b>Steel Assembly</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Tube<br>O.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Wall<br>thickness</b></td>
                                <td  valign="bottom" class="smalltext"><b>Tube<br>I.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Steel C1010</b></td>
                                <td  valign="bottom" class="smalltext"><b>Stainless 304 & 316</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.125</td>
                                <td  class="smalltext" nowrap>0.028</td>
                                <td  class="smalltext">0.069</td>
                                <td  class="smalltext" >6661</td>
                                <td  class="smalltext" >7994</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.125</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.055</td>
                                <td  class="smalltext" >8445</td>
                                <td  class="smalltext" >10134</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.188</td>
                                <td  class="smalltext" nowrap>0.028</td>
                                <td  class="smalltext">0.132</td>
                                <td  class="smalltext" >4245</td>
                                <td  class="smalltext" >5094</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.188</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.118</td>
                                <td  class="smalltext" >5435</td>
                                <td  class="smalltext" >6521</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.188</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.090</td>
                                <td  class="smalltext" >7839</td>
                                <td  class="smalltext" >9407</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.250</td>
                                <td  class="smalltext" nowrap>0.028</td>
                                <td  class="smalltext">0.194</td>
                                <td  class="smalltext" >3104</td>
                                <td  class="smalltext" >3725</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.250</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.180</td>
                                <td  class="smalltext" >3965</td>
                                <td  class="smalltext" >4758</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.250</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.152</td>
                                <td  class="smalltext" >5753</td>
                                <td  class="smalltext" >6903</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.250</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.120</td>
                                <td  class="smalltext" >7819</td>
                                <td  class="smalltext" >9382</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.250</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.084</td>
                                <td  class="smalltext" >9964</td>
                                <td  class="smalltext" >11957</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.028</td>
                                <td  class="smalltext">0.257</td>
                                <td  class="smalltext" >2433</td>
                                <td  class="smalltext" >2919</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.243</td>
                                <td  class="smalltext" >3098</td>
                                <td  class="smalltext" >3718</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.215</td>
                                <td  class="smalltext" >4486</td>
                                <td  class="smalltext" >5383</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.183</td>
                                <td  class="smalltext" >6131</td>
                                <td  class="smalltext" >7357</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.147</td>
                                <td  class="smalltext" >7982</td>
                                <td  class="smalltext" >9579</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.313</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.123</td>
                                <td  class="smalltext" >9156</td>
                                <td  class="smalltext" >10987</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.028</td>
                                <td  class="smalltext">0.319</td>
                                <td  class="smalltext" >2004</td>
                                <td  class="smalltext" >2405</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.305</td>
                                <td  class="smalltext" >2547</td>
                                <td  class="smalltext" >3056</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.277</td>
                                <td  class="smalltext" >3675</td>
                                <td  class="smalltext" >4410</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.245</td>
                                <td  class="smalltext" >5021</td>
                                <td  class="smalltext" >6025</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.209</td>
                                <td  class="smalltext" >6575</td>
                                <td  class="smalltext" >7890</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.185</td>
                                <td  class="smalltext" >7607</td>
                                <td  class="smalltext" >9128</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.375</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.157</td>
                                <td  class="smalltext" >8771</td>
                                <td  class="smalltext" >10526</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.430</td>
                                <td  class="smalltext" >1871</td>
                                <td  class="smalltext" >2245</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.402</td>
                                <td  class="smalltext" >2684</td>
                                <td  class="smalltext" >3221</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.370</td>
                                <td  class="smalltext" >3654</td>
                                <td  class="smalltext" >4385</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.334</td>
                                <td  class="smalltext" >4786</td>
                                <td  class="smalltext" >5744</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.310</td>
                                <td  class="smalltext" >5558</td>
                                <td  class="smalltext" >6670</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.282</td>
                                <td  class="smalltext" >6467</td>
                                <td  class="smalltext" >7760</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">0.260</td>
                                <td  class="smalltext" >7179</td>
                                <td  class="smalltext" >8615</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.232</td>
                                <td  class="smalltext" >8071</td>
                                <td  class="smalltext" >9685</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">0.204</td>
                                <td  class="smalltext" >8932</td>
                                <td  class="smalltext" >10719</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.500</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">0.124</td>
                                <td  class="smalltext" >11051</td>
                                <td  class="smalltext" >13262</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.555</td>
                                <td  class="smalltext" >1478</td>
                                <td  class="smalltext" >1773</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.527</td>
                                <td  class="smalltext" >2111</td>
                                <td  class="smalltext" >2534</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.495</td>
                                <td  class="smalltext" >2863</td>
                                <td  class="smalltext" >3436</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.459</td>
                                <td  class="smalltext" >3741</td>
                                <td  class="smalltext" >4489</td>
                            </tr>
                        </table>
                    </div>
                    <!--Table 10 ending-->
                </div>
                <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <!--Table 11 begining Tubing design pressures-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="10" valign="bottom" class="smalltext"><b>Steel Assembly</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Tube<br>O.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Wall<br>thickness</b></td>
                                <td  valign="bottom" class="smalltext"><b>Tube<br>I.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Steel C1010</b></td>
                                <td  valign="bottom" class="smalltext"><b>Stainless 304 & 316</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.435</td>
                                <td  class="smalltext" >4342</td>
                                <td  class="smalltext" >5210</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.407</td>
                                <td  class="smalltext" >5055</td>
                                <td  class="smalltext" >6067</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">0.385</td>
                                <td  class="smalltext" >5623</td>
                                <td  class="smalltext" >6748</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.625</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.357</td>
                                <td  class="smalltext" >6350</td>
                                <td  class="smalltext" >7620</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.680</td>
                                <td  class="smalltext" >1221</td>
                                <td  class="smalltext" >1465</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.652</td>
                                <td  class="smalltext" >1739</td>
                                <td  class="smalltext" >2087</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.620</td>
                                <td  class="smalltext" >2351</td>
                                <td  class="smalltext" >2821</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.584</td>
                                <td  class="smalltext" >3064</td>
                                <td  class="smalltext" >3676</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.560</td>
                                <td  class="smalltext" >3551</td>
                                <td  class="smalltext" >4261</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.532</td>
                                <td  class="smalltext" >4132</td>
                                <td  class="smalltext" >4958</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">0.510</td>
                                <td  class="smalltext" >4595</td>
                                <td  class="smalltext" >5514</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.482</td>
                                <td  class="smalltext" >5193</td>
                                <td  class="smalltext" >6231</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">0.454</td>
                                <td  class="smalltext" >5796</td>
                                <td  class="smalltext" >6955</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">0.750</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">0.374</td>
                                <td  class="smalltext" >7521</td>
                                <td  class="smalltext" >9026</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.805</td>
                                <td  class="smalltext" >1040</td>
                                <td  class="smalltext" >1248</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.777</td>
                                <td  class="smalltext" >1478</td>
                                <td  class="smalltext" >1773</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.745</td>
                                <td  class="smalltext" >1993</td>
                                <td  class="smalltext" >2392</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.709</td>
                                <td  class="smalltext" >2591</td>
                                <td  class="smalltext" >3110</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.685</td>
                                <td  class="smalltext" >3000</td>
                                <td  class="smalltext" >3600</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.657</td>
                                <td  class="smalltext" >3487</td>
                                <td  class="smalltext" >4184</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">0.635</td>
                                <td  class="smalltext" >3876</td>
                                <td  class="smalltext" >4651</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.607</td>
                                <td  class="smalltext" >4378</td>
                                <td  class="smalltext" >5253</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">0.875</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">0.579</td>
                                <td  class="smalltext" >4887</td>
                                <td  class="smalltext" >5864</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.035</td>
                                <td  class="smalltext">0.930</td>
                                <td  class="smalltext" >906</td>
                                <td  class="smalltext" >1087</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">0.902</td>
                                <td  class="smalltext" >1285</td>
                                <td  class="smalltext" >1542</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">0.870</td>
                                <td  class="smalltext" >1730</td>
                                <td  class="smalltext" >2076</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">0.834</td>
                                <td  class="smalltext" >2244</td>
                                <td  class="smalltext" >2693</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">0.810</td>
                                <td  class="smalltext" >2596</td>
                                <td  class="smalltext" >3115</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">0.782</td>
                                <td  class="smalltext" >3013</td>
                                <td  class="smalltext" >3616</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">0.760</td>
                                <td  class="smalltext" >3347</td>
                                <td  class="smalltext" >4016</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.732</td>
                                <td  class="smalltext" >3778</td>
                                <td  class="smalltext" >4533</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">0.704</td>
                                <td  class="smalltext" >4216</td>
                                <td  class="smalltext" >5059</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.156</td>
                                <td  class="smalltext">0.688</td>
                                <td  class="smalltext" >4468</td>
                                <td  class="smalltext" >5362</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">0.624</td>
                                <td  class="smalltext" >5494</td>
                                <td  class="smalltext" >6592</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" nowrap>0.220</td>
                                <td  class="smalltext">0.560</td>
                                <td  class="smalltext" >6532</td>
                                <td  class="smalltext" >7838</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.049</td>
                                <td  class="smalltext">1.152</td>
                                <td  class="smalltext" >1018</td>
                                <td  class="smalltext" >1222</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">1.120</td>
                                <td  class="smalltext" >1367</td>
                                <td  class="smalltext" >1641</td>
                            </tr>
                        </table>
                    </div>
                    <!--Table 11 ending-->

                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <br/>
                    <br/>
                    <!--Table 12 begining Tubing design pressures-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="10" valign="bottom" class="smalltext"><b>Tubing design pressures</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext"><b>Tube<br>O.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Wall<br>thickness</b></td>
                                <td  valign="bottom" class="smalltext"><b>Tube<br>I.D</b></td>
                                <td  valign="bottom" class="smalltext"><b>Steel C1010</b></td>
                                <td  valign="bottom" class="smalltext"><b>Stainless 304 & 316</b></td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">1.084</td>
                                <td  class="smalltext" >1769</td>
                                <td  class="smalltext" >2123</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">1.060</td>
                                <td  class="smalltext" >2042</td>
                                <td  class="smalltext" >2451</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">1.032</td>
                                <td  class="smalltext" >2367</td>
                                <td  class="smalltext" >2840</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">1.010</td>
                                <td  class="smalltext" >2625</td>
                                <td  class="smalltext" >3150</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">0.982</td>
                                <td  class="smalltext" >2959</td>
                                <td  class="smalltext" >3551</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">0.954</td>
                                <td  class="smalltext" >3298</td>
                                <td  class="smalltext" >3958</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.156</td>
                                <td  class="smalltext">0.938</td>
                                <td  class="smalltext" >3494</td>
                                <td  class="smalltext" >4193</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">0.874</td>
                                <td  class="smalltext" >4291</td>
                                <td  class="smalltext" >5149</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">1.250</td>
                                <td  class="smalltext" nowrap>0.220</td>
                                <td  class="smalltext">0.810</td>
                                <td  class="smalltext" >5107</td>
                                <td  class="smalltext" >6128</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">1.370</td>
                                <td  class="smalltext" >1130</td>
                                <td  class="smalltext" >1356</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">1.334</td>
                                <td  class="smalltext" >1459</td>
                                <td  class="smalltext" >1751</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">1.310</td>
                                <td  class="smalltext" >1683</td>
                                <td  class="smalltext" >2019</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">1.282</td>
                                <td  class="smalltext" >1947</td>
                                <td  class="smalltext" >2336</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">1.260</td>
                                <td  class="smalltext" >2158</td>
                                <td  class="smalltext" >2589</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">1.232</td>
                                <td  class="smalltext" >2429</td>
                                <td  class="smalltext" >2915</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">1.204</td>
                                <td  class="smalltext" >2704</td>
                                <td  class="smalltext" >3245</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.156</td>
                                <td  class="smalltext">1.188</td>
                                <td  class="smalltext" >2863</td>
                                <td  class="smalltext" >3436</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">1.124</td>
                                <td  class="smalltext" >3510</td>
                                <td  class="smalltext" >4212</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.220</td>
                                <td  class="smalltext">1.060</td>
                                <td  class="smalltext" >4174</td>
                                <td  class="smalltext" >5008</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" nowrap>0.250</td>
                                <td  class="smalltext">1.000</td>
                                <td  class="smalltext" >4808</td>
                                <td  class="smalltext" >5769</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.065</td>
                                <td  class="smalltext">1.870</td>
                                <td  class="smalltext" >839</td>
                                <td  class="smalltext" >1007</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.083</td>
                                <td  class="smalltext">1.834</td>
                                <td  class="smalltext" >1080</td>
                                <td  class="smalltext" >1296</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.095</td>
                                <td  class="smalltext">1.810</td>
                                <td  class="smalltext" >1244</td>
                                <td  class="smalltext" >1492</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.109</td>
                                <td  class="smalltext">1.782</td>
                                <td  class="smalltext" >1436</td>
                                <td  class="smalltext" >1724</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.120</td>
                                <td  class="smalltext">1.760</td>
                                <td  class="smalltext" >1589</td>
                                <td  class="smalltext" >1907</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">1.732</td>
                                <td  class="smalltext" >1786</td>
                                <td  class="smalltext" >2143</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.134</td>
                                <td  class="smalltext">1.732</td>
                                <td  class="smalltext" >1786</td>
                                <td  class="smalltext" >2143</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.148</td>
                                <td  class="smalltext">1.704</td>
                                <td  class="smalltext" >1985</td>
                                <td  class="smalltext" >2382</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.156</td>
                                <td  class="smalltext">1.688</td>
                                <td  class="smalltext" >2100</td>
                                <td  class="smalltext" >2520</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.188</td>
                                <td  class="smalltext">1.624</td>
                                <td  class="smalltext" >2566</td>
                                <td  class="smalltext" >3079</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.220</td>
                                <td  class="smalltext">1.560</td>
                                <td  class="smalltext" >3043</td>
                                <td  class="smalltext" >3652</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.250</td>
                                <td  class="smalltext">1.500</td>
                                <td  class="smalltext" >3500</td>
                                <td  class="smalltext" >4200</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext">2.000</td>
                                <td  class="smalltext" nowrap>0.281</td>
                                <td  class="smalltext">1.438</td>
                                <td  class="smalltext" >3980</td>
                                <td  class="smalltext" >4776</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 12 ending-->
                </div>
                <div class="col-md-6 col-xs-12 fontType">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <p>
                        When using thick walled tubing where flaring is impractical, the use of Flareless Fittings is recommended.
                        Pressures listed above are given in PSI.
                    </p>
                    <br/>
                    <p>
                        Selection of hydraulic tubing size is calculated by evaluating several different factors.
                    </p>
                    <br/>
                    <p>These factors include: </p>
                    <br/>
                    <ul style="list-style: decimal; padding-left:20px;">
                        <li>Type of fluid in the application</li>
                        <li>Maximum working pressure</li>
                        <li>Type of service(Normal, Severe, Hazardous)</li>
                        <li>Operating temperatures of application</li>
                        <li>Flow rate of application</li>
                    </ul>
                    <br/>
                    <p>
                        Tubing design pressures can be calculated using one of three equations as shown in SAE - J1065.
                        The values which are shown in the tubing design pressure chart were calculated using (LAME’s) equation.
                        Values as shown in the tubing design pressure chart are given using a safety factor of 4 to 1, which is
                        rated for normal service.
                    </p>
                    <br/>
                    <p>LAME’S equation P = S ( ( D2 - d2) / (D2 + d2) )</p>
                    <br/>
                    <ul style="list-style:none; padding-left:20px;">
                        <li>D = Outside tube diameter (Inches) </li>
                        <li>d = Inside tube diameter (Inches) </li>
                        <li>P = Recommended design pressure </li>
                        <li>S = Allowable stress of material (PSI)</li>
                        <br/>
                        <li>** Values for allowable stress of materials</li>
                        <li>C1010 Steel tubing: 12,500 PSI</li>
                        <li>304 and 316 Stainless tubing: 15,000 PSI </li>
                        <li>** Tubing design pressure values have not been derated for severity of service or elevated
                            operating temperatures. </li>
                        <li>** Consult your tubing supplier for detailed information regarding pressure and temperature ratings
                            for tubing. </li>
                        <br/>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-9 col-xs-12"><br/>
                    <!--Table 13 begining 1010 steel tubing wall thickness-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="12" valign="bottom" class="smalltext"><b>1010 Steel tubing wall thickness</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b></b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>6:1 Safety factor</b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>8:1 Safety factor</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b>Tube O.D</b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>Working pressures (PSI)</b></td>
                                <td colspan="6" valign="bottom" class="smalltext"><b>Working pressures (PSI)</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext" colspan="2"><b><span class="red"></span></b></td>
                                <td  valign="bottom" class="smalltext"><b>1000</b></td>
                                <td  valign="bottom" class="smalltext"><b>2000</b></td>
                                <td  valign="bottom" class="smalltext"><b><span class="red">3000</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>4000</b></td>
                                <td  valign="bottom" class="smalltext"><b>5000</b></td>
                                <td  valign="bottom" class="smalltext"><b>1000</b></td>
                                <td  valign="bottom" class="smalltext"><b>2000</b></td>
                                <td  valign="bottom" class="smalltext"><b>3000</b></td>
                                <td  valign="bottom" class="smalltext"><b>4000</b></td>
                                <td  valign="bottom" class="smalltext"><b>5000</b></td>

                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1/8</td>
                                <td  class="smalltext" nowrap>.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028/td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">3/16</td>
                                <td  class="smalltext" nowrap>.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1/4</td>
                                <td  class="smalltext" nowrap>.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">5/16</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.049</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">3/8</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1/2</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.072</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">5/8</td>
                                <td  class="smalltext" nowrap>.035</td>
                                <td  class="smalltext">.072</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.049</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">3/4</td>
                                <td  class="smalltext" nowrap>.049</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">7/8</td>
                                <td  class="smalltext" nowrap>.049</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1</td>
                                <td  class="smalltext" nowrap>.058</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.072</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1-1/4</td>
                                <td  class="smalltext" nowrap>.072</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1-1/2</td>
                                <td  class="smalltext" nowrap>.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">2</td>
                                <td  class="smalltext" nowrap>.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 13 ending-->
                </div>
                <div class="col-md-offset-1 col-md-9 col-xs-12"><br/>
                    <!--Table 14 begining 304 316 stainless steel steel tubing wall thickness-->
                    <div class="table-responsive">
                        <table class="table">

                            <tr align="center" bgcolor="ccccbb">
                                <td colspan="12" valign="bottom" class="smalltext"><b>304 & 316 Stainless Steel tubing wall thickness</b></td>
                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b></b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>6:1 Safety factor</b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>8:1 Safety factor</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td colspan="2" valign="bottom" class="smalltext"><b>Tube O.D</b></td>
                                <td colspan="5" valign="bottom" class="smalltext"><b>Working pressures (PSI)</b></td>
                                <td colspan="6" valign="bottom" class="smalltext"><b>Working pressures (PSI)</b></td>

                            </tr>
                            <tr align="center" bgcolor="cccc99">
                                <td  valign="bottom" class="smalltext" colspan="2"><b><span class="red"></span></b></td>
                                <td  valign="bottom" class="smalltext"><b>1000</b></td>
                                <td  valign="bottom" class="smalltext"><b>2000</b></td>
                                <td  valign="bottom" class="smalltext"><b><span class="red">3000</span></b></td>
                                <td  valign="bottom" class="smalltext"><b>4000</b></td>
                                <td  valign="bottom" class="smalltext"><b>5000</b></td>
                                <td  valign="bottom" class="smalltext"><b>1000</b></td>
                                <td  valign="bottom" class="smalltext"><b>2000</b></td>
                                <td  valign="bottom" class="smalltext"><b>3000</b></td>
                                <td  valign="bottom" class="smalltext"><b>4000</b></td>
                                <td  valign="bottom" class="smalltext"><b>5000</b></td>

                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1/8</td>
                                <td  class="smalltext" nowrap>.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.028</td>
                                <td  class="smalltext">.035</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">3/16</td>
                                <td  class="smalltext" nowrap>.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1/4</td>
                                <td  class="smalltext" nowrap>.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.030</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.065</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">5/16</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.049</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">3/8</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1/2</td>
                                <td  class="smalltext" nowrap>.032</td>
                                <td  class="smalltext">.042</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.032</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">5/8</td>
                                <td  class="smalltext" nowrap>.035</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.035</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">3/4</td>
                                <td  class="smalltext" nowrap>.035</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.049</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">7/8</td>
                                <td  class="smalltext" nowrap>.049</td>
                                <td  class="smalltext">.072</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.049</td>
                                <td  class="smalltext">.095</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1</td>
                                <td  class="smalltext" nowrap>.049</td>
                                <td  class="smalltext">.83</td>
                                <td  class="smalltext">.120</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.058</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>


                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">1-1/4</td>
                                <td  class="smalltext" nowrap>.058</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.065</td>
                                <td  class="smalltext">.134</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeee0">
                                <td  class="smalltext" colspan="2">1-1/2</td>
                                <td  class="smalltext" nowrap>.065</td>
                                <td  class="smalltext">.120</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                            <tr  align="center" bgcolor="eeeeee">
                                <td  class="smalltext" colspan="2">2</td>
                                <td  class="smalltext" nowrap>.083</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">.109</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                                <td  class="smalltext">-</td>
                            </tr>

                        </table>
                    </div>
                    <!--Table 14 ending-->
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading" style="padding-left: 12%;"><b>Tube line fabrication</b></h2></div>
                <div class="col-md-8 col-md-offset-1 col-xs-12 fontType">
                    <br/>
                    <p>
                        Equipment which makes use of hydraulic or pneumatic fluid lines must be properly designed for
                        efficiency, ease of assembly, and leak free operation. Consider the following points when designing
                        a fluid system.
                    </p>
                    <br/>
                    <ul style="list-style:decimal; padding-left: 20px;">
                        <li> Proper routing of lines to avoid pressure drops and excessive strain on joints. </li>
                        <br/>
                        <li>Make joints accessible for easy installation and future maintenance. </li>
                        <br/>
                        <li>Have tube supports installed to further eliminate strain on tube lines and excess noise from the system.</li>
                        <br/>
                    </ul>
                    <p>
                        ** Refer to drawings of fluid line fabrications for examples of proper and improper routing.
                    </p>
                    <br/>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-9.JPG')}}" alt=" " class="img img-responsive">
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-10.JPG')}}" alt=" " class="img img-responsive">
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-11.JPG')}}"alt=" " class="img img-responsive">
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-12.JPG')}}" alt=" " class="img img-responsive">
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-13.JPG')}}" alt=" " class="img img-responsive">
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-14.JPG')}}" alt=" " class="img img-responsive">
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-15.JPG')}}" alt=" " class="img img-responsive">
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="{{asset('images/assembly-16.JPG')}}" alt=" " class="img img-responsive">
                </div>
            </div>


        </div>
    </div>
    @endsection