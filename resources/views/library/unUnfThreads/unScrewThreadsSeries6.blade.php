@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries1')}}">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries2')}}">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries3')}}">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>

                        <a href="{{route('unScrewThreadsSeries4')}}">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries5')}}">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>1 5/8-6 UN to 1 15/16-20 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries7')}}">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries8')}}">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table">
                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"></a> Major <br><span class="red">Max Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   1.6225</td>
                    <td  class="smalltext">   1.6043</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5142</td>
                    <td  class="smalltext">   1.5060</td>
                    <td  class="smalltext">   1.4246</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4750</span></td>
                    <td  class="smalltext"><span class="red">   1.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5274</span></td>
                    <td  class="smalltext"><span class="red">   1.5167</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6068</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5167</td>
                    <td  class="smalltext">   1.5105</td>
                    <td  class="smalltext">   1.4271</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.4646</span></td>
                    <td  class="smalltext"><span class="red">   1.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5247</span></td>
                    <td  class="smalltext"><span class="red">   1.5167</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.6228</td>
                    <td  class="smalltext">   1.6078</td>
                    <td  class="smalltext">1.6003</td>
                    <td  class="smalltext">   1.5416</td>
                    <td  class="smalltext">   1.5342</td>
                    <td  class="smalltext">   1.4784</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5150</span></td>
                    <td  class="smalltext"><span class="red">   1.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5535</span></td>
                    <td  class="smalltext"><span class="red">   1.5438</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5438</td>
                    <td  class="smalltext">   1.5382</td>
                    <td  class="smalltext">   1.4806</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5047</span></td>
                    <td  class="smalltext"><span class="red">   1.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5510</span></td>
                    <td  class="smalltext"><span class="red">   1.5438</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.6231</td>
                    <td  class="smalltext">   1.6102</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5581</td>
                    <td  class="smalltext">   1.5517</td>
                    <td  class="smalltext">   1.5041</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5380</span></td>
                    <td  class="smalltext"><span class="red">   1.5170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5683</span></td>
                    <td  class="smalltext"><span class="red">   1.5600</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.6232</td>
                    <td  class="smalltext">   1.6118</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5691</td>
                    <td  class="smalltext">   1.5632</td>
                    <td  class="smalltext">   1.5240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5530</span></td>
                    <td  class="smalltext"><span class="red">   1.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5785</span></td>
                    <td  class="smalltext"><span class="red">   1.5709</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5709</td>
                    <td  class="smalltext">   1.5665</td>
                    <td  class="smalltext">   1.5258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5448</span></td>
                    <td  class="smalltext"><span class="red">   1.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5766</span></td>
                    <td  class="smalltext"><span class="red">   1.5709</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.6233</td>
                    <td  class="smalltext">   1.6130</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5769</td>
                    <td  class="smalltext">   1.5714</td>
                    <td  class="smalltext">   1.5383</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5640</span></td>
                    <td  class="smalltext"><span class="red">   1.5480</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5858</span></td>
                    <td  class="smalltext"><span class="red">   1.5786</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.6234</td>
                    <td  class="smalltext">   1.6140</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5828</td>
                    <td  class="smalltext">   1.5776</td>
                    <td  class="smalltext">   1.5489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5710</span></td>
                    <td  class="smalltext"><span class="red">   1.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5912</span></td>
                    <td  class="smalltext"><span class="red">   1.5844</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5844</td>
                    <td  class="smalltext">   1.5805</td>
                    <td  class="smalltext">   1.5505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5658</span></td>
                    <td  class="smalltext"><span class="red">   1.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5895</span></td>
                    <td  class="smalltext"><span class="red">   1.5844</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.6235</td>
                    <td  class="smalltext">   1.6148</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5874</td>
                    <td  class="smalltext">   1.5824</td>
                    <td  class="smalltext">   1.5574</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5780</span></td>
                    <td  class="smalltext"><span class="red">   1.5650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5954</span></td>
                    <td  class="smalltext"><span class="red">   1.5889</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6163</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5889</td>
                    <td  class="smalltext">   1.5852</td>
                    <td  class="smalltext">   1.5589</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5730</span></td>
                    <td  class="smalltext"><span class="red">   1.5650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5937</span></td>
                    <td  class="smalltext"><span class="red">   1.5889</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.6236</td>
                    <td  class="smalltext">   1.6155</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5911</td>
                    <td  class="smalltext">   1.5863</td>
                    <td  class="smalltext">   1.5641</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5820</span></td>
                    <td  class="smalltext"><span class="red">   1.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5987</span></td>
                    <td  class="smalltext"><span class="red">   1.5925</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6250</td>
                    <td  class="smalltext">   1.6169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5925</td>
                    <td  class="smalltext">   1.5889</td>
                    <td  class="smalltext">   1.5655</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5787</span></td>
                    <td  class="smalltext"><span class="red">   1.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5971</span></td>
                    <td  class="smalltext"><span class="red">   1.5925</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>5</sup>/<sub>8</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.6237</td>
                    <td  class="smalltext">   1.6165</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5966</td>
                    <td  class="smalltext">   1.5922</td>
                    <td  class="smalltext">   1.5741</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5900</span></td>
                    <td  class="smalltext"><span class="red">   1.5800</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6037</span></td>
                    <td  class="smalltext"><span class="red">   1.5979</span></td>
                    <td  class="smalltext"><span class="red">   1.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   1.6850</td>
                    <td  class="smalltext">   1.6668</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5767</td>
                    <td  class="smalltext">   1.5684</td>
                    <td  class="smalltext">   1.4866</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5380</span></td>
                    <td  class="smalltext"><span class="red">   1.5070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5900</span></td>
                    <td  class="smalltext"><span class="red">   1.5792</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6693</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.5792</td>
                    <td  class="smalltext">   1.5730</td>
                    <td  class="smalltext">   1.4891</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5271</span></td>
                    <td  class="smalltext"><span class="red">   1.5070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5873</span></td>
                    <td  class="smalltext"><span class="red">   1.5792</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.6853</td>
                    <td  class="smalltext">   1.6703</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6041</td>
                    <td  class="smalltext">   1.5966</td>
                    <td  class="smalltext">   1.5365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5770</span></td>
                    <td  class="smalltext"><span class="red">   1.5520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6160</span></td>
                    <td  class="smalltext"><span class="red">   1.6063</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6725</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6063</td>
                    <td  class="smalltext">   1.6007</td>
                    <td  class="smalltext">   1.5387</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5672</span></td>
                    <td  class="smalltext"><span class="red">   1.5520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6136</span></td>
                    <td  class="smalltext"><span class="red">   1.6063</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.6857</td>
                    <td  class="smalltext">   1.6743</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6316</td>
                    <td  class="smalltext">   1.6256</td>
                    <td  class="smalltext">   1.5865</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6150</span></td>
                    <td  class="smalltext"><span class="red">   1.5970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6412</span></td>
                    <td  class="smalltext"><span class="red">   1.6334</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6761</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6334</td>
                    <td  class="smalltext">   1.6289</td>
                    <td  class="smalltext">   1.5883</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6073</span></td>
                    <td  class="smalltext"><span class="red">   1.5970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6392</span></td>
                    <td  class="smalltext"><span class="red">   1.6334</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.6859</td>
                    <td  class="smalltext">   1.6765</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6453</td>
                    <td  class="smalltext">   1.6400</td>
                    <td  class="smalltext">   1.6114</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6340</span></td>
                    <td  class="smalltext"><span class="red">   1.6200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6538</span></td>
                    <td  class="smalltext"><span class="red">   1.6469</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6781</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6469</td>
                    <td  class="smalltext">   1.6429</td>
                    <td  class="smalltext">   1.6130</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6283</span></td>
                    <td  class="smalltext"><span class="red">   1.6200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6521</span></td>
                    <td  class="smalltext"><span class="red">   1.6469</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.6860</td>
                    <td  class="smalltext">   1.6773</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6499</td>
                    <td  class="smalltext">   1.6448</td>
                    <td  class="smalltext">   1.6199</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6400</span></td>
                    <td  class="smalltext"><span class="red">   1.6270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6580</span></td>
                    <td  class="smalltext"><span class="red">   1.6514</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6788</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6514</td>
                    <td  class="smalltext">   1.6476</td>
                    <td  class="smalltext">   1.6214</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6355</span></td>
                    <td  class="smalltext"><span class="red">   1.6270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6563</span></td>
                    <td  class="smalltext"><span class="red">   1.6514</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.6860</td>
                    <td  class="smalltext">   1.6779</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6535</td>
                    <td  class="smalltext">   1.6487</td>
                    <td  class="smalltext">   1.6265</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6450</span></td>
                    <td  class="smalltext"><span class="red">   1.6330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6613</span></td>
                    <td  class="smalltext"><span class="red">   1.6550</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>11</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.6875</td>
                    <td  class="smalltext">   1.6794</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6550</td>
                    <td  class="smalltext">   1.6514</td>
                    <td  class="smalltext">   1.6280</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6412</span></td>
                    <td  class="smalltext"><span class="red">   1.6330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6597</span></td>
                    <td  class="smalltext"><span class="red">   1.6550</span></td>
                    <td  class="smalltext"><span class="red">   1.6875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-5 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   1.7473</td>
                    <td  class="smalltext">   1.7165</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6174</td>
                    <td  class="smalltext">   1.6040</td>
                    <td  class="smalltext">   1.5092</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5680</span></td>
                    <td  class="smalltext"><span class="red">   1.5340</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6375</span></td>
                    <td  class="smalltext"><span class="red">   1.6201</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-5 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   1.7473</td>
                    <td  class="smalltext">   1.7268</td>
                    <td  class="smalltext">1.7165</td>
                    <td  class="smalltext">   1.6174</td>
                    <td  class="smalltext">   1.6085</td>
                    <td  class="smalltext">   1.5092</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5680</span></td>
                    <td  class="smalltext"><span class="red">   1.5340</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6317</span></td>
                    <td  class="smalltext"><span class="red">   1.6201</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-5 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7295</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6201</td>
                    <td  class="smalltext">   1.6134</td>
                    <td  class="smalltext">   1.5119</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5575</span></td>
                    <td  class="smalltext"><span class="red">   1.5340</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6288</span></td>
                    <td  class="smalltext"><span class="red">   1.6201</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   1.7475</td>
                    <td  class="smalltext">   1.7293</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6392</td>
                    <td  class="smalltext">   1.6309</td>
                    <td  class="smalltext">   1.5491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6000</span></td>
                    <td  class="smalltext"><span class="red">   1.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6525</span></td>
                    <td  class="smalltext"><span class="red">   1.6417</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7318</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6417</td>
                    <td  class="smalltext">   1.6354</td>
                    <td  class="smalltext">   1.5516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.5896</span></td>
                    <td  class="smalltext"><span class="red">   1.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6498</span></td>
                    <td  class="smalltext"><span class="red">   1.6417</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0023</td>
                    <td  class="smalltext">   1.7477</td>
                    <td  class="smalltext">   1.7327</td>
                    <td  class="smalltext">1.7252</td>
                    <td  class="smalltext">   1.6665</td>
                    <td  class="smalltext">   1.6590</td>
                    <td  class="smalltext">   1.5989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6400</span></td>
                    <td  class="smalltext"><span class="red">   1.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6786</span></td>
                    <td  class="smalltext"><span class="red">   1.6688</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6688</td>
                    <td  class="smalltext">   1.6632</td>
                    <td  class="smalltext">   1.6012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6297</span></td>
                    <td  class="smalltext"><span class="red">   1.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6762</span></td>
                    <td  class="smalltext"><span class="red">   1.6688</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.7481</td>
                    <td  class="smalltext">   1.7352</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6831</td>
                    <td  class="smalltext">   1.6766</td>
                    <td  class="smalltext">   1.6291</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6630</span></td>
                    <td  class="smalltext"><span class="red">   1.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6934</span></td>
                    <td  class="smalltext"><span class="red">   1.6850</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.7482</td>
                    <td  class="smalltext">   1.7368</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6941</td>
                    <td  class="smalltext">   1.6881</td>
                    <td  class="smalltext">   1.6490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6780</span></td>
                    <td  class="smalltext"><span class="red">   1.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7037</span></td>
                    <td  class="smalltext"><span class="red">   1.6959</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.6959</td>
                    <td  class="smalltext">   1.6914</td>
                    <td  class="smalltext">   1.6508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6698</span></td>
                    <td  class="smalltext"><span class="red">   1.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7017</span></td>
                    <td  class="smalltext"><span class="red">   1.6959</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.7483</td>
                    <td  class="smalltext">   1.7380</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7019</td>
                    <td  class="smalltext">   1.6963</td>
                    <td  class="smalltext">   1.6632</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6880</span></td>
                    <td  class="smalltext"><span class="red">   1.6730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7109</span></td>
                    <td  class="smalltext"><span class="red">   1.7036</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.7484</td>
                    <td  class="smalltext">   1.7390</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7078</td>
                    <td  class="smalltext">   1.7025</td>
                    <td  class="smalltext">   1.6739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6960</span></td>
                    <td  class="smalltext"><span class="red">   1.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7163</span></td>
                    <td  class="smalltext"><span class="red">   1.7094</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7094</td>
                    <td  class="smalltext">   1.7054</td>
                    <td  class="smalltext">   1.6755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6908</span></td>
                    <td  class="smalltext"><span class="red">   1.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7146</span></td>
                    <td  class="smalltext"><span class="red">   1.7094</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.7485</td>
                    <td  class="smalltext">   1.7398</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7124</td>
                    <td  class="smalltext">   1.7073</td>
                    <td  class="smalltext">   1.6824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7030</span></td>
                    <td  class="smalltext"><span class="red">   1.6900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7205</span></td>
                    <td  class="smalltext"><span class="red">   1.7139</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.7485</td>
                    <td  class="smalltext">   1.7404</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7160</td>
                    <td  class="smalltext">   1.7112</td>
                    <td  class="smalltext">   1.6890</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7070</span></td>
                    <td  class="smalltext"><span class="red">   1.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7238</span></td>
                    <td  class="smalltext"><span class="red">   1.7175</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.7500</td>
                    <td  class="smalltext">   1.7419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7175</td>
                    <td  class="smalltext">   1.7139</td>
                    <td  class="smalltext">   1.6905</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7037</span></td>
                    <td  class="smalltext"><span class="red">   1.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7222</span></td>
                    <td  class="smalltext"><span class="red">   1.7175</span></td>
                    <td  class="smalltext"><span class="red">   1.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   1.8100</td>
                    <td  class="smalltext">   1.7918</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7017</td>
                    <td  class="smalltext">   1.6933</td>
                    <td  class="smalltext">   1.6116</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6630</span></td>
                    <td  class="smalltext"><span class="red">   1.6320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7151</span></td>
                    <td  class="smalltext"><span class="red">   1.7042</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8125</td>
                    <td  class="smalltext">   1.7943</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7042</td>
                    <td  class="smalltext">   1.6979</td>
                    <td  class="smalltext">   1.6141</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6521</span></td>
                    <td  class="smalltext"><span class="red">   1.6320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7124</span></td>
                    <td  class="smalltext"><span class="red">   1.7042</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0023</td>
                    <td  class="smalltext">   1.8102</td>
                    <td  class="smalltext">   1.7952</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7290</td>
                    <td  class="smalltext">   1.7214</td>
                    <td  class="smalltext">   1.6614</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7020</span></td>
                    <td  class="smalltext"><span class="red">   1.6770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7412</span></td>
                    <td  class="smalltext"><span class="red">   1.7313</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8125</td>
                    <td  class="smalltext">   1.7975</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7313</td>
                    <td  class="smalltext">   1.7256</td>
                    <td  class="smalltext">   1.6637</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.6922</span></td>
                    <td  class="smalltext"><span class="red">   1.6770</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7387</span></td>
                    <td  class="smalltext"><span class="red">   1.7313</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.8107</td>
                    <td  class="smalltext">   1.7993</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7566</td>
                    <td  class="smalltext">   1.7506</td>
                    <td  class="smalltext">   1.7115</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7400</span></td>
                    <td  class="smalltext"><span class="red">   1.7220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7662</span></td>
                    <td  class="smalltext"><span class="red">   1.7584</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8125</td>
                    <td  class="smalltext">   1.8011</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7584</td>
                    <td  class="smalltext">   1.7539</td>
                    <td  class="smalltext">   1.7133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7323</span></td>
                    <td  class="smalltext"><span class="red">   1.7220</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7642</span></td>
                    <td  class="smalltext"><span class="red">   1.7584</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.8109</td>
                    <td  class="smalltext">   1.8015</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7703</td>
                    <td  class="smalltext">   1.7650</td>
                    <td  class="smalltext">   1.7364</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7590</span></td>
                    <td  class="smalltext"><span class="red">   1.7450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7788</span></td>
                    <td  class="smalltext"><span class="red">   1.7719</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8125</td>
                    <td  class="smalltext">   1.8031</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7719</td>
                    <td  class="smalltext">   1.7679</td>
                    <td  class="smalltext">   1.7380</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7533</span></td>
                    <td  class="smalltext"><span class="red">   1.7450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7771</span></td>
                    <td  class="smalltext"><span class="red">   1.7719</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.8110</td>
                    <td  class="smalltext">   1.8029</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7785</td>
                    <td  class="smalltext">   1.7737</td>
                    <td  class="smalltext">   1.7515</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7700</span></td>
                    <td  class="smalltext"><span class="red">   1.7580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7863</span></td>
                    <td  class="smalltext"><span class="red">   1.7800</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>13</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8125</td>
                    <td  class="smalltext">   1.8044</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7800</td>
                    <td  class="smalltext">   1.7764</td>
                    <td  class="smalltext">   1.7530</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7662</span></td>
                    <td  class="smalltext"><span class="red">   1.7580</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7847</span></td>
                    <td  class="smalltext"><span class="red">   1.7800</span></td>
                    <td  class="smalltext"><span class="red">   1.8125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   1.8725</td>
                    <td  class="smalltext">   1.8543</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7642</td>
                    <td  class="smalltext">   1.7558</td>
                    <td  class="smalltext">   1.6741</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7250</span></td>
                    <td  class="smalltext"><span class="red">   1.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7777</span></td>
                    <td  class="smalltext"><span class="red">   1.7667</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8750</td>
                    <td  class="smalltext">   1.8568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7667</td>
                    <td  class="smalltext">   1.7604</td>
                    <td  class="smalltext">   1.6766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7146</span></td>
                    <td  class="smalltext"><span class="red">   1.6950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7749</span></td>
                    <td  class="smalltext"><span class="red">   1.7667</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0023</td>
                    <td  class="smalltext">   1.8727</td>
                    <td  class="smalltext">   1.8577</td>
                    <td  class="smalltext">1.8502</td>
                    <td  class="smalltext">   1.7915</td>
                    <td  class="smalltext">   1.7838</td>
                    <td  class="smalltext">   1.7239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7650</span></td>
                    <td  class="smalltext"><span class="red">   1.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8038</span></td>
                    <td  class="smalltext"><span class="red">   1.7938</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8750</td>
                    <td  class="smalltext">   1.8600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.7938</td>
                    <td  class="smalltext">   1.7881</td>
                    <td  class="smalltext">   1.7262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7547</span></td>
                    <td  class="smalltext"><span class="red">   1.7400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8013</span></td>
                    <td  class="smalltext"><span class="red">   1.7938</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   1.8731</td>
                    <td  class="smalltext">   1.8602</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8081</td>
                    <td  class="smalltext">   1.8016</td>
                    <td  class="smalltext">   1.7541</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7880</span></td>
                    <td  class="smalltext"><span class="red">   1.7670</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8184</span></td>
                    <td  class="smalltext"><span class="red">   1.8100</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.8732</td>
                    <td  class="smalltext">   1.8618</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8191</td>
                    <td  class="smalltext">   1.8131</td>
                    <td  class="smalltext">   1.7740</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8030</span></td>
                    <td  class="smalltext"><span class="red">   1.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8287</span></td>
                    <td  class="smalltext"><span class="red">   1.8209</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8750</td>
                    <td  class="smalltext">   1.8636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8209</td>
                    <td  class="smalltext">   1.8164</td>
                    <td  class="smalltext">   1.7758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7948</span></td>
                    <td  class="smalltext"><span class="red">   1.7850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8267</span></td>
                    <td  class="smalltext"><span class="red">   1.8209</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.8733</td>
                    <td  class="smalltext">   1.8630</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8269</td>
                    <td  class="smalltext">   1.8213</td>
                    <td  class="smalltext">   1.7883</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8140</span></td>
                    <td  class="smalltext"><span class="red">   1.7980</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8359</span></td>
                    <td  class="smalltext"><span class="red">   1.8286</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.8734</td>
                    <td  class="smalltext">   1.8640</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8328</td>
                    <td  class="smalltext">   1.8275</td>
                    <td  class="smalltext">   1.7989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8210</span></td>
                    <td  class="smalltext"><span class="red">   1.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8413</span></td>
                    <td  class="smalltext"><span class="red">   1.8344</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8750</td>
                    <td  class="smalltext">   1.8656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8344</td>
                    <td  class="smalltext">   1.8304</td>
                    <td  class="smalltext">   1.8005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8158</span></td>
                    <td  class="smalltext"><span class="red">   1.8070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8396</span></td>
                    <td  class="smalltext"><span class="red">   1.8344</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.8735</td>
                    <td  class="smalltext">   1.8648</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8374</td>
                    <td  class="smalltext">   1.8323</td>
                    <td  class="smalltext">   1.8074</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8280</span></td>
                    <td  class="smalltext"><span class="red">   1.8150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8455</span></td>
                    <td  class="smalltext"><span class="red">   1.8389</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.8735</td>
                    <td  class="smalltext">   1.8654</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8410</td>
                    <td  class="smalltext">   1.8362</td>
                    <td  class="smalltext">   1.8140</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8320</span></td>
                    <td  class="smalltext"><span class="red">   1.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8488</span></td>
                    <td  class="smalltext"><span class="red">   1.8425</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>7</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.8750</td>
                    <td  class="smalltext">   1.8669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8425</td>
                    <td  class="smalltext">   1.8389</td>
                    <td  class="smalltext">   1.8155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8287</span></td>
                    <td  class="smalltext"><span class="red">   1.8210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8472</span></td>
                    <td  class="smalltext"><span class="red">   1.8425</span></td>
                    <td  class="smalltext"><span class="red">   1.8750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   1.9349</td>
                    <td  class="smalltext">   1.9167</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8266</td>
                    <td  class="smalltext">   1.8181</td>
                    <td  class="smalltext">   1.7365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7880</span></td>
                    <td  class="smalltext"><span class="red">   1.7570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8403</span></td>
                    <td  class="smalltext"><span class="red">   1.8292</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.9375</td>
                    <td  class="smalltext">   1.9193</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8292</td>
                    <td  class="smalltext">   1.8228</td>
                    <td  class="smalltext">   1.7391</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7771</span></td>
                    <td  class="smalltext"><span class="red">   1.7570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8375</span></td>
                    <td  class="smalltext"><span class="red">   1.8292</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0023</td>
                    <td  class="smalltext">   1.9352</td>
                    <td  class="smalltext">   1.9202</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8540</td>
                    <td  class="smalltext">   1.8463</td>
                    <td  class="smalltext">   1.7864</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8270</span></td>
                    <td  class="smalltext"><span class="red">   1.8020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8663</span></td>
                    <td  class="smalltext"><span class="red">   1.8563</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.9375</td>
                    <td  class="smalltext">   1.9225</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8563</td>
                    <td  class="smalltext">   1.8505</td>
                    <td  class="smalltext">   1.7887</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8172</span></td>
                    <td  class="smalltext"><span class="red">   1.8020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8638</span></td>
                    <td  class="smalltext"><span class="red">   1.8563</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.9357</td>
                    <td  class="smalltext">   1.9243</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8816</td>
                    <td  class="smalltext">   1.8755</td>
                    <td  class="smalltext">   1.8365</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8650</span></td>
                    <td  class="smalltext"><span class="red">   1.8470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8913</span></td>
                    <td  class="smalltext"><span class="red">   1.8834</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.9375</td>
                    <td  class="smalltext">   1.9261</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8834</td>
                    <td  class="smalltext">   1.8789</td>
                    <td  class="smalltext">   1.8383</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8573</span></td>
                    <td  class="smalltext"><span class="red">   1.8470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8893</span></td>
                    <td  class="smalltext"><span class="red">   1.8834</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.9359</td>
                    <td  class="smalltext">   1.9265</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8953</td>
                    <td  class="smalltext">   1.8899</td>
                    <td  class="smalltext">   1.8614</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8840</span></td>
                    <td  class="smalltext"><span class="red">   1.8700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9039</span></td>
                    <td  class="smalltext"><span class="red">   1.8969</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.9375</td>
                    <td  class="smalltext">   1.9281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8969</td>
                    <td  class="smalltext">   1.8929</td>
                    <td  class="smalltext">   1.8630</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8783</span></td>
                    <td  class="smalltext"><span class="red">   1.8700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9021</span></td>
                    <td  class="smalltext"><span class="red">   1.8969</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.9360</td>
                    <td  class="smalltext">   1.9279</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9035</td>
                    <td  class="smalltext">   1.8986</td>
                    <td  class="smalltext">   1.8765</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8950</span></td>
                    <td  class="smalltext"><span class="red">   1.8830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9114</span></td>
                    <td  class="smalltext"><span class="red">   1.9050</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>15</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.9375</td>
                    <td  class="smalltext">   1.9294</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9050</td>
                    <td  class="smalltext">   1.9013</td>
                    <td  class="smalltext">   1.8780</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8912</span></td>
                    <td  class="smalltext"><span class="red">   1.8830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9098</span></td>
                    <td  class="smalltext"><span class="red">   1.9050</span></td>
                    <td  class="smalltext"><span class="red">   1.9375</span></td>

                </tr>
            </table>
        </div>
    </div>
@endsection