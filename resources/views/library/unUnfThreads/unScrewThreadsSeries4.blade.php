@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries1')}}">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries2')}}">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries3')}}">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>

                        <span class="blue"><b>15/16-12 UN to 1 3/16-28 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries5')}}">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries6')}}">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries7')}}">2-4&frac12; UNC to 2&frac34;-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries8')}}">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>
        &nbsp;<br>
        <div class="table-responsive">
            <table class="table">

                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br><span class="red">Max Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.9358</td>
                    <td  class="smalltext">   0.9244</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8817</td>
                    <td  class="smalltext">   0.8760</td>
                    <td  class="smalltext">   0.8366</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8650</span></td>
                    <td  class="smalltext"><span class="red">   0.8470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8908</span></td>
                    <td  class="smalltext"><span class="red">   0.8834</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.9375</td>
                    <td  class="smalltext">   0.9261</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8834</td>
                    <td  class="smalltext">   0.8793</td>
                    <td  class="smalltext">   0.8383</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8575</span></td>
                    <td  class="smalltext"><span class="red">   0.8470</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8889</span></td>
                    <td  class="smalltext"><span class="red">   0.8834</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.9360</td>
                    <td  class="smalltext">   0.9266</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8954</td>
                    <td  class="smalltext">   0.8904</td>
                    <td  class="smalltext">   0.8615</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8840</span></td>
                    <td  class="smalltext"><span class="red">   0.8700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9034</span></td>
                    <td  class="smalltext"><span class="red">   0.8969</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.9375</td>
                    <td  class="smalltext">   0.9281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.8969</td>
                    <td  class="smalltext">   0.8932</td>
                    <td  class="smalltext">   0.8630</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8783</span></td>
                    <td  class="smalltext"><span class="red">   0.8700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9018</span></td>
                    <td  class="smalltext"><span class="red">   0.8969</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.9361</td>
                    <td  class="smalltext">   0.9280</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9036</td>
                    <td  class="smalltext">   0.8991</td>
                    <td  class="smalltext">   0.8766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8950</span></td>
                    <td  class="smalltext"><span class="red">   0.8830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9109</span></td>
                    <td  class="smalltext"><span class="red">   0.9050</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.9375</td>
                    <td  class="smalltext">   0.9294</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9050</td>
                    <td  class="smalltext">   0.9016</td>
                    <td  class="smalltext">   0.8780</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8912</span></td>
                    <td  class="smalltext"><span class="red">   0.8830</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9094</span></td>
                    <td  class="smalltext"><span class="red">   0.9050</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.9363</td>
                    <td  class="smalltext">   0.9298</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9131</td>
                    <td  class="smalltext">   0.9091</td>
                    <td  class="smalltext">   0.8937</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9070</span></td>
                    <td  class="smalltext"><span class="red">   0.8990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9195</span></td>
                    <td  class="smalltext"><span class="red">   0.9143</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.9375</td>
                    <td  class="smalltext">   0.9310</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9143</td>
                    <td  class="smalltext">   0.9113</td>
                    <td  class="smalltext">   0.8949</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9051</span></td>
                    <td  class="smalltext"><span class="red">   0.8990</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9182</span></td>
                    <td  class="smalltext"><span class="red">   0.9143</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.9364</td>
                    <td  class="smalltext">   0.9304</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9161</td>
                    <td  class="smalltext">   0.9123</td>
                    <td  class="smalltext">   0.8992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9110</span></td>
                    <td  class="smalltext"><span class="red">   0.9040</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9221</span></td>
                    <td  class="smalltext"><span class="red">   0.9172</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap><sup>15</sup>/<sub>16</sub>-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   0.9375</td>
                    <td  class="smalltext">   0.9315</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9172</td>
                    <td  class="smalltext">   0.9144</td>
                    <td  class="smalltext">   0.9003</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9094</span></td>
                    <td  class="smalltext"><span class="red">   0.9040</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9209</span></td>
                    <td  class="smalltext"><span class="red">   0.9172</span></td>
                    <td  class="smalltext"><span class="red">   0.9375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-8 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   0.9980</td>
                    <td  class="smalltext">   0.9755</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9168</td>
                    <td  class="smalltext">   0.9067</td>
                    <td  class="smalltext">   0.8492</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8900</span></td>
                    <td  class="smalltext"><span class="red">   0.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9320</span></td>
                    <td  class="smalltext"><span class="red">   0.9188</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-8 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   0.9980</td>
                    <td  class="smalltext">   0.9830</td>
                    <td  class="smalltext">0.9755</td>
                    <td  class="smalltext">   0.9168</td>
                    <td  class="smalltext">   0.9100</td>
                    <td  class="smalltext">   0.8492</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8900</span></td>
                    <td  class="smalltext"><span class="red">   0.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9276</span></td>
                    <td  class="smalltext"><span class="red">   0.9188</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-8 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9188</td>
                    <td  class="smalltext">   0.9137</td>
                    <td  class="smalltext">   0.8512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.8797</span></td>
                    <td  class="smalltext"><span class="red">   0.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9254</span></td>
                    <td  class="smalltext"><span class="red">   0.9188</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.9982</td>
                    <td  class="smalltext">   0.9853</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9332</td>
                    <td  class="smalltext">   0.9270</td>
                    <td  class="smalltext">   0.8792</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9130</span></td>
                    <td  class="smalltext"><span class="red">   0.8920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9430</span></td>
                    <td  class="smalltext"><span class="red">   0.9350</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.9982</td>
                    <td  class="smalltext">   0.9810</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9441</td>
                    <td  class="smalltext">   0.9353</td>
                    <td  class="smalltext">   0.8990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9280</span></td>
                    <td  class="smalltext"><span class="red">   0.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9573</span></td>
                    <td  class="smalltext"><span class="red">   0.9459</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   0.9982</td>
                    <td  class="smalltext">   0.9868</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9441</td>
                    <td  class="smalltext">   0.9382</td>
                    <td  class="smalltext">   0.8990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9280</span></td>
                    <td  class="smalltext"><span class="red">   0.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9535</span></td>
                    <td  class="smalltext"><span class="red">   0.9459</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9459</td>
                    <td  class="smalltext">   0.9415</td>
                    <td  class="smalltext">   0.9008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9198</span></td>
                    <td  class="smalltext"><span class="red">   0.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9516</span></td>
                    <td  class="smalltext"><span class="red">   0.9459</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-14UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.9983</td>
                    <td  class="smalltext">   0.9828</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9519</td>
                    <td  class="smalltext">   0.9435</td>
                    <td  class="smalltext">   0.9132</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9380</span></td>
                    <td  class="smalltext"><span class="red">   0.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9645</span></td>
                    <td  class="smalltext"><span class="red">   0.9536</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-14UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   0.9983</td>
                    <td  class="smalltext">   0.9880</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9519</td>
                    <td  class="smalltext">   0.9463</td>
                    <td  class="smalltext">   0.9132</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9380</span></td>
                    <td  class="smalltext"><span class="red">   0.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9609</span></td>
                    <td  class="smalltext"><span class="red">   0.9536</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-14UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9897</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9536</td>
                    <td  class="smalltext">   0.9494</td>
                    <td  class="smalltext">   0.9149</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9315</span></td>
                    <td  class="smalltext"><span class="red">   0.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9590</span></td>
                    <td  class="smalltext"><span class="red">   0.9536</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   0.9985</td>
                    <td  class="smalltext">   0.9891</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9579</td>
                    <td  class="smalltext">   0.9529</td>
                    <td  class="smalltext">   0.9240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9460</span></td>
                    <td  class="smalltext"><span class="red">   0.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9659</span></td>
                    <td  class="smalltext"><span class="red">   0.9594</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9594</td>
                    <td  class="smalltext">   0.9557</td>
                    <td  class="smalltext">   0.9255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9408</span></td>
                    <td  class="smalltext"><span class="red">   0.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9643</span></td>
                    <td  class="smalltext"><span class="red">   0.9594</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.9986</td>
                    <td  class="smalltext">   0.9899</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9625</td>
                    <td  class="smalltext">   0.9578</td>
                    <td  class="smalltext">   0.9325</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9530</span></td>
                    <td  class="smalltext"><span class="red">   0.9400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9701</span></td>
                    <td  class="smalltext"><span class="red">   0.9639</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   0.9986</td>
                    <td  class="smalltext">   0.9905</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9661</td>
                    <td  class="smalltext">   0.9616</td>
                    <td  class="smalltext">   0.9391</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9570</span></td>
                    <td  class="smalltext"><span class="red">   0.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9734</span></td>
                    <td  class="smalltext"><span class="red">   0.9675</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-20 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9675</td>
                    <td  class="smalltext">   0.9641</td>
                    <td  class="smalltext">   0.9405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9537</span></td>
                    <td  class="smalltext"><span class="red">   0.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9719</span></td>
                    <td  class="smalltext"><span class="red">   0.9675</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   0.9987</td>
                    <td  class="smalltext">   0.9915</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9716</td>
                    <td  class="smalltext">   0.9674</td>
                    <td  class="smalltext">   0.9491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9650</span></td>
                    <td  class="smalltext"><span class="red">   0.9550</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9784</span></td>
                    <td  class="smalltext"><span class="red">   0.9729</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-27 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.9988</td>
                    <td  class="smalltext">   0.9921</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9747</td>
                    <td  class="smalltext">   0.9707</td>
                    <td  class="smalltext">   0.9547</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9690</span></td>
                    <td  class="smalltext"><span class="red">   0.9600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9811</span></td>
                    <td  class="smalltext"><span class="red">   0.9759</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   0.9988</td>
                    <td  class="smalltext">   0.9923</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9756</td>
                    <td  class="smalltext">   0.9716</td>
                    <td  class="smalltext">   0.9562</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9700</span></td>
                    <td  class="smalltext"><span class="red">   0.9610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9820</span></td>
                    <td  class="smalltext"><span class="red">   0.9768</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9935</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9768</td>
                    <td  class="smalltext">   0.9738</td>
                    <td  class="smalltext">   0.9574</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9676</span></td>
                    <td  class="smalltext"><span class="red">   0.9610</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9807</span></td>
                    <td  class="smalltext"><span class="red">   0.9768</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0011</td>
                    <td  class="smalltext">   0.9989</td>
                    <td  class="smalltext">   0.9929</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9786</td>
                    <td  class="smalltext">   0.9748</td>
                    <td  class="smalltext">   0.9617</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9740</span></td>
                    <td  class="smalltext"><span class="red">   0.9660</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9846</span></td>
                    <td  class="smalltext"><span class="red">   0.9797</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1-32 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0000</td>
                    <td  class="smalltext">   0.9940</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9797</td>
                    <td  class="smalltext">   0.9769</td>
                    <td  class="smalltext">   0.9628</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9719</span></td>
                    <td  class="smalltext"><span class="red">   0.9660</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9834</span></td>
                    <td  class="smalltext"><span class="red">   0.9797</span></td>
                    <td  class="smalltext"><span class="red">   1.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   1.0605</td>
                    <td  class="smalltext">   1.0455</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9793</td>
                    <td  class="smalltext">   0.9725</td>
                    <td  class="smalltext">   0.9117</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9520</span></td>
                    <td  class="smalltext"><span class="red">   0.9270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9902</span></td>
                    <td  class="smalltext"><span class="red">   0.9813</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0475</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   0.9813</td>
                    <td  class="smalltext">   0.9762</td>
                    <td  class="smalltext">   0.9137</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9422</span></td>
                    <td  class="smalltext"><span class="red">   0.9270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9880</span></td>
                    <td  class="smalltext"><span class="red">   0.9813</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.0608</td>
                    <td  class="smalltext">   1.0494</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0067</td>
                    <td  class="smalltext">   1.0010</td>
                    <td  class="smalltext">   0.9616</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9900</span></td>
                    <td  class="smalltext"><span class="red">   0.9720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0158</span></td>
                    <td  class="smalltext"><span class="red">   1.0084</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0511</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0084</td>
                    <td  class="smalltext">   1.0042</td>
                    <td  class="smalltext">   0.9633</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9823</span></td>
                    <td  class="smalltext"><span class="red">   0.9720</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0139</span></td>
                    <td  class="smalltext"><span class="red">   1.0084</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.0610</td>
                    <td  class="smalltext">   1.0516</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0204</td>
                    <td  class="smalltext">   1.0154</td>
                    <td  class="smalltext">   0.9865</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0090</span></td>
                    <td  class="smalltext"><span class="red">   0.9950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0284</span></td>
                    <td  class="smalltext"><span class="red">   1.0219</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0531</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0219</td>
                    <td  class="smalltext">   1.0182</td>
                    <td  class="smalltext">   0.9880</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0033</span></td>
                    <td  class="smalltext"><span class="red">   0.9950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0268</span></td>
                    <td  class="smalltext"><span class="red">   1.0219</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.0611</td>
                    <td  class="smalltext">   1.0524</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0250</td>
                    <td  class="smalltext">   1.0203</td>
                    <td  class="smalltext">   0.9950</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0150</span></td>
                    <td  class="smalltext"><span class="red">   1.0020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0326</span></td>
                    <td  class="smalltext"><span class="red">   1.0264</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0538</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0264</td>
                    <td  class="smalltext">   1.0228</td>
                    <td  class="smalltext">   0.9964</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0105</span></td>
                    <td  class="smalltext"><span class="red">   1.0020</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0310</span></td>
                    <td  class="smalltext"><span class="red">   1.0264</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.0611</td>
                    <td  class="smalltext">   1.0530</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0286</td>
                    <td  class="smalltext">   1.0241</td>
                    <td  class="smalltext">   1.0016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0200</span></td>
                    <td  class="smalltext"><span class="red">   1.0080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0359</span></td>
                    <td  class="smalltext"><span class="red">   1.0300</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0544</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0300</td>
                    <td  class="smalltext">   1.0266</td>
                    <td  class="smalltext">   1.0030</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0162</span></td>
                    <td  class="smalltext"><span class="red">   1.0080</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0344</span></td>
                    <td  class="smalltext"><span class="red">   1.0300</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.0613</td>
                    <td  class="smalltext">   1.0548</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0381</td>
                    <td  class="smalltext">   1.0341</td>
                    <td  class="smalltext">   1.0187</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0320</span></td>
                    <td  class="smalltext"><span class="red">   1.0240</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0445</span></td>
                    <td  class="smalltext"><span class="red">   1.0393</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.0625</td>
                    <td  class="smalltext">   1.0560</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0393</td>
                    <td  class="smalltext">   1.0363</td>
                    <td  class="smalltext">   1.0199</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0301</span></td>
                    <td  class="smalltext"><span class="red">   1.0240</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0432</span></td>
                    <td  class="smalltext"><span class="red">   1.0393</span></td>
                    <td  class="smalltext"><span class="red">   1.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.1228</td>
                    <td  class="smalltext">   1.0982</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0300</td>
                    <td  class="smalltext">   1.0191</td>
                    <td  class="smalltext">   0.9527</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9980</span></td>
                    <td  class="smalltext"><span class="red">   0.9700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0463</span></td>
                    <td  class="smalltext"><span class="red">   1.0322</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0022</td>
                    <td  class="smalltext">   1.1228</td>
                    <td  class="smalltext">   1.1064</td>
                    <td  class="smalltext">1.0982</td>
                    <td  class="smalltext">   1.0300</td>
                    <td  class="smalltext">   1.0228</td>
                    <td  class="smalltext">   0.9527</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9980</span></td>
                    <td  class="smalltext"><span class="red">   0.9700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0416</span></td>
                    <td  class="smalltext"><span class="red">   1.0322</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-7 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1086</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0322</td>
                    <td  class="smalltext">   1.0268</td>
                    <td  class="smalltext">   0.9549</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   0.9875</span></td>
                    <td  class="smalltext"><span class="red">   0.9700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0393</span></td>
                    <td  class="smalltext"><span class="red">   1.0322</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   1.1229</td>
                    <td  class="smalltext">   1.1079</td>
                    <td  class="smalltext">1.1004</td>
                    <td  class="smalltext">   1.0417</td>
                    <td  class="smalltext">   1.0348</td>
                    <td  class="smalltext">   0.9741</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0150</span></td>
                    <td  class="smalltext"><span class="red">   0.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0528</span></td>
                    <td  class="smalltext"><span class="red">   1.0438</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0438</td>
                    <td  class="smalltext">   1.0386</td>
                    <td  class="smalltext">   0.9762</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0047</span></td>
                    <td  class="smalltext"><span class="red">   0.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0505</span></td>
                    <td  class="smalltext"><span class="red">   1.0438</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.1232</td>
                    <td  class="smalltext">   1.1103</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0582</td>
                    <td  class="smalltext">   1.0520</td>
                    <td  class="smalltext">   1.0042</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0380</span></td>
                    <td  class="smalltext"><span class="red">   1.0170</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0680</span></td>
                    <td  class="smalltext"><span class="red">   1.0600</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.1232</td>
                    <td  class="smalltext">   1.1060</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0691</td>
                    <td  class="smalltext">   1.0601</td>
                    <td  class="smalltext">   1.0240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0530</span></td>
                    <td  class="smalltext"><span class="red">   1.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0826</span></td>
                    <td  class="smalltext"><span class="red">   1.0709</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.1232</td>
                    <td  class="smalltext">   1.1118</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0691</td>
                    <td  class="smalltext">   1.0631</td>
                    <td  class="smalltext">   1.0240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0530</span></td>
                    <td  class="smalltext"><span class="red">   1.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0787</span></td>
                    <td  class="smalltext"><span class="red">   1.0709</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-12 UNF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0709</td>
                    <td  class="smalltext">   1.0664</td>
                    <td  class="smalltext">   1.0258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0448</span></td>
                    <td  class="smalltext"><span class="red">   1.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0768</span></td>
                    <td  class="smalltext"><span class="red">   1.0709</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.1234</td>
                    <td  class="smalltext">   1.1131</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0770</td>
                    <td  class="smalltext">   1.0717</td>
                    <td  class="smalltext">   1.0384</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0640</span></td>
                    <td  class="smalltext"><span class="red">   1.0480</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0855</span></td>
                    <td  class="smalltext"><span class="red">   1.0786</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.1235</td>
                    <td  class="smalltext">   1.1141</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0829</td>
                    <td  class="smalltext">   1.0779</td>
                    <td  class="smalltext">   1.0490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0710</span></td>
                    <td  class="smalltext"><span class="red">   1.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0909</span></td>
                    <td  class="smalltext"><span class="red">   1.0844</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0844</td>
                    <td  class="smalltext">   1.0807</td>
                    <td  class="smalltext">   1.0505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0658</span></td>
                    <td  class="smalltext"><span class="red">   1.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0893</span></td>
                    <td  class="smalltext"><span class="red">   1.0844</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.1236</td>
                    <td  class="smalltext">   1.1149</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0875</td>
                    <td  class="smalltext">   1.0828</td>
                    <td  class="smalltext">   1.0575</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0780</span></td>
                    <td  class="smalltext"><span class="red">   1.0650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0951</span></td>
                    <td  class="smalltext"><span class="red">   1.0889</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1163</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0889</td>
                    <td  class="smalltext">   1.0853</td>
                    <td  class="smalltext">   1.0589</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0730</span></td>
                    <td  class="smalltext"><span class="red">   1.0650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0935</span></td>
                    <td  class="smalltext"><span class="red">   1.0889</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.1236</td>
                    <td  class="smalltext">   1.1155</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0911</td>
                    <td  class="smalltext">   1.0866</td>
                    <td  class="smalltext">   1.0641</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0820</span></td>
                    <td  class="smalltext"><span class="red">   1.0710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0984</span></td>
                    <td  class="smalltext"><span class="red">   1.0925</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0925</td>
                    <td  class="smalltext">   1.0891</td>
                    <td  class="smalltext">   1.0655</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0787</span></td>
                    <td  class="smalltext"><span class="red">   1.0710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0969</span></td>
                    <td  class="smalltext"><span class="red">   1.0925</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-24 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0013</td>
                    <td  class="smalltext">   1.1237</td>
                    <td  class="smalltext">   1.1165</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.0966</td>
                    <td  class="smalltext">   1.0924</td>
                    <td  class="smalltext">   1.0742</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0900</span></td>
                    <td  class="smalltext"><span class="red">   1.0800</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1034</span></td>
                    <td  class="smalltext"><span class="red">   1.0979</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.1238</td>
                    <td  class="smalltext">   1.1173</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1006</td>
                    <td  class="smalltext">   1.0966</td>
                    <td  class="smalltext">   1.0812</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0950</span></td>
                    <td  class="smalltext"><span class="red">   1.0860</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1070</span></td>
                    <td  class="smalltext"><span class="red">   1.1018</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>1</sup>/<sub>8</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1250</td>
                    <td  class="smalltext">   1.1185</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1018</td>
                    <td  class="smalltext">   1.0988</td>
                    <td  class="smalltext">   1.0824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0926</span></td>
                    <td  class="smalltext"><span class="red">   1.0860</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1057</span></td>
                    <td  class="smalltext"><span class="red">   1.1018</span></td>
                    <td  class="smalltext"><span class="red">   1.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0021</td>
                    <td  class="smalltext">   1.1854</td>
                    <td  class="smalltext">   1.1704</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1042</td>
                    <td  class="smalltext">   1.0972</td>
                    <td  class="smalltext">   1.0366</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0770</span></td>
                    <td  class="smalltext"><span class="red">   1.0520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1154</span></td>
                    <td  class="smalltext"><span class="red">   1.1063</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1875</td>
                    <td  class="smalltext">   1.1725</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1063</td>
                    <td  class="smalltext">   1.1011</td>
                    <td  class="smalltext">   1.0387</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.0672</span></td>
                    <td  class="smalltext"><span class="red">   1.0520</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1131</span></td>
                    <td  class="smalltext"><span class="red">   1.1063</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   1.0017</td>
                    <td  class="smalltext">   1.1858</td>
                    <td  class="smalltext">   1.1744</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1317</td>
                    <td  class="smalltext">   1.1259</td>
                    <td  class="smalltext">   1.0866</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1150</span></td>
                    <td  class="smalltext"><span class="red">   1.0970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1409</span></td>
                    <td  class="smalltext"><span class="red">   1.1334</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1872</td>
                    <td  class="smalltext">   1.1761</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1334</td>
                    <td  class="smalltext">   1.1291</td>
                    <td  class="smalltext">   1.0883</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1073</span></td>
                    <td  class="smalltext"><span class="red">   1.0970</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1390</span></td>
                    <td  class="smalltext"><span class="red">   1.1334</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.1860</td>
                    <td  class="smalltext">   1.1766</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1454</td>
                    <td  class="smalltext">   1.1403</td>
                    <td  class="smalltext">   1.1115</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1340</span></td>
                    <td  class="smalltext"><span class="red">   1.1200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1535</span></td>
                    <td  class="smalltext"><span class="red">   1.1469</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1875</td>
                    <td  class="smalltext">   1.1781</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1469</td>
                    <td  class="smalltext">   1.1431</td>
                    <td  class="smalltext">   1.1130</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1283</span></td>
                    <td  class="smalltext"><span class="red">   1.1200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1519</span></td>
                    <td  class="smalltext"><span class="red">   1.1469</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.1860</td>
                    <td  class="smalltext">   1.1773</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1499</td>
                    <td  class="smalltext">   1.1450</td>
                    <td  class="smalltext">   1.1199</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1400</span></td>
                    <td  class="smalltext"><span class="red">   1.1270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1577</span></td>
                    <td  class="smalltext"><span class="red">   1.1514</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-18 UNEF</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1875</td>
                    <td  class="smalltext">   1.1788</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1514</td>
                    <td  class="smalltext">   1.1478</td>
                    <td  class="smalltext">   1.1214</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1355</span></td>
                    <td  class="smalltext"><span class="red">   1.1270</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1561</span></td>
                    <td  class="smalltext"><span class="red">   1.1514</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0014</td>
                    <td  class="smalltext">   1.1861</td>
                    <td  class="smalltext">   1.1780</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1536</td>
                    <td  class="smalltext">   1.1489</td>
                    <td  class="smalltext">   1.1266</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1450</span></td>
                    <td  class="smalltext"><span class="red">   1.1330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1611</span></td>
                    <td  class="smalltext"><span class="red">   1.1550</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1875</td>
                    <td  class="smalltext">   1.1794</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1550</td>
                    <td  class="smalltext">   1.1515</td>
                    <td  class="smalltext">   1.1280</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1412</span></td>
                    <td  class="smalltext"><span class="red">   1.1330</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1595</span></td>
                    <td  class="smalltext"><span class="red">   1.1550</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0012</td>
                    <td  class="smalltext">   1.1863</td>
                    <td  class="smalltext">   1.1798</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1631</td>
                    <td  class="smalltext">   1.1590</td>
                    <td  class="smalltext">   1.1437</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1570</span></td>
                    <td  class="smalltext"><span class="red">   1.1490</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1696</span></td>
                    <td  class="smalltext"><span class="red">   1.1643</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>1<sup>3</sup>/<sub>16</sub>-28 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   1.1875</td>
                    <td  class="smalltext">   1.1810</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.1643</td>
                    <td  class="smalltext">   1.1612</td>
                    <td  class="smalltext">   1.1449</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1551</span></td>
                    <td  class="smalltext"><span class="red">   1.1490</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.1683</span></td>
                    <td  class="smalltext"><span class="red">   1.1643</span></td>
                    <td  class="smalltext"><span class="red">   1.1875</span></td>

                </tr>

            </table>
        </div>
    </div>
    @endsection