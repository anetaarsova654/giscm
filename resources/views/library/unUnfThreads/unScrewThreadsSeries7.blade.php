@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><h2 class="subheading"><b>UN/UNF INCH THREADS</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12"><div class="clearfix"> </div></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <a name="pagetop"></a>
                <tr bgcolor="cccc99"><td class="smalltext" nowrap><b>Nominal Thread Size</b></td><td class="smalltext"><strong>Thread Range</strong></td></tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries1')}}">0-80 UNF to &frac14;-56 UNS</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 0-80 UNF to &frac14;-56 UNS
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries2')}}">5/16-18 UNC to 9/16 -32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/16-18 UNC to 9/16 -32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries3')}}">5/8-11 UNC to 7/8-32 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 5/8-11 UNC to 7/8-32 UN
                    </td>
                </tr>
                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>

                        <a href="{{route('unScrewThreadsSeries4')}}">15/16-12 UN to 1 3/16-28 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 15/16-12 UN to 1 3/16-28 UN
                    </td>
                </tr>
                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries5')}}">1 1/4-7 UNC to 1 9/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 1/4-7 UNC to 1 9/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries6')}}">1 5/8-6 UN to 1 15/16-20 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 1 5/8-6 UN to 1 15/16-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeee0">
                    <td class="smalltext"  valign="top" nowrap>
                        <span class="blue"><b>2-4&frac12; UNC to 2&frac34;-20 UN</b></span>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2-4&frac12; UNC to 2&frac34;-20 UN
                    </td>
                </tr>

                <tr bgcolor="eeeeee">
                    <td class="smalltext"  valign="top" nowrap>
                        <a href="{{route('unScrewThreadsSeries8')}}">2 7/8-6 UN to 4-16 UN</a>
                    </td>
                    <td class="footnotetext">
                        Nominal thread sizes from 2 7/8-6 UN to 4-16 UN
                    </td>
                </tr>
            </table>
        </div>

        &nbsp;<br>
        <div class="table-responsive">
            <table class="table">

                <tr align="center" bgcolor="ccccbb">
                    <td colspan="10" valign="bottom" class="smalltext"><b>External Thread /<span class="red">Internal Thread</span></b></td>
                </tr>
                <tr align="center" bgcolor="cccc99">
                    <td  valign="bottom" class="smalltext">&nbsp;</td>
                    <td  valign="bottom" class="smalltext"><b>Nominal Size, TPI, Series</b></td>
                    <td  valign="bottom" class="smalltext"><b>Class</b></td>
                    <td  valign="bottom" class="smalltext"><b>Allowance</b></td>
                    <td  valign="bottom" class="smalltext"><strong>Max<a href="unified.cfm#footer"><sup></sup></a> Major <br>
                            <span class="red">Max Minor</span></strong></td>
                    <td  valign="bottom" class="smalltext"><b>Min Major<br><span class="red">Min Minor</span></b></td>
                    <td  valign="bottom" class="smalltext"><b>Min <a href="unified.cfm#footer"><sup></sup></a></b></td>
                    <td  valign="bottom" class="smalltext"><b>Max<a href="unified.cfm#footer"><sup></sup></a> Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>Min Pitch</b></td>
                    <td  valign="bottom" class="smalltext"><b>UNR<a href="unified.cfm#footer"><sup></sup></a> Minor Dia<br><span class="red">Major Dia(Min)</span></b></td>
                </tr>

                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   1.9971</td>
                    <td  class="smalltext">   1.9641</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8528</td>
                    <td  class="smalltext">   1.8385</td>
                    <td  class="smalltext">   1.7324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7950</span></td>
                    <td  class="smalltext"><span class="red">   1.7590</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8743</span></td>
                    <td  class="smalltext"><span class="red">   1.8557</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   1.9971</td>
                    <td  class="smalltext">   1.9751</td>
                    <td  class="smalltext">1.9641</td>
                    <td  class="smalltext">   1.8528</td>
                    <td  class="smalltext">   1.8433</td>
                    <td  class="smalltext">   1.7324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7950</span></td>
                    <td  class="smalltext"><span class="red">   1.7590</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8681</span></td>
                    <td  class="smalltext"><span class="red">   1.8557</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9780</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8557</td>
                    <td  class="smalltext">   1.8486</td>
                    <td  class="smalltext">   1.7353</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.7861</span></td>
                    <td  class="smalltext"><span class="red">   1.7590</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8650</span></td>
                    <td  class="smalltext"><span class="red">   1.8557</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   1.9974</td>
                    <td  class="smalltext">   1.9792</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8891</td>
                    <td  class="smalltext">   1.8805</td>
                    <td  class="smalltext">   1.7990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8500</span></td>
                    <td  class="smalltext"><span class="red">   1.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9028</span></td>
                    <td  class="smalltext"><span class="red">   1.8917</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.8917</td>
                    <td  class="smalltext">   1.8853</td>
                    <td  class="smalltext">   1.8016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8396</span></td>
                    <td  class="smalltext"><span class="red">   1.8200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9000</span></td>
                    <td  class="smalltext"><span class="red">   1.8917</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0023</td>
                    <td  class="smalltext">   1.9977</td>
                    <td  class="smalltext">   1.9827</td>
                    <td  class="smalltext">1.9752</td>
                    <td  class="smalltext">   1.9165</td>
                    <td  class="smalltext">   1.9087</td>
                    <td  class="smalltext">   1.8489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8900</span></td>
                    <td  class="smalltext"><span class="red">   1.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9289</span></td>
                    <td  class="smalltext"><span class="red">   1.9188</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9188</td>
                    <td  class="smalltext">   1.9130</td>
                    <td  class="smalltext">   1.8512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.8797</span></td>
                    <td  class="smalltext"><span class="red">   1.8650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9264</span></td>
                    <td  class="smalltext"><span class="red">   1.9188</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   1.9980</td>
                    <td  class="smalltext">   1.9851</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9330</td>
                    <td  class="smalltext">   1.9265</td>
                    <td  class="smalltext">   1.8790</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9130</span></td>
                    <td  class="smalltext"><span class="red">   1.8920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9435</span></td>
                    <td  class="smalltext"><span class="red">   1.9350</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   1.9982</td>
                    <td  class="smalltext">   1.9868</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9441</td>
                    <td  class="smalltext">   1.9380</td>
                    <td  class="smalltext">   1.8990</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9280</span></td>
                    <td  class="smalltext"><span class="red">   1.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9538</span></td>
                    <td  class="smalltext"><span class="red">   1.9459</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9459</td>
                    <td  class="smalltext">   1.9414</td>
                    <td  class="smalltext">   1.9008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9198</span></td>
                    <td  class="smalltext"><span class="red">   1.9100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9518</span></td>
                    <td  class="smalltext"><span class="red">   1.9459</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   1.9983</td>
                    <td  class="smalltext">   1.9880</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9519</td>
                    <td  class="smalltext">   1.9462</td>
                    <td  class="smalltext">   1.9133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9380</span></td>
                    <td  class="smalltext"><span class="red">   1.9230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9610</span></td>
                    <td  class="smalltext"><span class="red">   1.9536</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   1.9984</td>
                    <td  class="smalltext">   1.9890</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9578</td>
                    <td  class="smalltext">   1.9524</td>
                    <td  class="smalltext">   1.9239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9460</span></td>
                    <td  class="smalltext"><span class="red">   1.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9664</span></td>
                    <td  class="smalltext"><span class="red">   1.9594</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9594</td>
                    <td  class="smalltext">   1.9554</td>
                    <td  class="smalltext">   1.9255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9408</span></td>
                    <td  class="smalltext"><span class="red">   1.9320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9646</span></td>
                    <td  class="smalltext"><span class="red">   1.9594</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.9985</td>
                    <td  class="smalltext">   1.9898</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9624</td>
                    <td  class="smalltext">   1.9573</td>
                    <td  class="smalltext">   1.9324</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9530</span></td>
                    <td  class="smalltext"><span class="red">   1.9400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9706</span></td>
                    <td  class="smalltext"><span class="red">   1.9639</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   1.9985</td>
                    <td  class="smalltext">   1.9904</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9660</td>
                    <td  class="smalltext">   1.9611</td>
                    <td  class="smalltext">   1.9390</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9570</span></td>
                    <td  class="smalltext"><span class="red">   1.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9739</span></td>
                    <td  class="smalltext"><span class="red">   1.9675</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0000</td>
                    <td  class="smalltext">   1.9919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   1.9675</td>
                    <td  class="smalltext">   1.9638</td>
                    <td  class="smalltext">   1.9405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9537</span></td>
                    <td  class="smalltext"><span class="red">   1.9460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9723</span></td>
                    <td  class="smalltext"><span class="red">   1.9675</span></td>
                    <td  class="smalltext"><span class="red">   2.0000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.0609</td>
                    <td  class="smalltext">   2.0515</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0203</td>
                    <td  class="smalltext">   2.0149</td>
                    <td  class="smalltext">   1.9864</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0090</span></td>
                    <td  class="smalltext"><span class="red">   1.9950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0289</span></td>
                    <td  class="smalltext"><span class="red">   2.0219</span></td>
                    <td  class="smalltext"><span class="red">   2.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.0625</td>
                    <td  class="smalltext">   2.0531</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0219</td>
                    <td  class="smalltext">   2.0179</td>
                    <td  class="smalltext">   1.9880</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0033</span></td>
                    <td  class="smalltext"><span class="red">   1.9950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0271</span></td>
                    <td  class="smalltext"><span class="red">   2.0219</span></td>
                    <td  class="smalltext"><span class="red">   2.0625</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   2.1224</td>
                    <td  class="smalltext">   2.1042</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0141</td>
                    <td  class="smalltext">   2.0054</td>
                    <td  class="smalltext">   1.9240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9750</span></td>
                    <td  class="smalltext"><span class="red">   1.9450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0280</span></td>
                    <td  class="smalltext"><span class="red">   2.0167</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1250</td>
                    <td  class="smalltext">   2.1068</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0167</td>
                    <td  class="smalltext">   2.0102</td>
                    <td  class="smalltext">   1.9266</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   1.9646</span></td>
                    <td  class="smalltext"><span class="red">   1.9450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0251</span></td>
                    <td  class="smalltext"><span class="red">   2.0167</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   2.1226</td>
                    <td  class="smalltext">   2.1076</td>
                    <td  class="smalltext">2.1001</td>
                    <td  class="smalltext">   2.0414</td>
                    <td  class="smalltext">   2.0335</td>
                    <td  class="smalltext">   1.9738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0150</span></td>
                    <td  class="smalltext"><span class="red">   1.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0540</span></td>
                    <td  class="smalltext"><span class="red">   2.0438</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1250</td>
                    <td  class="smalltext">   2.1100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0438</td>
                    <td  class="smalltext">   2.0379</td>
                    <td  class="smalltext">   1.9762</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0047</span></td>
                    <td  class="smalltext"><span class="red">   1.9900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0515</span></td>
                    <td  class="smalltext"><span class="red">   2.0438</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   2.1232</td>
                    <td  class="smalltext">   2.1118</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0691</td>
                    <td  class="smalltext">   2.0630</td>
                    <td  class="smalltext">   2.0240</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0530</span></td>
                    <td  class="smalltext"><span class="red">   2.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0788</span></td>
                    <td  class="smalltext"><span class="red">   2.0709</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1250</td>
                    <td  class="smalltext">   2.1136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0709</td>
                    <td  class="smalltext">   2.0664</td>
                    <td  class="smalltext">   2.0258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0448</span></td>
                    <td  class="smalltext"><span class="red">   2.0350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0768</span></td>
                    <td  class="smalltext"><span class="red">   2.0709</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.1234</td>
                    <td  class="smalltext">   2.1140</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0828</td>
                    <td  class="smalltext">   2.0774</td>
                    <td  class="smalltext">   2.0489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0710</span></td>
                    <td  class="smalltext"><span class="red">   2.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0914</span></td>
                    <td  class="smalltext"><span class="red">   2.0844</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1250</td>
                    <td  class="smalltext">   2.1156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0844</td>
                    <td  class="smalltext">   2.0803</td>
                    <td  class="smalltext">   2.0505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0658</span></td>
                    <td  class="smalltext"><span class="red">   2.0570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0896</span></td>
                    <td  class="smalltext"><span class="red">   2.0844</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.1235</td>
                    <td  class="smalltext">   2.1154</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0910</td>
                    <td  class="smalltext">   2.0861</td>
                    <td  class="smalltext">   2.0640</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0820</span></td>
                    <td  class="smalltext"><span class="red">   2.0710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0989</span></td>
                    <td  class="smalltext"><span class="red">   2.0925</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1250</td>
                    <td  class="smalltext">   2.1169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.0925</td>
                    <td  class="smalltext">   2.0888</td>
                    <td  class="smalltext">   2.0655</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0787</span></td>
                    <td  class="smalltext"><span class="red">   2.0710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0973</span></td>
                    <td  class="smalltext"><span class="red">   2.0925</span></td>
                    <td  class="smalltext"><span class="red">   2.1250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.1859</td>
                    <td  class="smalltext">   2.1765</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1453</td>
                    <td  class="smalltext">   2.1399</td>
                    <td  class="smalltext">   2.1114</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1340</span></td>
                    <td  class="smalltext"><span class="red">   2.1200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1539</span></td>
                    <td  class="smalltext"><span class="red">   2.1469</span></td>
                    <td  class="smalltext"><span class="red">   2.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.1875</td>
                    <td  class="smalltext">   2.1781</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1469</td>
                    <td  class="smalltext">   2.1428</td>
                    <td  class="smalltext">   2.1130</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1283</span></td>
                    <td  class="smalltext"><span class="red">   2.1200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1521</span></td>
                    <td  class="smalltext"><span class="red">   2.1469</span></td>
                    <td  class="smalltext"><span class="red">   2.1875</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   2.2471</td>
                    <td  class="smalltext">   2.2141</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1028</td>
                    <td  class="smalltext">   2.0882</td>
                    <td  class="smalltext">   1.9824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0450</span></td>
                    <td  class="smalltext"><span class="red">   2.0090</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1247</span></td>
                    <td  class="smalltext"><span class="red">   2.1057</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0029</td>
                    <td  class="smalltext">   2.2471</td>
                    <td  class="smalltext">   2.2251</td>
                    <td  class="smalltext">2.2141</td>
                    <td  class="smalltext">   2.1028</td>
                    <td  class="smalltext">   2.0931</td>
                    <td  class="smalltext">   1.9824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0450</span></td>
                    <td  class="smalltext"><span class="red">   2.0090</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1183</span></td>
                    <td  class="smalltext"><span class="red">   2.1057</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-4<sup>1</sup>/<sub>2</sub> UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2280</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1057</td>
                    <td  class="smalltext">   2.0984</td>
                    <td  class="smalltext">   1.9853</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0361</span></td>
                    <td  class="smalltext"><span class="red">   2.0090</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1152</span></td>
                    <td  class="smalltext"><span class="red">   2.1057</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0026</td>
                    <td  class="smalltext">   2.2474</td>
                    <td  class="smalltext">   2.2292</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1391</td>
                    <td  class="smalltext">   2.1303</td>
                    <td  class="smalltext">   2.0490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1000</span></td>
                    <td  class="smalltext"><span class="red">   2.0700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1531</span></td>
                    <td  class="smalltext"><span class="red">   2.1417</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2318</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1417</td>
                    <td  class="smalltext">   2.1351</td>
                    <td  class="smalltext">   2.0516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.0896</span></td>
                    <td  class="smalltext"><span class="red">   2.0700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1502</span></td>
                    <td  class="smalltext"><span class="red">   2.1417</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   2.2476</td>
                    <td  class="smalltext">   2.2326</td>
                    <td  class="smalltext">2.2251</td>
                    <td  class="smalltext">   2.1664</td>
                    <td  class="smalltext">   2.1584</td>
                    <td  class="smalltext">   2.0988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1400</span></td>
                    <td  class="smalltext"><span class="red">   2.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1792</span></td>
                    <td  class="smalltext"><span class="red">   2.1688</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1688</td>
                    <td  class="smalltext">   2.1628</td>
                    <td  class="smalltext">   2.1012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1297</span></td>
                    <td  class="smalltext"><span class="red">   2.1150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1766</span></td>
                    <td  class="smalltext"><span class="red">   2.1688</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   2.2480</td>
                    <td  class="smalltext">   2.2351</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1830</td>
                    <td  class="smalltext">   2.1765</td>
                    <td  class="smalltext">   2.1290</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1630</span></td>
                    <td  class="smalltext"><span class="red">   2.1420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1935</span></td>
                    <td  class="smalltext"><span class="red">   2.1850</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0018</td>
                    <td  class="smalltext">   2.2482</td>
                    <td  class="smalltext">   2.2368</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1941</td>
                    <td  class="smalltext">   2.1880</td>
                    <td  class="smalltext">   2.1490</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1780</span></td>
                    <td  class="smalltext"><span class="red">   2.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2038</span></td>
                    <td  class="smalltext"><span class="red">   2.1959</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.1959</td>
                    <td  class="smalltext">   2.1914</td>
                    <td  class="smalltext">   2.1508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1698</span></td>
                    <td  class="smalltext"><span class="red">   2.1600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2018</span></td>
                    <td  class="smalltext"><span class="red">   2.1959</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.2483</td>
                    <td  class="smalltext">   2.2380</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2019</td>
                    <td  class="smalltext">   2.1962</td>
                    <td  class="smalltext">   2.1633</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1880</span></td>
                    <td  class="smalltext"><span class="red">   2.1730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2110</span></td>
                    <td  class="smalltext"><span class="red">   2.2036</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.2484</td>
                    <td  class="smalltext">   2.2390</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2078</td>
                    <td  class="smalltext">   2.2024</td>
                    <td  class="smalltext">   2.1739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1960</span></td>
                    <td  class="smalltext"><span class="red">   2.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2164</span></td>
                    <td  class="smalltext"><span class="red">   2.2094</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2094</td>
                    <td  class="smalltext">   2.2053</td>
                    <td  class="smalltext">   2.1755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.1908</span></td>
                    <td  class="smalltext"><span class="red">   2.1820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2146</span></td>
                    <td  class="smalltext"><span class="red">   2.2094</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.2485</td>
                    <td  class="smalltext">   2.2398</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2124</td>
                    <td  class="smalltext">   2.2073</td>
                    <td  class="smalltext">   2.1824</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2030</span></td>
                    <td  class="smalltext"><span class="red">   2.1900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2206</span></td>
                    <td  class="smalltext"><span class="red">   2.2139</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.2485</td>
                    <td  class="smalltext">   2.2404</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2160</td>
                    <td  class="smalltext">   2.2111</td>
                    <td  class="smalltext">   2.1890</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2070</span></td>
                    <td  class="smalltext"><span class="red">   2.1960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2239</span></td>
                    <td  class="smalltext"><span class="red">   2.2175</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.2500</td>
                    <td  class="smalltext">   2.2419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2175</td>
                    <td  class="smalltext">   2.2137</td>
                    <td  class="smalltext">   2.1905</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2037</span></td>
                    <td  class="smalltext"><span class="red">   2.1960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2223</span></td>
                    <td  class="smalltext"><span class="red">   2.2175</span></td>
                    <td  class="smalltext"><span class="red">   2.2500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.3108</td>
                    <td  class="smalltext">   2.3014</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2702</td>
                    <td  class="smalltext">   2.2647</td>
                    <td  class="smalltext">   2.2363</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2590</span></td>
                    <td  class="smalltext"><span class="red">   2.2450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2791</span></td>
                    <td  class="smalltext"><span class="red">   2.2719</span></td>
                    <td  class="smalltext"><span class="red">   2.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3125</td>
                    <td  class="smalltext">   2.3031</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2719</td>
                    <td  class="smalltext">   2.2678</td>
                    <td  class="smalltext">   2.2380</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2533</span></td>
                    <td  class="smalltext"><span class="red">   2.2450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2773</span></td>
                    <td  class="smalltext"><span class="red">   2.2719</span></td>
                    <td  class="smalltext"><span class="red">   2.3125</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   2.3723</td>
                    <td  class="smalltext">   2.3541</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2640</td>
                    <td  class="smalltext">   2.2551</td>
                    <td  class="smalltext">   2.1739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2260</span></td>
                    <td  class="smalltext"><span class="red">   2.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2782</span></td>
                    <td  class="smalltext"><span class="red">   2.2667</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3750</td>
                    <td  class="smalltext">   2.3568</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2667</td>
                    <td  class="smalltext">   2.2601</td>
                    <td  class="smalltext">   2.1766</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2146</span></td>
                    <td  class="smalltext"><span class="red">   2.1950</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2753</span></td>
                    <td  class="smalltext"><span class="red">   2.2667</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   2.3726</td>
                    <td  class="smalltext">   2.3576</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2914</td>
                    <td  class="smalltext">   2.2833</td>
                    <td  class="smalltext">   2.2238</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2650</span></td>
                    <td  class="smalltext"><span class="red">   2.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3043</span></td>
                    <td  class="smalltext"><span class="red">   2.2938</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3750</td>
                    <td  class="smalltext">   2.3600</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.2938</td>
                    <td  class="smalltext">   2.2878</td>
                    <td  class="smalltext">   2.2262</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2547</span></td>
                    <td  class="smalltext"><span class="red">   2.2400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3017</span></td>
                    <td  class="smalltext"><span class="red">   2.2938</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.3731</td>
                    <td  class="smalltext">   2.3617</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3190</td>
                    <td  class="smalltext">   2.3128</td>
                    <td  class="smalltext">   2.2739</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3030</span></td>
                    <td  class="smalltext"><span class="red">   2.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3290</span></td>
                    <td  class="smalltext"><span class="red">   2.3209</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3750</td>
                    <td  class="smalltext">   2.3636</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3209</td>
                    <td  class="smalltext">   2.3163</td>
                    <td  class="smalltext">   2.2758</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2948</span></td>
                    <td  class="smalltext"><span class="red">   2.2850</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3269</span></td>
                    <td  class="smalltext"><span class="red">   2.3209</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.3733</td>
                    <td  class="smalltext">   2.3639</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3327</td>
                    <td  class="smalltext">   2.3272</td>
                    <td  class="smalltext">   2.2988</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3210</span></td>
                    <td  class="smalltext"><span class="red">   2.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3416</span></td>
                    <td  class="smalltext"><span class="red">   2.3344</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3750</td>
                    <td  class="smalltext">   2.3656</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3344</td>
                    <td  class="smalltext">   2.3303</td>
                    <td  class="smalltext">   2.3005</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3158</span></td>
                    <td  class="smalltext"><span class="red">   2.3070</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3398</span></td>
                    <td  class="smalltext"><span class="red">   2.3344</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.3735</td>
                    <td  class="smalltext">   2.3654</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3410</td>
                    <td  class="smalltext">   2.3359</td>
                    <td  class="smalltext">   2.3140</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3320</span></td>
                    <td  class="smalltext"><span class="red">   2.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3491</span></td>
                    <td  class="smalltext"><span class="red">   2.3425</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.3750</td>
                    <td  class="smalltext">   2.3669</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3425</td>
                    <td  class="smalltext">   2.3387</td>
                    <td  class="smalltext">   2.3155</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3287</span></td>
                    <td  class="smalltext"><span class="red">   2.3210</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3475</span></td>
                    <td  class="smalltext"><span class="red">   2.3425</span></td>
                    <td  class="smalltext"><span class="red">   2.3750</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.4358</td>
                    <td  class="smalltext">   2.4264</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3952</td>
                    <td  class="smalltext">   2.3897</td>
                    <td  class="smalltext">   2.3613</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3840</span></td>
                    <td  class="smalltext"><span class="red">   2.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4041</span></td>
                    <td  class="smalltext"><span class="red">   2.3969</span></td>
                    <td  class="smalltext"><span class="red">   2.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>7</sup>/<sub>16</sub>-16 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.4375</td>
                    <td  class="smalltext">   2.4281</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3969</td>
                    <td  class="smalltext">   2.3928</td>
                    <td  class="smalltext">   2.3630</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3783</span></td>
                    <td  class="smalltext"><span class="red">   2.3700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4023</span></td>
                    <td  class="smalltext"><span class="red">   2.3969</span></td>
                    <td  class="smalltext"><span class="red">   2.4375</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0031</td>
                    <td  class="smalltext">   2.4969</td>
                    <td  class="smalltext">   2.4612</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3345</td>
                    <td  class="smalltext">   2.3190</td>
                    <td  class="smalltext">   2.1992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2670</span></td>
                    <td  class="smalltext"><span class="red">   2.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3578</span></td>
                    <td  class="smalltext"><span class="red">   2.3376</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0031</td>
                    <td  class="smalltext">   2.4969</td>
                    <td  class="smalltext">   2.4731</td>
                    <td  class="smalltext">2.4612</td>
                    <td  class="smalltext">   2.3345</td>
                    <td  class="smalltext">   2.3241</td>
                    <td  class="smalltext">   2.1992</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2670</span></td>
                    <td  class="smalltext"><span class="red">   2.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3511</span></td>
                    <td  class="smalltext"><span class="red">   2.3376</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4762</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3376</td>
                    <td  class="smalltext">   2.3298</td>
                    <td  class="smalltext">   2.2023</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.2594</span></td>
                    <td  class="smalltext"><span class="red">   2.2290</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3477</span></td>
                    <td  class="smalltext"><span class="red">   2.3376</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   2.4973</td>
                    <td  class="smalltext">   2.4791</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3890</td>
                    <td  class="smalltext">   2.3800</td>
                    <td  class="smalltext">   2.2989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3500</span></td>
                    <td  class="smalltext"><span class="red">   2.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4033</span></td>
                    <td  class="smalltext"><span class="red">   2.3917</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4818</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.3917</td>
                    <td  class="smalltext">   2.3850</td>
                    <td  class="smalltext">   2.3016</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3396</span></td>
                    <td  class="smalltext"><span class="red">   2.3200</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4004</span></td>
                    <td  class="smalltext"><span class="red">   2.3917</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0024</td>
                    <td  class="smalltext">   2.4976</td>
                    <td  class="smalltext">   2.4826</td>
                    <td  class="smalltext">2.4751</td>
                    <td  class="smalltext">   2.4164</td>
                    <td  class="smalltext">   2.4082</td>
                    <td  class="smalltext">   2.3488</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3900</span></td>
                    <td  class="smalltext"><span class="red">   2.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4294</span></td>
                    <td  class="smalltext"><span class="red">   2.4188</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4850</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4188</td>
                    <td  class="smalltext">   2.4127</td>
                    <td  class="smalltext">   2.3512</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.3797</span></td>
                    <td  class="smalltext"><span class="red">   2.3650</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4268</span></td>
                    <td  class="smalltext"><span class="red">   2.4188</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   2.4980</td>
                    <td  class="smalltext">   2.4851</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4330</td>
                    <td  class="smalltext">   2.4263</td>
                    <td  class="smalltext">   2.3790</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4130</span></td>
                    <td  class="smalltext"><span class="red">   2.3920</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4437</span></td>
                    <td  class="smalltext"><span class="red">   2.4350</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.4981</td>
                    <td  class="smalltext">   2.4867</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4440</td>
                    <td  class="smalltext">   2.4378</td>
                    <td  class="smalltext">   2.3989</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4280</span></td>
                    <td  class="smalltext"><span class="red">   2.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4540</span></td>
                    <td  class="smalltext"><span class="red">   2.4459</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4886</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4459</td>
                    <td  class="smalltext">   2.4413</td>
                    <td  class="smalltext">   2.4008</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4198</span></td>
                    <td  class="smalltext"><span class="red">   2.4100</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4519</span></td>
                    <td  class="smalltext"><span class="red">   2.4459</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.4983</td>
                    <td  class="smalltext">   2.4880</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4519</td>
                    <td  class="smalltext">   2.4461</td>
                    <td  class="smalltext">   2.4133</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4380</span></td>
                    <td  class="smalltext"><span class="red">   2.4230</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4612</span></td>
                    <td  class="smalltext"><span class="red">   2.4536</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.4983</td>
                    <td  class="smalltext">   2.4889</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4577</td>
                    <td  class="smalltext">   2.4522</td>
                    <td  class="smalltext">   2.4238</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4460</span></td>
                    <td  class="smalltext"><span class="red">   2.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4666</span></td>
                    <td  class="smalltext"><span class="red">   2.4594</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4906</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4594</td>
                    <td  class="smalltext">   2.4553</td>
                    <td  class="smalltext">   2.4255</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4408</span></td>
                    <td  class="smalltext"><span class="red">   2.4320</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4648</span></td>
                    <td  class="smalltext"><span class="red">   2.4594</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.4984</td>
                    <td  class="smalltext">   2.4897</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4623</td>
                    <td  class="smalltext">   2.4570</td>
                    <td  class="smalltext">   2.4323</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4530</span></td>
                    <td  class="smalltext"><span class="red">   2.4400</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4708</span></td>
                    <td  class="smalltext"><span class="red">   2.4639</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.4985</td>
                    <td  class="smalltext">   2.4904</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4660</td>
                    <td  class="smalltext">   2.4609</td>
                    <td  class="smalltext">   2.4390</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4570</span></td>
                    <td  class="smalltext"><span class="red">   2.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4741</span></td>
                    <td  class="smalltext"><span class="red">   2.4675</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>1</sup>/<sub>2</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.5000</td>
                    <td  class="smalltext">   2.4919</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.4675</td>
                    <td  class="smalltext">   2.4637</td>
                    <td  class="smalltext">   2.4405</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4537</span></td>
                    <td  class="smalltext"><span class="red">   2.4460</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4725</span></td>
                    <td  class="smalltext"><span class="red">   2.4675</span></td>
                    <td  class="smalltext"><span class="red">   2.5000</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   2.6223</td>
                    <td  class="smalltext">   2.6041</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5140</td>
                    <td  class="smalltext">   2.5050</td>
                    <td  class="smalltext">   2.4239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4750</span></td>
                    <td  class="smalltext"><span class="red">   2.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5285</span></td>
                    <td  class="smalltext"><span class="red">   2.5167</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.6250</td>
                    <td  class="smalltext">   2.6068</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5167</td>
                    <td  class="smalltext">   2.5099</td>
                    <td  class="smalltext">   2.4266</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.4646</span></td>
                    <td  class="smalltext"><span class="red">   2.4450</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5255</span></td>
                    <td  class="smalltext"><span class="red">   2.5167</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   2.6225</td>
                    <td  class="smalltext">   2.6075</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5413</td>
                    <td  class="smalltext">   2.5331</td>
                    <td  class="smalltext">   2.4737</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5150</span></td>
                    <td  class="smalltext"><span class="red">   2.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5545</span></td>
                    <td  class="smalltext"><span class="red">   2.5438</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.6250</td>
                    <td  class="smalltext">   2.6100</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5438</td>
                    <td  class="smalltext">   2.5376</td>
                    <td  class="smalltext">   2.4762</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5047</span></td>
                    <td  class="smalltext"><span class="red">   2.4900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5518</span></td>
                    <td  class="smalltext"><span class="red">   2.5438</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.6231</td>
                    <td  class="smalltext">   2.6117</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5690</td>
                    <td  class="smalltext">   2.5628</td>
                    <td  class="smalltext">   2.5239</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5530</span></td>
                    <td  class="smalltext"><span class="red">   2.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5790</span></td>
                    <td  class="smalltext"><span class="red">   2.5709</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.6250</td>
                    <td  class="smalltext">   2.6136</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5709</td>
                    <td  class="smalltext">   2.5663</td>
                    <td  class="smalltext">   2.5258</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5448</span></td>
                    <td  class="smalltext"><span class="red">   2.5350</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5769</span></td>
                    <td  class="smalltext"><span class="red">   2.5709</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.6233</td>
                    <td  class="smalltext">   2.6139</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5827</td>
                    <td  class="smalltext">   2.5772</td>
                    <td  class="smalltext">   2.5488</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5710</span></td>
                    <td  class="smalltext"><span class="red">   2.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5916</span></td>
                    <td  class="smalltext"><span class="red">   2.5844</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.6250</td>
                    <td  class="smalltext">   2.6156</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5844</td>
                    <td  class="smalltext">   2.5803</td>
                    <td  class="smalltext">   2.5505</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5658</span></td>
                    <td  class="smalltext"><span class="red">   2.5570</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5898</span></td>
                    <td  class="smalltext"><span class="red">   2.5844</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.6235</td>
                    <td  class="smalltext">   2.6154</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5910</td>
                    <td  class="smalltext">   2.5859</td>
                    <td  class="smalltext">   2.5640</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5820</span></td>
                    <td  class="smalltext"><span class="red">   2.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5991</span></td>
                    <td  class="smalltext"><span class="red">   2.5925</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>5</sup>/<sub>8</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.6250</td>
                    <td  class="smalltext">   2.6169</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5925</td>
                    <td  class="smalltext">   2.5887</td>
                    <td  class="smalltext">   2.5655</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5787</span></td>
                    <td  class="smalltext"><span class="red">   2.5710</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5975</span></td>
                    <td  class="smalltext"><span class="red">   2.5925</span></td>
                    <td  class="smalltext"><span class="red">   2.6250</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">1A</a></td>
                    <td  class="smalltext">   0.0032</td>
                    <td  class="smalltext">   2.7468</td>
                    <td  class="smalltext">   2.7111</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5844</td>
                    <td  class="smalltext">   2.5686</td>
                    <td  class="smalltext">   2.4491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">1B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5170</span></td>
                    <td  class="smalltext"><span class="red">   2.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6082</span></td>
                    <td  class="smalltext"><span class="red">   2.5876</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0032</td>
                    <td  class="smalltext">   2.7468</td>
                    <td  class="smalltext">   2.7230</td>
                    <td  class="smalltext">2.7111</td>
                    <td  class="smalltext">   2.5844</td>
                    <td  class="smalltext">   2.5739</td>
                    <td  class="smalltext">   2.4491</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5170</span></td>
                    <td  class="smalltext"><span class="red">   2.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6013</span></td>
                    <td  class="smalltext"><span class="red">   2.5876</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-4 UNC</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7262</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.5876</td>
                    <td  class="smalltext">   2.5797</td>
                    <td  class="smalltext">   2.4523</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5094</span></td>
                    <td  class="smalltext"><span class="red">   2.4790</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5979</span></td>
                    <td  class="smalltext"><span class="red">   2.5876</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0027</td>
                    <td  class="smalltext">   2.7473</td>
                    <td  class="smalltext">   2.7291</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6390</td>
                    <td  class="smalltext">   2.6299</td>
                    <td  class="smalltext">   2.5489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6000</span></td>
                    <td  class="smalltext"><span class="red">   2.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6536</span></td>
                    <td  class="smalltext"><span class="red">   2.6417</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-6 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7318</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6417</td>
                    <td  class="smalltext">   2.6349</td>
                    <td  class="smalltext">   2.5516</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.5896</span></td>
                    <td  class="smalltext"><span class="red">   2.5700</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6506</span></td>
                    <td  class="smalltext"><span class="red">   2.6417</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0025</td>
                    <td  class="smalltext">   2.7475</td>
                    <td  class="smalltext">   2.7325</td>
                    <td  class="smalltext">2.7250</td>
                    <td  class="smalltext">   2.6663</td>
                    <td  class="smalltext">   2.6580</td>
                    <td  class="smalltext">   2.5987</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6400</span></td>
                    <td  class="smalltext"><span class="red">   2.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6796</span></td>
                    <td  class="smalltext"><span class="red">   2.6688</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-8 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7350</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6688</td>
                    <td  class="smalltext">   2.6625</td>
                    <td  class="smalltext">   2.6012</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6297</span></td>
                    <td  class="smalltext"><span class="red">   2.6150</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6769</span></td>
                    <td  class="smalltext"><span class="red">   2.6688</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-10 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0020</td>
                    <td  class="smalltext">   2.7480</td>
                    <td  class="smalltext">   2.7351</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6830</td>
                    <td  class="smalltext">   2.6763</td>
                    <td  class="smalltext">   2.6290</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6630</span></td>
                    <td  class="smalltext"><span class="red">   2.6420</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6937</span></td>
                    <td  class="smalltext"><span class="red">   2.6850</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0019</td>
                    <td  class="smalltext">   2.7481</td>
                    <td  class="smalltext">   2.7367</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6940</td>
                    <td  class="smalltext">   2.6878</td>
                    <td  class="smalltext">   2.6489</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6780</span></td>
                    <td  class="smalltext"><span class="red">   2.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7040</span></td>
                    <td  class="smalltext"><span class="red">   2.6959</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-12 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7386</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.6959</td>
                    <td  class="smalltext">   2.6913</td>
                    <td  class="smalltext">   2.6508</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6698</span></td>
                    <td  class="smalltext"><span class="red">   2.6600</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7019</span></td>
                    <td  class="smalltext"><span class="red">   2.6959</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-14 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.7483</td>
                    <td  class="smalltext">   2.7380</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7019</td>
                    <td  class="smalltext">   2.6961</td>
                    <td  class="smalltext">   2.6633</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6880</span></td>
                    <td  class="smalltext"><span class="red">   2.6730</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7112</span></td>
                    <td  class="smalltext"><span class="red">   2.7036</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0017</td>
                    <td  class="smalltext">   2.7483</td>
                    <td  class="smalltext">   2.7389</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7077</td>
                    <td  class="smalltext">   2.7022</td>
                    <td  class="smalltext">   2.6738</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6960</span></td>
                    <td  class="smalltext"><span class="red">   2.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7166</span></td>
                    <td  class="smalltext"><span class="red">   2.7094</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-16 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7406</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7094</td>
                    <td  class="smalltext">   2.7053</td>
                    <td  class="smalltext">   2.6755</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.6908</span></td>
                    <td  class="smalltext"><span class="red">   2.6820</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7148</span></td>
                    <td  class="smalltext"><span class="red">   2.7094</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-18 UNS</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0016</td>
                    <td  class="smalltext">   2.7484</td>
                    <td  class="smalltext">   2.7397</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7123</td>
                    <td  class="smalltext">   2.7070</td>
                    <td  class="smalltext">   2.6832</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7030</span></td>
                    <td  class="smalltext"><span class="red">   2.6900</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7208</span></td>
                    <td  class="smalltext"><span class="red">   2.7139</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">2A</a></td>
                    <td  class="smalltext">   0.0015</td>
                    <td  class="smalltext">   2.7485</td>
                    <td  class="smalltext">   2.7404</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7160</td>
                    <td  class="smalltext">   2.7109</td>
                    <td  class="smalltext">   2.6890</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7070</span></td>
                    <td  class="smalltext"><span class="red">   2.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7241</span></td>
                    <td  class="smalltext"><span class="red">   2.7175</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>


                <tr  align="center" bgcolor="eeeeee">
                    <td  class="smalltext">Ext.</td>
                    <td  class="smalltext" nowrap>2<sup>3</sup>/<sub>4</sub>-20 UN</td>
                    <td  class="smalltext"><a href="screws_intro.cfm#class">3A</a></td>
                    <td  class="smalltext">   0.0000</td>
                    <td  class="smalltext">   2.7500</td>
                    <td  class="smalltext">   2.7419</td>
                    <td  class="smalltext">-</td>
                    <td  class="smalltext">   2.7175</td>
                    <td  class="smalltext">   2.7137</td>
                    <td  class="smalltext">   2.6905</td>
                </tr>

                <tr  align="center" bgcolor="eeeee0">
                    <td  class="smalltext"><span class="red">Int.</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3B</a></span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7037</span></td>
                    <td  class="smalltext"><span class="red">   2.6960</span></td>
                    <td  class="smalltext">&nbsp;</td>
                    <td  class="smalltext"><span class="red">   2.7225</span></td>
                    <td  class="smalltext"><span class="red">   2.7175</span></td>
                    <td  class="smalltext"><span class="red">   2.7500</span></td>

                </tr>
            </table>
        </div>
    </div>
@endsection