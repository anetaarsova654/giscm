@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="header-bottom-on">
                @if (!Auth::check())

                    <p class="wel"><a href="{{route('login')}}">Welcome visitor you can login or create an account.</a></p><br>

                @endif
                <br />
                <div class="header-can">
                    <ul class="social-in">

                        <li><a href="https://www.facebook.com/GISCMCORP" target="_blank"><i class="facebook"> </i></a></li>
                        <li><a href="https://www.twitter.com/GISCMCORP" target="_blank"><i class="twitter"> </i></a></li>

                    </ul>
                    <div class="search">

                        {!!  Form::open(['url' => route('cadmodels.index'),'class'=>'form-horizontal','id'=>'searchBox','method'=>'GET']) !!}

                        {{ Form::text('q' ,null, ['id'=>'search','placeholder'=>'Model Name']) }}

                        <input type="submit" value="">

                        {!!  Form::close() !!}

                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="subheading"><b>Library</b></h2></div>
            <div class="col-md-12 cad-menu">
                <div class="col-md-2 col-md col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                    <a href="{{route('cadmodels.index')}}">
                        <center><a href="{{route('cadmodels.index')}}">
                                <img src="{{asset('images/CAD_Models.jpg')}}" border=0 alt="CAD Library" /></a></center><br/></a>
                    <div class="top-content">
                        <center><h4><a href="#">CAD Models</a></h4></center></div></div>
                <div class="col-md-2 col-md col-md-offset-1 col-xs-5 col-sm col-xs-5 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                    <a href="{{route('threadSpecifications')}}"><center> <a href="{{route('threadSpecifications')}}"><img src="{{asset('images/screw_thread.png')}}" border=0 alt="Thread Specifications" /></a></center>
                        <br/></a>
                    <div class="top-content"><center>
                            <h4><a href="{{route('threadSpecifications')}}">Thread Specifications</a></h4></center></div> </div>
                <div class="col-md-2 col-md col-xs-5 col-xs-offset-1 col-sm-6" style="height: 200px; margin-bottom: 20px;">
                    <a href="{{route('adapterAssemblySpecifications')}}">
                        <center><a href="{{route('adapterAssemblySpecifications')}}"><img src="{{asset('images/adapter_specs.jpg')}}" border=0 alt="Adapter Assembly Specifications" /></a></center><br/></a>
                    <div class="top-content">
                        <center><h4><a href="{{route('adapterAssemblySpecifications')}}">Adapter Assembly Specifications</a></h4></center>
                    </div></div></div>
        </div>
    </div>
    @endsection