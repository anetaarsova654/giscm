@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>Metric Threads</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row">
            <div class="table-responsive">
                <!--Table begining-->
                <table class="table">

                    <tr align="center" bgcolor="ccccbb">
                        <td colspan="10" valign="bottom" class="smalltext"><b>Metric Threads<span class="red">- Coarse Thread Pitches</span></b></td>
                    </tr>
                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b>Size - Nominal Diameter <a href="unified.cfm#footer"><sup></sup></a><br><span class="red">(mm)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Pitch1<a href="unified.cfm#footer"><sup></sup></a><br><span class="red">(mm)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Clearance Drill<a href="unified.cfm#footer"><sup></sup></a><br><span class="red">(mm)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Tap Drill(mm)<a href="unified.cfm#footer"><sup></sup></a><br><span class="red">(mm)</span></b></td>

                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 1.60</td>
                        <td  class="smalltext" nowrap>0.35</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">1.8</a></td>
                        <td  class="smalltext"> 1.25</td>

                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 2.00</span></td>
                        <td  class="smalltext">0.40</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">2.4</a></span></td>
                        <td  class="smalltext">1.60</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 2.50</td>
                        <td  class="smalltext" nowrap>0.45</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">2.90</a></td>
                        <td  class="smalltext">2.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 3.00</span></td>
                        <td  class="smalltext">0.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">3.40</a></span></td>
                        <td  class="smalltext">2.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 3.50</td>
                        <td  class="smalltext" nowrap>0.60</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">3.90</a></td>
                        <td  class="smalltext">2.90</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 4.00</span></td>
                        <td  class="smalltext">0.70</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">4.50</a></span></td>
                        <td  class="smalltext">3.30&nbsp;</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 5.00</td>
                        <td  class="smalltext" nowrap>0.80</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">5.50</a></td>
                        <td  class="smalltext">4.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 6.00</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">6.60</a></span></td>
                        <td  class="smalltext">5.00</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 8.00</td>
                        <td  class="smalltext" nowrap>1.25</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">9.00</a></td>
                        <td  class="smalltext">6.80</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 10.00</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">12.00</a></span></td>
                        <td  class="smalltext">8.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 12.00</td>
                        <td  class="smalltext" nowrap>1.75</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">14.00</a></td>
                        <td  class="smalltext"> 10.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 14.00</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">16.00</a></span></td>
                        <td  class="smalltext">12.00</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 16.00</td>
                        <td  class="smalltext" nowrap>2.00</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">18.00</a></td>
                        <td  class="smalltext">14.00</td>

                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 20.00</span></td>
                        <td  class="smalltext">2.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">22.00</a></span></td>
                        <td  class="smalltext">17.50</td>

                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 22.00</td>
                        <td  class="smalltext" nowrap>2.50</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">25.00</a></td>
                        <td  class="smalltext"> 19.50</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 24.00</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">27.00</a></span></td>
                        <td  class="smalltext">21.00</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 27.00</td>
                        <td  class="smalltext" nowrap>3.00</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">30.00</a></td>
                        <td  class="smalltext">24.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 30.00</span></td>
                        <td  class="smalltext">3.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">33.00</a></span></td>
                        <td  class="smalltext">26.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 36.00</td>
                        <td  class="smalltext" nowrap>4.00</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">40.00</a></td>
                        <td  class="smalltext">32.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 42.00</span></td>
                        <td  class="smalltext">4.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">46.00</a></span></td>
                        <td  class="smalltext">37.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 48.00</td>
                        <td  class="smalltext" nowrap>5.00</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">53.00</a></td>
                        <td  class="smalltext">43.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 56.00</span></td>
                        <td  class="smalltext">5.50</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">62.00</a></span></td>
                        <td  class="smalltext">50.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 64.00</td>
                        <td  class="smalltext" nowrap>6.00</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">70.00</a></td>
                        <td  class="smalltext">58.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 68.00</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext"><span class="red"><a href="screws_intro.cfm#class">74.00</a></span></td>
                        <td  class="smalltext">62.00</td>
                    </tr>
                </table>
                <!--Table ending-->
            </div>
            <div> <b><sup>1)</sup>For metric threads pitch is the distance between threads.</b></p>
                <ul style="list-style:none;">
                    <li><i><b>1 mm ~= 0.03937 inches</b></i></li>
                </ul>
            </div>
            <div class="table-responsive">
                <!--Second table-->
                <table class="table">

                    <tr align="center" bgcolor="ccccbb">
                        <td colspan="10" valign="bottom" class="smalltext"><b>Metric Threads<span class="red">- Coarse Thread Pitches</span></b></td>
                    </tr>
                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b>Size - Nominal Diameter <br><span class="red">(mm)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Pitch<sup>(1)</sup><br><span class="red">(mm)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>Tap Drill<br><span class="red">(mm)</span></b></td>

                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 1.0 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 0.80</td>

                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 1.1 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 0.90</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 1.2 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 1.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 1.4 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 1.20</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 1.6 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 1.40</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 1.8 x 0.2</td>
                        <td  class="smalltext" nowrap>0.20</td>
                        <td  class="smalltext"> 1.60</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 2 x 0.25</td>
                        <td  class="smalltext" nowrap>0.25</td>
                        <td  class="smalltext"> 1.75</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 2.2 x 0.25</td>
                        <td  class="smalltext" nowrap>0.25</td>
                        <td  class="smalltext"> 1.95</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 2.5 x 0.35</td>
                        <td  class="smalltext" nowrap>0.35</td>
                        <td  class="smalltext"> 2.10</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 3 x 0.35</td>
                        <td  class="smalltext" nowrap>0.35</td>
                        <td  class="smalltext"> 2.60</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 3.5 x 0.35</td>
                        <td  class="smalltext" nowrap>0.35</td>
                        <td  class="smalltext"> 3.10</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 4 x 0.5</td>
                        <td  class="smalltext" nowrap>0.50</td>
                        <td  class="smalltext"> 3.50</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 4.5 x 0.5</td>
                        <td  class="smalltext" nowrap>0.50</td>
                        <td  class="smalltext"> 4.00</td>

                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 5 x 0.5</td>
                        <td  class="smalltext" nowrap>0.50</td>
                        <td  class="smalltext"> 4.50</td>

                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 5.5 x 0.5</td>
                        <td  class="smalltext" nowrap>0.50</td>
                        <td  class="smalltext"> 5.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">M 6 x 0.75</td>
                        <td  class="smalltext" nowrap>0.75</td>
                        <td  class="smalltext"> 5.20</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">M 7 x 0.75</td>
                        <td  class="smalltext" nowrap>0.75</td>
                        <td  class="smalltext"> 6.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 8 x 0.75	</span></td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">7.20</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 8 x 1.00	</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">7.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 9 x 0.75	</span></td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">8.20</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 9 x 1.00	</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">8.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 10 x 0.75</span></td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">9.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 10 x 1.25</span></td>
                        <td  class="smalltext">1.25</td>
                        <td  class="smalltext">8.80</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 11 x 0.75</span></td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">10.20</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 11 x 1</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">10.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 12 x 1</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">11.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 12 x 1.25</span></td>
                        <td  class="smalltext">1.25</td>
                        <td  class="smalltext">10.80</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 12 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">10.50</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 14 x 1.00</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">13.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 14 x 1.25</span></td>
                        <td  class="smalltext">1.25</td>
                        <td  class="smalltext">12.80</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 14 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">12.50</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 15 x 1</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">14.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 15 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">13.50</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 16 x 1</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">15.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 16 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">14.50</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 17 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">16.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 17 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">15.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 18 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">17.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 18 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">16.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 18 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">16.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 20 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">19.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 20 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">18.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 20 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">18.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 22 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">21.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 22 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">20.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 22 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">20.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 24 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">23.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 24 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">22.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 24 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">22.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 25 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">24.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 25 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">23.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 25 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">23.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 27 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">26.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 27 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">25.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 27 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">25.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 28 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">27.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 28 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">26.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 27 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">26.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 30 x 1.0</span></td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">29.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 30 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">28.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 30 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">28.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 30 x 3.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">27.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 32 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">30.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 32 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">30.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 33 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">31.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 33 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">31.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 33 x 3.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">31.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 35 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">33.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 35 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">33.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 36 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">34.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 36 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">34.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 36 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">33.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 39 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">37.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 39 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">37.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 39 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">36.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 40 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">38.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 40 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">38.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 40 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">37.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 42 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">40.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 42 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">40.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 42 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">39.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 42 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">38.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 45 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">43.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 45 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">43.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 45 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">42.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 45 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">41.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 48 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">46.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 48 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">46.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 48 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">45.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 48 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">44.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 50 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">48.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 50 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">48.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 50 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">47.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 52 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">50.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 52 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">50.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 52 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">49.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 52 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">48.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 55 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">53.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 55 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">53.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 55 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">52.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 55 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">51.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 56 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">54.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 56 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">54.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 56 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">53.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 56 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">52.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 58 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">56.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 58 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">56.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 58 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">55.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 58 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">54.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 60 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">58.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 60 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">58.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 60 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">57.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 60 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">56.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 62 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">60.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 62 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">60.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 62 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">59.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 62 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">58.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 64 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">62.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 64 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">62.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 64 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">61.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 64 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">60.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 65 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">63.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 65 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">63.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 65 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">62.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 65 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">61.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 68 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">66.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 68 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">66.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 68 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">65.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 68 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">64.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 70 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">68.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 70 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">68.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 70 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">67.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 70 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">66.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 70 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">64.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 72 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">70.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 72 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">70.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 70 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">69.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 72 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">68.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 72 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">66.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 75 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">73.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 75 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">73.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 75 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">72.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 75 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">71.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 75 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">69.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 76 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">74.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 76 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">74.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 76 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">73.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 76 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">72.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 76 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">70.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext"><span class="red">M 80 x 1.5</span></td>
                        <td  class="smalltext">1.50</td>
                        <td  class="smalltext">78.50</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 80 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">78.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 80 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">77.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 80 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">76.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 80 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">74.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 85 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">83.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 85 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">82.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 85 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">81.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 85 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">79.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 90 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">88.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 90 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">87.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 90 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">86.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 90 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">84.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 95 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">93.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 95 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">92.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 95 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">91.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 95 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">89.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 100 x 2.0</span></td>
                        <td  class="smalltext">2.00</td>
                        <td  class="smalltext">98.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 100 x 3.0</span></td>
                        <td  class="smalltext">3.00</td>
                        <td  class="smalltext">97.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 100 x 4.0</span></td>
                        <td  class="smalltext">4.00</td>
                        <td  class="smalltext">96.00</td>
                    </tr>
                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext"><span class="red">M 100 x 6.0</span></td>
                        <td  class="smalltext">6.00</td>
                        <td  class="smalltext">94.00</td>
                    </tr>
                </table>
                <!--End of second table-->
            </div>

        </div>
    </div>
    @endsection