@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>NPTF Threads</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row">
            <!--Table begining-->
            <div class="table-responsive">
                <table class="table" style="margin-bottom: 0px;">

                    <tr align="center" bgcolor="ccccbb">
                        <td colspan="10" valign="bottom" class="smalltext"><b>Table A1-Dimensions of dryseal american standard external<span class="red">taper pipe thread blanks (Cut Threads)</span></b></td>
                    </tr>
                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br><span class="red">Size</span></b></td>
                        <td  valign="bottom" class="smalltext"><b>OD at Large End<br>NPTF at L<sub>2</sub> Length D<sub>2</sub><br>PTF-SAE Short<br> at L<sub>2</sub>- 1/2p Length<br>(Basic Thread One<br>Turn Large with<br>Max Truncation)<br><span class="red">+0.003 - 0.000</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br>OD at Small End, A<br>NPTF<br>(Basic Thread Two<br>Turns Large with<br>Max Truncation)<br><span class="red">+0.003 - 0.000</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br>OD at Small End, A<br>PTF-SAE Short<br>(Basic Thread 2-1/2<br>Turn Lare with Max<br>Truncation)<br><span class="red">+0.003 - 0.000</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br>Chamfer Dia<sup>(1), B</sup><br>(Minor Dia<sup>(1)</sup> at<br><span class="red">Small End)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><span class="red">Min Length from<br>Small End to<br>Shoulder, TL <br>NPTF<br> L<sub>2</sub>+(3p Approx)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><span class="red">Min Length from Small<br>End to Shoulder, TL<br>PTF-SAE Short<br>L<sub>2</sub> Short + (2-1/2p<br>Approx)</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><span class="red">Corner<br>Radius, R<br>Max</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br><span class="red">Recommended<br>Hole Size<sup>(2)</sup>, H</span></b></td>
                    </tr>

                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/16-27</td>
                        <td  class="smalltext" nowrap>0.315</td>
                        <td  class="smalltext">0.301</td>
                        <td  class="smalltext">0.302</td>
                        <td  class="smalltext">0.23</td>
                        <td  class="smalltext">0.38</td>
                        <td  class="smalltext">0.3167</td>
                        <td  class="smalltext">0.03</td>
                        <td  class="smalltext">0.12</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1/8-27</td>
                        <td  class="smalltext" nowrap>0.407</td>
                        <td  class="smalltext">0.393</td>
                        <td  class="smalltext">0.394</td>
                        <td  class="smalltext">0.32</td>
                        <td  class="smalltext">0.38</td>
                        <td  class="smalltext">0.3194</td>
                        <td  class="smalltext">0.03</td>
                        <td  class="smalltext">0.19</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/4-18</td>
                        <td  class="smalltext" nowrap>0.546</td>
                        <td  class="smalltext">0.523</td>
                        <td  class="smalltext">0.525</td>
                        <td  class="smalltext">0.42+0.00</td>
                        <td  class="smalltext">0.56</td>
                        <td  class="smalltext">0.4851</td>
                        <td  class="smalltext">0.06</td>
                        <td  class="smalltext">0.28</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/8-18</td>
                        <td  class="smalltext" nowrap>0.681</td>
                        <td  class="smalltext">0.658</td>
                        <td  class="smalltext">0.660</td>
                        <td  class="smalltext">0.55</td>
                        <td  class="smalltext">0.56</td>
                        <td  class="smalltext">0.4911</td>
                        <td  class="smalltext">0.06</td>
                        <td  class="smalltext">0.41</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/2-14</td>
                        <td  class="smalltext" nowrap>0.850</td>
                        <td  class="smalltext">0.820</td>
                        <td  class="smalltext">0.822</td>
                        <td  class="smalltext">0.68-0.02</td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">0.6409</td>
                        <td  class="smalltext">0.08</td>
                        <td  class="smalltext">0.56</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/4-14</td>
                        <td  class="smalltext" nowrap>1.060</td>
                        <td  class="smalltext">1.029</td>
                        <td  class="smalltext">1.031</td>
                        <td  class="smalltext">0.89</td>
                        <td  class="smalltext">0.75</td>
                        <td  class="smalltext">0.6528</td>
                        <td  class="smalltext">0.08</td>
                        <td  class="smalltext">0.72</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-11-1/2</td>
                        <td  class="smalltext" nowrap>1.327</td>
                        <td  class="smalltext">1.289</td>
                        <td  class="smalltext">1.292</td>
                        <td  class="smalltext">1.12</td>
                        <td  class="smalltext">0.94</td>
                        <td  class="smalltext">0.8132</td>
                        <td  class="smalltext">0.09</td>
                        <td  class="smalltext">1.94</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1-1/4-11-1/2</td>
                        <td  class="smalltext" nowrap>1.672</td>
                        <td  class="smalltext">1.633</td>
                        <td  class="smalltext">1.636</td>
                        <td  class="smalltext">1.46+0.00</td>
                        <td  class="smalltext">0.97</td>
                        <td  class="smalltext">0.8372</td>
                        <td  class="smalltext">0.09</td>
                        <td  class="smalltext">1.25</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-1/2-11-1/2</td>
                        <td  class="smalltext" nowrap>1.912</td>
                        <td  class="smalltext">1.872</td>
                        <td  class="smalltext">1.875</td>
                        <td  class="smalltext">1.70</td>
                        <td  class="smalltext">1.00</td>
                        <td  class="smalltext">0.8539</td>
                        <td  class="smalltext">0.09</td>
                        <td  class="smalltext">1.47</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">2 -11-1/2</td>
                        <td  class="smalltext" nowrap>2.387</td>
                        <td  class="smalltext">2.345</td>
                        <td  class="smalltext">2.348</td>
                        <td  class="smalltext">2.17-0.03</td>
                        <td  class="smalltext">1.03</td>
                        <td  class="smalltext">0.8869</td>
                        <td  class="smalltext">0.09</td>
                        <td  class="smalltext">1.94</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">2-1/2-8</td>
                        <td  class="smalltext" nowrap>2.893</td>
                        <td  class="smalltext">2.829</td>
                        <td  class="smalltext">2.833</td>
                        <td  class="smalltext">2.59</td>
                        <td  class="smalltext">1.52</td>
                        <td  class="smalltext">1.3250</td>
                        <td  class="smalltext">0.12</td>
                        <td  class="smalltext">2.31</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3 -8</td>
                        <td  class="smalltext" nowrap>3.518</td>
                        <td  class="smalltext">3.450</td>
                        <td  class="smalltext">3.454</td>
                        <td  class="smalltext">3.21</td>
                        <td  class="smalltext">1.58</td>
                        <td  class="smalltext">1.3875</td>
                        <td  class="smalltext">0.12</td>
                        <td  class="smalltext">2.91</td>
                    </tr>


                </table>
            </div>
            <!--Table ending-->
            <div style="background-color: #CCCC99">
                <ul style="list-style-type: none;">
                    <li class="smalltext"><b><br>1. External pipe threads shall be chamfered from a diameter (rounded to a two-place decimal) obtained by substracting 0,.016 in. for sizes below 1 in. and 0.025 in. for larger sizes from the minimum minor diameter at small end to produce a length of chamfered or partial thread equivalent to 1 to 1-1.2 times the pith (rounded to a three-place decimal).</b></li>
                    <li class="smalltext"><b><br>2. The hole sizes recommended represent a desirable maximum, strength of wall being considered. However, as considerations other than wall strength frequently control the hole size in specific applications, the recommendations should not be construed as a requirement of this SAE Standard.</b></li>
                </ul>
            </div>
        </div>

    </div>
@endsection