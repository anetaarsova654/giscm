@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row"> <!--style="padding-top: 2%;padding-left: 2%;">-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="{{route('threadSpecifications')}}"><h3 class="subheading"><i class="fa fa-home" aria-hidden="true"></i> Thread Specifications</h3></a></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2 class="subheading"><b>NPSM and NPTF Thread Hole Sizes</b></h2></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="clearfix"> </div></div>
        </div>
        <br />
        <div class="row">
            <!--Table begining-->
            <div class="table-responsive">
                <table class="table" style="margin-bottom: 0px;">

                    <tr align="center" bgcolor="ccccbb">
                        <td colspan="16" valign="bottom" class="smalltext"><b>Pipe-Thread drilled Hole Sizes</b></td>
                    </tr>
                    <tr align="center" bgcolor="cccc99">
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br><br><br><br><br><br><br><span class="red">Size</span></b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br>Straight<br>Pipe<br>Thread<br>Fuel<br>(NPSF)<br>Minor<br>Dia<sup>(1)</sup><br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br>Straight<br>Pipe<br>Thread<br>Fuel<br>(NPSF)<br>Drilled<br>Hole Size<br>+0.003<br>-0.001</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br>Straight Pipe<br>Thread<br>Intermediate<br>(NPSI)<br>Minor Dia<sup>(2)</sup><br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br>Straight Pipe<br>Thread<br>Intermediate<br>(NPSI)<br>Drilled Hole<br>Size<br>+0.003<br>-0.001</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br>Straight<br>Pipe<br>Thread<br>Desired<br>Lenght of<br>Full Thread<br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br>Straight<br>Pipe<br>Thread<br>Hole<br>Depth for<br>Plug End<br>Tap,<br>Tables B4<br>and B5</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br>Taper Pipe<br>Thread<br>NPTF(Not<br>Reamed)<br>2 FF<br>Thread<sup>(3)</sup>Minor Dia<br>2 Thread<br>Small from<br>Large End<br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br>Taper Pipe<br>Thread<br>NPTF(Not<br>Reamed)<br>2 FF<br>Thread<sup>(2)</sup><br>Drilled<br>Hole Size<br>+0.003<br>=0.001</b></td>
                        <td  valign="bottom" class="smalltext"><b>Taper Pipe<br>Thread<br>NPTF(Not<br>Reamed)<br>4 FF<br>Thread<sup>(2)</sup><br>Minor Dia<br>4<br>Thread Small<br>from Large<br>End<br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b>Taper<br>Pipe<br>Thread<br>NPTF<br>(Not<br>Reamed)<br>4 FF<br>Thread<sup>(2)</sup><br>Drilled<br>Hole Size<br>+0.003<br>-0.001</b></td>
                        <td  valign="bottom" class="smalltext"><b><br>Taper Pipe<br>Thread<br>NPTF<sup>(4)</sup><br>(Taper<br>Reamed)<br>Desired<br>Minor Dia<br>at Small<br>End<br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br>Taper Pipe<br>Thread<br>NPTF<sup>(4)</sup><br>(Taper<br>Reamed)<br>Drilled<br>Hole Size<br>+0.003<br>-0.001</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br>Taper Pipe<br>Thread<br>Desired<br>Length of<br>Full Thread<br>Min</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br>Taper Pipe<br>Thread<br>Hole Depth<br>for Standard<br>Tap, Table B2</b></td>
                        <td  valign="bottom" class="smalltext"><b><br><br><br><br><br><br><br><br><br><br>Countersink<br>90 Deg x dia<sup>(4)</sup></b></td>

                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/16-27</td>
                        <td  class="smalltext" nowrap>0.2482</td>
                        <td  class="smalltext"><a href="screws_intro.cfm#class">0.2500</a></td>
                        <td  class="smalltext">0.2505</td>
                        <td  class="smalltext">0.2500</td>
                        <td  class="smalltext">0.31</td>
                        <td  class="smalltext">0.47</td>
                        <td  class="smalltext">0.2480</td>
                        <td  class="smalltext">0.2460</td>
                        <td  class="smalltext">0.2434</td>
                        <td  class="smalltext">0.2420</td>
                        <td  class="smalltext">0.2356</td>
                        <td  class="smalltext">0.2344</td>
                        <td  class="smalltext">0.31</td>
                        <td  class="smalltext">0.56</td>
                        <td  class="smalltext">0.33</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1/8-27</td>
                        <td  class="smalltext" nowrap>0.3406</td>
                        <td  class="smalltext">0.3437</td>
                        <td  class="smalltext">0.3429</td>
                        <td  class="smalltext">0.3437</td>
                        <td  class="smalltext">0.31</td>
                        <td  class="smalltext">0.47</td>
                        <td  class="smalltext">0.3403</td>
                        <td  class="smalltext">0.3390</td>
                        <td  class="smalltext">0.3357</td>
                        <td  class="smalltext">0.3320</td>
                        <td  class="smalltext">0.3279</td>
                        <td  class="smalltext">0.3281</td>
                        <td  class="smalltext">0.31</td>
                        <td  class="smalltext">0.56</td>
                        <td  class="smalltext">0.42</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/4-18</td>
                        <td  class="smalltext" nowrap>0.4422</td>
                        <td  class="smalltext">0.4440</td>
                        <td  class="smalltext">0.4457</td>
                        <td  class="smalltext">0.4440</td>
                        <td  class="smalltext">0.47</td>
                        <td  class="smalltext">0.72</td>
                        <td  class="smalltext">0.4417</td>
                        <td  class="smalltext">0.4375</td>
                        <td  class="smalltext">0.4348</td>
                        <td  class="smalltext">0.4300</td>
                        <td  class="smalltext">0.4241</td>
                        <td  class="smalltext">0.4219</td>
                        <td  class="smalltext">0.47</td>
                        <td  class="smalltext">0.81</td>
                        <td  class="smalltext">0.55</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/8-18</td>
                        <td  class="smalltext">0.5776</td>
                        <td  class="smalltext">0.5781</td>
                        <td  class="smalltext">0.5811</td>
                        <td  class="smalltext">0.5781</td>
                        <td  class="smalltext">0.50</td>
                        <td  class="smalltext">0.72</td>
                        <td  class="smalltext">0.5771</td>
                        <td  class="smalltext">0.5781</td>
                        <td  class="smalltext">0.5702</td>
                        <td  class="smalltext">0.5700</td>
                        <td  class="smalltext">0.5587</td>
                        <td  class="smalltext">0.5625</td>
                        <td  class="smalltext">0.50</td>
                        <td  class="smalltext">0.81</td>
                        <td  class="smalltext">0.69+0.02</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1/2-14</td>
                        <td  class="smalltext">0.7133</td>
                        <td  class="smalltext">0.7187</td>
                        <td  class="smalltext">0.7180</td>
                        <td  class="smalltext">0.7187</td>
                        <td  class="smalltext">0.66</td>
                        <td  class="smalltext">0.94</td>
                        <td  class="smalltext">0.7127</td>
                        <td  class="smalltext">0.7031</td>
                        <td  class="smalltext">0.7038</td>
                        <td  class="smalltext">0.6960</td>
                        <td  class="smalltext">0.6873</td>
                        <td  class="smalltext">0.6875</td>
                        <td  class="smalltext">0.66</td>
                        <td  class="smalltext">1.06</td>
                        <td  class="smalltext">0.85-0.00</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3/4-14</td>
                        <td  class="smalltext">0.9238</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">0.9283</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">0.66</td>
                        <td  class="smalltext">0.94</td>
                        <td  class="smalltext">0.9232</td>
                        <td  class="smalltext">0.9219</td>
                        <td  class="smalltext">0.9143</td>
                        <td  class="smalltext">0.9062</td>
                        <td  class="smalltext">0.8976</td>
                        <td  class="smalltext">0.8906</td>
                        <td  class="smalltext">0.66</td>
                        <td  class="smalltext">1.06</td>
                        <td  class="smalltext">1.06</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-11-1/2</td>
                        <td  class="smalltext">1.1600</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1.1655</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">0.78</td>
                        <td  class="smalltext">1.16</td>
                        <td  class="smalltext">1.1593</td>
                        <td  class="smalltext">1.1562</td>
                        <td  class="smalltext">1.1484</td>
                        <td  class="smalltext">1.1406</td>
                        <td  class="smalltext">1.1290</td>
                        <td  class="smalltext">1.1250</td>
                        <td  class="smalltext">0.78</td>
                        <td  class="smalltext">1.25</td>
                        <td  class="smalltext">1.34</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">1-1/4-11-1/2</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1.5041</td>
                        <td  class="smalltext">1.5000</td>
                        <td  class="smalltext">1.4932</td>
                        <td  class="smalltext">1.4844</td>
                        <td  class="smalltext">1.4725</td>
                        <td  class="smalltext">1.4687</td>
                        <td  class="smalltext">0.81</td>
                        <td  class="smalltext">1.31</td>
                        <td  class="smalltext">1.68+0.03</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">1-1/2-11-1/2</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">1.7430</td>
                        <td  class="smalltext">1.7344</td>
                        <td  class="smalltext">1.7321</td>
                        <td  class="smalltext">1.7188</td>
                        <td  class="smalltext">1.7115</td>
                        <td  class="smalltext">1.7031</td>
                        <td  class="smalltext">0.81</td>
                        <td  class="smalltext">1.31</td>
                        <td  class="smalltext">1.92</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">2 -11-1/2</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">2.2170</td>
                        <td  class="smalltext">2.2187</td>
                        <td  class="smalltext">2.2061</td>
                        <td  class="smalltext">2.2031</td>
                        <td  class="smalltext">2.1844</td>
                        <td  class="smalltext">2.1875</td>
                        <td  class="smalltext">0.81</td>
                        <td  class="smalltext">1.31</td>
                        <td  class="smalltext">2.39-0.00</td>
                    </tr>


                    <tr  align="center" bgcolor="eeeeee">
                        <td  class="smalltext">2-1/2-8</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">2.6488</td>
                        <td  class="smalltext">2.6406</td>
                        <td  class="smalltext">2.6336</td>
                        <td  class="smalltext">2.6250</td>
                        <td  class="smalltext">2.5983</td>
                        <td  class="smalltext">2.5937</td>
                        <td  class="smalltext">1.25</td>
                        <td  class="smalltext">1.84</td>
                        <td  class="smalltext">2.89</td>
                    </tr>

                    <tr  align="center" bgcolor="eeeee0">
                        <td  class="smalltext">3 -8</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">-</td>
                        <td  class="smalltext">3.2751</td>
                        <td  class="smalltext">3.2656</td>
                        <td  class="smalltext">3.2595</td>
                        <td  class="smalltext">3.2500</td>
                        <td  class="smalltext">3.2194</td>
                        <td  class="smalltext">3.2187</td>
                        <td  class="smalltext">1.34</td>
                        <td  class="smalltext">1.91</td>
                        <td  class="smalltext">3.52</td>
                    </tr>


                </table>
            </div>

            <!--Table ending-->
            <div style="background-color: #CCCC99">
                <ul style="list-style-type: none;">
                    <li class="smalltext"><b><br>1. Minimum minor diameter for internal straight pipe threads is based upon minimum pitch diameter and minimum truncation and will vary with the pitch diameter.</b></li>
                    <li class="smalltext"><b><br>2. NPTF (taper reamed) drilled hole sizes are recommended for taper reaming before tapping. They also are used without taper reaming by taper drilling or allowing the tap to act as a reamer. Thread lenghts so produced are designated "Full or Complete Thread" on drawings.</b></li>
                    <li class="smalltext"><b><br>3. NTPF (not reamed) drilled hole sizes are recommended for taper tapping without reaming. [NPTF (2 FF thread)] minimum minor diameter two threads small from large end and closest drilled hole sizes are recommended only for low pressure use.[NPTF(4FF thread)] minimum minor diameter four threads small from large end and closes drilled hole sizes are recommended only for all pressures. Thread lenghts so produced are designated "Effective Thread" on drawings.</b></li>
                    <li class="smalltext"><b><br>4. Internal pipe thread shall be countersunk 90 deg included angle to a diameter (rounded to a two-place decimal) obtained by adding 0.016 in. for sizes below 1 in. and 0.025 in. for larger sizes to the maximum major diameter at large end.</b></li>
                </ul>
            </div>
        </div>

    </div>
    @endsection