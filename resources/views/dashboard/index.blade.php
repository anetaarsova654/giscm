@extends('layouts.app')

@section('content')
    <div class="container user-dashboard-content">
        <div class="row">
            <div class="user-dashboard-header">
                <h3 align="left">My Dashboard</h3>
                <div class="user-dashboard-detail">
                    <div class="user-detail">
                        <h5 class="detail">{{$user->firstname." ".$user->lastname}}</h5>
                        <h5 class="detail">Title:{{$user->classification}}</h5>
                        <h5 class="detail edit-profile-link"><a href="{{route('editprofile')}}">Edit Profile</a></h5>
                    </div>
                    <a href="" title="Edit Profile"><img  alt class="userProfileImg" src="{{ Storage::url($user->getProfileImage())}}"></a>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-10">
                        <div class="user-dashboard-contributions">
                            <div class="user-dashboard-contribution-header">
                                <h4 class="contribution-heading" align="left">My Files</h4>
                                <div class="form-group contribution-categories">
                                    {{--<select class="form-control" id="sel1" placeholder="Select category">
                                        <option>All</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>--}}
                                    {!!  Form::open(['url' => route('dashboard',$user->id),'class'=>'form-horizontal','method' => 'GET','id'=>'category-form']) !!}
                                        {{Form::select('category',$categories, null, ['class'=>'form-control','id'=>'category','placeholder' => 'All'])}}
                                    {!!  Form::close() !!}
                                </div>
                            </div>
                            <div >
                                @forelse(array_chunk($cadmodels->items(),4) as $cadmodel_chunk )
                                    <div class="row">
                                        @foreach($cadmodel_chunk as $cadmodel)
                                            <div class="col-sm-3">
                                                <div class="user-dashboard-contribution-content">
                                                    <div class="contributor-model">
                                                        <div class="model-image">
                                                            <a href="{{route('cadmodels.show',$cadmodel['id'])}}"><img alt="Model Image" src="{{ Storage::url($cadmodel['image_url']) }}" class="" style="width: 150px"></a>
                                                            <button onclick="window.location.href='{{route('cadmodels.edit',$cadmodel['id'])}}'" class="btn btn-primary edit"><i class="fa fa-pencil-square"></i></button>
                                                            <form  action="{{route('cadmodels.destroy',$cadmodel['id'])}}" method="POST">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE') }}
                                                                 <button class="btn btn-primary delete"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        </div>
                                                        <br>
                                                        <div class="model-name" >
                                                            <a href="{{route('cadmodels.show',$cadmodel['id'])}}" class=""><b>{{ $cadmodel['name'] }}</b></a><br>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @empty
                                    <p>No Contributions Available...</p>
                                @endforelse
                                    {{ $cadmodels->appends(request()->except('page'))->links() }}
                            </div>
                        </div>
                        <div class="user-dashboard-comments">
                            <div class="user-dashboard-comment-header">
                                <h4 class="comment-heading" align="left">Comments</h4>
                                <div class="form-group comment-categories">
                                    {{--<select class="form-control" id="sel1" placeholder="Select category">
                                        <option>All</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                            <div class="user-dashboard-comment-content">
                                @forelse($user->comments as $comment)
                                <div class="comment">
                                    {{$comment->comment}}
                                    <br>
                                    Posted on&nbsp;
                                    <a href="{{route('cadmodels.show',$comment->cadmodel->id)}}" >{{$comment->cadmodel->name}}</a> ({{$comment->created_at->format('d-M-Y')}})
                                    <br>
                                    <hr>
                                </div>
                                @empty
                                    <p>No Comments Available..</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        $('#category').change(function(){

            if(this.value == ''){
                console.log(this.value);

                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);

                    window.location.href = clean_uri;
                }
            }
            else{
                document.getElementById("category-form").submit();
            }


        });

    </script>

@endsection