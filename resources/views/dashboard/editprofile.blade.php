@extends('layouts.app')

@section('content')

<div class="container edit-profile-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="header">
                <h2>Edit Profile</h2>
            </div>
        </div>
    </div>
    {!!   Form::model($user,['url' => route('editprofile.update'),'files'=>true,'class'=>'form-horizontal','method'=>'POST']) !!}
    {{csrf_field()}}
    {{ method_field('PATCH') }}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-inputs">
                    <div class="form-group">
                        {{ Form::label('firstname', 'First Name') }}
                        {{ Form::text('firstname' ,null, ['class' => 'form-control','id'=>'firstname','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('lasttname', 'Last Name') }}
                        {{ Form::text('lastname' ,null, ['class' => 'form-control','id'=>'lastname','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('company', 'Company') }}
                        {{ Form::text('company' ,null, ['class' => 'form-control','id'=>'company']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('address', 'Address') }}
                        {{ Form::text('address' ,null, ['class' => 'form-control','id'=>'address','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'Phone') }}
                        {{ Form::text('phone' ,null, ['class' => 'form-control','id'=>'phone','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select name="country" class="form-control" id="country" required>
                            @include('dashboard.partials._countries')
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::label('zipcode', 'Zip Code') }}
                        {{ Form::number('zipcode' ,null, ['class' => 'form-control','id'=>'zipcode','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('city', 'City') }}
                        {{ Form::text('city' ,null, ['class' => 'form-control','id'=>'city','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        <label for="state">State/Province</label>
                        <select name="state" class="form-control" id="state" required>
                            @include('dashboard.partials._states')
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-inputs">
                    <div class="form-group">
                        {{ Form::label('skills', 'Skills') }}
                        {{ Form::textarea('skills' ,null, ['class' => 'form-control' ,'id'=>'skills','rows'=>'4']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('interests', 'Interests') }}
                        {{ Form::textarea('interests' ,null, ['class' => 'form-control' ,'id'=>'interests','rows'=>'4']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('about', 'About') }}
                        {{ Form::textarea('about' ,null, ['class' => 'form-control' ,'id'=>'about','rows'=>'4']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'Website') }}
                        {{ Form::text('website' ,null, ['class' => 'form-control','id'=>'website']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('image_url', 'Profile Image') }}
                        {{ Form::file('image' ,null, ['class' => 'form-control','id'=>'image_url']) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-submit">
                    <input type="submit" class="btn btn-primary btnsubmit" value="Update">
                </div>
            </div>
        </div>
    {!!  Form::close() !!}
</div>


@endsection
