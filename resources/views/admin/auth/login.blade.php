<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2478828590166997",
    enable_page_level_ads: true});</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><meta name="keywords" content="Automotive , Aerospace , Construction , Mining , Agriculture , FREE 3D CAD models , Caterpillar , John Deere , CNH , New Holland , Volvo , Komatsu , Hitachi , Liebherr , Doosan Infracore , Sany , XCMG , Terex , 3D CAD , AutoCad , Solidworks , PTC , SurfCad , Catia , VR , Virtual Reality , CAD , CAM , 3D Printing , Hewlett Packard , HP , pdm , plm , cloud based , version control , cad revisions , cad , file management , solidworks epdm , product data management , share cad , collaborate , library , free , cad models , cad drawings , models , open source , solid works , solidedge , autodesk , alibre , autocad , catia , sketchup , keycreator , pro/engineer , spaceclaim , creo , stratasys , 3d printing , 3d printers , additive manufacturing , 3d print , digital manufacturing , product development , 3d printing app , 3d printing software , uprint , fortus , dimension , fdm " /><meta name="description" content="Global Industry Standard CAD Models (GISCM), a resource and social networking platform for engineering students and professionals "/><meta name="keywords" content="GISCM , CAD , CAD drawings , drawings , online CAD drawings , 3D models , SAE standards , ISO standards , JIS standards , 2D drawings , STEP format , o-ring drawings , external hex plug drawings, barbed hose adapters , Hose Adapters , Hose Fittings , Tube Fittings , Tube Adapters , Tube Nuts, 37 Degree fittings , 37 Degree Adapters , JIC Adapters ,  Pipe Adapters , Pipe fittings , Swivel  Adapters , Swivel fittings , 90 degree elbow fittings , Metric plugs , Inch plugs , BSP Plugs , Parker fittings , Parker adapters , Flange adapters , O-ring face seal , oring face seal ORFS , ORFS adapters , O-ring Face Seal adapters , O-ring Face Seal Fittings , O-rings "/><meta name="author" content="GISCM Inc"/><meta name="expires" content="Never"/><meta name="copyright" content="© 2017"/><meta name="designer" content="Chicad Group"/><meta name="publisher" content="GISCM, 2017"/><meta name="classification" content="Engineering Resource and Social Community"/><meta name="revisit-after" content="30 Days"/><meta name="distribution" content="Global"/><meta name="robots" content="INDEX, FOLLOW"/><meta name="GOOGLEBOT" content="INDEX, FOLLOW" /><meta name="rating" content="15-64"/><meta name="location" content="Grand Rapids, Michigan"><meta name="city" content="Chicago, Mumbai, Toronto, Shanghai, London, Hong Kong, Houston"><meta name="country" content="USA, India, China, United Kingdom, Japan"><meta name="rating" content="GENERAL">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Global Industry Standard CAD Models (GISCM)</title>

    <!-- Styles -->
    <link href="{!! asset('css/bootstrap.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/style-custom.css') !!}" rel="stylesheet">
    <!-- animation-effect -->
    <link href="{!! asset('css/animate.min.css') !!}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 login-form">
            <div class="panel panel-success">
                <div class="panel-heading">Admin Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Login
                                </button>

                               {{-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>



