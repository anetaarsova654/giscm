304 Nuts
304-C Nut
319 Sleeve
C0304-C Cap Assembly with Insert
C0318 Nuts
C0319A Ferrules
C0319B Ferrules
MFS6801 ORFS METRIC THREAD 90 DEG ELBOW
ORFS SWIVEL TO MALE PIPE ADAPTER
SAE 2403 JIC UNION
SAE 37 Degree (JIC) Straight Thread Adapters
SAE 5406 REDUCER BUSHING
SAE 5406HP SOCKET HEAD PIPE PLUG
SAE 5406P HEX HEAD PIPE PLUG
SAE 5406SHP SQUARE HEAD PIPE PLUG
SAE 5500 90 DEGREE MALE PIPE ELBOW
SAE 5501 45 DEGREE MALE PIPE ELBOW
SAE 5600 Male Pipe Tee Adapter
SAE 6400L JIC Male Connector Long
SAE FS0306-C STAKE NUT
SAE FS0318 ORFS NUT
SAE FS0403 ORFS BRAZE-ON ADAPTER
SAE FS2403 ORFS UNION
SAE FS2408 ORFS PLUG
SAE FS2603 UNION TEE
SAE FS2650 ORFS UNION CROSS
SAE FS6400L ORFS STRAIGHT THD ADAPTER LONG
SAE FS6500 ORFS 90 DEG SWIVEL ELBOW
SAE MFS0318 ORFS METRIC HEX NUT
SAE MFS0403 METRIC HEX BRAZE-ON ADAPTERS
SAE MFS2700 ORFS BULKHEAD STRAIGHT ADAPTERS
SAE MFS6400 ORFS STRAIGHT METRIC THREAD ADAPTER
SAE MFS6400L ORFS STRT METRIC THD ADAPTER-LONG
Specials
SAE O-Ring Face Seal Adapters
SAE FS6400 Straight THD Connector
SAE FS6801 90 Degree Straight THD Elbow
SAE Pipe Adapters
SAE 5404 Hex Nipple
SAE 5000 Coupling
SAE Beaded Hose Barb Adapters
4604 Bead to Male Straight THD Adapter
4501 90 Degree Bead to Male Pipe Elbow
4404 Bead to Male Pipe Thread Adapter
4503 45 Degree Bead to Male Pipe Elbow
4601 90 Degree Beaded Straight Thread Elbow
SAE FS6602 O-Ring Swivel Nut Run Tee Adapter
4603 45 Degree Bead to Strt Thd Elbow
SAE 37 Degree Flare Adapters
SAE 6400 JIC Male Connector
90 Degree Straight THD Adapter
SAE Swivel Adapters
1404 Male Pipe to Female Pipe Swivel
6900 Straight THD to Female Pipe Swivel
1501 90 Degree Male Pipe to Female Pipe Swivel
SAE 6400 90° Straight Thread Elbow
SAE Flareless Adapters
Male Connector - Flareless
Female Connector - Flareless
SAE AND ISO Plugs
External Hex Plug
Internal Hex Plug
SAE Washers
SAE AND ISO Locknuts
Straight THD Locknut
Bulkhead Locknut
SAE O-Rings
SAE O-Ring Face Seal (ORFS) O-RINGS
SAE AND ISO O-Ring Stud End O-RINGS
Flanges
1935-61 Flat Socket Weld Pipe
1935-62 Flat Socket Weld Pipe
1927-61 NPTF Female Flange
1927-62 NPTF Female Flange
1929-61 SAE J1926-1 Thread Port Flange
1929-62 SAE J1926-1 Thread Port Flange